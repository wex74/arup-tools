﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewCreator.Model
{
    public class GridColumn
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Index { get; set; }

        public GridColumn(string name, int width)
        {
            Name = name;
            Width = width;
        }
    }
}
