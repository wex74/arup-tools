﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.TextFormatting;
using Autodesk.Revit.DB;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator.Model.Base_Classes
{
    public class LevelInfo
    {
        public LevelInfo(Level level, Discipline discipline)
        {
            ViewInfos = new List<ViewInfo>();

            Discipline = discipline;
            Level = level;
            Name = Level.Name;

        }

        public LevelInfo()
        {
        }

        public string Name { get; set; }
        public Discipline Discipline { get; set; }
        public Level Level { get; set; }
        public List<ViewInfo> ViewInfos { get; set; }
        public List<Plan> Plans { get; set; }


        public bool IsVisible
        {
            get
            {

               return MainApp.DataAccess.VisibleLevels.Exists(x => x == Level.Id.IntegerValue);
            }
        }


        public void RemoveDeletedPlans()
        {
            Plans.RemoveAll(x => View_Utils.GetView(x.ViewId) == null);
        }


        public int RemoveView(int id)
        {
            return Plans.RemoveAll(x => x.ViewId == id);
        }

        public int RemoveViewInfo(ViewInfo viewInfo)
        {
            return ViewInfos.RemoveAll(x => x.Id == viewInfo.Id);
        }


        public bool HasPlans(string search)
        {
            if (string.IsNullOrEmpty(search)) return Plans.Any();

            return Plans.Any(x => x.Name.Contains(search, StringComparison.OrdinalIgnoreCase));
        }

     

        public void UpdateNewView(ViewInfo viewInfo, Plan plan)
        {
            ViewInfos.Remove(viewInfo);

            if (plan.ViewId > 0)
            {
                Plans.Add(plan);
            }
        }


        public bool PlanExists(int id)
        {
            return Plans.Exists(x => x.ViewId == id);
        }

        public bool ViewInfoExists(ViewInfo viewInfo)
        {
            return ViewInfos.Exists(x => x.Id == viewInfo.Id);
        }


        public void ClearViews()
        {
            ViewInfos.Clear();
        }


       


        public List<ViewInfo> GetViewInfos()
        {
            return ViewInfos.OrderBy(x => x.ToString()).ToList();
        }


        public List<Plan> GetPlans()
        {
            return Plans.OrderBy(x => x.Name).ToList();
        }

        public List<Plan> GetPlans(string search)
        {
            if (string.IsNullOrEmpty(search)) return GetPlans();
            return GetPlans().Where(x => x.Name.Contains(search, StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public override string ToString()
        {
            return Name;
        }


        public override bool Equals(object obj)
        {
            var item = obj as LevelInfo;

            if (item == null)
            {
                return false;
            }

            return Level.Id.Equals(item.Level.Id);
        }

        public override int GetHashCode()
        {
            return Level.GetHashCode();
        }
    }
}