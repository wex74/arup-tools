﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Model
{
    public class Plan
    {
        public int ViewId { get; set; }
        public string Name { get; set; }
        public string Level { get; set; }
        public Discipline Discipline { get; set; }
        public bool Favourite { get; set; }
        public List<DependentPlan> DependentPlans { get; private set; }

        public Plan()
        {
            DependentPlans = new List<DependentPlan>();
        }


        public Plan(View view)
        {
            GetViewInfo(view);
        }

        public void Update()
        {
            var view = GetView();

            if (view != null)
            {
                GetViewInfo(view);
            }
           
        }


        private void GetViewInfo(View view)
        {
            DependentPlans = new List<DependentPlan>();

            if (view == null) return;

            var disc = view.View_Discipline();

            if (view.ViewType == ViewType.EngineeringPlan)
            {
                Discipline = Discipline.Structural;
            }
            else
            {
                if (disc > -1) Discipline = (Discipline) disc;
            }


            ViewId = view.Id.IntegerValue;
            Name = view.ViewName;
            Level = view.GenLevel.Name;
            Favourite = User_Utils.IsFavourite(ViewId);

            if (view.GetDependentViewIds().Any())
            {
                GetDependentPlans(view);
            }
        }

        public View GetView()
        {
            return View_Utils.GetView(ViewId);
        }

        public Level GetLevel()
        {
            return Level_Utils.GetLevel(Level);
        }

        public void GetDependentPlans(View view = null)
        {
            DependentPlans.Clear();

            if (view == null) view = GetView();
            
            if (view != null)
            {
                foreach (var id in view.GetDependentViewIds())
                {
                    var dependentView = View_Utils.GetView(id);

                    if (dependentView != null)
                    {
                        DependentPlans.Add(new DependentPlan(dependentView, view.IdToInt()));
                    }
                }
            }
        }

        public string TextInfo
        {
            get
            {
                var search = ToString();

                foreach (var DependentPlan in DependentPlans)
                {
                    search += DependentPlan + " ";
                }

                return search;
            }
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
