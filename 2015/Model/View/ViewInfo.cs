﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.DB;
using LiteDB;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Model
{
    public class ViewInfo
    {
        public ViewInfo()
        {
            Id = Guid.NewGuid();
            DependentViews = new List<string>();
        }

        public bool IsEmpty
        {
            get
            {
                return Id == Guid.Empty;
            }
        }
        [BsonId]
        public Guid Id { get; set; }

        public Discipline Discipline { get; set; }
        public ViewType ViewType { get; set; }
        public string Name { get; set; }
        public Level Level { get; set; }
        public string Prefix { get; set; }
        public string Template { get; set; }
        public string Scopebox { get; set; }
        public List<string> DependentViews { get; set; }
        public int ParentViewId { get; set; }
        public bool Favourite { get; set; }

        public View GetParentView()
        {
            return View_Utils.GetView(ParentViewId);
        }

        public override string ToString()
        {
            if (GetParentView() != null)
            {
                return Name;
            }

            var name = SettingName;

         

            if (Level != null)
            {
                name = name + " - " + Level.Name;
            }


            return name;
        }




        public string TextInfo
        {
            get
            {
                var search = ToString();

                foreach (var DependentView in DependentViews)
                {
                    search += DependentView + " ";
                }

                if (GetParentView() != null)
                {
                    search += GetParentView().ViewName + " ";
                }

                return search;
            }
        }

        public string SettingName
        {
            get
            {
                var name = "";



                if (Prefix != "")
                {
                    name = Prefix; 
                }

                if (!string.IsNullOrEmpty(Name))
                {
                    if (Prefix != "")
                    {
                        name = Prefix + " - " + Name;
                    }
                    else
                    {
                        name = Name;
                    }
                }

                return name;
            }
        }



        public override bool Equals(object obj)
        {
            var item = obj as ViewInfo;

            if (item == null)
            {
                return false;
            }

            return Id.Equals(item.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}