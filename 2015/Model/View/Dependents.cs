﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.DB;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Model
{
    public class Dependents
    {
        public int ViewId { get; set; }
        public List<string> ScopeBoxNames { get; set; }
        public int DependentViewCount { get; set; }
        public bool AddToMyViews { get; set; }

        public Dependents(View view, bool addMyViews = false)
        {
            ViewId = view.Id.IntegerValue;
            ScopeBoxNames = new List<string>();
        }

        public View GetView()
        {
            return View_Utils.GetView(ViewId);
        }


        private bool CanExecute()
        {
            var view = GetView();
            if (view == null) return false;

            if (!view.CanViewBeDuplicated(ViewDuplicateOption.AsDependent))
            {
                MessageBox.Show("Unable to create dependent view from " + view.ViewName);
                return false;
            }

            if (view.ViewSpecific)
            {
                MessageBox.Show("View is already a dependent view");
                return false;
            }

            return true;

        }

        public  void AddDependents()
        {
            if (GetDependents())
            {
                var count = ScopeBoxNames.Count + DependentViewCount;

                var n = count;
                var s = "{0} of " + n + " views processed...";
                var caption = "Creating Dependent Views";

                var view = GetView();

                using (var pf = new ProgressForm(caption, s, n))
                {
                    foreach (var scopeBox in ScopeBoxNames)
                    {
                        View_Utils.AddDependentView(view, scopeBox);
                        pf.Increment();
                    }

                    for (int i = 1; i < DependentViewCount +1; i++)
                    {
                        View_Utils.AddDependentView(view, i.ToString());
                        pf.Increment();
                    }
                }
                
            }


            
        }


        public bool  GetDependents()
        {
            if (!CanExecute()) return false;

            var frm = new frmSelectScopeboxes();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                ScopeBoxNames.AddRange(frm.SelectedScopboxes);
                DependentViewCount = frm.Dependents;
                return true;
            }

            return false;
        }
    }
}
