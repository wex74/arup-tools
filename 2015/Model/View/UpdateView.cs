﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Model
{
    public class UpdateView
    {
        private string _viewName;
        private string _template;
        private string _scopeBox;
        private bool _favourite;
        private int _scale;

        private bool _isValidPlan;


        public string ViewName { get;  set; }
        public string Template { get;  set; }
        public string ScopeBox { get;  set; }
        public bool Favourite { get;  set; }

        public bool IsChanged { get;  set; }
        public bool IsDependent { get; set; }
        public int Scale { get;  set; }
        public int ViewId { get; set; }
        public bool UpdateName { get; private set; }
        public bool UpdateTemplate { get; private set; }
        public bool UpdateScopeBox { get; private set; }
        public bool UpdateFavourite { get; private set; }

        public bool UpdateScale { get; private set; }

        public UpdateView(View view)
        {
            ViewId = view.Id.IntegerValue;

            Reload();

        }


        public View GetView()
        {
            return View_Utils.GetView(ViewId);
        }

        public void Reload()
        {
            var view = GetView();
          
            if (view != null)
            {
                IsDependent = view.View_IsDependent();
                 _isValidPlan = view.IsValidPlan();

                _viewName = view.ViewName;
                _template = view.View_TemplateName();
                if (_isValidPlan) _scopeBox = view.View_ScopeBoxName();
                _scale = view.Scale;
                _favourite = MainApp.AppData.UserModelData.GetUserModelData().UserFavourites.Exists(view.IdToInt());
            }
           

            ViewName = _viewName;
            Template = _template;

            if (view.IsValidPlan())
            {
                ScopeBox = _scopeBox;
            }
           
            Scale = _scale;

        
            Favourite = _favourite;

            CheckUpdate();
        }


        public void CheckUpdate()
        {
            UpdateName = ViewName != _viewName;
            UpdateTemplate = Template != _template;
            if (_isValidPlan) UpdateScopeBox = ScopeBox != _scopeBox;
            UpdateFavourite = Favourite != _favourite;
            UpdateScale = Scale != _scale;

            IsChanged = UpdateName || UpdateTemplate || UpdateScopeBox || UpdateFavourite || UpdateScale;
        }

        public void SetName(string name)
        {
            ViewName = name;
            CheckUpdate();
        }

        public void SetTemplate(string template)
        {
            Template = template;
            CheckUpdate();
        }

        public void SetScopeBox(string scope)
        {
            if (_isValidPlan)
            {
                ScopeBox = scope;
                CheckUpdate();
            }
           
        }

        public void SetScale(int scale)
        {
            Scale = scale;
            CheckUpdate();
        }

        public void SetFavourite(bool value)
        {
            Favourite = value;
            CheckUpdate();
        }

        public void SetValues(string name, string template, string scopebox, bool myview, int scale)
        {
            ViewName = name;
            Template = template;
            if (_isValidPlan) ScopeBox = scopebox;
            Favourite = myview;
            Scale = scale;
            CheckUpdate();


        }
    }
}
