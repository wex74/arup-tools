﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Model
{
    public class DependentPlan
    {
        public int ViewId { get; set; }

        public int ParentViewId { get; set; }
        public string Name { get; set; }
        public string Level { get; set; }
        public Discipline Discipline { get; set; }

        public DependentPlan()
        {
        }

        public DependentPlan(View view, int parent)
        {
            if (view == null) return;
            ParentViewId = parent;
            ViewId = view.Id.IntegerValue;
            Name = view.ViewName;
            Level = view.GenLevel.Name;


            var disc = view.View_Discipline();

            if (view.ViewType == ViewType.EngineeringPlan)
            {
                Discipline = Discipline.Structural;
            }
            else
            {
                if (disc > -1) Discipline = (Discipline)disc;
            }
        }

        public Level GetLevel()
        {
            return Level_Utils.GetLevel(Level);
        }

        public View GetView()
        {
            return View_Utils.GetView(ViewId);
        }

        public View GetParentView()
        {
            return View_Utils.GetView(ParentViewId);
        }
    }
}
