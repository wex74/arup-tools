﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using ViewCreator.Data;
using ViewCreator.Model.Base_Classes;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;


namespace ViewCreator.Model
{
    public class ServiceLevels
    {
        public Discipline Discipline { get; private set; }
        public bool IsLoaded { get; set; }
        public List<LevelInfo> LevelInfos { get; set; }
        public string Search { get; set; }

        public ServiceLevels(Discipline discipline)
        {
            Discipline = discipline;
            LevelInfos = new List<LevelInfo>();
            LoadPlans();   
        }

        public void LoadPlans()
        {
            //var views = new List<View>();

            //if (Discipline == Discipline.Structural)
            //{
            //    views = View_Utils.GetEngineeringFloorPlans();
            //}
            //else
            //{
            //    views = Plan_Utils.GetDisciplinePlans(App_Utils.GetServiceTag(Discipline));
            //}

            //var plans =  views.Select(x => new Plan(x)).ToList();



            //if (LevelInfos.Count == 0)
            //{
            //    foreach (var level in Level_Utils.GetLevels())
            //    {
            //        var levelInfo = new LevelInfo(level, Discipline);
            //        levelInfo.Plans = plans.Where(x => x.Level == level.Name).ToList();

            //        LevelInfos.Add(levelInfo);
            //    }
               
            //}
            //else
            //{
            //    foreach (var levelInfo in LevelInfos)
            //    {
            //        levelInfo.Plans = plans.Where(x => x.Level == levelInfo.Level.Name).ToList();
            //    }
            //}
            

        }


        public void RemoveDependentPlan(DependentPlan dependentPlan, Plan plan)
        {
            var levelInfo = LevelInfos.FirstOrDefault(x => x.Level.Name == plan.Level);
            if (levelInfo != null)
            {
                var p = levelInfo.Plans.FirstOrDefault(x => x.ViewId == plan.ViewId);
                if (p != null)
                {
                    p.DependentPlans.RemoveAll(x => x.ViewId == dependentPlan.ViewId);
                }
            }
        }

        public void RemovePlan(Plan plan)
        {
            var levelInfo = LevelInfos.FirstOrDefault(x => x.Level.Name == plan.Level);
            if (levelInfo != null) levelInfo.Plans.RemoveAll(x => x.ViewId == plan.ViewId);
        }

        public void AddPlan(Plan plan)
        {
            var levelInfo = LevelInfos.FirstOrDefault(x => x.Level.Name == plan.Level);
            if (levelInfo != null) levelInfo.Plans.Add(plan);
        }

        public List<ViewInfo> GetViewInfos()
        {
            var viewInfos = new List<ViewInfo>();

            foreach (var levelInfo in LevelInfos)
            {
                viewInfos.AddRange(levelInfo.GetViewInfos());
            }


            return viewInfos.OrderBy(x => x.ToString()).ToList();
        }

        public void RemoveDeletedViews()
        {
            foreach (var levelInfo in LevelInfos)
            {
                levelInfo.RemoveDeletedPlans();
            }
        }
        public int RemoveView(int id)
        {
            var levelInfo = LevelInfos.FirstOrDefault(x => x.PlanExists(id));

            if (levelInfo != null)
            {
               return levelInfo.RemoveView(id);
            }

            return 0;
        }

        public int RemoveViewInfo(ViewInfo viewInfo)
        {
            var levelInfo = LevelInfos.FirstOrDefault(x => x.ViewInfoExists(viewInfo));

            if (levelInfo != null)
            {
                return levelInfo.RemoveViewInfo(viewInfo);
            }
            return 0;
        }


        public List<Plan> GetPlans()
        {
            var plans = new List<Plan>();

            foreach (var levelInfo in LevelInfos)
            {
                plans.AddRange(levelInfo.Plans);
            }
            return plans.OrderBy(x => x.Name).ToList();
        }

        public List<Plan> GetPlans(string search)
        {
            if (string.IsNullOrEmpty(search)) return GetPlans();

            return GetPlans().Where(x => x.Name.Contains(search, StringComparison.OrdinalIgnoreCase)).ToList();
        }


       

        public void ClearViews()
        {
            foreach (var levelInfo in LevelInfos)
            {
                levelInfo.ClearViews();
            }
        }


        public List<string> GetSearchList()
        {

            var viewNames =  GetPlans().Select(x => x.Name);
            var list = new List<string>();

            foreach (var viewName in viewNames)
            {
                if (viewName.Length > 2)
                {
                    var pos = viewName.LastIndexOf("-", StringComparison.OrdinalIgnoreCase);
                    if (pos > 0)
                    {
                        list.Add(viewName.Substring(pos + 2, viewName.Length - pos - 2).Trim());
                    }
                }
                
                
            }
            return list.OrderBy(x => x).Distinct().ToList();
        }



    }
}
