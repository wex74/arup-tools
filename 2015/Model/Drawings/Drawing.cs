﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using LiteDB;
using ViewCreator.Helpers;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Model
{
    public class Drawing
    {
        [BsonId]
        public Guid Id { get; set; }
        public int SheetId { get; set; }
        public string DrawingNo { get; set; }
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Title4 { get; set; }

        public bool Build { get; set; }
        public int ViewId { get; set; }
        public string DrawingType { get; set; }
        public string Discipline { get; set; }
        public string SheetTemplate { get; set; }

        public Drawing()
        {
            Id = Guid.NewGuid();
            Discipline = "";
            DrawingType = "";
        }

        public Drawing(string number)
        {
            Id = Guid.NewGuid();
            DrawingNo = number;
            Discipline = "";
            DrawingType = "";
        }

        
        public Drawing(int sheetId)
        {
            Id = Guid.NewGuid();
            SheetId = sheetId;

            Update();

            SheetTemplate = "";
        }

        public bool SheetTemplateExists
        {
            get { return Sheet_Utils.SheetTemplateExists(SheetTemplate); }
        }

        public ViewSheet GetViewSheet()
        {
            return Sheet_Utils.GetViewSheet(SheetId);
        }


        public bool IsNew
        {
            get { return SheetId == 0; }
        }


        public string SearchText
        {
            get { return this.GetFullName() + " " + DrawingType; }
        }


        public void Update()
        {
            var sheet = GetViewSheet();
            if (sheet != null)
            {
                DrawingNo = sheet.SheetNumber;

                Title1 = sheet.GetTitle(1);
                Title2 = sheet.GetTitle(2);
                Title3 = sheet.GetTitle(3);
                Title4 = sheet.GetTitle(4);

                Discipline = sheet.Sheet_DisciplineName();
                DrawingType = sheet.Sheet_DrawingType();
            }
          
        }
       

        public bool UpdateRequired()
        {
            var sheet = GetViewSheet();
            if (sheet != null)
            {
                if (DrawingNo != sheet.SheetNumber
                    || Title1 != sheet.GetTitle(1)
                    || Title2 != sheet.GetTitle(2)
                    || Title3 != sheet.GetTitle(3)
                    || Title4 != sheet.GetTitle(4)
                    || Discipline != sheet.Sheet_DisciplineName()
                    || DrawingType != sheet.Sheet_DrawingType()
                    )
                {
                    return true;
                }
            }

            return false;
        }



        public override string ToString()
        {
           return  this.GetFullName();
        }


        public override bool Equals(object obj)
        {
            var item = obj as Drawing;

            if (item == null)
            {
                return false;
            }

            return Id.Equals(item.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
