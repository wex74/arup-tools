﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using LiteDB;
using ViewCreator.Helpers;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Model
{
    public class Drawing
    {
        [BsonId]
        public Guid Id { get; set; }
        public ViewSheet Sheet { get; set; }
        public string DrawingNo { get; set; }
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Title4 { get; set; }

        public string ViewTemplate { get; set; }
        public string SheetTemplate { get; set; }

        public Drawing()
        {
            Id = Guid.NewGuid();
        }

        public Drawing(string number)
        {
            Id = Guid.NewGuid();
            DrawingNo = number;
        }


        public Drawing(ViewSheet sheet)
        {
            Id = Guid.Empty;
            Sheet = sheet;
            DrawingNo = sheet.SheetNumber;
            Title1 = sheet.GetTitle(1);
            Title2 = sheet.GetTitle(2);
            Title3 = sheet.GetTitle(3);
            Title4 = sheet.GetTitle(4);

        }

        public bool UpdateRequired()
        {
            if (Sheet != null)
            {
                if (DrawingNo != Sheet.SheetNumber
                    || Title1 != Sheet.GetTitle(1)
                    || Title2 != Sheet.GetTitle(2)
                    || Title3 != Sheet.GetTitle(3)
                    || Title4 != Sheet.GetTitle(4)
                    )
                {
                    return true;
                }
            }

            return false;
        }



        public override string ToString()
        {
           return  this.GetFullName();
        }


        public override bool Equals(object obj)
        {
            var item = obj as Drawing;

            if (item == null)
            {
                return false;
            }

            return Id.Equals(item.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
