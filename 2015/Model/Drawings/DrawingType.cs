﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewCreator.Model.Drawings
{
     public class DrawingType
    {
         public string Number { get; set; }
         public string Name { get; set; }

         public List<string> FindNames { get; set; }
         public DrawingType(string number, string name)
         {
            FindNames  = new List<string>();
            Number = number;
            Name = name;
         }


        public override string ToString()
        {
            return Number + " " + Name;
        }


        public override bool Equals(object obj)
        {
            var item = obj as DrawingType;

            if (item == null)
            {
                return false;
            }

            return ToString().Equals(item.ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
