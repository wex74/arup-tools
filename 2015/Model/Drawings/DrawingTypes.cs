﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewCreator.Model.Drawings
{
   
    public class DrawingTypes : IEnumerable<DrawingType>
    {

        List<DrawingType> mylist = new List<DrawingType>();

        public DrawingType this[int index]
        {
            get { return mylist[index]; }
            set { mylist.Insert(index, value); }
        }

        public DrawingTypes()
        {
            mylist = new List<DrawingType>();

            mylist.Add(new DrawingType("0000", "Cover Sheet/General"));
            mylist.Add(new DrawingType("1000", "Schematics"));
            mylist.Add(new DrawingType("2000", "Layouts"));
            mylist.Add(new DrawingType("3000", "Sections and Detailed Views"));
            mylist.Add(new DrawingType("4000", "Standard Details"));
            mylist.Add(new DrawingType("5000", "Equipment Schedules"));
        }
     



        public IEnumerator<DrawingType> GetEnumerator()
        {
            return mylist.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
