﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewCreator.Model.Settings.User
{
    public  class UserFavourites
    {
        public List<int> ViewIds { get; set; }

        public UserFavourites()
        {
            ViewIds = new List<int>();
        }

        public void Add(int id)
        {
            if (!Exists(id))
            {
                ViewIds.Add(id);
            }
        }

        public void Remove(int id)
        {
            if (Exists(id))
            {
                ViewIds.RemoveAll(x => x == id);
            }
        }

        public bool Exists(int id)
        {
            return ViewIds.Exists(x => x == id);
        }
    }
}
