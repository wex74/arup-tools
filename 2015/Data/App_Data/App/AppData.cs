﻿using System.Collections.Generic;
using System.Linq;
using ViewCreator.Data.App;
using ViewCreator.Data.Repositories;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Model.Settings.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator.Data.App_Data.App
{
    /// <summary>
    ///     AppData
    ///     Entry point to store project, user and usermodel data for application
    /// </summary>
    public class AppData
    {
        public AppData()
        {
            Debug_Utils.WriteLine("AppData", "Launching App");

            UserData = new UserSettingsRepository();
            UserModelData = new UserModelDataRepository();
            Projects = new List<ProjectData>();
        }


        public List<ProjectData> Projects { get; set; }
        public UserSettingsRepository UserData { get; set; }
        public UserModelDataRepository UserModelData { get; set; }


        public void Load()
        {
        }

        /// <summary>
        ///     Add or set current project if does not exist in this app session
        /// </summary>
        public void LoadProject()
        {
            var path = RVT_Utils.GetCentralModelFullPath(RVT_Utils.Document);

            // project not valid 
            if (path == "")
            {
                CurrentProject = null;

                Debug_Utils.WriteLine("LoadProject", "Project not valid: ");
                return;
            }

            // valid project check to see if it exists
            if (!ProjectExists(path))
            {
                Debug_Utils.WriteLine("AppData > LoadProject", "New Project: " + path);

                // add new project
                CurrentProject = new ProjectData(path);
                Projects.Add(CurrentProject);
                ScreensUI.LoadControls();
               
            }
            else
            {
                // load project
                if (!CurrentProject.Path.Matches(path))
                {
                    Debug_Utils.WriteLine("AppData > LoadProject", "Set Project: " + path);

                    CurrentProject = GetProject(path);
                    ScreensUI.LoadControls();
   
                }
            }
        }

        #region Project methods and property shortcuts

        public bool ProjectExists(string path)
        {
            return Projects.Exists(x => x.Path.Matches(path));
        }


        public ProjectData GetProject(string path)
        {
            return Projects.FirstOrDefault(x => x.Path == path);
        }

        public ProjectPlans ProjectPlans
        {
            get { return CurrentProject.ProjectPlans; }
        }

        public ProjectViewInfos ProjectViewInfos
        {
            get { return CurrentProject.ProjectViewInfos; }
        }

        public ProjectDrawings ProjectDrawings
        {
            get { return CurrentProject.ProjectDrawings; }
        }

        public ProjectSections ProjectSections
        {
            get { return CurrentProject.ProjectSections; }
        }

        public ProjectThreeds ProjectThreeds
        {
            get { return CurrentProject.ProjectThreeds; }
        }


        public ProjectData CurrentProject { get; private set; }

        #endregion

      
        #region ModelSettings methods and property shortcuts

        public ModelSettingsRepository CurrentSettingsData
        {
            get
            {
                return CurrentProject.ModelSettingsData;
            }
        }

        public ModelSetting GetSettings(Discipline discipline)
        {
            return CurrentSettingsData.GetSettings(discipline);
        }

        public List<ModelSetting> StructuralSettings
        {
            get
            {
                return CurrentSettingsData.ModelSettings.StructuralSettings;
            }
        }

        public List<ModelSetting> BuildingSettings
        {
            get
            {
                return CurrentSettingsData.ModelSettings.BuildingSettings;
            }
        }



        public FormSettings FormSettings
        {
            get { return CurrentProject.UserModelData.FormSettings; }
        }


        #endregion
       


        #region User methods and property shortcuts

        public bool IsUserStructures
        {
            get { return UserData.UserSettings.SectionDiscipline == SectionDiscipline.Structures; }
        }

        public bool IsUserBuildings
        {
            get { return UserData.UserSettings.SectionDiscipline == SectionDiscipline.Buildings; }
        }

        public bool IsUserRevit
        {
            get { return UserData.UserSettings.SectionDiscipline == SectionDiscipline.Revit; }
        }

        public Discipline UserDiscipline
        {
            get { return UserData.UserSettings.Discipline; }
        }

        public UserModelData UserModel
        {
            get { return CurrentProject.UserModelData; }
        }


        public SectionDiscipline UserSectionDiscipline
        {
            get { return UserData.UserSettings.SectionDiscipline; }
        }


        public UserSettings UserSettings
        {
            get { return UserData.UserSettings; }
        }

        #endregion

        #region Save methods 

        public void SaveUserModelData()
        {
            UserModelData.Save();
        }

        public void SaveUserData()
        {
            UserData.Save();
        }

        public void SaveSettingsData()
        {
            CurrentSettingsData.Save();
        }

        #endregion
    }
}