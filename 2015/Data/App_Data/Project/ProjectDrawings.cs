﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Model.Settings.Model
{
    public class ProjectDrawings
    {
        private List<Drawing> _drawings;

        public event EventHandler DrawingAdded;
        public event EventHandler DrawingUpdated;
        public event EventHandler DrawingDeleted;


        public ProjectDrawings()
        {
            Debug_Utils.WriteLine("ProjectDrawings", "Loading project sheets");

            // load all plans on startup
            var stopWatch = new Stopwatch();
            stopWatch.Start();
 
            LoadDrawings();
            stopWatch.Stop();
            var ts = stopWatch.Elapsed;

            Debug_Utils.WriteLine("ProjectDrawings", _drawings.Count + "  drawings loaded");
            Debug_Utils.WriteLine("ProjectDrawings > Timer", ts.Milliseconds + " Milliseconds");
        }


        protected virtual void OnDrawingAdded(Drawing drawing, EventArgs e)
        {
            var handler = DrawingAdded;
            if (handler != null)
            {
                Debug_Utils.WriteLine("ProjectPlans", "OnDrawingAdded: " + drawing);
                handler(drawing, e);
            }
        }

        protected virtual void OnDrawingUpdated(Drawing drawing, EventArgs e)
        {
            var handler = DrawingUpdated;
            if (handler != null)
            {
                Debug_Utils.WriteLine("ProjectPlans", "OnDrawingUpdated: " + drawing);
                handler(drawing, e);
            }
        }

        protected virtual void OnDrawingDeleted(int id, EventArgs e)
        {
            var handler = DrawingDeleted;
            if (handler != null)
            {
                Debug_Utils.WriteLine("ProjectPlans", "OnDrawingDeleted: " + id);
                handler(id, e);
            }
        }

        public void Add(ViewSheet sheet, bool raiseEvent = true)
        {
            var id = sheet.IdToInt();
            if (!Exists(id))
            {
                var drawing = new Drawing(id);
                _drawings.Add(drawing);
                if (raiseEvent)
                {
                    OnDrawingAdded(drawing, new EventArgs());
                }
               
            }
        }

        public void Add(List<Drawing> drawings)
        {
            _drawings.AddRange(drawings);
        }

        public void Add(Drawing drawing)
        {
            _drawings.Add(drawing);
        }

        public void Update(int id)
        {
            if (Exists(id))
            {
               
            }
        }

        public void Remove(int id)
        {
            if (Exists(id))
            {
                _drawings.RemoveAll(x => x.SheetId == id);
                OnDrawingDeleted(id, new EventArgs());
            }
           
        }

        public void RemoveNew(string id)
        {
            if (Exists(id))
            {
                _drawings.RemoveAll(x => x.Id.ToString() == id);
            }

        }



        public bool Exists(int id)
        {
            return _drawings.Exists(x => x.SheetId == id);
        }



        public bool Exists(string id)
        {
            return _drawings.Exists(x => x.Id.ToString() == id);
        }


        public Drawing GetDrawing(int id)
        {
            return _drawings.FirstOrDefault(x => x.SheetId == id);
        }

        public List<Drawing> GetDrawings()
        {
            return _drawings.OrderBy(x => x.GetFullName()).ToList();
        }

        public List<Drawing> GetNewDrawings()
        {
            return _drawings.Where(x => x.IsNew && x.SheetTemplateExists).OrderBy(x => x.GetFullName()).ToList();
        }

        public List<Drawing> GetDrawings(string search)
        {
            return GetDrawings().Where(x => x.GetFullName().Matches(search)).ToList();
        }



        public void LoadDrawings()
        {
            var sheetIds = Sheet_Utils.GetSheets().Select(x => x.Id.IntegerValue).ToList();
            _drawings = new List<Drawing>();

            foreach (var id in sheetIds)
            {
                _drawings.Add(new Drawing(id));
            }

        }
    }
}
