﻿using RevitManager_RVT.Utils;
using ViewCreator.Data.App;
using ViewCreator.Data.App_Data;
using ViewCreator.Data.Repositories;
using ViewCreator.Model.Settings.Model;
using ViewCreator.Utils.General;

namespace ViewCreator.Model.Settings
{

    /// <summary>
    /// ProjectData
    /// store and manage data for each model loaded into the Revit session
    /// </summary>

    public class ProjectData
    {
        public ProjectData()
        {
        }

        public ProjectData(string path)
        {
            Debug_Utils.WriteLine("ProjectData", "Creating new project: " + path);

            Path = path;


            // create or set user model data
            if (MainApp.AppData.UserModelData.ModelExists(path))
            {
                // load UserModelData 
                Debug_Utils.WriteLine("ProjectData", "Retrieving UserModelData for " + path);
                UserModelData = MainApp.AppData.UserModelData.GetUserModelData(path);
            }
            else
            {
                // create UserModelData
                Debug_Utils.WriteLine("ProjectData", "Creating UserModelData for " + path);
                UserModelData = MainApp.AppData.UserModelData.AddModel(path);

                // initalise data for new UserModelData
                if (UserModelData != null)
                {
                    UserModelData.FormSettings.BuildingsScreenData.SetInitData();
                    UserModelData.FormSettings.PlansScreenData.SetInitData();
                }
            }

            Debug_Utils.WriteLine("ProjectData", "New project created: " + path);

            // exit here if project data not created
            if (!Path_Utils.ModelDataFileExists(path))
            {
                Debug_Utils.WriteLine("ProjectData", "No ModelDataFile for " + path);

                return;
            }

            // project & model is valid 
            // load settings and views
            LoadData();
        }

        public string Path { get; set; }

        public  bool PlansScreenVisible;
        public  bool BuildingsScreenVisible;
        public  bool StructuresScreenVisible;

        public UserModelData UserModelData { get; set; }
        public ProjectPlans ProjectPlans { get; private set; }
        public ProjectDrawings ProjectDrawings { get; private set; }
        public ProjectSections ProjectSections { get; private set; }
        public ProjectThreeds ProjectThreeds { get; private set; }
        public ProjectViewInfos ProjectViewInfos { get; private set; }
        public ModelSettingsRepository ModelSettingsData { get; private set; }

        public bool DataFileExists
        {
            get { return Path_Utils.ModelDataExists; }
        }

        public string DataFile
        {
            get { return Path_Utils.ModelDataFile; }
        }

        public void LoadData(bool loadDefaultSettings = false)
        {
            Debug_Utils.WriteLine("ProjectData", "Loading project data and views... ");

            // load project data
            ModelSettingsData = new ModelSettingsRepository();
            ProjectDrawings = new ProjectDrawings();
            ProjectPlans = new ProjectPlans();
            ProjectSections = new ProjectSections();
            ProjectViewInfos = new ProjectViewInfos();

            // used to rest or create new settings
            if (loadDefaultSettings)
            {
                ModelSettingsData.LoadDefaultSettings();
            }
        }

        public void DeleteData()
        {
            Debug_Utils.WriteLine("ProjectData", "Deleting project data... ");

            File_Utils.DeleteFile(DataFile);
        }

        public void Load()
        {
        }

        public void Unload()
        {
        }
    }
}