﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator.Data.App_Data
{
    /// <summary>
    /// ProjectViewInfos
    /// Store, manage and raise events for viewinfos in project model 
    /// </summary>
    public class ProjectViewInfos
    {
        private List<ViewInfo> _viewInfos;
        public event EventHandler ViewInfoAdded;
        public event EventHandler ViewInfoDeleted;

        public ProjectViewInfos()
        {
            _viewInfos = new List<ViewInfo>();
        }

        public int Count
        {
            get { return _viewInfos.Count; }
        }

        public int DependentsCount
        {
            get
            {
                var dependents = _viewInfos.Select(x => x.DependentViews).ToList();

                if (!dependents.Any()) return 0;

                var count = 0;
                foreach (var dependent in dependents)
                {
                    count = count + dependent.Count;
                }
                return count;
            }
        }

   
        public int TotalCount
        {
            get { return Count + DependentsCount; }
        }




        protected virtual void OnViewInfoAdded(ViewInfo viewInfo, EventArgs e)
        {
            EventHandler handler = ViewInfoAdded;
            if (handler != null)
            {
                handler(viewInfo, e);
            }
        }

        protected virtual void OnViewInfoDeleted(string id, EventArgs e)
        {
            EventHandler handler = ViewInfoDeleted;
            if (handler != null)
            {
                handler(id, e);
            }
        }



        public void Add(ViewInfo viewInfo)
        {
            if (!Exists(viewInfo))
            {
                _viewInfos.Add(viewInfo);
                OnViewInfoAdded(viewInfo, new EventArgs());

            }
        }


        public void Remove(List<ViewInfo> viewInfos)
        {
            foreach (var viewInfo in viewInfos)
            {
                Remove(viewInfo);
            }
        }

      
        public void Remove(ViewInfo viewInfo)
        {
            if (Exists(viewInfo))
            {
                var id = viewInfo.Id.ToString();
                _viewInfos.RemoveAll(x => x.Id == viewInfo.Id);
                OnViewInfoDeleted(id, new EventArgs());
            }
        }



        public bool Exists(ViewInfo viewInfo)
        {
            return _viewInfos.Exists(x => x.Id == viewInfo.Id);
        }

     



    
        public List<ViewInfo> GetViewInfos()
        {
            return _viewInfos.OrderBy(x => x.ToString()).ToList();
        }
        public List<ViewInfo> GetViewInfos(Plan plan)
        {
            return GetPlanViewInfos().Where(x => x.ParentViewId == plan.ViewId).ToList();
        }

        public List<ViewInfo> GetPlanViewInfos()
        {
            return _viewInfos.Where(x => x.ParentViewId > 0).ToList(); 
        }




        public List<ViewInfo> SearchViewInfos(string search)
        {
            return GetViewInfos().Where(x => x.ToString().Matches(search)).ToList();
        }

        public List<ViewInfo> GetViewInfos(string level)
        {
            return GetViewInfos().Where(x => x.Level.Name == level).ToList();
        }

        public List<ViewInfo> GetViewInfos(string level, Discipline discipline)
        {
            return GetViewInfos().Where(x => x.Level.Name == level && x.Discipline == discipline).ToList();
        }

        internal List<ViewInfo> GetViewInfos(Discipline discipline, string search)
        {
            return GetViewInfos().Where(x => x.Discipline == discipline && x.ToString().Matches(search)).ToList();
        }
        internal List<ViewInfo> GetViewInfos(Discipline discipline)
        {
            return GetViewInfos().Where(x => x.Discipline == discipline).ToList();
        }
    }
}
