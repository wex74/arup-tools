﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Autodesk.Revit.DB;
using ViewCreator.Forms.Sections.Views.Controls;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Data.App
{

    /// <summary>
    /// ProjectPlans
    /// Store, manage and raise events for plans in project model  
    /// </summary>

    public class ProjectPlans
    {
        private readonly List<Plan> _plans;

        public ProjectPlans()
        {
            Debug_Utils.WriteLine("ProjectPlans", "Loading project plans");

            // load all plans on startup

            TimeSpan ts =  new TimeSpan();

            if (MainApp.AppData.IsUserBuildings)
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                _plans = Plan_Utils.GetBuildingPlans().Select(x => new Plan(x)).ToList();
                stopWatch.Stop();
                ts = stopWatch.Elapsed;
            }



            if (MainApp.AppData.IsUserStructures)
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                _plans = Plan_Utils.GetStructuralPlans().Select(x => new Plan(x)).ToList();
                stopWatch.Stop();
                ts = stopWatch.Elapsed;
            }



            Debug_Utils.WriteLine("ProjectPlans", _plans.Count + "  plans loaded");
            Debug_Utils.WriteLine("ProjectPlans > Timer", ts.Milliseconds + " Milliseconds");
        }

        public int Count
        {
            get { return _plans.Count; }
        }

        public int DependentsCount
        {
            get
            {
                var dependents = _plans.Select(x => x.DependentPlans).ToList();

                if (!dependents.Any()) return 0;

                var count = 0;
                foreach (var dependent in dependents)
                {
                    count = count + dependent.Count;
                }
                return count;
            }
        }


        public int TotalCount
        {
            get { return Count + DependentsCount; }
        }

        public event EventHandler PlanAdded;
        public event EventHandler PlanUpdated;
        public event EventHandler PlanDeleted;


        protected virtual void OnPlanAdded(Plan plan, EventArgs e)
        {
            var handler = PlanAdded;
            if (handler != null)
            {
                Debug_Utils.WriteLine("ProjectPlans", "OnPlanAdded: " + plan);
                handler(plan, e);
            }
        }

        protected virtual void OnPlanUpdated(Plan plan, EventArgs e)
        {
            var handler = PlanUpdated;
            if (handler != null)
            {
                Debug_Utils.WriteLine("ProjectPlans", "OnPlanUpdated: " + plan);
                handler(plan, e);
            }
        }

        protected virtual void OnPlanDeleted(int id, EventArgs e)
        {
            var handler = PlanDeleted;
            if (handler != null)
            {
                Debug_Utils.WriteLine("ProjectPlans", "OnPlanDeleted: " + id);
                handler(id, e);
            }
        }

        public void UpdateDependent(int id)
        {
            var view = View_Utils.GetView(id);

            if (view != null)
            {
                var ownerViewId = view.GetPrimaryViewId().IntegerValue;
                var plan = GetPlan(ownerViewId);

                if (plan != null)
                {
                    plan.Update();
                    OnPlanUpdated(plan, new EventArgs());
                }
            }
        }


        public void Update(int id)
        {
            if (Exists(id))
            {
                var plan = GetPlan(id);

                if (plan != null)
                {
                    plan.Update();
                    OnPlanUpdated(plan, new EventArgs());
                }
            }
        }

        public void Add(View view, bool raiseEvent = true)
        {
            // if is dependent load into existing plan
            if (view.View_IsDependent())
            {
                var ownerViewId = view.GetPrimaryViewId().IntegerValue;
                var plan = GetPlan(ownerViewId);
                if (plan != null)
                {
                    plan.GetDependentPlans();
                    if (raiseEvent)
                    {
                        OnPlanAdded(plan, new EventArgs());
                    }
               
                }

                return;
            }

            // add plan to collection
            if (!Exists(view.Id.IntegerValue))
            {
                var plan = new Plan(view);
                _plans.Add(plan);

                if (raiseEvent)
                {
                    OnPlanAdded(plan, new EventArgs());
                }
            }
        }

        public void Add(List<View> views)
        {
            foreach (var view in views)
            {
                Add(view, false);
            }
        }

        public void UpdateDependents(List<View> views)
        {
            foreach (var view in views)
            {
                var plan = GetPlan(view.Id.IntegerValue);
                if (plan != null)
                {
                    plan.GetDependentPlans(view);
                }
            }
        }


        public void Remove(int id)
        {
            if (Exists(id))
            {
                _plans.RemoveAll(x => x.ViewId == id);
                OnPlanDeleted(id, new EventArgs());
            }
        }

        public bool Exists(int id)
        {
            return _plans.Exists(x => x.ViewId == id);
        }


        public Plan GetPlan(int id)
        {
            return _plans.FirstOrDefault(x => x.ViewId == id);
        }

        public List<Plan> GetPlans()
        {
            return _plans.OrderBy(x => x.Name).ToList();
        }


        public List<Plan> GetPlans(string search)
        {
            return GetPlans().Where(x => x.Name.Matches(search)).ToList();
        }


        public List<Plan> GetPlans(string level, Discipline discipline)
        {
            return GetPlans().Where(
                x => x.Level == level
                     && x.Discipline == discipline
                ).ToList();
        }


        public List<Plan> GetPlans(Discipline discipline)
        {
            return GetPlans().Where(x => x.Discipline == discipline).ToList();
        }

        //public List<Plan> GetPlans(string search, Discipline discipline)
        //{
        //    return GetPlans().Where(x => x.Name.Search(search) && x.Discipline == discipline).ToList();
        //}

        public List<Plan> GetPlans(string search, BuildingsScreenView view)
        {
            var plans = new List<Plan>();
            if (view.ShowCoordination)
            {
                plans.AddRange(_plans.Where(x => x.Discipline == Discipline.Coordination).ToList());
            }

            if (view.ShowMechanical)
            {
                plans.AddRange(_plans.Where(x => x.Discipline == Discipline.Mechanical).ToList());
            }

            if (view.ShowElectrical)
            {
                plans.AddRange(_plans.Where(x => x.Discipline == Discipline.Electrical).ToList());
            }

            if (view.ShowHydraulic)
            {
                plans.AddRange(_plans.Where(x => x.Discipline == Discipline.Hydraulic).ToList());
            }


            if (view.ShowFire)
            {
                plans.AddRange(_plans.Where(x => x.Discipline == Discipline.Fire).ToList());
            }

            plans = plans.Where(x => x.Name.Matches(search)).ToList();

            return plans;
        }

        internal List<Plan> GetPlans(Discipline discipline, string search)
        {
            return GetPlans().Where(x => x.Discipline == discipline && x.ToString().Matches(search)).ToList();
        }
    }
}