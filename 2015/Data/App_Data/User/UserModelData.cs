﻿using System.Collections.Generic;
using System.IO;
using LiteDB;
using ViewCreator.Model.Settings.User;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Model.Settings
{

    /// <summary>
    /// ProjectData
    /// store and manage data for each model loaded into the Revit session
    /// </summary>

    public class UserModelData
    {
        public UserModelData(string path)
        {
            CentralModelFullPath = path;
            NavisworksExportPath = Path.GetDirectoryName(path);
            PDFExportPath = Path.GetDirectoryName(path);
            ImageExportPath = Path.GetDirectoryName(path);
            UserFavourites = new UserFavourites();

            VisibleLevelIds = new List<int>();

            FormSettings = new FormSettings();

            foreach (var level in Level_Utils.GetLevels())
            {
                VisibleLevelIds.Add(level.Id.IntegerValue);
            }

            Debug_Utils.WriteLine("UserModelData", "Added UserModelData " + path);
        }


        public UserModelData()
        {
        }

        [BsonId]
        public string CentralModelFullPath { get; set; }
        public string NavisworksExportPath { get; set; }
        public string PDFExportPath { get; set; }
        public string ImageExportPath { get; set; }
        public List<int> VisibleLevelIds { get; set; }
        public UserFavourites UserFavourites { get; set; }
        public FormSettings FormSettings { get; set; }
    }
}