﻿using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Model
{
    public class UserSettings
    {
        public UserSettings()
        {
            Id = Guid.NewGuid();

            ViewTemplates = new List<string>();
            Prefixes = new List<string>();
            PlanNames = new List<string>();
            SearchWords = new List<string>();
            SectionDiscipline = SectionDiscipline.Revit;
            Discipline = Discipline.Coordination;
        }

        [BsonId]
        public Guid Id { get; set; }

        public List<string> Prefixes { get; set; }
        public List<string> PlanNames { get; set; }
        public List<string> SearchWords { get; set; }
        public List<string> ViewTemplates { get; set; }

        public SectionDiscipline SectionDiscipline { get; set; }
        public Discipline Discipline { get; set; }


        public List<string> GetViewTemplates()
        {
            var names = Template_Utils.GetViewTemplateNames();
            var list = new List<string>();

            foreach (var template in ViewTemplates)
            {
                if (names.Exists(x => x == template))
                {
                    list.Add(template);
                }
            }

            return list.OrderBy(x => x).Distinct().ToList();
        }

        public List<string> GetPrefixes()
        {
            return Prefixes.OrderBy(x => x).Distinct().ToList();
        }

        public List<string> GetPlanNames()
        {
            return PlanNames.OrderBy(x => x).Distinct().ToList();
        }

        public List<string> GetSearchWords()
        {
            return SearchWords.OrderBy(x => x).Distinct().ToList();
        }
    }
}