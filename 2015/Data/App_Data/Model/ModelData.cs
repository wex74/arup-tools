﻿using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;
using ViewCreator.Utils.App;

namespace ViewCreator.Model.Settings
{
    // Todo - check class still required
    /// <summary>
    ///     ModelData
    ///     May be superseded
    /// </summary>
    public class ModelData
    {
        public ModelData()
        {
            Id = Guid.NewGuid();
            Drawings = new List<Drawing>();
        }

        [BsonId]
        public Guid Id { get; set; }

        public List<Drawing> Drawings { get; set; }

        public List<Drawing> GetDrawings()
        {
            return Drawings.OrderBy(x => x.GetFullName()).ToList();
        }
    }
}