﻿using System;
using System.Collections.Generic;
using LiteDB;

namespace ViewCreator.Model.Settings.Model
{
    /// <summary>
    ///     ModelSettings 
    ///     stores data for Structural & Buildings disciplines
    /// </summary>
    public class ModelSettings
    {
        public ModelSettings()
        {
            StructuralSettings = new List<ModelSetting>();
            BuildingSettings = new List<ModelSetting>();
        }

        [BsonId]
        public Guid Id { get; set; }

        public List<ModelSetting> StructuralSettings { get; set; }
        public List<ModelSetting> BuildingSettings { get; set; }
    }
}