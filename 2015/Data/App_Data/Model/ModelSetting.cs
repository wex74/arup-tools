﻿using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;
using ViewCreator.Utils.App;

namespace ViewCreator.Model.Settings
{
    /// <summary>
    ///     ModelSetting 
    ///     data saved for each discipline
    /// </summary>
    public class ModelSetting
    {
        public ModelSetting()
        {
            Id = Guid.NewGuid();
            PrefixNames = new List<string>();
            ViewInfos = new List<ViewInfo>();
            DefaultTemplate = "";
            DefaultPrefix = "";
        }

        [BsonId]
        public Guid Id { get; set; }

        public Discipline Discipline { get; set; }
        public List<string> PrefixNames { get; set; }
        public List<ViewInfo> ViewInfos { get; set; }
        public string DefaultTemplate { get; set; }
        public string DefaultPrefix { get; set; }


        public List<ViewInfo> GetViewInfos()
        {
            return ViewInfos.OrderBy(x => x.ToString()).ToList();
        }

        public List<string> GetViewInfoSettingNames()
        {
            return GetViewInfos().Select(x => x.SettingName).ToList();
        }
    }
}