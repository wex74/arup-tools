﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Model.Settings.Model
{
    public class ProjectSheets
    {
        private List<int> _sheetIds;
        private List<Drawing> _drawings;

        public ProjectSheets()
        {
            _sheetIds = Sheet_Utils.GetSheets().Select(x => x.Id.IntegerValue).ToList();
            LoadDrawings();
        }

        public void AddSheet(int id)
        {
            if (!SheetExist(id))
            {
                _sheetIds.Add(id);
                _drawings.Add(new Drawing(id));
            }
        }

   

        public void RemoveSheet(int id)
        {
            _sheetIds.RemoveAll(x => x == id);
        }


        public bool SheetExist(int id)
        {
            return _sheetIds.Exists(x => x == id);
        }

        public List<ViewSheet> GetSheets()
        {
            var sheets = _sheetIds.Select(x => Sheet_Utils.GetViewSheet(x)).ToList();
            return sheets.Where(x => x != null).ToList();
        }

        public List<Drawing> GetDrawings()
        {
            return _drawings;
        } 

        public void LoadDrawings()
        {
            _drawings = new List<Drawing>();

            foreach (var id in _sheetIds)
            {
                _drawings.Add(new Drawing(id));
            }

        }
    }
}
