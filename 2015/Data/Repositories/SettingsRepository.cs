﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Autodesk.Revit.DB;
using LiteDB;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Services;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator.Data.Repositories
{
    public class SettingsRepository
    {
        private List<ModelSetting> _settings;

        public SettingsRepository()
        {
            _settings = new List<ModelSetting>();

            DataFile = Path_Utils.ModelDataFile;

            if (DataFile == "") return;

            if (DataFileExists)
            {
                using (var db = new LiteDatabase(DataFile))
                {
                    var data = db.GetCollection<ModelSetting>("Settings");


                    var settings = data.FindAll();

                    foreach (var setting in settings)
                    {
                        _settings.Add(setting);
                    }
                }
            }
        }


        public bool DataFileExists
        {
            get { return File.Exists(DataFile); }
        }


        public SectionDiscipline SectionDiscipline
        {
            get
            {
                if (_settings.Exists(x => x.Discipline == Discipline.Structural))
                {
                    return SectionDiscipline.Structures;
                }


                return SectionDiscipline.Buildings;
            }
        }

        public string DataFile { get; set; }

        public string DataPath
        {
            get { return Path.GetDirectoryName(DataFile); }
        }

        public ModelSetting GetCurrentModelSetting
        {
            get { return null; }
            // get { return _settings.FirstOrDefault(x => x.Discipline == App.AppData.); }
        }

        public void Reset()
        {
            _settings = new List<ModelSetting>();

            using (var db = new LiteDatabase(DataFile))
            {
                var data = db.GetCollection<ModelSetting>("Settings");

                LoadDefaultSettings(data);
            }
        }


        public void CreateSettings()
        {
            _settings = new List<ModelSetting>();

            using (var db = new LiteDatabase(DataFile))
            {
                var data = db.GetCollection<ModelSetting>("Settings");

                LoadDefaultSettings(data);
            }
        }

        private void LoadDefaultSettings(LiteCollection<ModelSetting> data)
        {
            ModelSetting modelSetting = null;

            if (MainApp.AppData.IsUserBuildings)
            {
                modelSetting = LoadArchitecturalSettings();
                data.Insert(modelSetting);
                _settings.Add(modelSetting);
                data.EnsureIndex(x => x.Id);

                modelSetting = LoadCoordinationSettings();
                data.Insert(modelSetting);
                _settings.Add(modelSetting);
                data.EnsureIndex(x => x.Id);

                modelSetting = LoadMechanicalSettings();
                data.Insert(modelSetting);
                _settings.Add(modelSetting);
                data.EnsureIndex(x => x.Id);

                modelSetting = LoadElectricalSettings();
                data.Insert(modelSetting);
                _settings.Add(modelSetting);
                data.EnsureIndex(x => x.Id);

                modelSetting = LoadHydraulicgSettings();
                data.Insert(modelSetting);
                _settings.Add(modelSetting);
                data.EnsureIndex(x => x.Id);

                modelSetting = LoadFireSettings();
                data.Insert(modelSetting);
                _settings.Add(modelSetting);
                data.EnsureIndex(x => x.Id);
            }


            if (MainApp.AppData.IsUserStructures)
            {
                modelSetting = LoadStructuralSettings();
                data.Insert(modelSetting);
                _settings.Add(modelSetting);
                data.EnsureIndex(x => x.Id);
            }
        }

        public static bool CheckDB(string path)
        {
            var pass = false;
            try
            {
                using (var db = new LiteDatabase(path))
                {
                    pass = db.CollectionExists("Settings");
                }
            }
            catch (Exception)
            {
                pass = false;
            }

            return pass;
        }

        public ModelSetting GetSettings(Discipline discipline)
        {
            return _settings.FirstOrDefault(x => x.Discipline == discipline);
        }

        public ModelSetting GetSettings(string discipline)
        {
            return _settings.FirstOrDefault(x => x.Discipline.ToString() == discipline);
        }

        public List<ModelSetting> GetSettings()
        {
            return _settings;
        }

        public bool Save()
        {
            using (var db = new LiteDatabase(DataFile))
            {
                var data = db.GetCollection<ModelSetting>("Settings");
                return data.Update(GetCurrentModelSetting);
            }
        }

        public bool Save(Discipline discipline)
        {
            using (var db = new LiteDatabase(DataFile))
            {
                var data = db.GetCollection<ModelSetting>("Settings");
                return data.Update(GetSettings(discipline));
            }
        }

        public bool EditPlan(ViewInfo plan, string name, string template, string prefix, ViewType viewType,
            Discipline discipline)
        {
            plan.Name = name;
            plan.Template = template;
            plan.Prefix = prefix;
            plan.ViewType = viewType;

            return Save(discipline);
        }

        public ViewInfo AddPlan(ViewInfo viewInfo, Discipline discipline)
        {
            var plan = new ViewInfo
            {
                Name = viewInfo.Name,
                Discipline = GetCurrentModelSetting.Discipline,
                Prefix = viewInfo.Prefix,
                Template = viewInfo.Template
            };

            GetSettings(discipline).ViewInfos.Add(viewInfo);

            Save(discipline);
            return plan;
        }

        public bool RemovePlan(ViewInfo plan, Discipline discipline)
        {
            GetSettings(discipline).ViewInfos.RemoveAll(x => x.Id == plan.Id);
            return Save(discipline);
        }

        private ModelSetting LoadArchitecturalSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = ArchitecturalHelper.Discipline;
            settings.ViewInfos.AddRange(ArchitecturalHelper.GetViewInfos());
            settings.PrefixNames.AddRange(ArchitecturalHelper.PrefixNames);
            settings.DefaultTemplate = ArchitecturalHelper.DefaultTemplate;
            settings.DefaultPrefix = ArchitecturalHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadCoordinationSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = CoordinationHelper.Discipline;
            settings.ViewInfos.AddRange(CoordinationHelper.GetViewInfos());
            settings.PrefixNames.AddRange(CoordinationHelper.PrefixNames);
            settings.DefaultTemplate = CoordinationHelper.DefaultTemplate;
            settings.DefaultPrefix = CoordinationHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadStructuralSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = StructuralHelper.Discipline;
            settings.ViewInfos.AddRange(StructuralHelper.GetViewInfos());
            settings.PrefixNames.AddRange(StructuralHelper.PrefixNames);
            settings.DefaultTemplate = StructuralHelper.DefaultTemplate;
            settings.DefaultPrefix = StructuralHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadMechanicalSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = MechanicalHelper.Discipline;
            settings.ViewInfos.AddRange(MechanicalHelper.GetViewInfos());
            settings.PrefixNames.AddRange(MechanicalHelper.PrefixNames);
            settings.DefaultTemplate = MechanicalHelper.DefaultTemplate;
            settings.DefaultPrefix = MechanicalHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadElectricalSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = ElectricalHelper.Discipline;
            settings.ViewInfos.AddRange(ElectricalHelper.GetViewInfos());
            settings.PrefixNames.AddRange(ElectricalHelper.PrefixNames);
            settings.DefaultTemplate = ElectricalHelper.DefaultTemplate;
            settings.DefaultPrefix = ElectricalHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadHydraulicgSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = HydraulicHelper.Discipline;
            settings.ViewInfos.AddRange(HydraulicHelper.GetViewInfos());
            settings.PrefixNames.AddRange(HydraulicHelper.PrefixNames);
            settings.DefaultTemplate = HydraulicHelper.DefaultTemplate;
            settings.DefaultPrefix = HydraulicHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadFireSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = FireHelper.Discipline;
            settings.ViewInfos.AddRange(FireHelper.GetViewInfos());
            settings.PrefixNames.AddRange(FireHelper.PrefixNames);
            settings.DefaultTemplate = FireHelper.DefaultTemplate;
            settings.DefaultPrefix = FireHelper.DefaultPrefix;

            return settings;
        }
    }
}