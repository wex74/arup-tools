﻿using System.IO;
using System.Linq;
using LiteDB;
using ViewCreator.Model.Settings;
using ViewCreator.Model.Settings.Model;
using ViewCreator.Services;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator.Data.Repositories
{
    public class ModelSettingsRepository
    {
        public ModelSettingsRepository()
        {
            ModelSettings = new ModelSettings();

            DataFile = Path_Utils.ModelDataFile;

            if (DataFile == "") return;

            if (DataFileExists)
            {
                using (var db = new LiteDatabase(DataFile))
                {
                    var data = db.GetCollection<ModelSettings>("ModelSettings");


                    ModelSettings = data.FindAll().FirstOrDefault();

                    if (ModelSettings != null)
                        Debug_Utils.WriteLine("ModelSettingsRepository", "Loading ModelSettings " + DataFile);
                }
            }
        }


        public bool DataFileExists
        {
            get { return File.Exists(DataFile); }
        }


        public string DataFile { get; set; }


        public ModelSettings ModelSettings { get; private set; }


        public void LoadDefaultSettings()
        {
            ModelSetting settings = null;

            using (var db = new LiteDatabase(DataFile))
            {
                var data = db.GetCollection<ModelSettings>("ModelSettings");

                ModelSettings = new ModelSettings();


                settings = LoadArchitecturalSettings();
                ModelSettings.BuildingSettings.Add(settings);

                settings = LoadCoordinationSettings();
                ModelSettings.BuildingSettings.Add(settings);

                settings = LoadMechanicalSettings();
                ModelSettings.BuildingSettings.Add(settings);

                settings = LoadElectricalSettings();
                ModelSettings.BuildingSettings.Add(settings);

                settings = LoadHydraulicgSettings();
                ModelSettings.BuildingSettings.Add(settings);

                settings = LoadFireSettings();
                ModelSettings.BuildingSettings.Add(settings);

                settings = LoadStructuralSettings();
                ModelSettings.StructuralSettings.Add(settings);

                data.Insert(ModelSettings);
                data.EnsureIndex(x => x.Id);
            }

            Debug_Utils.WriteLine("ModelSettingsRepository", "Default Settings loaded");
        }

        public bool Save()
        {
            using (var db = new LiteDatabase(DataFile))
            {
                var data = db.GetCollection<ModelSettings>("ModelSettings");
                return data.Update(ModelSettings);
            }
        }


        private ModelSetting LoadArchitecturalSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = ArchitecturalHelper.Discipline;
            settings.ViewInfos.AddRange(ArchitecturalHelper.GetViewInfos());
            settings.PrefixNames.AddRange(ArchitecturalHelper.PrefixNames);
            settings.DefaultTemplate = ArchitecturalHelper.DefaultTemplate;
            settings.DefaultPrefix = ArchitecturalHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadCoordinationSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = CoordinationHelper.Discipline;
            settings.ViewInfos.AddRange(CoordinationHelper.GetViewInfos());
            settings.PrefixNames.AddRange(CoordinationHelper.PrefixNames);
            settings.DefaultTemplate = CoordinationHelper.DefaultTemplate;
            settings.DefaultPrefix = CoordinationHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadStructuralSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = StructuralHelper.Discipline;
            settings.ViewInfos.AddRange(StructuralHelper.GetViewInfos());
            settings.PrefixNames.AddRange(StructuralHelper.PrefixNames);
            settings.DefaultTemplate = StructuralHelper.DefaultTemplate;
            settings.DefaultPrefix = StructuralHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadMechanicalSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = MechanicalHelper.Discipline;
            settings.ViewInfos.AddRange(MechanicalHelper.GetViewInfos());
            settings.PrefixNames.AddRange(MechanicalHelper.PrefixNames);
            settings.DefaultTemplate = MechanicalHelper.DefaultTemplate;
            settings.DefaultPrefix = MechanicalHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadElectricalSettings()
        {
            var settings = new ModelSetting();
            settings.Discipline = ElectricalHelper.Discipline;
            settings.ViewInfos.AddRange(ElectricalHelper.GetViewInfos());
            settings.PrefixNames.AddRange(ElectricalHelper.PrefixNames);
            settings.DefaultTemplate = ElectricalHelper.DefaultTemplate;
            settings.DefaultPrefix = ElectricalHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadHydraulicgSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = HydraulicHelper.Discipline;
            settings.ViewInfos.AddRange(HydraulicHelper.GetViewInfos());
            settings.PrefixNames.AddRange(HydraulicHelper.PrefixNames);
            settings.DefaultTemplate = HydraulicHelper.DefaultTemplate;
            settings.DefaultPrefix = HydraulicHelper.DefaultPrefix;

            return settings;
        }

        private ModelSetting LoadFireSettings()
        {
            var settings = new ModelSetting();

            settings.Discipline = FireHelper.Discipline;
            settings.ViewInfos.AddRange(FireHelper.GetViewInfos());
            settings.PrefixNames.AddRange(FireHelper.PrefixNames);
            settings.DefaultTemplate = FireHelper.DefaultTemplate;
            settings.DefaultPrefix = FireHelper.DefaultPrefix;

            return settings;
        }

        public void CreateSettings()
        {
            LoadDefaultSettings();
        }

        public ModelSetting GetSettings(Discipline discipline)
        {
            if (discipline == Discipline.Structural)
            {
                return ModelSettings.StructuralSettings.FirstOrDefault(x => x.Discipline == discipline);
            }
            return ModelSettings.BuildingSettings.FirstOrDefault(x => x.Discipline == discipline);
        }
    }
}