﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using LiteDB;
using ViewCreator.Data.Forms;
using ViewCreator.Model;
using ViewCreator.Utils.General;

namespace ViewCreator.Data.Repositories
{
    public class UserSettingsRepository
    {
        private readonly string _dbFile;

        public UserSettingsRepository()
        {
            _settings = new UserSettings();

            _dbFile = Path_Utils.UserDataFile;
            var exists = File.Exists(_dbFile);

            using (var db = new LiteDatabase(_dbFile))
            {
                var data = db.GetCollection<UserSettings>("UserSettings");

                if (exists)
                {
                    var settings = data.FindAll();

                    _settings = settings.FirstOrDefault();
                }
                else
                {
                    data.Insert(_settings);
                    data.EnsureIndex(x => x.Id);
                    Debug_Utils.WriteLine("UserSettingsRepository", "Created user data " + _dbFile);
                }
            }
        }

        private UserSettings _settings { get; set; }

     
        public UserSettings UserSettings
        {
            get { return _settings; }
        }

    
        public void Save()
        {
            using (var db = new LiteDatabase(_dbFile))
            {
                var data = db.GetCollection<UserSettings>("UserSettings");
                data.Update(_settings);
            }
        }
    }
}