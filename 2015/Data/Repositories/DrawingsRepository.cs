﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LiteDB;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator.Data.Repositories
{
    public class DrawingsRepository
    {
        private readonly string _dbFile;

        public DrawingsRepository()
        {
            ModelData = new ModelData();

            _dbFile = Path_Utils.ModelDataFile;

            if (_dbFile == "") return;

            var exists = File.Exists(_dbFile);

            using (var db = new LiteDatabase(_dbFile))
            {
                var data = db.GetCollection<ModelData>("ModelData");

                if (exists)
                {
                    ModelData = data.FindAll().FirstOrDefault();

                    if (ModelData == null)
                    {
                        ModelData = new ModelData();
                        data.Insert(ModelData);
                        data.EnsureIndex(x => x.Id);
                    }
                }
                else
                {
                    data.Insert(ModelData);
                    data.EnsureIndex(x => x.Id);
                }
            }
        }

        public ModelData ModelData { get; set; }

        public List<Drawing> Drawings
        {
            get { return ModelData.Drawings; }
        }

        public List<Drawing> GetDrawings(string search, string discipline,
            DrawingsViewType viewType = DrawingsViewType.New)
        {
            var drawings = Drawings;

            if (!string.IsNullOrEmpty(search))
            {
                drawings =
                    drawings.Where(x => x.GetFullName().Contains(search, StringComparison.OrdinalIgnoreCase)).ToList();
            }

            if (!string.IsNullOrEmpty(discipline))
            {
                if (discipline != "All")
                {
                    drawings =
                        drawings.Where(x => x.GetFullName().Contains(discipline, StringComparison.OrdinalIgnoreCase))
                            .ToList();
                }
            }

            return drawings.OrderBy(x => x.GetFullName()).ToList();
        }

        public bool DrawingExists(Guid id)
        {
            return ModelData.Drawings.Exists(x => x.Id == id);
        }

        public void DeleteDrawing(Guid id)
        {
            if (DrawingExists(id))
            {
                ModelData.Drawings.RemoveAll(x => x.Id == id);
                Save();
            }
        }

        public void AddDrawing(Drawing drawing)
        {
            Drawings.Add(drawing);
            Save();
        }

        public void AddDrawings(List<Drawing> drawings)
        {
            foreach (var drawing in drawings)
            {
                Drawings.Add(drawing);
            }

            Save();
        }

        public void DeleteDrawings(List<Drawing> drawings)
        {
            foreach (var drawing in drawings)
            {
                Drawings.RemoveAll(x => x.Id == drawing.Id);
            }
            Save();
        }

        public void Save()
        {
            using (var db = new LiteDatabase(_dbFile))
            {
                var data = db.GetCollection<ModelData>("ModelData");
                data.Update(ModelData);
            }
        }
    }
}