﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewCreator.Model;
using ViewCreator.Model.Base_Classes;
using ViewCreator.Utils;
using ViewCreator.Utils.App;


namespace ViewCreator.Data
{
    public class ViewRepository
    {
        private readonly List<ServiceLevels> _services;

        public ViewRepository()
        {
            _services = new List<ServiceLevels>();

            foreach (Discipline discipline in Enum.GetValues(typeof (Discipline)))
            {
                _services.Add(new ServiceLevels(discipline));
            }
        }
            
        public ServiceLevels GetServiceLevels(Discipline discipline)
        {
            return _services.FirstOrDefault(x => x.Discipline == discipline);
        }

        public ServiceLevels GetServiceLevels(ViewInfo viewInfo)
        {
            return _services.FirstOrDefault(x => x.Discipline == viewInfo.Discipline);
        }

        public LevelInfo GetLevelInfo(ViewInfo viewInfo)
        {
            var serviceLevels = GetServiceLevels(viewInfo);

            if (serviceLevels != null)
            {
                return serviceLevels.LevelInfos.FirstOrDefault(x => x.Level.Name == viewInfo.Level.Name);
            }

            return null;
        }

        public LevelInfo GetLevelInfo(Discipline discipline, string level)
        {
            var serviceLevels = GetServiceLevels(discipline);

            if (serviceLevels != null)
            {
                return serviceLevels.LevelInfos.FirstOrDefault(x => x.Level.Name == level);
            }

            return null;
        }

        public int GetViewInfoCount()
        {
            var count = 0;
            foreach (var viewInfo in GetViewInfos())
            {
                count++;
                count = count + viewInfo.DependentViews.Count;
            }
            return count;
        }

        public List<ViewInfo> GetViewInfos()
        {
            var viewInfos = new List<ViewInfo>();

            foreach (var service in _services)
            {
                viewInfos.AddRange(service.GetViewInfos());
            }


            return viewInfos.OrderBy(x => x.ToString()).ToList();
        }

        public Plan GetPlan(int id)
        {
            return GetPlans().FirstOrDefault(x => x.ViewId == id);
        }

        public DependentPlan GetDependentPlan(int id, int parentId)
        {
            var parent = GetPlan(parentId);
            if (parent != null)
            {
                return parent.DependentPlans.FirstOrDefault(x => x.ViewId == id);
            }
            return null;
        }

        public List<Plan> GetPlans()
        {
            var plans = new List<Plan>();

            foreach (var service in _services)
            {
                plans.AddRange(service.GetPlans());
            }


            return plans.OrderBy(x => x.Name).ToList();
        }

        //public List<ViewInfo> GetCurrentViewInfos()
        //{
        //    return App.DataAccess.ServiceLevels.GetViewInfos().ToList();
        //}
    }
}