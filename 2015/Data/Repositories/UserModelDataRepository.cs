﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Autodesk.Revit.DB;
using LiteDB;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.General;

namespace ViewCreator.Data.Repositories
{
    public class    UserModelDataRepository
    {
        private readonly string _dbFile;
        private readonly List<UserModelData> _userModels;


        public UserModelDataRepository()
        {
            _userModels = new List<UserModelData>();
            _dbFile = Path_Utils.UserModelDataFile;

            if (_dbFile == "") return;

            var exists = File.Exists(_dbFile);

            using (var db = new LiteDatabase(_dbFile))
            {
                var data = db.GetCollection<UserModelData>("UserModelData");

                if (exists)
                {
                    var modelDatas = data.FindAll();

                    foreach (var modelData in modelDatas)
                    {
                        _userModels.Add(modelData);

                        //Debug_Utils.WriteLine("UserModelDataRepository", "Loading UserModelData: " + modelData.CentralModelFullPath);
                    }
                }
            }
        }

      


        public bool ModelExists(string path)
        {
            return _userModels.Exists(x => x.CentralModelFullPath == path);
        }

        public UserModelData AddModel(string path)
        {
            UserModelData userModelData = null;

            using (var db = new LiteDatabase(_dbFile))
            {
                var data = db.GetCollection<UserModelData>("UserModelData");

                userModelData = new UserModelData(path);
                data.Insert(userModelData);
                _userModels.Add(userModelData);
                data.EnsureIndex(x => x.CentralModelFullPath);

            }

            return userModelData;
        }

        public UserModelData GetUserModelData()
        {
            return
                _userModels.FirstOrDefault(
                    x =>
                        x.CentralModelFullPath.Contains(RVT_Utils.GetCentralModelFullPath(),
                            StringComparison.InvariantCultureIgnoreCase));
        }
        public UserModelData GetUserModelData(string path)
        {
            return
                _userModels.FirstOrDefault(x => x.CentralModelFullPath == path);
        }



    

     

        //public void ReIndexDrawings(List<Drawing> drawings)
        //{
        //    var index = 1;

        //    foreach (var drawing in drawings)
        //    {
        //        var dbDrawing = GetDrawing(drawing);
        //        if (dbDrawing != null) dbDrawing.Index = index;
        //        index++;
        //    }
        //    Save();
        //}

     

      












        public bool Save()
        {
            using (var db = new LiteDatabase(_dbFile))
            {
                var data = db.GetCollection<UserModelData>("UserModelData");
                return data.Update(MainApp.AppData.CurrentProject.UserModelData);
            }
        }
    }
}