﻿using System.Collections.Generic;
using Autodesk.Internal.Windows;
using ViewCreator.Data.Repositories;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;

namespace ViewCreator.Data
{
    public class DataAccess
    {
        public DataAccess()
        {
            ViewData = new ViewRepository();
            UserData = new UserSettingsRepository();
            SettingsData = new SettingsRepository();
            ModelsData = new  UserModelDataRepository();
        }

        public string DataPath
        {
            get { return SettingsData.DataPath; }
        }

        public List<int> VisibleLevels
        {
            get { return ModelsData.GetUserModelData().VisibleLevelIds; }
        }

        public UserModelDataRepository ModelsData { get; set; }
        public SettingsRepository SettingsData { get; private set; }
        public ViewRepository ViewData { get; private set; }
        public UserSettingsRepository UserData { get; set; }


        public ModelSetting CurrentModelSetting
        {
            get { return SettingsData.GetCurrentModelSetting; }
        }

        public UserSettings UserSettings
        {
            get { return UserData.UserSettings; }
        }

        public ServiceLevels ServiceLevels
        {
            get { return ViewData.GetServiceLevels(UserSettings.Discipline); }
        }

        public void ReloadSettingsData()
        {
            SettingsData = new SettingsRepository();
        }

        public void ReloadViewData()
        {
            ViewData = new ViewRepository();
        }
    }
}