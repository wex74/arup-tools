﻿namespace ViewCreator.Data.Forms
{
    public class SelectViewTemplateData
    {
        public string Search { get; set; }

        public SelectViewTemplateData()
        {
            Search = "";
        }
    }
}
