﻿using System.Collections.Generic;
using ViewCreator.Model;
using ViewCreator.Utils;

namespace ViewCreator.Data.Forms
{
    public class ManageDrawingsData
    {
        public string Search { get; set; }
        public string Discipline { get; set; }
        public List<Drawing> Drawings { get; set; }
        public bool ShowSheets { get; set; }


        public ManageDrawingsData()
        {
            Drawings = new List<Drawing>();
            Discipline = "All";
        }
    }
}
