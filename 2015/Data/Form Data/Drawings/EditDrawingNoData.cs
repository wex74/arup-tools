﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class EditDrawingNoData
    {
        public string Prefix { get; set; }
        public string Suffix { get; set; }

        public int Number { get; set; }
    }
}
