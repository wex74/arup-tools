﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class CreateDrawingsData
    {
        public string Prefix { get; set; }
        public string Suffix { get; set; }

        public string DisciplineName { get; set; }
        public string SheetTemplate { get; set; }


        public int BasementLevels { get; set; }
        public int GroundLevels { get; set; }
        public int LevelLevels { get; set; }
        public int MezzanineLevels { get; set; }
        public int PlantLevels { get; set; }
        public int RoofLevels { get; set; }


        public int BasementNumbering { get; set; }
        public int GroundNumbering { get; set; }
        public int LevelsNumbering { get; set; }
        public int MezzanineNumbering { get; set; }
        public int PlantNumbering { get; set; }
        public int RoofNumbering { get; set; }

        public int Details { get; set; }
        public int Schematics { get; set; }
        public int Schedules { get; set; }
        public int Sections { get; set; }
        public int Elevations { get; set; }


        public int DetailsNumbering { get; set; }
        public int SchematicsNumbering { get; set; }
        public int SchedulesNumbering { get; set; }
        public int SectionsNumbering { get; set; }
        public int ElevationsNumbering { get; set; }

        public bool AddCover { get; set; }
        public bool AddTitle { get; set; }


        public List<string> Scopeboxes { get; set; }

        public CreateDrawingsData()
        {
            Scopeboxes = new List<string>();



        }
    }
}
