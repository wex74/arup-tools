﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class NewDrawingData
    {
        public bool Scopeboxes { get; set; }
        public bool AutoSelectView { get; set; }
        public bool UseNextNo { get; set; }
        public string SheetTemplate{ get; set; }
        public string Discipline { get; set; }
        public string DrawingType { get; set; }
        public string Prefix { get; set; }
        public string Number { get; set; }
        public string Suffix { get; set; }
        public string Title { get; set; }
        public NewDrawingData()
        {
            
        }
    }
}
