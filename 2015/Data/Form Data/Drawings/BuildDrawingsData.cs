﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class BuildDrawingsData
    {
        public List<string> Drawings { get; set; }

        public BuildDrawingsData()
        {
            Drawings = new List<string>();
        }
    }
}
