﻿using System.Collections.Generic;
using ViewCreator.Model;
using ViewCreator.Utils;

namespace ViewCreator.Data.Forms
{
    public class ManageSheetTemplatessData
    {
        public string Search { get; set; }
        public string Discipline { get; set; }


        public ManageSheetTemplatessData()
        {
            Discipline = "All";
        }
    }
}
