namespace ViewCreator.Forms.Sections.Views.Controls
{
    public class BuildingsScreenView
    {
        public BuildingsScreenView()
        {
            ShowPlans = true;
            ShowThreeds = true;
            ShowSections = true;


            ShowCoordination = false;

            ShowMechanical = false;
            ShowElectrical = false;
            ShowHydraulic = false;
            ShowFire = false;

            ShowLevels = true;

            ShowDependents = false;
        }

        public bool ShowPlans { get; set; }
        public bool ShowThreeds { get; set; }
        public bool ShowSections { get; set; }

        public bool ShowFavoutites { get; set; }
        public bool ShowDependents { get; set; }
        public bool ShowCoordination { get; set; }
        public bool ShowMechanical { get; set; }
        public bool ShowElectrical { get; set; }
        public bool ShowHydraulic { get; set; }
        public bool ShowFire { get; set; }
        public bool ShowLevels { get; set; }
        public bool ShowFlat { get; set; }
        public bool ShowRls { get; set; }
        public bool ShowDisciplines { get; set; }



    }
}