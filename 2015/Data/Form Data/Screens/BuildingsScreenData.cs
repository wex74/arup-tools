﻿using System.Collections.Generic;
using ViewCreator.Utils;
using ViewCreator.Data.App;
using ViewCreator.Forms.Sections.Views.Controls;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Data.Forms
{
    public class BuildingsScreenData
    {
        public string Search { get; set; }

        public bool SingleDiscipline { get; set; }

        public BuildingsScreenView View { get; set; }

        public string Scopebox { get; set; }

        public List<string> Levels { get; set; }



        public BuildingsScreenData()
        {
            SingleDiscipline = true;

            View = new BuildingsScreenView();

            Levels = new List<string>();

            Scopebox = "";
            Search = "";
        }


        public void SetInitData()
        {
            Levels = Level_Utils.GetLevelNames();

            if (MainApp.AppData.IsUserBuildings)
            {
                if (MainApp.AppData.UserDiscipline == Discipline.Coordination)
                {
                    View.ShowCoordination = true;
                }

                if (MainApp.AppData.UserDiscipline == Discipline.Mechanical)
                {
                    View.ShowMechanical = true;
                }

                if (MainApp.AppData.UserDiscipline == Discipline.Electrical)
                {
                    View.ShowElectrical = true;
                }

                if (MainApp.AppData.UserDiscipline == Discipline.Hydraulic)
                {
                    View.ShowHydraulic = true;
                }

                if (MainApp.AppData.UserDiscipline == Discipline.Fire)
                {
                    View.ShowFire = true;
                }
            }

        }


        public int DisciplineTotalCount
        {
            get
            {
                return 5;
            }
        }

        public int DisciplineCount
        {
            get
            {
                var count = 0;
                if (View.ShowCoordination) count++;
                if (View.ShowMechanical) count++;
                if (View.ShowElectrical) count++;
                if (View.ShowHydraulic) count++;
                if (View.ShowFire) count++;
                return count;
            }
        }

    }
}
