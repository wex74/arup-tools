﻿using System.Collections.Generic;
using ViewCreator.Utils;
using ViewCreator.Data.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Data.Forms
{
    public class StructuresScreenData
    {
        public string Search { get; set; }

        public bool ShowPlans { get; set; }
        public bool ShowThreeds { get; set; }
        public bool ShowSections { get; set; }



        public bool ShowFavourites { get; set; }



        public bool ShowLevels { get; set; }
        public bool ShowFlat { get; set; }
        public bool ShowRLs { get; set; }

        public bool ShowDependents { get; set; }

        public string Scopebox { get; set; }

        public List<string> Levels { get; set; }



        public StructuresScreenData()
        {
            ShowPlans = true;
            ShowThreeds = true;
            ShowSections = true;


            ShowLevels = true;

            ShowDependents = false;

            Levels = new List<string>();

            Scopebox = "";
            Search = "";
        }


        public void SetInitData()
        {
           

        }


    }
}
