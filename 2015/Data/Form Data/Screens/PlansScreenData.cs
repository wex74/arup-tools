﻿using System.Collections.Generic;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Data.Forms
{
    public class PlansScreenData
    {
        public Discipline Discipline { get; set; }
        public bool ShowActiveLevels { get; set; }
        public bool ShowViews { get; set; }
        public bool ShowDependentViews { get; set; }
        public bool ShowSheets { get; set; }
        public List<string> Levels { get; set; }
        public string Search { get; set; }

        public PlansScreenData()
        {
            Search = "";
            Levels = new List<string>();
            ShowActiveLevels = false;
            ShowViews = true;
            ShowDependentViews = true;
            ShowSheets = true;
        }


        public void SetInitData()
        {
            Levels = Level_Utils.GetLevelNames();

        }

    }
}
