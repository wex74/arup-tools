﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiteDB;
using ViewCreator.Data.Forms;

namespace ViewCreator.Model.Settings
{
    public class FormSettings
    {
        [BsonId]
        public Guid Id { get; set; }

        public DependentViewsData DependentViewsData { get; set; }
        public PlansScreenData PlansScreenData { get; set; }
        public AddViewsData AddViewsData { get; set; }
        public SelectViewTemplateData SelectViewTemplateData { get; set; }
        public SelectsScopeboxesData SelectsScopeboxesData { get; set; }
        public AddCustomViewsData AddCustomViewsData { get; set; }
        public QuickPlansData QuickPlanData { get; set; }
        public CreateDrawingsData CreateDrawingsData { get; set; }
        public EditDrawingNoData EditDrawingNoData { get; set; }
        public BuildDrawingsData BuildDrawingsData { get; set; }
        public ManageDrawingsData ManageDrawingsData { get; set; }
        public ManageSheetTemplatessData ManageSheetTemplatessData { get; set; }
        public NewDrawingData NewDrawingData { get; set; }
        public EditViewsData EditViewsData { get; set; }
        public InsertViewsData InsertViewsData { get; set; }
        public SettingsData SettingsData { get; set; }
        public BuildingsScreenData BuildingsScreenData { get; set; }
        public StructuresScreenData StructuresScreenData { get; set; }

        public FormSettings()
        {
            Id = Guid.NewGuid();

            DependentViewsData = new DependentViewsData();
            PlansScreenData = new PlansScreenData();
            AddViewsData = new AddViewsData();
            SelectViewTemplateData = new SelectViewTemplateData();
            SelectsScopeboxesData = new SelectsScopeboxesData();
            AddCustomViewsData = new AddCustomViewsData();
            QuickPlanData = new QuickPlansData();
            CreateDrawingsData = new CreateDrawingsData();
            EditDrawingNoData = new EditDrawingNoData();
            BuildDrawingsData = new BuildDrawingsData();
            ManageDrawingsData = new ManageDrawingsData();
            ManageSheetTemplatessData = new ManageSheetTemplatessData();
            InsertViewsData = new InsertViewsData();
            EditViewsData = new EditViewsData();
            NewDrawingData = new NewDrawingData();
            SettingsData = new SettingsData();
            BuildingsScreenData = new BuildingsScreenData();
            StructuresScreenData = new StructuresScreenData();
        }

    }
}
