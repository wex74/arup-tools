﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class DependentViewsData
    {
        public DependentViewsData()
        {
            ScopeBoxes = new List<string>();
            Search = "";
            NewViews = true;
        }

        public string Search { get; set; }
        public bool NewViews { get; set; }
        public List<string> ScopeBoxes { get; set; }
        public int Dependents { get; set; }
    }
}