﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class AddViewsData
    {
        public string SetName { get; set; }
        public bool UserCopy { get; set; }
        public string Search { get; set; }
        public string Scopebox { get; set; }
        public List<string> Levels { get; set; }
        public AddViewsData()
        {
            Levels = new List<string>();
            UserCopy = false;
            SetName = "Default";
            Scopebox = "None";
            Search = "";
        }
    }
}
