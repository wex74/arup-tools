﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class SelectsScopeboxesData
    {
        public SelectsScopeboxesData()
        {
            ScopeBoxes = new List<string>();
        }

        public List<string> ScopeBoxes { get; set; }
        public int Dependents { get; set; }
    }
}