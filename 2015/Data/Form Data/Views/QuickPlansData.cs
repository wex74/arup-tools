﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class QuickPlansData
    {
        public List<string> Levels { get; set; }
        public string Prefix { get; set; }
        public string PlanName { get; set; }
        public string Template { get; set; }
        public string ViewType { get; set; }
        public string ScopeBox { get; set; }
        public bool Favourite { get; set; }

        public QuickPlansData()
        {
            Levels = new List<string>();
        }
    }
}
