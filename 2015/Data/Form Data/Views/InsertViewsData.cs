﻿namespace ViewCreator.Data.Forms
{
    public class InsertViewsData
    {
        public string SheetSearch { get; set; }
        public string ViewSearch { get; set; }
        public bool UseDualSearch { get; set; }
  
        public InsertViewsData()
        {
            UseDualSearch = true;
            SheetSearch = "";
            ViewSearch = "";
        }
    }
}
