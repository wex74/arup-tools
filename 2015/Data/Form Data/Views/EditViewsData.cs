﻿using System.Collections.Generic;

namespace ViewCreator.Data.Forms
{
    public class EditViewsData
    {
        public string Search { get; set; }
        public bool ShowDependents { get; set; }
        public EditViewsData()
        {
            Search = "";
        }
    }
}
