﻿namespace ViewCreator.Data.Forms
{
    public class AddCustomViewsData
    {
        public bool CheckAllLevels { get; set; }
        public bool CheckAllViews { get; set; }
        public bool UserCopy { get; set; }
        public string Scopebox { get; set; }
        public AddCustomViewsData()
        {
            CheckAllLevels = true;
            CheckAllViews = true;
            UserCopy = false;
            Scopebox = "None";
        }
    }
}
