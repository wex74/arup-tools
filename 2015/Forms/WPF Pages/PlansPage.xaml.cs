﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using Autodesk.Revit.UI;
using ViewCreator.Forms.Controls;

namespace ViewCreatorWPF.Pages
{
    /// <summary>
    ///     Interaction logic for MainPage.xaml
    /// </summary>
    public partial class PlansPage : Page, IDockablePaneProvider
    {
        public PlansPage()
        {
            InitializeComponent();
        }

        public ctlPlansScreen Control { get; set; }

        public void SetupDockablePane(DockablePaneProviderData data)
        {
            data.FrameworkElement = this;

            data.InitialState.DockPosition = DockPosition.Tabbed;
            data.InitialState.TabBehind = DockablePanes.BuiltInDockablePanes.PropertiesPalette;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
       
        }

        public void LoadControl()
        {
            var host =
           new WindowsFormsHost();

            Control = new ctlPlansScreen();
            host.Child = Control;
            HostGrid.Children.Add(host);
        }
    }
}