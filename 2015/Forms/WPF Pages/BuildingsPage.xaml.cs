﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Autodesk.Revit.UI;
using ViewCreator.Forms.Controls;
using ViewCreator.Forms.Sections.Views.Controls;

namespace ViewCreatorWPF
{
    /// <summary>
    /// Interaction logic for MyViewsPage.xaml
    /// </summary>
    public partial class BuildingsPage : Page, IDockablePaneProvider
    {
        public BuildingsPage()
        {
            InitializeComponent();
        }

        public ctlBuildingsScreen Control { get; set; }

        public void SetupDockablePane(DockablePaneProviderData data)
        {
            data.FrameworkElement = this;

            data.InitialState.DockPosition = DockPosition.Tabbed;
            data.InitialState.TabBehind = DockablePanes.BuiltInDockablePanes.PropertiesPalette;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        public void LoadControl()
        {
            var host =
               new WindowsFormsHost();

            Control = new ctlBuildingsScreen();
            host.Child = Control;
            HostGrid.Children.Add(host);
        }
    }
}
