﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Utils;
using Autodesk.Revit.DB;
using ViewCreator.Data;
using ViewCreator.Data.Forms;
using ViewCreator.Data.Repositories;
using ViewCreator.Model;
using ViewCreator.Utils.RVT;
using Form = System.Windows.Forms.Form;
using View = Autodesk.Revit.DB.View;


namespace ViewCreator.Forms
{
    public partial class frmSelectViewTemplate : Form
    {
        private SelectViewTemplateData _data;
        public string TemplateName { get; set; }

        private List<View> _templates;
 
        public frmSelectViewTemplate()
        {
            InitializeComponent();
        }

        private void frmTemplateSetter_Load(object sender, EventArgs e)
        {
            _data = MainApp.AppData.FormSettings.SelectViewTemplateData;
            txtSearch.Text = _data.Search;

            if (string.IsNullOrEmpty(txtSearch.Text)) BuildTemplates();

        }

        public void BuildTemplates()
        {
            tvTemplates.Nodes.Clear();

            var node = tvTemplates.Nodes.Add("None");
            node.Tag = "None";

            foreach (var template in Template_Utils.GetViewTemplateNames(txtSearch.Text))
            {
                node = tvTemplates.Nodes.Add(template);
                node.Tag = template;

                if (template == TemplateName)
                {
                    tvTemplates.SelectedNode = node;
                }
            }

            if (tvTemplates.SelectedNode == null)
            {
                if (tvTemplates.Nodes.Count > 0)
                {
                    tvTemplates.SelectedNode = tvTemplates.Nodes[0];
                }
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (tvTemplates.SelectedNode != null)
            {
                TemplateName = tvTemplates.SelectedNode.Text;
                
                DialogResult = DialogResult.OK;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            BuildTemplates();
        }

        private void frmSelectViewTemplate_FormClosing(object sender, FormClosingEventArgs e)
        {
            _data.Search = txtSearch.Text;
            MainApp.AppData.UserData.Save();
        }
    }
}
