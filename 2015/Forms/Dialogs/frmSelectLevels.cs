﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Data;
using ViewCreator.Utils;
using Autodesk.Revit.DB;
using ViewCreator.Model.Settings;
using ViewCreator.Utils.UI;
using Form = System.Windows.Forms.Form;
using List_Utils = ViewCreator.Utils.UI.List_Utils;

namespace ViewCreator.Forms
{
    public partial class frmSelectLevels : Form
    {
        private List<string> _levels;
        public List<string> SelectedLevels { get; set; }
        private List<string> _selectedLevelsToShow;

        public frmSelectLevels()
        {
            InitializeComponent();
        }

        public frmSelectLevels(List<string> levels, List<string> selectedLevels)
        {
            _levels = levels;
            _selectedLevelsToShow = selectedLevels;
            InitializeComponent();
        }

        private void frmLevels_Load(object sender, EventArgs e)
        {

            if (_levels == null ||  _selectedLevelsToShow == null) return;

            lstLevels.LoadAndSetList(_levels, _selectedLevelsToShow);

            if (_levels.Count == _selectedLevelsToShow.Count)
            {
                ckAllLevels.Checked = true;
            }

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            SelectedLevels = List_Utils.GetCheckedListboxValues(lstLevels);
            DialogResult = DialogResult.OK;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ckAllLevels_CheckedChanged(object sender, EventArgs e)
        {
            for (int x = 0; x < lstLevels.Items.Count; x++)
            {
                lstLevels.SetItemChecked(x, ckAllLevels.Checked);
            }
        }

        private void lstLevels_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
