﻿namespace ViewCreator.Forms
{
    partial class frmSelectDiscipline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstDisciplines = new System.Windows.Forms.CheckedListBox();
            this.ckAllLevels = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstDisciplines
            // 
            this.lstDisciplines.CheckOnClick = true;
            this.lstDisciplines.FormattingEnabled = true;
            this.lstDisciplines.Location = new System.Drawing.Point(12, 35);
            this.lstDisciplines.Name = "lstDisciplines";
            this.lstDisciplines.Size = new System.Drawing.Size(200, 229);
            this.lstDisciplines.TabIndex = 67;
            // 
            // ckAllLevels
            // 
            this.ckAllLevels.AutoSize = true;
            this.ckAllLevels.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckAllLevels.Location = new System.Drawing.Point(107, 12);
            this.ckAllLevels.Name = "ckAllLevels";
            this.ckAllLevels.Size = new System.Drawing.Size(105, 17);
            this.ckAllLevels.TabIndex = 68;
            this.ckAllLevels.Text = "Check All Levels";
            this.ckAllLevels.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(59, 286);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 70;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(140, 286);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 69;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // frmDisciplines
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 321);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.ckAllLevels);
            this.Controls.Add(this.lstDisciplines);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmDisciplines";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Disciplines";
            this.Load += new System.EventHandler(this.frmDisciplines_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox lstDisciplines;
        private System.Windows.Forms.CheckBox ckAllLevels;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
    }
}