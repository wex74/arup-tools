﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Add_Edit_Forms
{
    public partial class frmSelectScopeboxes : Form
    {
        private SelectsScopeboxesData _formData;
        private bool _loaded;
        public List<string> SelectedScopboxes;
        public int Dependents { get; set; }


        public frmSelectScopeboxes()
        {
            InitializeComponent();
        }

       

        private void frmSelectScopeboxes_Load(object sender, EventArgs e)
        {
            _formData = MainApp.AppData.FormSettings.SelectsScopeboxesData;

            lstScopeboxes.LoadAndSetList(Scopebox_Utils.GetScopeBoxNames(), _formData.ScopeBoxes);
            ckAllScopeBoxes.Checked = lstScopeboxes.AllItemsChecked();

            numBox.Value = _formData.Dependents;

            if (lstScopeboxes.Items.Count == 0)
            {
                lstScopeboxes.Enabled = false;
                ckAllScopeBoxes.Enabled = false;

                if (numBox.Value == 0)
                {
                    numBox.Value = 1;
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Dependents = (int) numBox.Value;
            SelectedScopboxes = lstScopeboxes.GetCheckedListboxValues();

            _formData.ScopeBoxes = SelectedScopboxes;
            _formData.Dependents = Dependents;

            MainApp.AppData.SaveUserModelData();

            if (Dependents == 0 && SelectedScopboxes.Count == 0)
            {
                MessageBox.Show("No Scopeboxes selected or number dependent views nominated");
                return;
            }


            DialogResult = DialogResult.OK;
        }


        public int Count
        {
            get
            {
                return SelectedScopboxes.Count + Dependents;
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmSelectScopeboxes_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void ckAllScopeBoxes_CheckedChanged(object sender, EventArgs e)
        {
            lstScopeboxes.CheckAllItems(ckAllScopeBoxes.Checked);
        }
    }
}