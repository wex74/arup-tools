﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Events;
using ViewCreator.Forms.Controls;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Utils;

namespace ViewCreator.Forms.Sections.Drawings.Controls
{
    public partial class ctlDrawingsScreen : UserControl
    {
        private Drawing _drawing;

        public ctlDrawingsScreen()
        {
            InitializeComponent();
        }


        public void LoadControl()
        {
            LoadDrawings();
        }



        public void LoadDrawings()
        {
            //var drawings = GetSearchDrawings();

            //ctlViewDrawings.LoadControl(drawings);
        }


        //private List<Drawing> GetSearchDrawings()
        //{
        //    var drawings = App.AppData.CurrentProject.ProjectSheets.GetDrawings();

        //    if (ctlSearch.Search != "")
        //    {
        //        drawings = drawings.Where(x => x.GetFullName().Search(ctlSearch.TextInfo)).ToList();
        //    }

        //    return drawings;
        //} 
        public void SelectCurrentView()
        {
           // ctlViewDrawings.SelectCurrentView();
        }

        //private void ctlViewDrawings_DrawingDoubleClicked(object sender, EventArgs e)
        //{
        //    _drawing = (Drawing)sender;

        //    var sheet = _drawing.GetViewSheet();
        //    if (sheet != null)
        //    {
        //        ExternalEvents_View.Run_Navigate(sheet);
        //    }
     
        //}

        //private void ctlSearch_SearchChanged(object sender, EventArgs e)
        //{
        //    LoadDrawings();
        //}
    }
}
