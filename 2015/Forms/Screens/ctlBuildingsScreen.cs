﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Events.Views;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.General;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlBuildingsScreen : UserControl
    {
        /// <summary>
        /// Load building services views
        /// Filter & search
        /// </summary>
        /// 
        private BuildingsScreenData _formData;
        private List<Plan> _plans;
        private string _search;
        private bool _loaded;

        public ctlBuildingsScreen()
        {
            InitializeComponent();
        }


        private void ctlBuildingsScreen_Load(object sender, EventArgs e)
        {
        }


        public void LoadControl()
        {
            Debug_Utils.WriteLine("ctlBuildingsScreen", "LoadControl");

            // Set data variables 
            _formData = MainApp.AppData.FormSettings.BuildingsScreenData;
            _plans = MainApp.AppData.CurrentProject.ProjectPlans.GetPlans(ctlSearch.Search);

            // Set UI from data
            tbPlan.Checked = _formData.View.ShowPlans;
            tbDependents.Checked = _formData.View.ShowDependents;
            tbFavourites.Checked = _formData.View.ShowFavoutites;

            // Load controls
            ctlSearch.LoadControl(_formData.Search);
            ctlFilter.LoadControl();
            ctlViews.LoadControl();

            tbDiscipline.Visible = MainApp.AppData.IsUserBuildings;

            _loaded = true;

            LoadViews();
        }

        private void LoadViews()
        {
            ctlViews.LoadViews();
        }




        private void ctlSearch_SearchChanged_1(object sender, EventArgs e)
        {
            _search = (string) sender;
            _formData.Search = _search;
            SaveData();
            LoadViews();
        }


        private void tbPlan_Click(object sender, EventArgs e)
        {
            tbPlan.Checked = !tbPlan.Checked;
            _formData.View.ShowPlans = tbPlan.Checked;
            SaveData();
            LoadViews();
        }

        private void tbThreed_Click(object sender, EventArgs e)
        {
            tbThreed.Checked = !tbThreed.Checked;
            LoadViews();
        }

        private void tbSection_Click(object sender, EventArgs e)
        {
            tbSection.Checked = !tbSection.Checked;
            LoadViews();
        }

        private void ctlViews_PlanDoubleClicked(object sender, EventArgs e)
        {         
            var plan = (Plan)sender;
            var view = plan.GetView();

            if (view.Id != RVT_Utils.ActiveView.Id)
            {
                NavigateEvent.Run(view);
            }
            else
            {
                EditViewUI.ShowForm();
            }
        }

        private void ctlViews_Load(object sender, EventArgs e)
        {
        }


        private void ctlViewsFilter_DisciplinesChanged(object sender, EventArgs e)
        {
            LoadViews();
        }

        private void ctlViewsFilter_LevelsChanged(object sender, EventArgs e)
        {
            LoadViews();
        }


        private void ClearViews()
        {
            _formData.View.ShowFlat = false;
            _formData.View.ShowRls = false;
            _formData.View.ShowDisciplines = false;
            _formData.View.ShowLevels = false;
        }


        private void tbFlat_Click_1(object sender, EventArgs e)
        {
            ClearViews();
            _formData.View.ShowFlat = true;

            SaveData();
            LoadViews();
        }

        private void tbRLs_Click(object sender, EventArgs e)
        {
            ClearViews();
            _formData.View.ShowRls = true;

            SaveData();
            LoadViews();
        }

        private void tbDiscipline_Click(object sender, EventArgs e)
        {
            ClearViews();
            _formData.View.ShowDisciplines = true;


            SaveData();
            LoadViews();
        }

        private void tbLevels_Click_1(object sender, EventArgs e)
        {
            ClearViews();
            _formData.View.ShowLevels = true;

            SaveData();
            LoadViews();
        }

        private static void SaveData()
        {
            MainApp.AppData.SaveUserModelData();
        }

        private void tbViews_DropDownOpening(object sender, EventArgs e)
        {
            tbLevels.Checked = _formData.View.ShowLevels;
            tbDiscipline.Checked = _formData.View.ShowDisciplines;
            tbFlat.Checked = _formData.View.ShowFlat;
            tbRLs.Checked = _formData.View.ShowRls;
        }

        private void tbDependents_Click(object sender, EventArgs e)
        {
            tbDependents.Checked = !tbDependents.Checked;
            _formData.View.ShowDependents = tbDependents.Checked;
            SaveData();
            LoadViews();
        }


        private void tbFavourites_Click(object sender, EventArgs e)
        {
            tbFavourites.Checked = !tbFavourites.Checked;
            _formData.View.ShowFavoutites = tbFavourites.Checked;
            SaveData();
            LoadViews();
        }

        private void ctlViews_DoubleClicked(object sender, EventArgs e)
        {
            var view = ctlViews.GetView();

            if (view != null)
            {
                if (view.Id != RVT_Utils.ActiveView.Id)
                {
                    NavigateEvent.Run(view);
                }
                else
                {
                    if (!EditViewUI.FormVisible)
                    {
                        EditViewUI.ShowForm();
                    }
                    else
                    {
                        EditViewUI.ReLoad();
                    }
                }

            }
        }

        private void tbEdit_Click(object sender, EventArgs e)
        {

            var views = ctlViews.GetViews();

            if (views.Any())
            {
                EditViewsUI.ShowForm(views);
            }

           
        }
    }
}