﻿using ViewCreator.Utils.App;

namespace ViewCreator.Forms.Controls
{
    partial class ctlPlansScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlPlansScreen));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.tbBuild = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.dpmnuAdd = new System.Windows.Forms.ToolStripDropDownButton();
            this.tbAddPlanViews = new System.Windows.Forms.ToolStripMenuItem();
            this.tbAddCustomPlanViews = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbAddDependentPlans = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuAddAll = new System.Windows.Forms.ToolStripMenuItem();
            this.addAllDiscipline = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbEdit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbDeleteDrop = new System.Windows.Forms.ToolStripDropDownButton();
            this.tbDeleteSelectItems = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDeleteCheckedItems = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbSettings = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabelInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ctlViews = new ViewCreator.Forms.Controls.ctlPlansViews();
            this.ctlFilter = new ViewCreator.Forms.Controls.ctlPlansFilter();
            this.ctlSearch = new ViewCreator.Forms.Controls.ctlSearch();
            this.ctlDisciplines = new ViewCreator.Forms.Controls.ctlDisciplines();
            this.toolStrip.SuspendLayout();
            this.panel3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Electrical");
            this.imageList.Images.SetKeyName(1, "Fire");
            this.imageList.Images.SetKeyName(2, "Hydraulic");
            this.imageList.Images.SetKeyName(3, "Mechanical");
            this.imageList.Images.SetKeyName(4, "Structural");
            this.imageList.Images.SetKeyName(5, "Plan");
            this.imageList.Images.SetKeyName(6, "Architectural");
            this.imageList.Images.SetKeyName(7, "Coordination");
            this.imageList.Images.SetKeyName(8, "Scopebox");
            this.imageList.Images.SetKeyName(9, "Threed");
            this.imageList.Images.SetKeyName(10, "Dependent");
            this.imageList.Images.SetKeyName(11, "Level");
            this.imageList.Images.SetKeyName(12, "Section");
            this.imageList.Images.SetKeyName(13, "Sheet");
            this.imageList.Images.SetKeyName(14, "Schedule");
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbBuild,
            this.toolStripSeparator1,
            this.dpmnuAdd,
            this.toolStripSeparator2,
            this.tbEdit,
            this.toolStripSeparator4,
            this.tbDeleteDrop,
            this.toolStripSeparator3,
            this.tbSettings});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(357, 27);
            this.toolStrip.TabIndex = 79;
            this.toolStrip.Text = "toolStrip";
            // 
            // tbBuild
            // 
            this.tbBuild.Image = global::ViewCreator.Properties.Resources.Tick;
            this.tbBuild.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbBuild.Name = "tbBuild";
            this.tbBuild.Size = new System.Drawing.Size(67, 24);
            this.tbBuild.Text = "Build";
            this.tbBuild.Click += new System.EventHandler(this.tbBuild_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
            // 
            // dpmnuAdd
            // 
            this.dpmnuAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbAddPlanViews,
            this.tbAddCustomPlanViews,
            this.toolStripMenuItem1,
            this.tbAddDependentPlans,
            this.toolStripMenuItem2,
            this.mnuAddAll,
            this.addAllDiscipline});
            this.dpmnuAdd.Image = global::ViewCreator.Properties.Resources.Add1;
            this.dpmnuAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dpmnuAdd.Name = "dpmnuAdd";
            this.dpmnuAdd.Size = new System.Drawing.Size(71, 24);
            this.dpmnuAdd.Text = "Add";
            this.dpmnuAdd.DropDownOpening += new System.EventHandler(this.dpmnuAdd_DropDownOpening);
            // 
            // tbAddPlanViews
            // 
            this.tbAddPlanViews.Image = global::ViewCreator.Properties.Resources.NewPlan;
            this.tbAddPlanViews.Name = "tbAddPlanViews";
            this.tbAddPlanViews.Size = new System.Drawing.Size(209, 26);
            this.tbAddPlanViews.Text = "Plans";
            this.tbAddPlanViews.Click += new System.EventHandler(this.tbAddPlanViews_Click);
            // 
            // tbAddCustomPlanViews
            // 
            this.tbAddCustomPlanViews.Image = global::ViewCreator.Properties.Resources.NewPlan;
            this.tbAddCustomPlanViews.Name = "tbAddCustomPlanViews";
            this.tbAddCustomPlanViews.Size = new System.Drawing.Size(209, 26);
            this.tbAddCustomPlanViews.Text = "Custom Plans";
            this.tbAddCustomPlanViews.Click += new System.EventHandler(this.tbAddCustomPlanViews_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(206, 6);
            // 
            // tbAddDependentPlans
            // 
            this.tbAddDependentPlans.Image = global::ViewCreator.Properties.Resources.Dependant;
            this.tbAddDependentPlans.Name = "tbAddDependentPlans";
            this.tbAddDependentPlans.Size = new System.Drawing.Size(209, 26);
            this.tbAddDependentPlans.Text = "Dependent Views";
            this.tbAddDependentPlans.Click += new System.EventHandler(this.tbAddDependentPlans_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(206, 6);
            // 
            // mnuAddAll
            // 
            this.mnuAddAll.Name = "mnuAddAll";
            this.mnuAddAll.Size = new System.Drawing.Size(209, 26);
            this.mnuAddAll.Text = "All Plans";
            this.mnuAddAll.Click += new System.EventHandler(this.mnuAddAll_Click);
            // 
            // addAllDiscipline
            // 
            this.addAllDiscipline.Name = "addAllDiscipline";
            this.addAllDiscipline.Size = new System.Drawing.Size(209, 26);
            this.addAllDiscipline.Text = "All Discipline Plans";
            this.addAllDiscipline.Click += new System.EventHandler(this.addAllDiscipline_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
            // 
            // tbEdit
            // 
            this.tbEdit.Image = global::ViewCreator.Properties.Resources.Edit;
            this.tbEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbEdit.Name = "tbEdit";
            this.tbEdit.Size = new System.Drawing.Size(59, 24);
            this.tbEdit.Text = "Edit";
            this.tbEdit.Click += new System.EventHandler(this.tbEdit_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 27);
            // 
            // tbDeleteDrop
            // 
            this.tbDeleteDrop.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbDeleteSelectItems,
            this.mnuDeleteCheckedItems});
            this.tbDeleteDrop.Image = global::ViewCreator.Properties.Resources.Delete;
            this.tbDeleteDrop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbDeleteDrop.Name = "tbDeleteDrop";
            this.tbDeleteDrop.Size = new System.Drawing.Size(87, 24);
            this.tbDeleteDrop.Text = "Delete";
            this.tbDeleteDrop.DropDownOpening += new System.EventHandler(this.tbDeleteDrop_DropDownOpening);
            this.tbDeleteDrop.Click += new System.EventHandler(this.tbDelete_Click);
            // 
            // tbDeleteSelectItems
            // 
            this.tbDeleteSelectItems.Image = global::ViewCreator.Properties.Resources.Checkall;
            this.tbDeleteSelectItems.Name = "tbDeleteSelectItems";
            this.tbDeleteSelectItems.Size = new System.Drawing.Size(228, 26);
            this.tbDeleteSelectItems.Text = "Select Items";
            this.tbDeleteSelectItems.Click += new System.EventHandler(this.tbDeleteSelectItems_Click);
            // 
            // mnuDeleteCheckedItems
            // 
            this.mnuDeleteCheckedItems.Image = global::ViewCreator.Properties.Resources.RemoveFile;
            this.mnuDeleteCheckedItems.Name = "mnuDeleteCheckedItems";
            this.mnuDeleteCheckedItems.Size = new System.Drawing.Size(228, 26);
            this.mnuDeleteCheckedItems.Text = "Delete Checked Items";
            this.mnuDeleteCheckedItems.Click += new System.EventHandler(this.mnuDeleteCheckedItems_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // tbSettings
            // 
            this.tbSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbSettings.Image = global::ViewCreator.Properties.Resources.Settings;
            this.tbSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbSettings.Name = "tbSettings";
            this.tbSettings.Size = new System.Drawing.Size(24, 24);
            this.tbSettings.Text = "Settings";
            this.tbSettings.Click += new System.EventHandler(this.tbSettings_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ctlSearch);
            this.panel3.Controls.Add(this.ctlDisciplines);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 98);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(357, 75);
            this.panel3.TabIndex = 82;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabelInfo,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 487);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(357, 25);
            this.statusStrip1.TabIndex = 83;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabelInfo
            // 
            this.statusLabelInfo.Name = "statusLabelInfo";
            this.statusLabelInfo.Size = new System.Drawing.Size(45, 20);
            this.statusLabelInfo.Text = "Label";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 20);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSlateGray;
            this.panel1.Controls.Add(this.ctlFilter);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(7, 0, 7, 6);
            this.panel1.Size = new System.Drawing.Size(357, 98);
            this.panel1.TabIndex = 84;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 27);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(357, 173);
            this.panel2.TabIndex = 85;
            // 
            // ctlViews
            // 
            this.ctlViews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlViews.Location = new System.Drawing.Point(0, 200);
            this.ctlViews.Margin = new System.Windows.Forms.Padding(5);
            this.ctlViews.Name = "ctlViews";
            this.ctlViews.Size = new System.Drawing.Size(357, 287);
            this.ctlViews.TabIndex = 81;
            this.ctlViews.ViewDoubleClicked += new System.EventHandler(this.ctlViews_ViewDoubleClicked);
            this.ctlViews.ViewsLoaded += new System.EventHandler(this.ctlViews_ViewsLoaded);
            this.ctlViews.ViewsUpdated += new System.EventHandler(this.ctlViews_ViewsUpdated);
            // 
            // ctlFilter
            // 
            this.ctlFilter.BackColor = System.Drawing.Color.LightSlateGray;
            this.ctlFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlFilter.Location = new System.Drawing.Point(7, 0);
            this.ctlFilter.Margin = new System.Windows.Forms.Padding(5);
            this.ctlFilter.Name = "ctlFilter";
            this.ctlFilter.Size = new System.Drawing.Size(343, 92);
            this.ctlFilter.TabIndex = 80;
            this.ctlFilter.ActiveLevelsChanged += new System.EventHandler(this.ctlFilter_ActiveLevelsChanged);
            this.ctlFilter.ShowDependentViewsChanged += new System.EventHandler(this.ctlFilter_ShowDependentViewsChanged);
            this.ctlFilter.LevelsChanged += new System.EventHandler(this.ctlFilter_LevelsChanged);
            // 
            // ctlSearch
            // 
            this.ctlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctlSearch.Location = new System.Drawing.Point(0, 35);
            this.ctlSearch.Margin = new System.Windows.Forms.Padding(5);
            this.ctlSearch.Name = "ctlSearch";
            this.ctlSearch.Search = "";
            this.ctlSearch.Size = new System.Drawing.Size(357, 32);
            this.ctlSearch.TabIndex = 84;
            this.ctlSearch.SearchChanged += new System.EventHandler(this.ctlSearch_SearchChanged);
            // 
            // ctlDisciplines
            // 
            this.ctlDisciplines.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctlDisciplines.Location = new System.Drawing.Point(0, 0);
            this.ctlDisciplines.Margin = new System.Windows.Forms.Padding(5);
            this.ctlDisciplines.Name = "ctlDisciplines";
            this.ctlDisciplines.Size = new System.Drawing.Size(357, 35);
            this.ctlDisciplines.TabIndex = 85;
            this.ctlDisciplines.DisciplineChanged += new System.EventHandler(this.ctlDisciplines_DisciplineChanged);
            // 
            // ctlPlansScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ctlViews);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.statusStrip1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ctlPlansScreen";
            this.Size = new System.Drawing.Size(357, 512);
            this.Load += new System.EventHandler(this.ctlPlans_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton tbBuild;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton dpmnuAdd;
        private System.Windows.Forms.ToolStripMenuItem tbAddPlanViews;
        private System.Windows.Forms.ToolStripMenuItem tbAddCustomPlanViews;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tbAddDependentPlans;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbEdit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbSettings;
        private ctlPlansFilter ctlFilter;
        private ctlPlansViews ctlViews;
        private System.Windows.Forms.Panel panel3;
        private ctlSearch ctlSearch;
        private ctlDisciplines ctlDisciplines;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelInfo;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mnuAddAll;
        private System.Windows.Forms.ToolStripMenuItem addAllDiscipline;
        private System.Windows.Forms.ToolStripDropDownButton tbDeleteDrop;
        private System.Windows.Forms.ToolStripMenuItem tbDeleteSelectItems;
        private System.Windows.Forms.ToolStripMenuItem mnuDeleteCheckedItems;
    }
}
