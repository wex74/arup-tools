﻿namespace ViewCreator.Forms.Controls
{
    partial class ctlBuildingsScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlBuildingsScreen));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStripViews = new System.Windows.Forms.ToolStrip();
            this.tbEdit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tbDependents = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbPlan = new System.Windows.Forms.ToolStripButton();
            this.tbThreed = new System.Windows.Forms.ToolStripButton();
            this.tbSection = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.tbFavourites = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbViews = new System.Windows.Forms.ToolStripDropDownButton();
            this.tbFlat = new System.Windows.Forms.ToolStripMenuItem();
            this.tbLevels = new System.Windows.Forms.ToolStripMenuItem();
            this.tbRLs = new System.Windows.Forms.ToolStripMenuItem();
            this.tbDiscipline = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ctlSearch = new ViewCreator.Forms.Controls.ctlSearch();
            this.ctlViews = new ViewCreator.Forms.Sections.Views.Controls.ctlBuildingsViews();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ctlFilter = new ViewCreator.Forms.Sections.Views.Controls.ctlBuildingsFilter();
            this.toolStripViews.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Electrical");
            this.imageList.Images.SetKeyName(1, "Fire");
            this.imageList.Images.SetKeyName(2, "Hydraulic");
            this.imageList.Images.SetKeyName(3, "Mechanical");
            this.imageList.Images.SetKeyName(4, "Structural");
            this.imageList.Images.SetKeyName(5, "Plan");
            this.imageList.Images.SetKeyName(6, "Architectural");
            this.imageList.Images.SetKeyName(7, "Coordination");
            this.imageList.Images.SetKeyName(8, "Scopebox");
            this.imageList.Images.SetKeyName(9, "Threed");
            this.imageList.Images.SetKeyName(10, "Dependent");
            this.imageList.Images.SetKeyName(11, "Level");
            this.imageList.Images.SetKeyName(12, "Section");
            this.imageList.Images.SetKeyName(13, "Sheet");
            this.imageList.Images.SetKeyName(14, "Schedule");
            // 
            // toolStripViews
            // 
            this.toolStripViews.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripViews.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStripViews.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbEdit,
            this.toolStripSeparator6,
            this.tbDependents,
            this.toolStripSeparator5,
            this.tbPlan,
            this.tbThreed,
            this.tbSection,
            this.toolStripSeparator8,
            this.tbFavourites,
            this.toolStripSeparator3,
            this.tbViews});
            this.toolStripViews.Location = new System.Drawing.Point(0, 0);
            this.toolStripViews.Name = "toolStripViews";
            this.toolStripViews.Size = new System.Drawing.Size(350, 27);
            this.toolStripViews.TabIndex = 95;
            this.toolStripViews.Text = "toolStrip1";
            // 
            // tbEdit
            // 
            this.tbEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbEdit.Image = global::ViewCreator.Properties.Resources.Manage;
            this.tbEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbEdit.Name = "tbEdit";
            this.tbEdit.Size = new System.Drawing.Size(24, 24);
            this.tbEdit.Text = "toolStripButton1";
            this.tbEdit.Click += new System.EventHandler(this.tbEdit_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 27);
            // 
            // tbDependents
            // 
            this.tbDependents.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbDependents.Image = ((System.Drawing.Image)(resources.GetObject("tbDependents.Image")));
            this.tbDependents.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbDependents.Name = "tbDependents";
            this.tbDependents.Size = new System.Drawing.Size(24, 24);
            this.tbDependents.Text = "toolStripButton2";
            this.tbDependents.Click += new System.EventHandler(this.tbDependents_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 27);
            // 
            // tbPlan
            // 
            this.tbPlan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbPlan.Image = global::ViewCreator.Properties.Resources.Plan;
            this.tbPlan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbPlan.Name = "tbPlan";
            this.tbPlan.Size = new System.Drawing.Size(24, 24);
            this.tbPlan.Text = "toolStripButton1";
            this.tbPlan.Click += new System.EventHandler(this.tbPlan_Click);
            // 
            // tbThreed
            // 
            this.tbThreed.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbThreed.Image = global::ViewCreator.Properties.Resources.threeD;
            this.tbThreed.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbThreed.Name = "tbThreed";
            this.tbThreed.Size = new System.Drawing.Size(24, 24);
            this.tbThreed.Text = "toolStripButton2";
            this.tbThreed.Click += new System.EventHandler(this.tbThreed_Click);
            // 
            // tbSection
            // 
            this.tbSection.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbSection.Image = global::ViewCreator.Properties.Resources.Sections;
            this.tbSection.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbSection.Name = "tbSection";
            this.tbSection.Size = new System.Drawing.Size(24, 24);
            this.tbSection.Text = "toolStripButton4";
            this.tbSection.Click += new System.EventHandler(this.tbSection_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 27);
            // 
            // tbFavourites
            // 
            this.tbFavourites.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbFavourites.Image = global::ViewCreator.Properties.Resources.Star;
            this.tbFavourites.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbFavourites.Name = "tbFavourites";
            this.tbFavourites.Size = new System.Drawing.Size(24, 24);
            this.tbFavourites.Text = "toolStripButton1";
            this.tbFavourites.Click += new System.EventHandler(this.tbFavourites_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 27);
            // 
            // tbViews
            // 
            this.tbViews.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbViews.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbFlat,
            this.tbLevels,
            this.tbRLs,
            this.tbDiscipline});
            this.tbViews.Image = global::ViewCreator.Properties.Resources.View;
            this.tbViews.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbViews.Name = "tbViews";
            this.tbViews.Size = new System.Drawing.Size(34, 24);
            this.tbViews.Text = "toolStripSplitButton1";
            this.tbViews.DropDownOpening += new System.EventHandler(this.tbViews_DropDownOpening);
            // 
            // tbFlat
            // 
            this.tbFlat.Image = global::ViewCreator.Properties.Resources.Flat;
            this.tbFlat.Name = "tbFlat";
            this.tbFlat.Size = new System.Drawing.Size(182, 26);
            this.tbFlat.Text = "Flat";
            this.tbFlat.Click += new System.EventHandler(this.tbFlat_Click_1);
            // 
            // tbLevels
            // 
            this.tbLevels.Image = global::ViewCreator.Properties.Resources.Level;
            this.tbLevels.Name = "tbLevels";
            this.tbLevels.Size = new System.Drawing.Size(182, 26);
            this.tbLevels.Text = "Levels";
            this.tbLevels.Click += new System.EventHandler(this.tbLevels_Click_1);
            // 
            // tbRLs
            // 
            this.tbRLs.Image = global::ViewCreator.Properties.Resources.RL;
            this.tbRLs.Name = "tbRLs";
            this.tbRLs.Size = new System.Drawing.Size(182, 26);
            this.tbRLs.Text = "Levels with RLs";
            this.tbRLs.Click += new System.EventHandler(this.tbRLs_Click);
            // 
            // tbDiscipline
            // 
            this.tbDiscipline.Image = global::ViewCreator.Properties.Resources.Buildings;
            this.tbDiscipline.Name = "tbDiscipline";
            this.tbDiscipline.Size = new System.Drawing.Size(182, 26);
            this.tbDiscipline.Text = "Discipline";
            this.tbDiscipline.Click += new System.EventHandler(this.tbDiscipline_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ctlViews);
            this.panel3.Controls.Add(this.statusStrip1);
            this.panel3.Controls.Add(this.ctlSearch);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 119);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(350, 539);
            this.panel3.TabIndex = 109;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Location = new System.Drawing.Point(0, 517);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(350, 22);
            this.statusStrip1.TabIndex = 107;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // ctlSearch
            // 
            this.ctlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctlSearch.Location = new System.Drawing.Point(0, 0);
            this.ctlSearch.Margin = new System.Windows.Forms.Padding(5);
            this.ctlSearch.Name = "ctlSearch";
            this.ctlSearch.Search = "";
            this.ctlSearch.Size = new System.Drawing.Size(350, 30);
            this.ctlSearch.TabIndex = 106;
            this.ctlSearch.SearchChanged += new System.EventHandler(this.ctlSearch_SearchChanged_1);
            // 
            // ctlViews
            // 
            this.ctlViews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlViews.Location = new System.Drawing.Point(0, 30);
            this.ctlViews.Margin = new System.Windows.Forms.Padding(5);
            this.ctlViews.Name = "ctlViews";
            this.ctlViews.Search = null;
            this.ctlViews.Size = new System.Drawing.Size(350, 487);
            this.ctlViews.TabIndex = 1;
            this.ctlViews.DoubleClicked += new System.EventHandler(this.ctlViews_DoubleClicked);
            this.ctlViews.Load += new System.EventHandler(this.ctlViews_Load);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightSlateGray;
            this.panel1.Controls.Add(this.ctlFilter);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(7, 0, 7, 6);
            this.panel1.Size = new System.Drawing.Size(350, 92);
            this.panel1.TabIndex = 110;
            // 
            // ctlFilter
            // 
            this.ctlFilter.BackColor = System.Drawing.Color.LightSlateGray;
            this.ctlFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlFilter.Location = new System.Drawing.Point(7, 0);
            this.ctlFilter.Margin = new System.Windows.Forms.Padding(5);
            this.ctlFilter.Name = "ctlFilter";
            this.ctlFilter.Size = new System.Drawing.Size(336, 86);
            this.ctlFilter.TabIndex = 0;
            this.ctlFilter.DisciplinesChanged += new System.EventHandler(this.ctlViewsFilter_DisciplinesChanged);
            this.ctlFilter.LevelsChanged += new System.EventHandler(this.ctlViewsFilter_LevelsChanged);
            // 
            // ctlBuildingsScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStripViews);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ctlBuildingsScreen";
            this.Size = new System.Drawing.Size(350, 662);
            this.Load += new System.EventHandler(this.ctlBuildingsScreen_Load);
            this.toolStripViews.ResumeLayout(false);
            this.toolStripViews.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private Sections.Views.Controls.ctlBuildingsViews ctlViews;
        private System.Windows.Forms.ToolStrip toolStripViews;
        private System.Windows.Forms.ToolStripButton tbEdit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tbPlan;
        private System.Windows.Forms.ToolStripButton tbThreed;
        private System.Windows.Forms.ToolStripButton tbSection;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private ctlSearch ctlSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbFavourites;
        private System.Windows.Forms.ToolStripButton tbDependents;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private Sections.Views.Controls.ctlBuildingsFilter ctlFilter;
        private System.Windows.Forms.ToolStripDropDownButton tbViews;
        private System.Windows.Forms.ToolStripMenuItem tbFlat;
        private System.Windows.Forms.ToolStripMenuItem tbLevels;
        private System.Windows.Forms.ToolStripMenuItem tbRLs;
        private System.Windows.Forms.ToolStripMenuItem tbDiscipline;
        private System.Windows.Forms.StatusStrip statusStrip1;
    }
}
