﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Data.App;
using ViewCreator.Data.App_Data;
using ViewCreator.Data.Forms;
using ViewCreator.Events;
using ViewCreator.Events.Views;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlPlansScreen : UserControl
    {
        /// <summary>
        /// Manage creation of plans
        /// Setup project structure and batch build plan views & dependents
        /// </summary>

        private Discipline _discipline;
        private View _view;
        private bool _loaded;
        private PlansScreenData _formData;
        private ServiceLevels _serviceLevels;
        private ProjectViewInfos _projectViewInfos;
        private ProjectPlans _projectPlans;

        public ctlPlansScreen()
        {
            InitializeComponent();
        }

        private void ctlPlans_Load(object sender, EventArgs e)
        {
           
        }

        public void LoadControl()
        {
            Debug_Utils.WriteLine("ctlPlansScreen", "LoadControl");

            // Set data variables 
            _formData = MainApp.AppData.FormSettings.PlansScreenData;
            _projectViewInfos = MainApp.AppData.CurrentProject.ProjectViewInfos;
            _projectPlans = MainApp.AppData.CurrentProject.ProjectPlans;

            // Load controls
            if (MainApp.AppData.IsUserStructures)
            {
                ctlDisciplines.LoadControl(Discipline.Structural);
            }
            else
            {
                ctlDisciplines.LoadControl(_formData.Discipline);
            }
          
            ctlFilter.LoadControl();
            ctlViews.LoadControl();
            ctlSearch.LoadControl(_formData.Search);
            _loaded = true;

           LoadViewControl();

        }

        private void ctlDisciplines_DisciplineChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            _discipline = (Discipline) sender;
            _formData.Discipline = _discipline;

            SaveFormData();

            LoadViewControl();
        }

        private void SaveFormData()
        {
            MainApp.AppData.SaveUserModelData();
        }

        private void LoadViewControl()
        {
            ctlViews.LoadViews();
        }

        private void ctlSearch_SearchChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            _formData.Search = ctlSearch.Search;
            SaveFormData();

            LoadViewControl();
        }

       

        private void ctlFilter_ActiveLevelsChanged(object sender, EventArgs e)
        {
            LoadViewControl();
        }

        private void ctlFilter_LevelsChanged(object sender, EventArgs e)
        {
            LoadViewControl();
        }

        private void ctlFilter_ShowDependentViewsChanged(object sender, EventArgs e)
        {
            ctlViews.ShowHideDependents();
        }


        private void tbBuild_Click(object sender, EventArgs e)
        {
            AddPlansEvent.Run();
        }


        private void SetUi()
        {
            tbBuild.Enabled = _projectViewInfos.Count > 0;

            var text = "Empty Plans (" + _projectViewInfos.TotalCount + ") | ";
            text += "Model Plans (" + _projectPlans.TotalCount + ") |";

            statusLabelInfo.Text = text;
        }

        private void tbAddPlanViews_Click(object sender, EventArgs e)
        {
            // Select and add Empty plans (ViewInfos) to project
            var frm = new frmAddPlans();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                foreach (var viewInfo in frm.ViewInfos)
                {
                    _projectViewInfos.Add(viewInfo);
                }

                LoadViewControl();
            }
        }

        private void tbAddCustomPlanViews_Click(object sender, EventArgs e)
        {
            // Todo - update code

            var frm = new frmAddCustomViews();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadViewControl();
            }
        }


        public List<ViewInfo> GetViewInfos()
        {
            return _projectViewInfos.GetViewInfos(_discipline); 
        }

        public List<Plan> GetPlans()
        {
            return _projectPlans.GetPlans(_discipline); ;
        }

        private void tbAddDependentPlans_Click(object sender, EventArgs e)
        {
            var frm = new frmAddDependentViews(_discipline, GetViewInfos(), GetPlans());

            if (frm.ShowDialog() == DialogResult.OK)
            {
                LoadViewControl();
            }
        }

        private void tbDeleteAllViews_Click(object sender, EventArgs e)
        {
            // Todo - update code
            //var plans = ctlViews.GetPlans();

            //if (plans.Any())
            //{
            //    ExternalEvents_View.DeletePlans_Views = plans.Select(x => x.GetView()).ToList();
            //    ExternalEvents_View.Run_DeletePlans();
            //}  
        }

        private void tbDeleteAllNewPlans_Click(object sender, EventArgs e)
        {
            // Todo - update code
            //LoadViewControl();
        }

        private void tbDeleteView_Click(object sender, EventArgs e)
        {
            // Todo - update code
            //var plan = ctlViews.GetPlan();

            //if (plan != null)
            //{
            //    ExternalEvents_View.DeletePlan_View = plan.GetView();
            //    ExternalEvents_View.Run_DeletePlan();
            //}
        }

        private void tbDeleteNewPlan_Click(object sender, EventArgs e)
        {
            // Todo - update code
            //var viewInfo = ctlViews.GetViewInfo();

            //if (viewInfo != null)
            //{
            //    var id = viewInfo.Id.ToString();

            //    var deleted = _serviceLevels.RemoveViewInfo(viewInfo);

            //    if (deleted == 1)
            //    {
            //        ctlViews.RemoveNode(id);
            //    }
            //}
        }

        public void SelectCurrentView()
        {
            ctlViews.SelectCurrentView();
        }

     

        private void tbSettings_Click(object sender, EventArgs e)
        {
            SettingsUI.ShowForm();
        }


        private void tbEdit_Click(object sender, EventArgs e)
        {
            var views = ctlViews.GetViews();

            if (views.Any())
            {
                EditViewsUI.ShowForm(views);
            }
        }




        private void ctlViews_ViewDoubleClicked(object sender, EventArgs e)
        {
            _view = (View)sender;

            if (_view.Id != RVT_Utils.ActiveView.Id)
            {
                NavigateEvent.Run(_view);
            }
            else
            {
                EditViewUI.ShowForm();
            }
           
        }

        private void ctlViews_ViewsLoaded(object sender, EventArgs e)
        {
            // Update UI on ViewsLoaded
            SetUi();
        }

        private void mnuAddAll_Click(object sender, EventArgs e)
        {
            foreach (var disc in Discipline_Utils.GetDisciplineNames())
            {
                var discipline = Discipline_Utils.GetDiscipline(disc);

                AddAllPlans(discipline);
            }

            LoadViewControl();
        }

        private void dpmnuAdd_DropDownOpening(object sender, EventArgs e)
        {
            addAllDiscipline.Text = "Add All " + _discipline + " Plans";
            addAllDiscipline.Image = imageList.Images[_discipline.ToString()];
        }

        private void addAllDiscipline_Click(object sender, EventArgs e)
        {
            var discipline = Discipline_Utils.GetDiscipline(_discipline.ToString());

            AddAllPlans(discipline);

            LoadViewControl();
        }

        private void AddAllPlans(Discipline discipline)
        {
            foreach (var level in _formData.Levels)
            {
                var settings = MainApp.AppData.CurrentSettingsData.GetSettings(discipline);

                foreach (var vi in settings.ViewInfos)
                {
                    var viewInfo = new ViewInfo
                    {
                        Discipline = vi.Discipline,
                        ViewType = vi.ViewType,
                        Name = vi.Name,
                        Level = Level_Utils.GetLevel(level),
                        Prefix = vi.Prefix,
                        Template = vi.Template,
                    };



                    var scopeboxes = Scopebox_Utils.GetScopeBoxNames();
                    if (scopeboxes.Any())
                    {
                        viewInfo.DependentViews.AddRange(scopeboxes);
                    }

                    _projectViewInfos.Add(viewInfo);
                }
            }
        }

        private void ctlViews_ViewsUpdated(object sender, EventArgs e)
        {
            SetUi();
        }

        private void tbDelete_Click(object sender, EventArgs e)
        {

        }

        private void tbDeleteSelectItems_Click(object sender, EventArgs e)
        {
            tbDeleteSelectItems.Checked = !tbDeleteSelectItems.Checked;
            ctlViews.ShowCheckboxes(tbDeleteSelectItems.Checked);
        }

        private void mnuDeleteCheckedItems_Click(object sender, EventArgs e)
        {
            var plans = ctlViews.GetCheckedPlans();
            var viewInfos = ctlViews.GetCheckedViewInfos();
            var dependentViewInfos = ctlViews.GetCheckedDependentViewInfos();
            var dependentPlans = ctlViews.GetCheckedDependentPlans();


            if (viewInfos.Any())
            {
                _projectViewInfos.Remove(viewInfos);
            }

            if (dependentViewInfos.Any())
            {
                _projectViewInfos.Remove(dependentViewInfos);
            }

            if (dependentPlans.Any())
            {
                var parentPlans = new List<Plan>();

                foreach (var dependent in dependentPlans)
                {
                    if (!parentPlans.Exists(x => x.ViewId == dependent.ParentViewId))
                    {
                        parentPlans.Add(_projectPlans.GetPlan(dependent.ParentViewId));
                    }
                }

                if (parentPlans.Any())
                {
                    DeleteDependentPlansEvent.Run(parentPlans, dependentPlans);
                }
                
            }

            if (plans.Any())
            {
                DeletePlansEvent.Run(plans);
            }
        }

        private void tbDeleteDrop_DropDownOpening(object sender, EventArgs e)
        {
            mnuDeleteCheckedItems.Visible = tbDeleteSelectItems.Checked;
        }
    }
}