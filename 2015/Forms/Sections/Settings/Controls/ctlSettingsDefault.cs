﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Data;
using ViewCreator.Data.Forms;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlSettingsDefault : UserControl
    {
        private ModelSetting _modelSetting;
        private Discipline _discipline;
        private SettingsData _formData;


        public ctlSettingsDefault()
        {
            InitializeComponent();
        }

        private void ctlSettingsDefault_Load(object sender, EventArgs e)
        {
            
        }


        public void LoadForm()
        {
            _formData = MainApp.AppData.FormSettings.SettingsData;
            _discipline = _formData.Discipline;

            _modelSetting = MainApp.AppData.GetSettings(_discipline);

            if (_modelSetting == null) return;

            cboTemplate.LoadCombo(Template_Utils.GetViewTemplateNames(),_modelSetting.DefaultTemplate,"None");
            cboPrefix.LoadCombo(_modelSetting.PrefixNames, _modelSetting.DefaultPrefix, "");

        }


        private void cboPrefix_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            cboTemplate.Text = UI_Utils.SelectViewTemplate(cboTemplate.Text);
        }
    }
}
