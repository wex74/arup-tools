﻿namespace ViewCreator.Forms.Controls
{
    partial class ctlViewSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddPlan = new System.Windows.Forms.Button();
            this.btnEditPlan = new System.Windows.Forms.Button();
            this.btnRemovePlan = new System.Windows.Forms.Button();
            this.lvViews = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAddPlan
            // 
            this.btnAddPlan.Location = new System.Drawing.Point(679, 0);
            this.btnAddPlan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddPlan.Name = "btnAddPlan";
            this.btnAddPlan.Size = new System.Drawing.Size(80, 28);
            this.btnAddPlan.TabIndex = 64;
            this.btnAddPlan.Text = "Add";
            this.btnAddPlan.UseVisualStyleBackColor = true;
            this.btnAddPlan.Click += new System.EventHandler(this.btnAddPlan_Click);
            // 
            // btnEditPlan
            // 
            this.btnEditPlan.Location = new System.Drawing.Point(679, 36);
            this.btnEditPlan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditPlan.Name = "btnEditPlan";
            this.btnEditPlan.Size = new System.Drawing.Size(80, 28);
            this.btnEditPlan.TabIndex = 63;
            this.btnEditPlan.Text = "Edit";
            this.btnEditPlan.UseVisualStyleBackColor = true;
            this.btnEditPlan.Click += new System.EventHandler(this.btnEditPlan_Click);
            // 
            // btnRemovePlan
            // 
            this.btnRemovePlan.Location = new System.Drawing.Point(679, 71);
            this.btnRemovePlan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRemovePlan.Name = "btnRemovePlan";
            this.btnRemovePlan.Size = new System.Drawing.Size(80, 28);
            this.btnRemovePlan.TabIndex = 62;
            this.btnRemovePlan.Text = "Remove";
            this.btnRemovePlan.UseVisualStyleBackColor = true;
            this.btnRemovePlan.Click += new System.EventHandler(this.btnRemovePlan_Click);
            // 
            // lvViews
            // 
            this.lvViews.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader2,
            this.columnHeader4});
            this.lvViews.FullRowSelect = true;
            this.lvViews.HideSelection = false;
            this.lvViews.Location = new System.Drawing.Point(0, 0);
            this.lvViews.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lvViews.Name = "lvViews";
            this.lvViews.Size = new System.Drawing.Size(669, 335);
            this.lvViews.TabIndex = 60;
            this.lvViews.UseCompatibleStateImageBehavior = false;
            this.lvViews.View = System.Windows.Forms.View.Details;
            this.lvViews.SelectedIndexChanged += new System.EventHandler(this.lvViews_SelectedIndexChanged);
            this.lvViews.DoubleClick += new System.EventHandler(this.lvViews_DoubleClick);
            this.lvViews.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvViews_KeyDown);
            // 
            // columnHeader1
            // 
            this.columnHeader1.DisplayIndex = 1;
            this.columnHeader1.Text = "Name";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 0;
            this.columnHeader3.Text = "Prefix";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Template";
            this.columnHeader2.Width = 170;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "View Type";
            this.columnHeader4.Width = 120;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(679, 119);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(80, 28);
            this.btnClear.TabIndex = 65;
            this.btnClear.Text = "Clear All";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // ctlViewSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAddPlan);
            this.Controls.Add(this.btnEditPlan);
            this.Controls.Add(this.lvViews);
            this.Controls.Add(this.btnRemovePlan);
            this.Location = new System.Drawing.Point(5, 5);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ctlViewSettings";
            this.Size = new System.Drawing.Size(768, 335);
            this.Load += new System.EventHandler(this.ctlViewSettings_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAddPlan;
        private System.Windows.Forms.Button btnEditPlan;
        private System.Windows.Forms.Button btnRemovePlan;
        private System.Windows.Forms.ListView lvViews;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnClear;
    }
}
