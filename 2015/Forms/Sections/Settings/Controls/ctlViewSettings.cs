﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Forms.Controls
{
    /// <summary>
    ///     ctlViewSettings
    ///     Manage ModelSettings
    /// </summary>
    public partial class ctlViewSettings : UserControl
    {
        private Discipline _discipline;
        private SettingsData _formData;
        private bool _loaded;
        private ModelSetting _modelSetting;
        private ViewInfo _viewInfo;
        private List<ViewInfo> _viewInfos;

        public ctlViewSettings()
        {
            InitializeComponent();
        }

        private void ctlViewSettings_Load(object sender, EventArgs e)
        {
        }

        public void ClearForm()
        {
            lvViews.Items.Clear();
        }

        public void LoadForm()
        {
            _formData = MainApp.AppData.FormSettings.SettingsData;
            _discipline = _formData.Discipline;
            _loaded = false;

            _modelSetting = MainApp.AppData.GetSettings(_discipline);
            if (_modelSetting == null) return;
            BuildViews();

            _loaded = true;
        }

        private void BuildViews()
        {
            _loaded = false;

            lvViews.Items.Clear();
            _viewInfos = _modelSetting.GetViewInfos();

            foreach (var viewInfo in _viewInfos)
            {
                var item = lvViews.Items.Add(viewInfo.Name);
                item.SubItems.Add(viewInfo.Prefix);
                item.SubItems.Add(viewInfo.Template);
                item.SubItems.Add(viewInfo.ViewType.ToString());

                item.Tag = viewInfo;

                if (_viewInfo != null)
                {
                    if (_viewInfo.Name == viewInfo.Name)
                    {
                        item.Selected = true;
                    }
                }
            }

            if (lvViews.SelectedItems.Count == 0)
            {
                if (lvViews.Items.Count > 0)
                {
                    lvViews.Items[0].Selected = true;
                }
            }

            _loaded = true;

            lvViews_SelectedIndexChanged(null, null);


            EnableControls();
        }

        private void lvViews_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (lvViews.SelectedItems.Count == 0) return;

            _viewInfo = (ViewInfo) lvViews.SelectedItems[0].Tag;
        }

        private void btnAddPlan_Click(object sender, EventArgs e)
        {
            var frm = new frmAddEditPlan();
            frm.Discipline = _discipline;
            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                _modelSetting.ViewInfos.Add(frm.ViewInfo);
                MainApp.AppData.SaveSettingsData();

                _viewInfo = frm.ViewInfo;

                BuildViews();

               
            }
        }

        private void btnEditPlan_Click(object sender, EventArgs e)
        {
            EditPlan();
        }

        private void EditPlan()
        {
            var frm = new frmAddEditPlan();
            frm.Discipline = _discipline;
            frm.ViewInfo = _viewInfo;
            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                var viewInfo = frm.ViewInfo;

                _viewInfo.Prefix = viewInfo.Prefix;
                _viewInfo.Name = viewInfo.Name;
                _viewInfo.Template = viewInfo.Template;
                _viewInfo.ViewType = viewInfo.ViewType;

                MainApp.AppData.SaveSettingsData();
                BuildViews();

                
            }
        }

        private void btnRemovePlan_Click(object sender, EventArgs e)
        {
            RemovePlan();
        }

        private void RemovePlan()
        {
            var text = _discipline + " Plan - " + _viewInfo.Name;


            if (UI_Utils.GetMessageBoxDeleteResult(text, "Plan") == DialogResult.Yes)
            {
                var index = _viewInfos.IndexOf(_viewInfo);

                _modelSetting.ViewInfos.RemoveAll(x => x.Id == _viewInfo.Id);
                MainApp.AppData.SaveSettingsData();

                if (index > 0)
                {
                    _viewInfo = _viewInfos[index - 1];
                }
                else
                {
                    _viewInfo = null;
                }

                BuildViews();

            }
        }

        private void EnableControls()
        {
            if (_viewInfos.Count == 0)
            {
                btnEditPlan.Enabled = false;
                btnRemovePlan.Enabled = false;
            }

            btnEditPlan.Enabled = _viewInfo != null;
            btnRemovePlan.Enabled = _viewInfo != null;
        }

        private void lvViews_DoubleClick(object sender, EventArgs e)
        {
            EditPlan();
        }

        private void lvViews_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (_viewInfo != null) return;
                RemovePlan();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            var text = "All " + _discipline + " Plans (" + _modelSetting.ViewInfos.Count + ")";

            if (UI_Utils.GetMessageBoxDeleteResult(text, "Plan") == DialogResult.Yes)
            {
                _modelSetting.ViewInfos.Clear();
                MainApp.AppData.SaveSettingsData();

                BuildViews();

            }
        }
    }
}