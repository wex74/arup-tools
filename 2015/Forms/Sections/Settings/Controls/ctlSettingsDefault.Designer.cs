﻿namespace ViewCreator.Forms.Controls
{
    partial class ctlSettingsDefault
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboPrefix = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectTemplate = new System.Windows.Forms.Button();
            this.cboTemplate = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPlanTypes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboPrefix
            // 
            this.cboPrefix.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPrefix.FormattingEnabled = true;
            this.cboPrefix.Location = new System.Drawing.Point(257, 26);
            this.cboPrefix.Name = "cboPrefix";
            this.cboPrefix.Size = new System.Drawing.Size(181, 21);
            this.cboPrefix.TabIndex = 80;
            this.cboPrefix.SelectedIndexChanged += new System.EventHandler(this.cboPrefix_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(255, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 79;
            this.label1.Text = "Default Prefix:";
            // 
            // btnSelectTemplate
            // 
            this.btnSelectTemplate.Location = new System.Drawing.Point(201, 24);
            this.btnSelectTemplate.Name = "btnSelectTemplate";
            this.btnSelectTemplate.Size = new System.Drawing.Size(23, 23);
            this.btnSelectTemplate.TabIndex = 78;
            this.btnSelectTemplate.Text = "...";
            this.btnSelectTemplate.UseVisualStyleBackColor = true;
            this.btnSelectTemplate.Click += new System.EventHandler(this.btnSelectTemplate_Click);
            // 
            // cboTemplate
            // 
            this.cboTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTemplate.FormattingEnabled = true;
            this.cboTemplate.Location = new System.Drawing.Point(14, 24);
            this.cboTemplate.Name = "cboTemplate";
            this.cboTemplate.Size = new System.Drawing.Size(181, 21);
            this.cboTemplate.TabIndex = 77;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 76;
            this.label5.Text = "Default Template:";
            // 
            // btnPlanTypes
            // 
            this.btnPlanTypes.Location = new System.Drawing.Point(444, 24);
            this.btnPlanTypes.Name = "btnPlanTypes";
            this.btnPlanTypes.Size = new System.Drawing.Size(23, 23);
            this.btnPlanTypes.TabIndex = 75;
            this.btnPlanTypes.Text = "...";
            this.btnPlanTypes.UseVisualStyleBackColor = true;
            // 
            // ctlSettingsDefault
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cboTemplate);
            this.Controls.Add(this.cboPrefix);
            this.Controls.Add(this.btnPlanTypes);
            this.Controls.Add(this.btnSelectTemplate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Name = "ctlSettingsDefault";
            this.Size = new System.Drawing.Size(476, 53);
            this.Load += new System.EventHandler(this.ctlSettingsDefault_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboPrefix;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectTemplate;
        private System.Windows.Forms.ComboBox cboTemplate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPlanTypes;
    }
}
