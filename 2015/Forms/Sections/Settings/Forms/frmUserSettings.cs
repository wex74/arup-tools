﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Forms.Main_Dialogs
{
    public partial class frmUserSettings : Form
    {
        private UserSettings _settings;
        private bool _loaded;

        public frmUserSettings()
        {
            InitializeComponent();
        }

        private void frmUserOptions_Load(object sender, EventArgs e)
        {
            _settings = MainApp.AppData.UserData.UserSettings;

            optBuildings.Checked = MainApp.AppData.IsUserBuildings;
            optStructures.Checked = MainApp.AppData.IsUserStructures;
            optRevit.Checked = MainApp.AppData.IsUserRevit;

            optMechanical.Checked = MainApp.AppData.UserDiscipline == Discipline.Mechanical;
            optElectrical.Checked = MainApp.AppData.UserDiscipline == Discipline.Electrical;
            optHydraulic.Checked = MainApp.AppData.UserDiscipline == Discipline.Hydraulic;
            optFire.Checked = MainApp.AppData.UserDiscipline == Discipline.Fire;
            optCoordination.Checked = MainApp.AppData.UserDiscipline == Discipline.Coordination;

            var count = 0;
            foreach (var val in _settings.GetPrefixes())
            {
                var value = val;
                if (count > 0) value = "\r\n" + val;
                txtPrefixNames.AppendText(value);
                count ++;
            }

            count = 0;
            foreach (var val in _settings.GetPlanNames())
            {
                var value = val;
                if (count > 0) value = "\r\n" + val;
                txtPlanNames.AppendText(value);
                count++;
            }

            count = 0;
            foreach (var val in _settings.GetViewTemplates())
            {
                var value = val;
                if (count > 0) value = "\r\n" + val;
                txtTemplateNames.AppendText(value);
                count++;
            }


            _loaded = true;
        }

    

        private void cboUserSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!_loaded) return;


        }

        private void button7_Click(object sender, EventArgs e)
        {
          Close();
        }

        private void frmUserSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            var prefixes = new List<string>();
            foreach (var txt in txtPrefixNames.Lines)
            {
                prefixes.Add(txt);
            }
            _settings.Prefixes = prefixes;

            var plans = new List<string>();
            foreach (var txt in txtPlanNames.Lines)
            {
                plans.Add(txt);
            }
            _settings.PlanNames = plans;


            var templates = new List<string>();
            foreach (var txt in txtTemplateNames.Lines)
            {
                templates.Add(txt);
            }
            _settings.ViewTemplates = templates;



            MainApp.AppData.SaveUserData();
        }

        private void optBuildings_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optBuildings.Checked)
            {
                _settings.SectionDiscipline = SectionDiscipline.Buildings;
            }

        }

        private void optStructures_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optStructures.Checked)
            {
                _settings.SectionDiscipline = SectionDiscipline.Structures;
                
            }
        }

        private void optRevit_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optRevit.Checked)
            {
                _settings.SectionDiscipline = SectionDiscipline.Revit;

            }
        }

        private void optMechanical_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optMechanical.Checked)
            {
                _settings.Discipline = Discipline.Mechanical;

            }

            
        }

        private void optElectrical_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optElectrical.Checked)
            {
                _settings.Discipline = Discipline.Electrical;

            }
        }

        private void optHydraulic_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optHydraulic.Checked)
            {
                _settings.Discipline = Discipline.Hydraulic;

            }
        }

        private void optFire_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optFire.Checked)
            {
                _settings.Discipline = Discipline.Fire;

            }
        }

        private void optCoordination_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (optCoordination.Checked)
            {
                _settings.Discipline = Discipline.Coordination;

            }
        }
    }
}
