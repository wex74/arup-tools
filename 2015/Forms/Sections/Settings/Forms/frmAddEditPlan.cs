﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Data;
using ViewCreator.Model;
using ViewCreator.Utils;
using Autodesk.Revit.DB;
using ViewCreator.Model.Settings;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using ViewCreator.Utils.General;
using Form = System.Windows.Forms.Form;

namespace ViewCreator.Forms
{
    public partial class frmAddEditPlan : Form
    {
        public Discipline Discipline { get; set; }
        private ModelSetting _modelSetting;

        private bool _loaded;
        private bool _add;

        private List<string> _settingNames;
        public ViewInfo ViewInfo { get; set; }

        public frmAddEditPlan()
        {
            InitializeComponent();
        }

        private void frmAddEditPlan_Load(object sender, EventArgs e)
        {
            _modelSetting = MainApp.AppData.GetSettings(Discipline);
            _settingNames = _modelSetting.GetViewInfoSettingNames();

            // get default viewtype on user role
            var viewType = ViewType.FloorPlan.ToString();
            if (MainApp.AppData.IsUserStructures) viewType =  ViewType.EngineeringPlan.ToString();

            // load lists
            cboTemplate.LoadCombo(Template_Utils.GetViewTemplateNames(), _modelSetting.DefaultTemplate, "None");
            cboPrefix.LoadCombo(_modelSetting.PrefixNames, _modelSetting.DefaultPrefix, "");
            cboViewType.LoadCombo(View_Utils.GetPlanViewTypes(), viewType, null);

           
            if (ViewInfo != null)
            {
                _add = false;

                Text = "Edit " + Discipline + " Plan - " + ViewInfo.Name;
               
                txtPlanName.Text = ViewInfo.Name;
                cboTemplate.Text = ViewInfo.Template;
                cboPrefix.Text = ViewInfo.Prefix;
                cboViewType.Text = ViewInfo.ViewType.ToString();

            }
            else
            {
                _add = true;

                Text = "Add " + Discipline + " Plan";
                txtPlanName.Text = Discipline + " Plan";
            }
        }


     


        private void cboPlanTypes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

     
        private void btnOk_Click(object sender, EventArgs e)
        {


            var name = txtPlanName.Text;
            var prefix = cboPrefix.Text;
            var template = cboTemplate.Text;
            var viewType = cboViewType.Text;

            if (prefix == "" && name == "")
            {
                MessageBox.Show("Prefix & Name cannot be empty");
                return;
            }


            if (template == "")
            {
                MessageBox.Show("Template cannot be empty");
                return;
            }

            if (viewType == "")
            {
                MessageBox.Show("ViewType cannot be empty");
                return;
            }

            if (prefix != "" && !_modelSetting.PrefixNames.Exists(x => x == prefix))
            {
                _modelSetting.PrefixNames.Add(prefix);
                MainApp.AppData.SaveSettingsData();
            }


            var viewInfo = new ViewInfo();
            viewInfo.Name = name;
            viewInfo.Discipline = Discipline;
            viewInfo.Template = template;
            viewInfo.Prefix = prefix;
            viewInfo.ViewType = View_Utils.GetViewType(viewType);


            if (_add)
            {
                if (_settingNames.Exists(x => x == viewInfo.SettingName))
                {
                    MessageBox.Show(viewInfo.SettingName + " Exists");
                    return;
                }

            }
            else
            {
                if (_settingNames.Exists(x => x == viewInfo.SettingName)
                    && viewInfo.SettingName != ViewInfo.SettingName)
                {
                    MessageBox.Show(viewInfo.SettingName + " Exists");
                    return;
                }
            }

            ViewInfo = viewInfo;
            DialogResult = DialogResult.OK;


        }

        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            cboTemplate.Text = UI_Utils.SelectViewTemplate(cboTemplate.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
