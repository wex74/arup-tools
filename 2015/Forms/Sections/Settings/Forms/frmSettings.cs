﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Model.Settings;
using ViewCreator.UI;
using ViewCreator.Utils.App;

namespace ViewCreator.Forms
{
    /// <summary>
    ///     frmSettings
    ///     Manage ModelSettings
    /// </summary>
    public partial class frmSettings : Form
    {
        private Discipline _discipline;
        private SettingsData _formData;
        private ProjectData _project;

        public frmSettings()
        {
            InitializeComponent();
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        public void LoadForm()
        {
            Text = "View Creator Settings";

            // get current project
            _project = MainApp.AppData.CurrentProject;

            // check modelsettings data file exists
            var datafileExists = _project.DataFileExists;

            // set path on label
            lblFile.Text = _project.DataFile;

            // enable controls
            ctlSettingsDefault.Enabled = datafileExists;
            ctlViewPlans.Enabled = datafileExists;
            tvServices.Enabled = datafileExists;

            if (!_project.DataFileExists)
            {
                // clear and set form as no data has been created
                lblFile.BackColor = Color.LightPink;

                ctlViewPlans.ClearForm();
                tvServices.Nodes.Clear();

                btnData.Text = "Create";
            }
            else
            {
                // set form 
                _formData = MainApp.AppData.FormSettings.SettingsData;

                lblFile.BackColor = Color.LightYellow;
                lblFile.ForeColor = Color.Black;

                btnData.Text = "Delete";

                tvServices.Nodes.Clear();

                // build disciplines based on user role
                foreach (var service in Discipline_Utils.GetDisciplineNames())
                {
                    var node = tvServices.Nodes.Add(service);
                    node.ImageKey = service;
                    node.SelectedImageKey = service;

                    // select current
                    if (service == _formData.Discipline.ToString())
                    {
                        tvServices.SelectedNode = node;
                    }
                }

                // select 1st node if none selected
                if (tvServices.SelectedNode == null)
                {
                    if (tvServices.Nodes.Count > 0)
                    {
                        tvServices.SelectedNode = tvServices.Nodes[0];
                    }
                }


                LoadControls();
            }
        }

        private void LoadControls()
        {
            ctlSettingsDefault.LoadForm();
            ctlViewPlans.LoadForm();
        }


        private void button7_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void tvServices_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // load discipline
            if (tvServices.SelectedNode == null) return;
            var discipline = Discipline_Utils.GetDiscipline(tvServices.SelectedNode.Text);

            if (_discipline != discipline)
            {
                _discipline = discipline;
                SaveData();
                LoadControls();
            }
        }

        private void SaveData()
        {
            _formData.Discipline = _discipline;
            MainApp.AppData.SaveSettingsData();
        }


        private void frmSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            SettingsUI.FormVisible = false;
        }

      

        private void ctlSettingsDefault_Load(object sender, EventArgs e)
        {
        }

        private void btnData_Click(object sender, EventArgs e)
        {
            // if data file does not exist
            if (btnData.Text == "Create")
            {
                // create and load project data with default settings
                _project.LoadData(true);
                LoadForm();
            }
            else
            {
                // delete project settings
                if (btnData.Text == "Delete")
                {
                    _project.DeleteData();
                    LoadForm();
                }
            }

            
          
        }

       
    }
}