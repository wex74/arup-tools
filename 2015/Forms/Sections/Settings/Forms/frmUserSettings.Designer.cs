﻿namespace ViewCreator.Forms.Main_Dialogs
{
    partial class frmUserSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optRevit = new System.Windows.Forms.RadioButton();
            this.optStructures = new System.Windows.Forms.RadioButton();
            this.optBuildings = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.optCoordination = new System.Windows.Forms.RadioButton();
            this.optFire = new System.Windows.Forms.RadioButton();
            this.optHydraulic = new System.Windows.Forms.RadioButton();
            this.optElectrical = new System.Windows.Forms.RadioButton();
            this.optMechanical = new System.Windows.Forms.RadioButton();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtTemplateNames = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtPlanNames = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtPrefixNames = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(497, 15);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 56;
            this.button7.Text = "Close";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.optRevit);
            this.groupBox1.Controls.Add(this.optStructures);
            this.groupBox1.Controls.Add(this.optBuildings);
            this.groupBox1.Location = new System.Drawing.Point(20, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(205, 217);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Section Discipline";
            // 
            // optRevit
            // 
            this.optRevit.Image = global::ViewCreator.Properties.Resources.RVT;
            this.optRevit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optRevit.Location = new System.Drawing.Point(22, 87);
            this.optRevit.Name = "optRevit";
            this.optRevit.Size = new System.Drawing.Size(95, 24);
            this.optRevit.TabIndex = 60;
            this.optRevit.TabStop = true;
            this.optRevit.Text = "Revit User";
            this.optRevit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optRevit.UseVisualStyleBackColor = true;
            this.optRevit.CheckedChanged += new System.EventHandler(this.optRevit_CheckedChanged);
            // 
            // optStructures
            // 
            this.optStructures.Image = global::ViewCreator.Properties.Resources.Structures;
            this.optStructures.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optStructures.Location = new System.Drawing.Point(22, 57);
            this.optStructures.Name = "optStructures";
            this.optStructures.Size = new System.Drawing.Size(149, 24);
            this.optStructures.TabIndex = 59;
            this.optStructures.TabStop = true;
            this.optStructures.Text = "Structural Engineering";
            this.optStructures.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optStructures.UseVisualStyleBackColor = true;
            this.optStructures.CheckedChanged += new System.EventHandler(this.optStructures_CheckedChanged);
            // 
            // optBuildings
            // 
            this.optBuildings.Image = global::ViewCreator.Properties.Resources.Buildings;
            this.optBuildings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optBuildings.Location = new System.Drawing.Point(22, 27);
            this.optBuildings.Name = "optBuildings";
            this.optBuildings.Size = new System.Drawing.Size(132, 24);
            this.optBuildings.TabIndex = 58;
            this.optBuildings.TabStop = true;
            this.optBuildings.Text = "Buildings Services";
            this.optBuildings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optBuildings.UseVisualStyleBackColor = true;
            this.optBuildings.CheckedChanged += new System.EventHandler(this.optBuildings_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 316);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 50);
            this.panel1.TabIndex = 113;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.optCoordination);
            this.groupBox2.Controls.Add(this.optFire);
            this.groupBox2.Controls.Add(this.optHydraulic);
            this.groupBox2.Controls.Add(this.optElectrical);
            this.groupBox2.Controls.Add(this.optMechanical);
            this.groupBox2.Location = new System.Drawing.Point(244, 31);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(173, 217);
            this.groupBox2.TabIndex = 114;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Building Discipline";
            // 
            // optCoordination
            // 
            this.optCoordination.Image = global::ViewCreator.Properties.Resources.Coordination;
            this.optCoordination.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optCoordination.Location = new System.Drawing.Point(27, 147);
            this.optCoordination.Name = "optCoordination";
            this.optCoordination.Size = new System.Drawing.Size(112, 24);
            this.optCoordination.TabIndex = 62;
            this.optCoordination.TabStop = true;
            this.optCoordination.Text = "Coordination";
            this.optCoordination.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optCoordination.UseVisualStyleBackColor = true;
            this.optCoordination.CheckedChanged += new System.EventHandler(this.optCoordination_CheckedChanged);
            // 
            // optFire
            // 
            this.optFire.Image = global::ViewCreator.Properties.Resources.Fire;
            this.optFire.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optFire.Location = new System.Drawing.Point(27, 117);
            this.optFire.Name = "optFire";
            this.optFire.Size = new System.Drawing.Size(68, 24);
            this.optFire.TabIndex = 61;
            this.optFire.TabStop = true;
            this.optFire.Text = "Fire";
            this.optFire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optFire.UseVisualStyleBackColor = true;
            this.optFire.CheckedChanged += new System.EventHandler(this.optFire_CheckedChanged);
            // 
            // optHydraulic
            // 
            this.optHydraulic.Image = global::ViewCreator.Properties.Resources.Hydraulic;
            this.optHydraulic.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optHydraulic.Location = new System.Drawing.Point(27, 87);
            this.optHydraulic.Name = "optHydraulic";
            this.optHydraulic.Size = new System.Drawing.Size(95, 24);
            this.optHydraulic.TabIndex = 60;
            this.optHydraulic.TabStop = true;
            this.optHydraulic.Text = "Hydraulic";
            this.optHydraulic.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optHydraulic.UseVisualStyleBackColor = true;
            this.optHydraulic.CheckedChanged += new System.EventHandler(this.optHydraulic_CheckedChanged);
            // 
            // optElectrical
            // 
            this.optElectrical.Image = global::ViewCreator.Properties.Resources.Electrical;
            this.optElectrical.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optElectrical.Location = new System.Drawing.Point(27, 57);
            this.optElectrical.Name = "optElectrical";
            this.optElectrical.Size = new System.Drawing.Size(95, 24);
            this.optElectrical.TabIndex = 59;
            this.optElectrical.TabStop = true;
            this.optElectrical.Text = "Electrical";
            this.optElectrical.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optElectrical.UseVisualStyleBackColor = true;
            this.optElectrical.CheckedChanged += new System.EventHandler(this.optElectrical_CheckedChanged);
            // 
            // optMechanical
            // 
            this.optMechanical.Image = global::ViewCreator.Properties.Resources.Mechanical;
            this.optMechanical.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optMechanical.Location = new System.Drawing.Point(27, 27);
            this.optMechanical.Name = "optMechanical";
            this.optMechanical.Size = new System.Drawing.Size(105, 24);
            this.optMechanical.TabIndex = 58;
            this.optMechanical.TabStop = true;
            this.optMechanical.Text = "Mechanical";
            this.optMechanical.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optMechanical.UseVisualStyleBackColor = true;
            this.optMechanical.CheckedChanged += new System.EventHandler(this.optMechanical_CheckedChanged);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.ItemSize = new System.Drawing.Size(100, 19);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(584, 316);
            this.tabControl.TabIndex = 115;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(599, 306);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Discipline";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(576, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Lists";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtTemplateNames);
            this.groupBox5.Location = new System.Drawing.Point(332, 11);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(236, 262);
            this.groupBox5.TabIndex = 119;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "View Templates";
            // 
            // txtTemplateNames
            // 
            this.txtTemplateNames.Location = new System.Drawing.Point(17, 30);
            this.txtTemplateNames.Multiline = true;
            this.txtTemplateNames.Name = "txtTemplateNames";
            this.txtTemplateNames.Size = new System.Drawing.Size(200, 208);
            this.txtTemplateNames.TabIndex = 116;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtPlanNames);
            this.groupBox4.Location = new System.Drawing.Point(161, 11);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(165, 262);
            this.groupBox4.TabIndex = 118;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Plan Names";
            // 
            // txtPlanNames
            // 
            this.txtPlanNames.Location = new System.Drawing.Point(17, 30);
            this.txtPlanNames.Multiline = true;
            this.txtPlanNames.Name = "txtPlanNames";
            this.txtPlanNames.Size = new System.Drawing.Size(130, 208);
            this.txtPlanNames.TabIndex = 116;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtPrefixNames);
            this.groupBox3.Location = new System.Drawing.Point(14, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(141, 262);
            this.groupBox3.TabIndex = 117;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Prefix Names";
            // 
            // txtPrefixNames
            // 
            this.txtPrefixNames.Location = new System.Drawing.Point(19, 30);
            this.txtPrefixNames.Multiline = true;
            this.txtPrefixNames.Name = "txtPrefixNames";
            this.txtPrefixNames.Size = new System.Drawing.Size(101, 208);
            this.txtPrefixNames.TabIndex = 116;
            // 
            // frmUserSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(584, 366);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmUserSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmUserSettings_FormClosing);
            this.Load += new System.EventHandler(this.frmUserOptions_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton optBuildings;
        private System.Windows.Forms.RadioButton optStructures;
        private System.Windows.Forms.RadioButton optRevit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton optCoordination;
        private System.Windows.Forms.RadioButton optFire;
        private System.Windows.Forms.RadioButton optHydraulic;
        private System.Windows.Forms.RadioButton optElectrical;
        private System.Windows.Forms.RadioButton optMechanical;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtPrefixNames;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtPlanNames;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtTemplateNames;
    }
}