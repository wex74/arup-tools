﻿namespace ViewCreator.Forms
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.tvServices = new System.Windows.Forms.TreeView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblFile = new System.Windows.Forms.Label();
            this.btnData = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ctlSettingsDefault = new ViewCreator.Forms.Controls.ctlSettingsDefault();
            this.ctlViewPlans = new ViewCreator.Forms.Controls.ctlViewSettings();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Architectural");
            this.imageList.Images.SetKeyName(1, "Coordination");
            this.imageList.Images.SetKeyName(2, "Electrical");
            this.imageList.Images.SetKeyName(3, "Fire");
            this.imageList.Images.SetKeyName(4, "Hydraulic");
            this.imageList.Images.SetKeyName(5, "Mechanical");
            this.imageList.Images.SetKeyName(6, "Structural");
            this.imageList.Images.SetKeyName(7, "threeD");
            this.imageList.Images.SetKeyName(8, "NewPlan");
            // 
            // tvServices
            // 
            this.tvServices.FullRowSelect = true;
            this.tvServices.HideSelection = false;
            this.tvServices.ImageIndex = 0;
            this.tvServices.ImageList = this.imageList;
            this.tvServices.Location = new System.Drawing.Point(15, 110);
            this.tvServices.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tvServices.Name = "tvServices";
            this.tvServices.SelectedImageIndex = 0;
            this.tvServices.ShowLines = false;
            this.tvServices.ShowPlusMinus = false;
            this.tvServices.ShowRootLines = false;
            this.tvServices.Size = new System.Drawing.Size(199, 335);
            this.tvServices.TabIndex = 62;
            this.tvServices.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvServices_AfterSelect);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblFile);
            this.groupBox2.Controls.Add(this.btnData);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(15, 17);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(979, 70);
            this.groupBox2.TabIndex = 81;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data File Path:";
            // 
            // lblFile
            // 
            this.lblFile.BackColor = System.Drawing.SystemColors.Info;
            this.lblFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFile.Location = new System.Drawing.Point(20, 28);
            this.lblFile.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(859, 25);
            this.lblFile.TabIndex = 83;
            this.lblFile.Text = "label1";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnData
            // 
            this.btnData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnData.Location = new System.Drawing.Point(887, 27);
            this.btnData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnData.Name = "btnData";
            this.btnData.Size = new System.Drawing.Size(80, 28);
            this.btnData.TabIndex = 76;
            this.btnData.Text = "Data...";
            this.btnData.UseVisualStyleBackColor = true;
            this.btnData.Click += new System.EventHandler(this.btnData_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(876, 18);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(100, 28);
            this.button7.TabIndex = 55;
            this.button7.Text = "Close";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 534);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 62);
            this.panel1.TabIndex = 112;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightSlateGray;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1008, 98);
            this.panel2.TabIndex = 113;
            // 
            // ctlSettingsDefault
            // 
            this.ctlSettingsDefault.Location = new System.Drawing.Point(20, 458);
            this.ctlSettingsDefault.Margin = new System.Windows.Forms.Padding(5);
            this.ctlSettingsDefault.Name = "ctlSettingsDefault";
            this.ctlSettingsDefault.Size = new System.Drawing.Size(637, 71);
            this.ctlSettingsDefault.TabIndex = 61;
            this.ctlSettingsDefault.Load += new System.EventHandler(this.ctlSettingsDefault_Load);
            // 
            // ctlViewPlans
            // 
            this.ctlViewPlans.Location = new System.Drawing.Point(221, 110);
            this.ctlViewPlans.Margin = new System.Windows.Forms.Padding(5);
            this.ctlViewPlans.Name = "ctlViewPlans";
            this.ctlViewPlans.Size = new System.Drawing.Size(767, 335);
            this.ctlViewPlans.TabIndex = 0;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1008, 596);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ctlSettingsDefault);
            this.Controls.Add(this.tvServices);
            this.Controls.Add(this.ctlViewPlans);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSettings_FormClosing);
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TreeView tvServices;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnData;
        private Controls.ctlViewSettings ctlViewPlans;
        private Controls.ctlSettingsDefault ctlSettingsDefault;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}