﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Model;

namespace ViewCreator.Forms.Controls.Views
{
    public partial class ctlViewDrawingsList : UserControl
    {
        private Drawing _drawing;
        private List<Drawing> _drawings;


        public ctlViewDrawingsList()
        {
            InitializeComponent();
        }


        public void LoadControl(List<Drawing> drawings)
        {
            _drawings = drawings;
            BuildDrawings();
        }

        private void BuildDrawings()
        {

            foreach (var drawing in _drawings)
            {
                var item = lvDrawings.Items.Add(drawing.DrawingNo);
                item.SubItems.Add(drawing.Title1);
                item.SubItems.Add(drawing.Title2);
                item.SubItems.Add(drawing.Title3);
                item.SubItems.Add(drawing.Title4);

            }


        }
    }
}
