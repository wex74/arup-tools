﻿namespace ViewCreator.Forms.Controls.Views
{
    partial class ctlDrawings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdDrawings = new SourceGrid.Grid();
            this.SuspendLayout();
            // 
            // grdDrawings
            // 
            this.grdDrawings.BackColor = System.Drawing.Color.White;
            this.grdDrawings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDrawings.EnableSort = true;
            this.grdDrawings.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.grdDrawings.Location = new System.Drawing.Point(0, 0);
            this.grdDrawings.Name = "grdDrawings";
            this.grdDrawings.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows;
            this.grdDrawings.SelectionMode = SourceGrid.GridSelectionMode.Row;
            this.grdDrawings.Size = new System.Drawing.Size(436, 374);
            this.grdDrawings.TabIndex = 40;
            this.grdDrawings.TabStop = true;
            this.grdDrawings.ToolTipText = "";
            // 
            // ctlDrawings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grdDrawings);
            this.Name = "ctlDrawings";
            this.Size = new System.Drawing.Size(436, 374);
            this.Load += new System.EventHandler(this.ctlSheets_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SourceGrid.Grid grdDrawings;

    }
}
