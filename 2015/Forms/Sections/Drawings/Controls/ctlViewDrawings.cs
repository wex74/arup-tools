﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Sections.Drawings.Controls
{
    public partial class ctlViewDrawings : UserControl
    {
        private List<Drawing> _drawings;
        private Drawing _drawing;
        private bool _loaded;

        public event EventHandler DrawingDoubleClicked;
        public event EventHandler DrawingChanged;

        public ctlViewDrawings()
        {
            InitializeComponent();
        }



        public void LoadControl(List<Drawing> drawings )
        {
            _drawings = drawings; 
             BuildDrawings();
        }


        protected virtual void OnDrawingDoubleClicked(Drawing drawing, EventArgs e)
        {
            EventHandler handler = DrawingDoubleClicked;
            if (handler != null)
            {
                handler(drawing, e);
            }
        }

        protected virtual void OnDrawingChanged(Drawing drawing, EventArgs e)
        {
            EventHandler handler = DrawingChanged;
            if (handler != null)
            {
                handler(drawing, e);
            }
        }


        private void BuildDrawings()
        {
            treeView.Nodes.Clear();

            foreach (var drawing in _drawings)
            {
                AddDrawingNode(drawing);
                
            }


            SelectCurrentView();

            _loaded = true;

        }



        private TreeNode AddDrawingNode(Drawing drawing)
        {
            var node = treeView.Nodes.Add(drawing.GetFullName());
            node.SelectedImageKey = "Sheet";
            node.ImageKey = "Sheet";
            node.Name = drawing.SheetId.ToString();
            node.Tag = drawing;

            return node;
        }

        private void treeView_DoubleClick(object sender, EventArgs e)
        {

            if (_drawing != null)
            {
                OnDrawingDoubleClicked(_drawing, e);
            }
            
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var tag = treeView.SelectedNode.Tag;

            if (tag == null) return;


            if (Tree_Utils.IsDrawing(tag))
            {
                _drawing = (Drawing) tag;
                OnDrawingChanged(_drawing, e);
            }
        }

        public void SelectCurrentView()
        {
            treeView.SelectCurrentView();
        }

    }
}
