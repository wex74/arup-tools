﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevAge.Drawing.VisualElements;
using RevitManager_SRC.Classes;
using SourceGrid;
using SourceGrid.Cells;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using ComboBox = SourceGrid.Cells.Editors.ComboBox;
using GridColumn = ViewCreator.Model.GridColumn;

namespace ViewCreator.Forms.Controls.Views
{
    public partial class ctlSheetTemplates : UserControl
    {
        private List<GridColumn> _columns;
        private Drawing _drawing;
        private List<Drawing> _drawings;
        private bool _isLoaded;

        private List<string> _sheetTemplates;

        public ctlSheetTemplates()
        {
            InitializeComponent();
        }


        public void LoadControl(List<Drawing> drawings)
        {
            _columns = new List<GridColumn>();
            _columns.Add(new GridColumn("Drawing", 300));
            _columns.Add(new GridColumn("Sheet Template", 150));


            foreach (var column in _columns)
            {
                column.Index = _columns.IndexOf(column);
            }

            _sheetTemplates = Sheet_Utils.GetSheetTemplateNames();
            _sheetTemplates.Insert(0, "");

            _drawings = drawings;

            BuildDrawings();
        }



        public void SelectAll()
        {
            var range = new Range(1, 1, grdDrawings.Rows.Count, grdDrawings.Columns.Count);

            grdDrawings.Selection.SelectRange(range, true);
        }


        public void BuildDrawings()
        {
            _isLoaded = false;

            Grid_Utils.BuildColumns(grdDrawings, _columns);
            int row;

            var count = 1;

            foreach (var drawing in _drawings)
            {
                var col = 0;

                DrawingCellBackColor viewNormal = new DrawingCellBackColor(drawing, Color.White, Color.Honeydew);


                row = grdDrawings.RowsCount;
                grdDrawings.Rows.Insert(row);

                grdDrawings.Rows[row].Tag = drawing;


                var cellDrawingNo = grdDrawings[row, col] = new Cell("");
                cellDrawingNo.Value = drawing.GetFullName();
                cellDrawingNo.View = viewNormal;
                col++;


                var cboTemplate = new ComboBox(typeof(string), _sheetTemplates.ToArray(), true);
                var cellTemplate = grdDrawings[row, col] = new Cell("");
                cellTemplate.Value = drawing.SheetTemplate;
                cellTemplate.Editor = cboTemplate;
                cboTemplate.Control.Sorted = true;
                cellTemplate.View = viewNormal;
                cboTemplate.Control.DropDownStyle = ComboBoxStyle.DropDown;
                cboTemplate.EditableMode = EditableMode.Default;
                cboTemplate.Control.TextChanged += (Drawing_TemplateChanged);

                col++;


                count++;
            }


            _isLoaded = true;
        }

        private void Drawing_TemplateChanged(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (System.Windows.Forms.ComboBox)sender;
            if (thisBox.Text == "") return;


            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing)grdDrawings.Rows[row].Tag;


            if (drawing.SheetTemplate != thisBox.Text)
            {
                drawing.SheetTemplate = thisBox.Text;
            }
        }


        public List<Drawing> GetSelectedDrawings()
        {
            var rows = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex();
            return rows.Select(row => (Drawing)grdDrawings.Rows[row].Tag).Where(x => x != null).ToList();
        }


        public class DrawingCellBackColor : SourceGrid.Cells.Views.Cell
        {
            private Drawing _drawing;

            public DrawingCellBackColor(Drawing drawing, Color templateColor, Color noTemplateColor)
            {


                _drawing = drawing;
                BackColorTemplate = new BackgroundSolid(templateColor);
                BackColorNoTemplate = new BackgroundSolid(noTemplateColor);

            }




            private IVisualElement _mBackColorTemplate;

            public IVisualElement BackColorTemplate
            {
                get { return _mBackColorTemplate; }
                set { _mBackColorTemplate = value; }
            }

            private IVisualElement _mBackColorNoTemplate;

            public IVisualElement BackColorNoTemplate
            {
                get { return _mBackColorNoTemplate; }
                set { _mBackColorNoTemplate = value; }
            }



            protected override void PrepareView(CellContext context)
            {
                base.PrepareView(context);



                if (!_drawing.SheetTemplateExists)
                {
                    Background = BackColorNoTemplate;
                }
                else
                {
                    Background = BackColorTemplate;
                }

            }

        }

    }
}
