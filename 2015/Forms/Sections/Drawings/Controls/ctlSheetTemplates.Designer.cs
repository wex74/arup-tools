﻿namespace ViewCreator.Forms.Controls.Views
{
    partial class ctlSheetTemplates
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdDrawings = new SourceGrid.Grid();
            this.SuspendLayout();
            // 
            // grdDrawings
            // 
            this.grdDrawings.BackColor = System.Drawing.Color.White;
            this.grdDrawings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grdDrawings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDrawings.EnableSort = true;
            this.grdDrawings.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.grdDrawings.Location = new System.Drawing.Point(0, 0);
            this.grdDrawings.Name = "grdDrawings";
            this.grdDrawings.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows;
            this.grdDrawings.SelectionMode = SourceGrid.GridSelectionMode.Row;
            this.grdDrawings.Size = new System.Drawing.Size(502, 438);
            this.grdDrawings.TabIndex = 41;
            this.grdDrawings.TabStop = true;
            this.grdDrawings.ToolTipText = "";
            // 
            // ctlSheetTemplates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grdDrawings);
            this.Name = "ctlSheetTemplates";
            this.Size = new System.Drawing.Size(502, 438);
            this.ResumeLayout(false);

        }

        #endregion

        private SourceGrid.Grid grdDrawings;
    }
}
