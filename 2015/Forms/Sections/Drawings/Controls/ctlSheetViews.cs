﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Forms.Controls.Views
{
    public partial class ctlSheetViews : UserControl
    {
        private List<ViewSheet> _sheets;

        public ctlSheetViews()
        {
            InitializeComponent();
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        public void LoadControl(List<ViewSheet> sheets)
        {
            _sheets = sheets;


            BuildSheets();
            treeView.ExpandAll();
        }

        private void BuildSheets()
        {
            foreach (var sheet in _sheets)
            {
                var name = sheet.SheetName();
                var image = App_Utils.GetDisciplineImage(name);

                var sheetNode = treeView.Nodes.Add(name);
                sheetNode.ImageKey = image;
                sheetNode.SelectedImageKey = image;
                sheetNode.Tag = sheet;
                sheetNode.Name = sheet.Id.ToString();


                BuildViews(sheet, sheetNode);
            }
        }


        private void BuildViews(ViewSheet sheet, TreeNode node)
        {
            foreach (var vp in sheet.GetSheetViewports())
            {
                var view = View_Utils.GetView(vp.ViewId);


                var name = view.ViewName;
                var image = view.GetViewImage();

                var viewNode = node.Nodes.Add(name);
                viewNode.ImageKey = image;
                viewNode.SelectedImageKey = image;
                viewNode.Tag = view;
                viewNode.Name = vp.Id.ToString();
            }
        }
    }
}
