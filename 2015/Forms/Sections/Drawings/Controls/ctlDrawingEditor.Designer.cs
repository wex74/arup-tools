﻿namespace ViewCreator.Forms.Controls.Parts
{
    partial class ctlDrawingEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.cboDiscipline = new System.Windows.Forms.ComboBox();
            this.btnDiscipline = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.cboCopyTitlesDown = new System.Windows.Forms.ComboBox();
            this.btnCopyDown = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboMoveTitle = new System.Windows.Forms.ComboBox();
            this.btnMoveForward = new System.Windows.Forms.Button();
            this.btnMoveBack = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboUpdateTitle = new System.Windows.Forms.ComboBox();
            this.cboTitleNo = new System.Windows.Forms.ComboBox();
            this.btnUpdateDescriptions = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtUpdateDrawingNo = new System.Windows.Forms.TextBox();
            this.cboDrawingNoUpdateType = new System.Windows.Forms.ComboBox();
            this.btnUpdateDrawingNo = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cboDrawingType = new System.Windows.Forms.ComboBox();
            this.btnDrawingType = new System.Windows.Forms.Button();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.cboDiscipline);
            this.groupBox7.Controls.Add(this.btnDiscipline);
            this.groupBox7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox7.Location = new System.Drawing.Point(790, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(203, 68);
            this.groupBox7.TabIndex = 76;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Discipline";
            // 
            // cboDiscipline
            // 
            this.cboDiscipline.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscipline.FormattingEnabled = true;
            this.cboDiscipline.Location = new System.Drawing.Point(15, 29);
            this.cboDiscipline.Name = "cboDiscipline";
            this.cboDiscipline.Size = new System.Drawing.Size(149, 21);
            this.cboDiscipline.TabIndex = 66;
            this.cboDiscipline.SelectedIndexChanged += new System.EventHandler(this.cboTemplate_SelectedIndexChanged);
            // 
            // btnDiscipline
            // 
            this.btnDiscipline.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnDiscipline.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDiscipline.Location = new System.Drawing.Point(170, 29);
            this.btnDiscipline.Name = "btnDiscipline";
            this.btnDiscipline.Size = new System.Drawing.Size(22, 22);
            this.btnDiscipline.TabIndex = 47;
            this.btnDiscipline.Text = "ü";
            this.btnDiscipline.UseVisualStyleBackColor = true;
            this.btnDiscipline.Click += new System.EventHandler(this.btnDiscipline_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.cboCopyTitlesDown);
            this.groupBox5.Controls.Add(this.btnCopyDown);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox5.Location = new System.Drawing.Point(647, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(137, 68);
            this.groupBox5.TabIndex = 74;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Copy Titles";
            // 
            // cboCopyTitlesDown
            // 
            this.cboCopyTitlesDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCopyTitlesDown.FormattingEnabled = true;
            this.cboCopyTitlesDown.Items.AddRange(new object[] {
            "All",
            "Title1",
            "Title2",
            "Title3",
            "Title4"});
            this.cboCopyTitlesDown.Location = new System.Drawing.Point(44, 30);
            this.cboCopyTitlesDown.Name = "cboCopyTitlesDown";
            this.cboCopyTitlesDown.Size = new System.Drawing.Size(80, 21);
            this.cboCopyTitlesDown.TabIndex = 68;
            // 
            // btnCopyDown
            // 
            this.btnCopyDown.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnCopyDown.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCopyDown.Location = new System.Drawing.Point(16, 29);
            this.btnCopyDown.Name = "btnCopyDown";
            this.btnCopyDown.Size = new System.Drawing.Size(22, 22);
            this.btnCopyDown.TabIndex = 47;
            this.btnCopyDown.Text = "ê";
            this.btnCopyDown.UseVisualStyleBackColor = true;
            this.btnCopyDown.Click += new System.EventHandler(this.btnCopyDown_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboMoveTitle);
            this.groupBox2.Controls.Add(this.btnMoveForward);
            this.groupBox2.Controls.Add(this.btnMoveBack);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(482, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(159, 68);
            this.groupBox2.TabIndex = 73;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Move Title";
            // 
            // cboMoveTitle
            // 
            this.cboMoveTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMoveTitle.FormattingEnabled = true;
            this.cboMoveTitle.Items.AddRange(new object[] {
            "Title1",
            "Title2",
            "Title3",
            "Title4"});
            this.cboMoveTitle.Location = new System.Drawing.Point(65, 30);
            this.cboMoveTitle.Name = "cboMoveTitle";
            this.cboMoveTitle.Size = new System.Drawing.Size(80, 21);
            this.cboMoveTitle.TabIndex = 67;
            // 
            // btnMoveForward
            // 
            this.btnMoveForward.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnMoveForward.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMoveForward.Location = new System.Drawing.Point(37, 32);
            this.btnMoveForward.Name = "btnMoveForward";
            this.btnMoveForward.Size = new System.Drawing.Size(22, 22);
            this.btnMoveForward.TabIndex = 48;
            this.btnMoveForward.Text = "è";
            this.btnMoveForward.UseVisualStyleBackColor = true;
            this.btnMoveForward.Click += new System.EventHandler(this.btnMoveForward_Click);
            // 
            // btnMoveBack
            // 
            this.btnMoveBack.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnMoveBack.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMoveBack.Location = new System.Drawing.Point(9, 31);
            this.btnMoveBack.Name = "btnMoveBack";
            this.btnMoveBack.Size = new System.Drawing.Size(22, 22);
            this.btnMoveBack.TabIndex = 47;
            this.btnMoveBack.Text = "ç";
            this.btnMoveBack.UseVisualStyleBackColor = true;
            this.btnMoveBack.Click += new System.EventHandler(this.btnMoveBack_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboUpdateTitle);
            this.groupBox1.Controls.Add(this.cboTitleNo);
            this.groupBox1.Controls.Add(this.btnUpdateDescriptions);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(234, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 68);
            this.groupBox1.TabIndex = 72;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Update Title";
            // 
            // cboUpdateTitle
            // 
            this.cboUpdateTitle.FormattingEnabled = true;
            this.cboUpdateTitle.Items.AddRange(new object[] {
            "Level",
            "Plan",
            "Detail",
            "Schematic"});
            this.cboUpdateTitle.Location = new System.Drawing.Point(15, 31);
            this.cboUpdateTitle.Name = "cboUpdateTitle";
            this.cboUpdateTitle.Size = new System.Drawing.Size(100, 21);
            this.cboUpdateTitle.TabIndex = 67;
            // 
            // cboTitleNo
            // 
            this.cboTitleNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTitleNo.FormattingEnabled = true;
            this.cboTitleNo.Items.AddRange(new object[] {
            "Title1",
            "Title2",
            "Title3",
            "Title4"});
            this.cboTitleNo.Location = new System.Drawing.Point(122, 30);
            this.cboTitleNo.Name = "cboTitleNo";
            this.cboTitleNo.Size = new System.Drawing.Size(80, 21);
            this.cboTitleNo.TabIndex = 66;
            // 
            // btnUpdateDescriptions
            // 
            this.btnUpdateDescriptions.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnUpdateDescriptions.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnUpdateDescriptions.Location = new System.Drawing.Point(208, 30);
            this.btnUpdateDescriptions.Name = "btnUpdateDescriptions";
            this.btnUpdateDescriptions.Size = new System.Drawing.Size(22, 22);
            this.btnUpdateDescriptions.TabIndex = 47;
            this.btnUpdateDescriptions.Text = "ü";
            this.btnUpdateDescriptions.UseVisualStyleBackColor = true;
            this.btnUpdateDescriptions.Click += new System.EventHandler(this.btnUpdateDescriptions_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtUpdateDrawingNo);
            this.groupBox4.Controls.Add(this.cboDrawingNoUpdateType);
            this.groupBox4.Controls.Add(this.btnUpdateDrawingNo);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(225, 68);
            this.groupBox4.TabIndex = 71;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Update Drawing No";
            // 
            // txtUpdateDrawingNo
            // 
            this.txtUpdateDrawingNo.Location = new System.Drawing.Point(15, 29);
            this.txtUpdateDrawingNo.Name = "txtUpdateDrawingNo";
            this.txtUpdateDrawingNo.Size = new System.Drawing.Size(80, 20);
            this.txtUpdateDrawingNo.TabIndex = 51;
            // 
            // cboDrawingNoUpdateType
            // 
            this.cboDrawingNoUpdateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDrawingNoUpdateType.FormattingEnabled = true;
            this.cboDrawingNoUpdateType.Items.AddRange(new object[] {
            "Prefix",
            "Suffix",
            "Remove"});
            this.cboDrawingNoUpdateType.Location = new System.Drawing.Point(102, 29);
            this.cboDrawingNoUpdateType.Name = "cboDrawingNoUpdateType";
            this.cboDrawingNoUpdateType.Size = new System.Drawing.Size(80, 21);
            this.cboDrawingNoUpdateType.TabIndex = 52;
            // 
            // btnUpdateDrawingNo
            // 
            this.btnUpdateDrawingNo.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnUpdateDrawingNo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnUpdateDrawingNo.Location = new System.Drawing.Point(188, 30);
            this.btnUpdateDrawingNo.Name = "btnUpdateDrawingNo";
            this.btnUpdateDrawingNo.Size = new System.Drawing.Size(22, 22);
            this.btnUpdateDrawingNo.TabIndex = 50;
            this.btnUpdateDrawingNo.Text = "ü";
            this.btnUpdateDrawingNo.UseVisualStyleBackColor = true;
            this.btnUpdateDrawingNo.Click += new System.EventHandler(this.btnUpdateDrawingNo_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cboDrawingType);
            this.groupBox3.Controls.Add(this.btnDrawingType);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Location = new System.Drawing.Point(999, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(203, 68);
            this.groupBox3.TabIndex = 77;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Drawing Type";
            // 
            // cboDrawingType
            // 
            this.cboDrawingType.FormattingEnabled = true;
            this.cboDrawingType.Location = new System.Drawing.Point(15, 29);
            this.cboDrawingType.Name = "cboDrawingType";
            this.cboDrawingType.Size = new System.Drawing.Size(149, 21);
            this.cboDrawingType.TabIndex = 66;
            // 
            // btnDrawingType
            // 
            this.btnDrawingType.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnDrawingType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDrawingType.Location = new System.Drawing.Point(170, 29);
            this.btnDrawingType.Name = "btnDrawingType";
            this.btnDrawingType.Size = new System.Drawing.Size(22, 22);
            this.btnDrawingType.TabIndex = 47;
            this.btnDrawingType.Text = "ü";
            this.btnDrawingType.UseVisualStyleBackColor = true;
            this.btnDrawingType.Click += new System.EventHandler(this.btnDrawingType_Click);
            // 
            // ctlDrawingEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Name = "ctlDrawingEditor";
            this.Size = new System.Drawing.Size(1206, 76);
            this.Load += new System.EventHandler(this.ctlDrawingEditor_Load);
            this.groupBox7.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox cboDiscipline;
        private System.Windows.Forms.Button btnDiscipline;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cboCopyTitlesDown;
        private System.Windows.Forms.Button btnCopyDown;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboMoveTitle;
        private System.Windows.Forms.Button btnMoveForward;
        private System.Windows.Forms.Button btnMoveBack;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboUpdateTitle;
        private System.Windows.Forms.ComboBox cboTitleNo;
        private System.Windows.Forms.Button btnUpdateDescriptions;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtUpdateDrawingNo;
        private System.Windows.Forms.ComboBox cboDrawingNoUpdateType;
        private System.Windows.Forms.Button btnUpdateDrawingNo;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cboDrawingType;
        private System.Windows.Forms.Button btnDrawingType;
    }
}
