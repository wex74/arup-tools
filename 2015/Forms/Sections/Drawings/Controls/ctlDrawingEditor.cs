﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Forms.Controls.Views;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Controls.Parts
{
    public partial class ctlDrawingEditor : UserControl
    {
        private ctlDrawings _ctlDrawings;
        private List<Drawing> _drawings;

        public event EventHandler DrawingsUpdated;

        public ctlDrawingEditor()
        {
            InitializeComponent();
        }

        private void ctlDrawingEditor_Load(object sender, EventArgs e)
        {

        }


        public void LoadControl(ctlDrawings ctlDrawings)
        {
            _ctlDrawings = ctlDrawings;

            cboDiscipline.LoadCombo(Discipline_Utils.GetDisciplineServiceNames(), "", "");
            cboDrawingType.LoadCombo(App_Utils.GetDrawingTypeNames(""), "", "");

        }



        protected virtual void OnDrawingsUpdated(List<Drawing> drawings, EventArgs e)
        {
            EventHandler handler = DrawingsUpdated;
            if (handler != null)
            {
                handler(drawings, e);
            }
        }

        private void btnUpdateDrawingNo_Click(object sender, EventArgs e)
        {
            if (cboDrawingNoUpdateType.SelectedIndex < 0) return;

            _drawings = _ctlDrawings.GetSelected();
            if (!_drawings.Any()) return;

            foreach (var drawing in _drawings)
            {

                if (drawing != null)
                {
                    if (cboDrawingNoUpdateType.SelectedIndex == 0)
                    {
                        drawing.DrawingNo = txtUpdateDrawingNo.Text + drawing.DrawingNo;
                    }


                    if (cboDrawingNoUpdateType.SelectedIndex == 1)
                    {
                        drawing.DrawingNo = drawing.DrawingNo + txtUpdateDrawingNo.Text;
                    }

                    if (cboDrawingNoUpdateType.SelectedIndex == 2)
                    {
                        drawing.DrawingNo = drawing.DrawingNo.Replace(txtUpdateDrawingNo.Text, "");
                    }

                }


            }

            txtUpdateDrawingNo.Text = "";
            cboDrawingNoUpdateType.SelectedIndex = -1;
            OnDrawingsUpdated(_drawings, e);
        }

        private void btnUpdateDescriptions_Click(object sender, EventArgs e)
        {
            if (cboTitleNo.Text == "") return;

            var title = cboUpdateTitle.Text;

            _drawings = _ctlDrawings.GetSelected();
            if (!_drawings.Any()) return;

            foreach (var drawing in _drawings)
            {
                if (drawing != null)
                {
                    switch (cboTitleNo.Text)
                    {
                        case "Title1":
                            drawing.Title1 = title;
                            break;

                        case "Title2":
                            drawing.Title2 = title;
                            break;

                        case "Title3":
                            drawing.Title3 = title;
                            break;

                        case "Title4":
                            drawing.Title4 = title;
                            break;
                    }


                }


            }

            cboUpdateTitle.Text = "";
            cboTitleNo.SelectedIndex = -1;

            OnDrawingsUpdated(_drawings, e);

        }

        private void btnMoveBack_Click(object sender, EventArgs e)
        {
            if (cboMoveTitle.Text == "") return;

            _drawings = _ctlDrawings.GetSelected();
            if (!_drawings.Any()) return;

            foreach (var drawing in _drawings)
            {
  
                if (drawing != null)
                {
                    switch (cboMoveTitle.Text)
                    {

                        case "Title2":
                            if (drawing.Title2 != "")
                            {
                                drawing.Title1 = drawing.Title2;
                                drawing.Title2 = "";
                            }
                            break;

                        case "Title3":
                            if (drawing.Title3 != "")
                            {
                                drawing.Title2 = drawing.Title3;
                                drawing.Title3 = "";
                            }

                            break;

                        case "Title4":
                            if (drawing.Title4 != "")
                            {
                                drawing.Title3 = drawing.Title4;
                                drawing.Title4 = "";
                            }

                            break;
                    }
                }
            }



            switch (cboMoveTitle.Text)
            {

                case "Title2":
                    cboMoveTitle.Text = "Title1";
                    break;

                case "Title3":
                    cboMoveTitle.Text = "Title2";
                    break;

                case "Title4":
                    cboMoveTitle.Text = "Title3";
                    break;

            }


            cboMoveTitle.SelectedIndex = -1;
            OnDrawingsUpdated(_drawings, e);

        }

        private void btnMoveForward_Click(object sender, EventArgs e)
        {
            if (cboMoveTitle.Text == "") return;

            _drawings = _ctlDrawings.GetSelected();
            if (!_drawings.Any()) return;

            foreach (var drawing in _drawings)
            {
                if (drawing != null)
                {
                    switch (cboMoveTitle.Text)
                    {
                        case "Title1":
                            if (drawing.Title1 != "")
                            {
                                drawing.Title2 = drawing.Title1;
                                drawing.Title1 = "";
                            }
                            break;

                        case "Title2":
                            if (drawing.Title2 != "")
                            {
                                drawing.Title3 = drawing.Title2;
                                drawing.Title2 = "";
                            }
                            break;

                        case "Title3":
                            if (drawing.Title3 != "")
                            {
                                drawing.Title4 = drawing.Title3;
                                drawing.Title3 = "";
                            }
                            break;
                    }
                }
            }



            switch (cboMoveTitle.Text)
            {
                case "Title1":
                    cboMoveTitle.Text = "Title2";
                    break;

                case "Title2":
                    cboMoveTitle.Text = "Title3";
                    break;

                case "Title3":
                    cboMoveTitle.Text = "Title4";
                    break;
            }

            cboMoveTitle.SelectedIndex = -1;

            OnDrawingsUpdated(_drawings, e);

        }

        private void btnCopyDown_Click(object sender, EventArgs e)
        {
            if (cboCopyTitlesDown.Text == "") return;

            var title1 = "";
            var title2 = "";
            var title3 = "";
            var title4 = "";

            _drawings = _ctlDrawings.GetSelected();
            if (!_drawings.Any()) return;

            int count = 0;
            foreach (var drawing in _drawings)
            {

                if (drawing != null)
                {

                    if (count == 0)
                    {
                        title1 = drawing.Title1;
                        title2 = drawing.Title2;
                        title3 = drawing.Title3;
                        title4 = drawing.Title4;
                    }




                    if (count > 0)
                    {
                        switch (cboCopyTitlesDown.Text)
                        {
                            case "All":
                                drawing.Title1 = title1;
                                drawing.Title2 = title2;
                                drawing.Title3 = title3;
                                drawing.Title4 = title4;
                                break;

                            case "Title1":
                                drawing.Title1 = title1;
                                break;

                            case "Title2":
                                drawing.Title2 = title2;
                                break;

                            case "Title3":
                                drawing.Title3 = title3;
                                break;

                            case "Title4":
                                drawing.Title4 = title4;
                                break;
                        }


                    }
                }

                count++;
            }

            cboCopyTitlesDown.SelectedIndex = -1;

            OnDrawingsUpdated(_drawings, e);


        }

    


        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            cboDiscipline.Text = UI_Utils.SelectViewTemplate(cboDiscipline.Text);
        }

        private void cboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       

        private void btnDiscipline_Click(object sender, EventArgs e)
        {
            _drawings = _ctlDrawings.GetSelected();
            if (!_drawings.Any()) return;

            foreach (var drawing in _drawings)
            {

                if (drawing != null)
                {
                    if (drawing.Discipline != cboDiscipline.Text)
                    {
                        drawing.Discipline = cboDiscipline.Text;
                    }

                }


            }

            cboDiscipline.SelectedIndex = -1;
            OnDrawingsUpdated(_drawings, e);
        }

        private void btnDrawingType_Click(object sender, EventArgs e)
        {
            _drawings = _ctlDrawings.GetSelected();
            if (!_drawings.Any()) return;

            foreach (var drawing in _drawings)
            {

                if (drawing != null)
                {
                    var discType = Discipline_Utils.GetDisciplinePrefix(drawing.Discipline) + cboDrawingType.Text;
                    if (drawing.DrawingType != discType)
                    {
                        drawing.DrawingType = discType;
                    }

                }


            }

            cboDrawingType.SelectedIndex = -1;
            OnDrawingsUpdated(_drawings, e);
        }
    
    }
}
