﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevAge.Drawing.VisualElements;
using RevitManager_SRC.Classes;
using SourceGrid;
using SourceGrid.Cells.Views;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.UI;
using ComboBox = SourceGrid.Cells.Editors.ComboBox;
using GridColumn = ViewCreator.Model.GridColumn;

namespace ViewCreator.Forms.Controls.Views
{
    public partial class ctlDrawings : UserControl
    {
        private List<GridColumn> _columns;
        private List<string> _disciplines;
        private Drawing _drawing;
        private List<Drawing> _drawings;
        private List<Drawing> _allDrawings;
        private List<string> _drawingTypes;
        private bool _isLoaded;

        public ctlDrawings()
        {
            InitializeComponent();
        }

        public string[] DrawingInfoCharsToRemove
        {
            get { return new[] {"<", ">", "?", "|", "`", "*", "\""}; }
        }

        public int[] GetSelectedRows
        {
            get { return grdDrawings.Selection.GetSelectionRegion().GetRowsIndex(); }
        }

        public event EventHandler DrawingChanged;

        public Drawing GetDrawing()
        {
            return _drawing;
        }

        public List<Drawing> GetDrawings()
        {
            return _drawings;
        }

        protected virtual void OnDrawingChanged(Drawing drawing, EventArgs e)
        {
            var handler = DrawingChanged;
            if (handler != null)
            {
                handler(drawing, e);
            }
        }

        private void ctlSheets_Load(object sender, EventArgs e)
        {
        }

        public void LoadControl(List<Drawing> drawings)
        {
            ScreensUI.DrawingsControl = this;

            _columns = new List<GridColumn>();
            _columns.Add(new GridColumn("Drg No", 100));
            _columns.Add(new GridColumn("Title 1", 150));
            _columns.Add(new GridColumn("Title 2", 200));
            _columns.Add(new GridColumn("Title 3", 150));
            _columns.Add(new GridColumn("Title 4", 100));
            _columns.Add(new GridColumn("Discipline", 175));
            _columns.Add(new GridColumn("Drawing Type", 230));


            foreach (var column in _columns)
            {
                column.Index = _columns.IndexOf(column);
            }

            _disciplines = Discipline_Utils.GetDisciplineServiceNames();
            _disciplines.Insert(0, "");
            _drawingTypes = App_Utils.GetDrawingTypeNames("");
            _drawingTypes.Insert(0, "");

            _drawings = drawings;

            BuildDrawings();
        }

        public void BuildDrawings()
        {
            _isLoaded = false;

            Grid_Utils.BuildColumns(grdDrawings, _columns);
            int row;

            var count = 1;

            foreach (var drawing in _drawings)
            {
                var col = 0;

                var viewNormal = new DrawingCellBackColor(drawing, Color.White, Color.Honeydew);


                row = grdDrawings.RowsCount;
                grdDrawings.Rows.Insert(row);

                grdDrawings.Rows[row].Tag = drawing;


                var txtDrawingNo = new SourceGrid.Cells.Editors.TextBox(typeof (string));
                var cellDrawingNo = grdDrawings[row, col] = new SourceGrid.Cells.Cell("");
                cellDrawingNo.Value = drawing.DrawingNo;
                cellDrawingNo.Editor = txtDrawingNo;
                cellDrawingNo.View = viewNormal;
                txtDrawingNo.EditableMode = EditableMode.Default;
                txtDrawingNo.Control.TextChanged += RevitDrawing_DrawingNoChanged;
                col++;

                var txtTitle1 = new SourceGrid.Cells.Editors.TextBox(typeof (string));
                var cellTitle1 = grdDrawings[row, col] = new SourceGrid.Cells.Cell("");
                cellTitle1.Value = drawing.Title1;
                cellTitle1.Editor = txtTitle1;
                cellTitle1.View = viewNormal;
                txtTitle1.EditableMode = EditableMode.Default;
                txtTitle1.Control.TextChanged += RevitDrawing_Title1;
                col++;


                var txtTitle2 = new SourceGrid.Cells.Editors.TextBox(typeof (string));
                var cellTitle2 = grdDrawings[row, col] = new SourceGrid.Cells.Cell("");
                cellTitle2.Value = drawing.Title2;
                cellTitle2.Editor = txtTitle2;
                cellTitle2.View = viewNormal;
                txtTitle2.EditableMode = EditableMode.Default;
                txtTitle2.Control.TextChanged += RevitDrawing_Title2;
                col++;

                var txtTitle3 = new SourceGrid.Cells.Editors.TextBox(typeof (string));
                var cellTitle3 = grdDrawings[row, col] = new SourceGrid.Cells.Cell("");
                cellTitle3.Value = drawing.Title3;
                cellTitle3.Editor = txtTitle3;
                cellTitle3.View = viewNormal;
                txtTitle3.EditableMode = EditableMode.Default;
                txtTitle3.Control.TextChanged += RevitDrawing_Title3;
                col++;

                var txtTitle4 = new SourceGrid.Cells.Editors.TextBox(typeof (string));
                var cellTitle4 = grdDrawings[row, col] = new SourceGrid.Cells.Cell("");
                cellTitle4.Value = drawing.Title4;
                cellTitle4.Editor = txtTitle4;
                cellTitle4.View = viewNormal;
                txtTitle4.EditableMode = EditableMode.Default;
                txtTitle4.Control.TextChanged += RevitDrawing_Title4;
                col++;


                var cboDiscipline = new ComboBox(typeof (string), _disciplines.ToArray(), true);
                var cellDiscipline = grdDrawings[row, col] = new SourceGrid.Cells.Cell("");
                cellDiscipline.Value = drawing.Discipline;
                cellDiscipline.Editor = cboDiscipline;
                cboDiscipline.Control.Sorted = true;
                cellDiscipline.View = viewNormal;
                cboDiscipline.Control.DropDownStyle = ComboBoxStyle.DropDownList;
                cboDiscipline.EditableMode = EditableMode.Default;
                cboDiscipline.Control.TextChanged += Drawing_DisciplineChanged;
                col++;


                var cboDrawingType = new ComboBox(typeof (string), _drawingTypes.ToArray(), true);
                var cellDrawingType = grdDrawings[row, col] = new SourceGrid.Cells.Cell("");
                cellDrawingType.Value = drawing.DrawingType;
                cellDrawingType.Editor = cboDrawingType;
                cboDrawingType.Control.Sorted = true;
                cellDrawingType.View = viewNormal;
                cboDrawingType.Control.DropDownStyle = ComboBoxStyle.DropDownList;
                cboDrawingType.EditableMode = EditableMode.Default;
                cboDrawingType.Control.TextChanged += Drawing_DrawingTypeChanged;

                count++;
            }


            _isLoaded = true;
        }

        public void SelectRow1()
        {
            var position = new Position(0, 1);
            grdDrawings.Selection.Focus(position, true);
        }

        public void SelectAll()
        {
            var range = new Range(1, 1, grdDrawings.Rows.Count, grdDrawings.Columns.Count);

            grdDrawings.Selection.SelectRange(range, true);
        }

        public void SelectNew()
        {
            foreach (var row in grdDrawings.Rows)
            {
                var drawing = (Drawing) row.Tag;

                if (drawing != null)
                {
                    if (drawing.SheetId == null)
                    {
                        var position = new Position(row.Index, 1);
                        grdDrawings.Selection.Focus(position, false);
                    }
                }
            }
        }

        public void SelectDrawing(Drawing drawing)
        {
            if (drawing == null)
            {
                SelectRow1();
                return;
            }


            foreach (var row in grdDrawings.Rows)
            {
                var drg = (Drawing) row.Tag;

                if (drg != null)
                {
                    if (drg.Id == drawing.Id)
                    {
                        var position = new Position(row.Index, 1);
                        grdDrawings.Selection.Focus(position, true);
                        break;
                    }
                }
            }
        }

        public void SelectRows(int[] rows)
        {
            foreach (var row in rows)
            {
                grdDrawings.Selection.SelectRow(row, true);
            }
        }

        public void SelectDrawings(List<Drawing> drawings)
        {
            foreach (var row in grdDrawings.Rows)
            {
                var drg = (Drawing) row.Tag;

                if (drg != null)
                {
                    if (drawings.Exists(x => x.Id == drg.Id))
                    {
                        var position = new Position(row.Index, 1);
                        grdDrawings.Selection.Focus(position, false);
                    }
                }
            }
        }

        public List<Drawing> GetSelected()
        {
            var rows = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex();
            return rows.Select(row => (Drawing) grdDrawings.Rows[row].Tag).Where(x => x != null).ToList();
        }

        public Drawing SelectedDrawing()
        {
            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            return (Drawing) grdDrawings.Rows[row].Tag;;
        }

        private void RevitDrawing_DrawingNoChanged(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (TextBox) sender;
            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing) grdDrawings.Rows[row].Tag;

            var text = CheckTextBox(thisBox, drawing.DrawingNo);
            if (text != "")
            {
                drawing.DrawingNo = text;
            }
        }

        private string CheckTextBox(TextBox textBox, string textToCheck)
        {
            if (textBox.Text != textToCheck)
            {
                var textCheck = General_Utils.CleanInput(textBox.Text, DrawingInfoCharsToRemove);

                if (textCheck != textBox.Text)
                {
                    textBox.Text = textCheck;
                }
                return textCheck;
            }

            return "";
        }

        private void RevitDrawing_Title1(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (TextBox) sender;
            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing) grdDrawings.Rows[row].Tag;

            var text = CheckTextBox(thisBox, drawing.Title1);
            if (text != "")
            {
                drawing.Title1 = text;
            }
        }

        private void RevitDrawing_Title2(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (TextBox) sender;
            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing) grdDrawings.Rows[row].Tag;

            var text = CheckTextBox(thisBox, drawing.Title2);
            if (text != "")
            {
                drawing.Title2 = text;
            }
        }

        private void RevitDrawing_Title3(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (TextBox) sender;
            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing) grdDrawings.Rows[row].Tag;

            var text = CheckTextBox(thisBox, drawing.Title3);
            if (text != "")
            {
                drawing.Title3 = text;
            }
        }

        private void RevitDrawing_Title4(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (TextBox) sender;
            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing) grdDrawings.Rows[row].Tag;


            var text = CheckTextBox(thisBox, drawing.DrawingNo);
            if (text != "")
            {
                drawing.Title4 = text;
            }
        }

        private void Drawing_DisciplineChanged(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (System.Windows.Forms.ComboBox) sender;
            if (thisBox.Text == "") return;


            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing) grdDrawings.Rows[row].Tag;


            if (drawing.Discipline != thisBox.Text)
            {
                drawing.Discipline = thisBox.Text;
            }
        }

        private void Drawing_DrawingTypeChanged(object sender, EventArgs e)
        {
            if (!_isLoaded) return;

            var thisBox = (System.Windows.Forms.ComboBox) sender;
            if (thisBox.Text == "") return;


            var row = grdDrawings.Selection.GetSelectionRegion().GetRowsIndex().First();
            var drawing = (Drawing) grdDrawings.Rows[row].Tag;


            if (drawing.DrawingType != thisBox.Text)
            {
                drawing.DrawingType = thisBox.Text;
            }
        }


        public class DrawingCellBackColor : Cell
        {
            private readonly Drawing _drawing;


            public DrawingCellBackColor(Drawing drawing, Color newColor, Color modelColor)
            {
                _drawing = drawing;
                BackColorNew = new BackgroundSolid(newColor);
                BackColorModel = new BackgroundSolid(modelColor);
            }

            public IVisualElement BackColorNew { get; set; }

            public IVisualElement BackColorModel { get; set; }


            protected override void PrepareView(CellContext context)
            {
                base.PrepareView(context);


                if (_drawing.SheetId > 0)
                {
                    Background = BackColorModel;
                }
                else
                {
                    Background = BackColorNew;
                }
            }
        }
    }
}