﻿namespace ViewCreator.Forms.Sections.Drawings.Forms
{
    partial class frmInsertViewOnSheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboDiscipline = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ctlSearch = new ViewCreator.Forms.Controls.ctlSearch();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optRevit = new System.Windows.Forms.RadioButton();
            this.optStructures = new System.Windows.Forms.RadioButton();
            this.optBuildings = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 365);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(505, 50);
            this.panel1.TabIndex = 107;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(338, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 94;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(419, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 93;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Beige;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(505, 131);
            this.panel2.TabIndex = 109;
            // 
            // cboDiscipline
            // 
            this.cboDiscipline.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscipline.FormattingEnabled = true;
            this.cboDiscipline.Location = new System.Drawing.Point(245, 40);
            this.cboDiscipline.Name = "cboDiscipline";
            this.cboDiscipline.Size = new System.Drawing.Size(225, 21);
            this.cboDiscipline.TabIndex = 90;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(242, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 89;
            this.label5.Text = "Discipline";
            // 
            // ctlSearch
            // 
            this.ctlSearch.Location = new System.Drawing.Point(14, 39);
            this.ctlSearch.Name = "ctlSearch";
            this.ctlSearch.Search = "";
            this.ctlSearch.Size = new System.Drawing.Size(225, 24);
            this.ctlSearch.TabIndex = 48;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "Search Views";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.optRevit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.optStructures);
            this.groupBox1.Controls.Add(this.ctlSearch);
            this.groupBox1.Controls.Add(this.optBuildings);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cboDiscipline);
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(486, 108);
            this.groupBox1.TabIndex = 91;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // optRevit
            // 
            this.optRevit.Image = global::ViewCreator.Properties.Resources.Sections;
            this.optRevit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optRevit.Location = new System.Drawing.Point(170, 74);
            this.optRevit.Name = "optRevit";
            this.optRevit.Size = new System.Drawing.Size(80, 24);
            this.optRevit.TabIndex = 60;
            this.optRevit.TabStop = true;
            this.optRevit.Text = "Section";
            this.optRevit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optRevit.UseVisualStyleBackColor = true;
            // 
            // optStructures
            // 
            this.optStructures.Image = global::ViewCreator.Properties.Resources.threeD;
            this.optStructures.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optStructures.Location = new System.Drawing.Point(81, 74);
            this.optStructures.Name = "optStructures";
            this.optStructures.Size = new System.Drawing.Size(83, 24);
            this.optStructures.TabIndex = 59;
            this.optStructures.TabStop = true;
            this.optStructures.Text = "Three D";
            this.optStructures.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optStructures.UseVisualStyleBackColor = true;
            // 
            // optBuildings
            // 
            this.optBuildings.Image = global::ViewCreator.Properties.Resources.NewPlan;
            this.optBuildings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.optBuildings.Location = new System.Drawing.Point(14, 74);
            this.optBuildings.Name = "optBuildings";
            this.optBuildings.Size = new System.Drawing.Size(64, 24);
            this.optBuildings.TabIndex = 58;
            this.optBuildings.TabStop = true;
            this.optBuildings.Text = "Plan";
            this.optBuildings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.optBuildings.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.Image = global::ViewCreator.Properties.Resources.Dependant;
            this.radioButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.radioButton1.Location = new System.Drawing.Point(256, 74);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(98, 24);
            this.radioButton1.TabIndex = 91;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Dependent";
            this.radioButton1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.checkBox1.Location = new System.Drawing.Point(178, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBox1.Size = new System.Drawing.Size(143, 17);
            this.checkBox1.TabIndex = 110;
            this.checkBox1.Text = "Insert @ Center of Sheet";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // frmInsertViewOnSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 415);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmInsertViewOnSheet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert View On Sheet";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private ViewCreator.Forms.Controls.ctlSearch ctlSearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboDiscipline;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton optRevit;
        private System.Windows.Forms.RadioButton optStructures;
        private System.Windows.Forms.RadioButton optBuildings;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}