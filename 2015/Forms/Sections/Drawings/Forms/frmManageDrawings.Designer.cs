﻿namespace ViewCreator.Forms.Main_Dialogs
{
    partial class frmManageDrawings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAutoType = new System.Windows.Forms.Button();
            this.btnSheetTemplates = new System.Windows.Forms.Button();
            this.ctlDrawings = new ViewCreator.Forms.Controls.Views.ctlDrawings();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.btnReNumberDrawings = new System.Windows.Forms.Button();
            this.btnAutoBuild = new System.Windows.Forms.Button();
            this.btnDuplicateDrawing = new System.Windows.Forms.Button();
            this.btnDeleteDrawing = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboDiscipline = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.ckShowSheets = new System.Windows.Forms.CheckBox();
            this.ctlSearch = new ViewCreator.Forms.Controls.ctlSearch();
            this.button3 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ctlDrawingEditor = new ViewCreator.Forms.Controls.Parts.ctlDrawingEditor();
            this.groupBox3.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnOk);
            this.groupBox3.Controls.Add(this.btnLoad);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.btnAutoType);
            this.groupBox3.Controls.Add(this.btnSheetTemplates);
            this.groupBox3.Controls.Add(this.ctlDrawings);
            this.groupBox3.Controls.Add(this.btnReNumberDrawings);
            this.groupBox3.Controls.Add(this.btnAutoBuild);
            this.groupBox3.Controls.Add(this.btnDuplicateDrawing);
            this.groupBox3.Controls.Add(this.btnDeleteDrawing);
            this.groupBox3.Location = new System.Drawing.Point(8, 192);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1225, 405);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Drawings";
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.SeaGreen;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOk.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnOk.Location = new System.Drawing.Point(1026, 370);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 23);
            this.btnOk.TabIndex = 92;
            this.btnOk.Text = "Apply Changes";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(1135, 254);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(80, 23);
            this.btnLoad.TabIndex = 93;
            this.btnLoad.Text = "Open Sheet";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1135, 167);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 23);
            this.button2.TabIndex = 66;
            this.button2.Text = "Move Down";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1135, 138);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 65;
            this.button1.Text = "Move Up";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnAutoType
            // 
            this.btnAutoType.Location = new System.Drawing.Point(1135, 196);
            this.btnAutoType.Name = "btnAutoType";
            this.btnAutoType.Size = new System.Drawing.Size(80, 23);
            this.btnAutoType.TabIndex = 64;
            this.btnAutoType.Text = "Update Type";
            this.btnAutoType.UseVisualStyleBackColor = true;
            this.btnAutoType.Click += new System.EventHandler(this.btnAuotType_Click);
            // 
            // btnSheetTemplates
            // 
            this.btnSheetTemplates.Location = new System.Drawing.Point(1135, 225);
            this.btnSheetTemplates.Name = "btnSheetTemplates";
            this.btnSheetTemplates.Size = new System.Drawing.Size(80, 23);
            this.btnSheetTemplates.TabIndex = 63;
            this.btnSheetTemplates.Text = "Templates";
            this.btnSheetTemplates.UseVisualStyleBackColor = true;
            this.btnSheetTemplates.Click += new System.EventHandler(this.btnSheetTemplates_Click);
            // 
            // ctlDrawings
            // 
            this.ctlDrawings.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ctlDrawings.ContextMenuStrip = this.contextMenu;
            this.ctlDrawings.Location = new System.Drawing.Point(14, 22);
            this.ctlDrawings.Name = "ctlDrawings";
            this.ctlDrawings.Size = new System.Drawing.Size(1112, 338);
            this.ctlDrawings.TabIndex = 48;
            this.ctlDrawings.DrawingChanged += new System.EventHandler(this.ctlDrawings_DrawingChanged);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSelectAll});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(123, 26);
            // 
            // mnuSelectAll
            // 
            this.mnuSelectAll.Name = "mnuSelectAll";
            this.mnuSelectAll.Size = new System.Drawing.Size(122, 22);
            this.mnuSelectAll.Text = "Select All";
            this.mnuSelectAll.Click += new System.EventHandler(this.mnuSelectAll_Click);
            // 
            // btnReNumberDrawings
            // 
            this.btnReNumberDrawings.Location = new System.Drawing.Point(1135, 109);
            this.btnReNumberDrawings.Name = "btnReNumberDrawings";
            this.btnReNumberDrawings.Size = new System.Drawing.Size(80, 23);
            this.btnReNumberDrawings.TabIndex = 62;
            this.btnReNumberDrawings.Text = "Re-Number";
            this.btnReNumberDrawings.UseVisualStyleBackColor = true;
            this.btnReNumberDrawings.Click += new System.EventHandler(this.btnReNumberDrawings_Click);
            // 
            // btnAutoBuild
            // 
            this.btnAutoBuild.Location = new System.Drawing.Point(1135, 22);
            this.btnAutoBuild.Name = "btnAutoBuild";
            this.btnAutoBuild.Size = new System.Drawing.Size(80, 23);
            this.btnAutoBuild.TabIndex = 44;
            this.btnAutoBuild.Text = "Add";
            this.btnAutoBuild.UseVisualStyleBackColor = true;
            this.btnAutoBuild.Click += new System.EventHandler(this.btnAutoBuild_Click);
            // 
            // btnDuplicateDrawing
            // 
            this.btnDuplicateDrawing.Location = new System.Drawing.Point(1135, 80);
            this.btnDuplicateDrawing.Name = "btnDuplicateDrawing";
            this.btnDuplicateDrawing.Size = new System.Drawing.Size(80, 23);
            this.btnDuplicateDrawing.TabIndex = 41;
            this.btnDuplicateDrawing.Text = "Duplicate";
            this.btnDuplicateDrawing.UseVisualStyleBackColor = true;
            this.btnDuplicateDrawing.Click += new System.EventHandler(this.btnDuplicateDrawing_Click);
            // 
            // btnDeleteDrawing
            // 
            this.btnDeleteDrawing.Location = new System.Drawing.Point(1135, 51);
            this.btnDeleteDrawing.Name = "btnDeleteDrawing";
            this.btnDeleteDrawing.Size = new System.Drawing.Size(80, 23);
            this.btnDeleteDrawing.TabIndex = 40;
            this.btnDeleteDrawing.Text = "Delete";
            this.btnDeleteDrawing.UseVisualStyleBackColor = true;
            this.btnDeleteDrawing.Click += new System.EventHandler(this.btnDeleteDrawing_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 49;
            this.label1.Text = "Search Drawings";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(271, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 89;
            this.label5.Text = "Discipline";
            // 
            // cboDiscipline
            // 
            this.cboDiscipline.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscipline.FormattingEnabled = true;
            this.cboDiscipline.Location = new System.Drawing.Point(274, 39);
            this.cboDiscipline.Name = "cboDiscipline";
            this.cboDiscipline.Size = new System.Drawing.Size(225, 21);
            this.cboDiscipline.TabIndex = 90;
            this.cboDiscipline.SelectedIndexChanged += new System.EventHandler(this.cboDiscipline_SelectedIndexChanged);
            this.cboDiscipline.TextChanged += new System.EventHandler(this.cboDiscipline_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.ckShowSheets);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ctlSearch);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.cboDiscipline);
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1226, 72);
            this.groupBox1.TabIndex = 91;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(518, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 94;
            this.label2.Text = "Section";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(521, 39);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(225, 21);
            this.comboBox1.TabIndex = 95;
            // 
            // ckShowSheets
            // 
            this.ckShowSheets.AutoSize = true;
            this.ckShowSheets.Location = new System.Drawing.Point(770, 43);
            this.ckShowSheets.Name = "ckShowSheets";
            this.ckShowSheets.Size = new System.Drawing.Size(132, 17);
            this.ckShowSheets.TabIndex = 93;
            this.ckShowSheets.Text = "Show Sheets in Model";
            this.ckShowSheets.UseVisualStyleBackColor = true;
            this.ckShowSheets.CheckedChanged += new System.EventHandler(this.ckShowSheets_CheckedChanged);
            // 
            // ctlSearch
            // 
            this.ctlSearch.Location = new System.Drawing.Point(14, 39);
            this.ctlSearch.Name = "ctlSearch";
            this.ctlSearch.Search = "";
            this.ctlSearch.Size = new System.Drawing.Size(225, 24);
            this.ctlSearch.TabIndex = 48;
            this.ctlSearch.SearchChanged += new System.EventHandler(this.ctlSearch_SearchChanged);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1159, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 91;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.button3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 607);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1244, 50);
            this.panel1.TabIndex = 107;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1244, 92);
            this.panel2.TabIndex = 108;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SlateGray;
            this.panel3.Controls.Add(this.ctlDrawingEditor);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 92);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1244, 92);
            this.panel3.TabIndex = 109;
            // 
            // ctlDrawingEditor
            // 
            this.ctlDrawingEditor.BackColor = System.Drawing.Color.LightSlateGray;
            this.ctlDrawingEditor.Location = new System.Drawing.Point(8, 6);
            this.ctlDrawingEditor.Name = "ctlDrawingEditor";
            this.ctlDrawingEditor.Size = new System.Drawing.Size(1226, 76);
            this.ctlDrawingEditor.TabIndex = 51;
            this.ctlDrawingEditor.DrawingsUpdated += new System.EventHandler(this.ctlDrawingEditor_DrawingsUpdated);
            this.ctlDrawingEditor.Load += new System.EventHandler(this.ctlDrawingEditor_Load);
            // 
            // frmManageDrawings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1244, 657);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmManageDrawings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manage Drawings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmManageDrawings_FormClosing);
            this.Load += new System.EventHandler(this.frmManageDrawings_Load);
            this.groupBox3.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private Controls.Views.ctlDrawings ctlDrawings;
        private System.Windows.Forms.Button btnReNumberDrawings;
        private System.Windows.Forms.Button btnAutoBuild;
        private System.Windows.Forms.Button btnDuplicateDrawing;
        private System.Windows.Forms.Button btnDeleteDrawing;
        private Controls.ctlSearch ctlSearch;
        private System.Windows.Forms.Label label1;
        private Controls.Parts.ctlDrawingEditor ctlDrawingEditor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboDiscipline;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuSelectAll;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox ckShowSheets;
        private System.Windows.Forms.Button btnSheetTemplates;
        private System.Windows.Forms.Button btnAutoType;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}