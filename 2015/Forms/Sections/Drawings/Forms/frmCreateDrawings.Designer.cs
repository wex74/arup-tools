﻿namespace ViewCreator.Forms.Add_Edit_Forms
{
    partial class frmCreateDrawings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numRoof = new System.Windows.Forms.NumericUpDown();
            this.txtRoof = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.numPlant = new System.Windows.Forms.NumericUpDown();
            this.txtPlant = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numMezz = new System.Windows.Forms.NumericUpDown();
            this.txtMezz = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.numGround = new System.Windows.Forms.NumericUpDown();
            this.txtGround = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numLevel = new System.Windows.Forms.NumericUpDown();
            this.numBasements = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLevels = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBasements = new System.Windows.Forms.NumericUpDown();
            this.txtSections = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSchedules = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSchematics = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDetails = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numTitle = new System.Windows.Forms.NumericUpDown();
            this.numCover = new System.Windows.Forms.NumericUpDown();
            this.ckAddCover = new System.Windows.Forms.CheckBox();
            this.ckAddTitle = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPrefix = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboDiscipline = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ckAll = new System.Windows.Forms.CheckBox();
            this.lstScopeboxes = new System.Windows.Forms.CheckedListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtSuffix = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboSheetTemplate = new System.Windows.Forms.ComboBox();
            this.btnViewDrawings = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.numSections = new System.Windows.Forms.NumericUpDown();
            this.numSchedules = new System.Windows.Forms.NumericUpDown();
            this.numSchematics = new System.Windows.Forms.NumericUpDown();
            this.numDetails = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.numElevations = new System.Windows.Forms.NumericUpDown();
            this.txtElevations = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRoof)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRoof)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMezz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMezz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGround)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGround)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBasements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLevels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBasements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSchematics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetails)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCover)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSchedules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSchematics)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDetails)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numElevations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtElevations)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOk.Location = new System.Drawing.Point(379, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 81;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button3.Location = new System.Drawing.Point(460, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 80;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.numRoof);
            this.groupBox2.Controls.Add(this.txtRoof);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.numPlant);
            this.groupBox2.Controls.Add(this.txtPlant);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.numMezz);
            this.groupBox2.Controls.Add(this.txtMezz);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.numGround);
            this.groupBox2.Controls.Add(this.txtGround);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.numLevel);
            this.groupBox2.Controls.Add(this.numBasements);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtLevels);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtBasements);
            this.groupBox2.Location = new System.Drawing.Point(288, 135);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(246, 192);
            this.groupBox2.TabIndex = 85;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Levels";
            // 
            // numRoof
            // 
            this.numRoof.Location = new System.Drawing.Point(137, 156);
            this.numRoof.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numRoof.Name = "numRoof";
            this.numRoof.Size = new System.Drawing.Size(95, 20);
            this.numRoof.TabIndex = 36;
            this.numRoof.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // txtRoof
            // 
            this.txtRoof.Location = new System.Drawing.Point(20, 156);
            this.txtRoof.Name = "txtRoof";
            this.txtRoof.Size = new System.Drawing.Size(40, 20);
            this.txtRoof.TabIndex = 34;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(66, 160);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 35;
            this.label12.Text = "Roof";
            // 
            // numPlant
            // 
            this.numPlant.Location = new System.Drawing.Point(137, 130);
            this.numPlant.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPlant.Name = "numPlant";
            this.numPlant.Size = new System.Drawing.Size(95, 20);
            this.numPlant.TabIndex = 33;
            this.numPlant.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // txtPlant
            // 
            this.txtPlant.Location = new System.Drawing.Point(20, 130);
            this.txtPlant.Name = "txtPlant";
            this.txtPlant.Size = new System.Drawing.Size(40, 20);
            this.txtPlant.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(66, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Plant";
            // 
            // numMezz
            // 
            this.numMezz.Location = new System.Drawing.Point(137, 104);
            this.numMezz.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numMezz.Name = "numMezz";
            this.numMezz.Size = new System.Drawing.Size(95, 20);
            this.numMezz.TabIndex = 30;
            this.numMezz.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // txtMezz
            // 
            this.txtMezz.Location = new System.Drawing.Point(20, 104);
            this.txtMezz.Name = "txtMezz";
            this.txtMezz.Size = new System.Drawing.Size(40, 20);
            this.txtMezz.TabIndex = 28;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(66, 108);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Mezzanine";
            // 
            // numGround
            // 
            this.numGround.Location = new System.Drawing.Point(137, 52);
            this.numGround.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numGround.Name = "numGround";
            this.numGround.Size = new System.Drawing.Size(95, 20);
            this.numGround.TabIndex = 27;
            this.numGround.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // txtGround
            // 
            this.txtGround.Location = new System.Drawing.Point(20, 52);
            this.txtGround.Name = "txtGround";
            this.txtGround.Size = new System.Drawing.Size(40, 20);
            this.txtGround.TabIndex = 25;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(66, 56);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "Ground";
            // 
            // numLevel
            // 
            this.numLevel.Location = new System.Drawing.Point(137, 78);
            this.numLevel.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numLevel.Name = "numLevel";
            this.numLevel.Size = new System.Drawing.Size(95, 20);
            this.numLevel.TabIndex = 18;
            this.numLevel.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // numBasements
            // 
            this.numBasements.Location = new System.Drawing.Point(137, 26);
            this.numBasements.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numBasements.Name = "numBasements";
            this.numBasements.Size = new System.Drawing.Size(95, 20);
            this.numBasements.TabIndex = 19;
            this.numBasements.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Basements";
            // 
            // txtLevels
            // 
            this.txtLevels.Location = new System.Drawing.Point(20, 78);
            this.txtLevels.Name = "txtLevels";
            this.txtLevels.Size = new System.Drawing.Size(40, 20);
            this.txtLevels.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(66, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Levels";
            // 
            // txtBasements
            // 
            this.txtBasements.Location = new System.Drawing.Point(20, 26);
            this.txtBasements.Name = "txtBasements";
            this.txtBasements.Size = new System.Drawing.Size(40, 20);
            this.txtBasements.TabIndex = 3;
            // 
            // txtSections
            // 
            this.txtSections.Location = new System.Drawing.Point(20, 52);
            this.txtSections.Name = "txtSections";
            this.txtSections.Size = new System.Drawing.Size(40, 20);
            this.txtSections.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(66, 56);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Sections";
            // 
            // txtSchedules
            // 
            this.txtSchedules.Location = new System.Drawing.Point(20, 106);
            this.txtSchedules.Name = "txtSchedules";
            this.txtSchedules.Size = new System.Drawing.Size(40, 20);
            this.txtSchedules.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(66, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Scheules";
            // 
            // txtSchematics
            // 
            this.txtSchematics.Location = new System.Drawing.Point(20, 26);
            this.txtSchematics.Name = "txtSchematics";
            this.txtSchematics.Size = new System.Drawing.Size(40, 20);
            this.txtSchematics.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(66, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Schematics";
            // 
            // txtDetails
            // 
            this.txtDetails.Location = new System.Drawing.Point(20, 80);
            this.txtDetails.Name = "txtDetails";
            this.txtDetails.Size = new System.Drawing.Size(40, 20);
            this.txtDetails.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Details";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numTitle);
            this.groupBox1.Controls.Add(this.numCover);
            this.groupBox1.Controls.Add(this.ckAddCover);
            this.groupBox1.Controls.Add(this.ckAddTitle);
            this.groupBox1.Location = new System.Drawing.Point(12, 135);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 98);
            this.groupBox1.TabIndex = 84;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cover Drawings";
            // 
            // numTitle
            // 
            this.numTitle.Location = new System.Drawing.Point(141, 56);
            this.numTitle.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numTitle.Name = "numTitle";
            this.numTitle.Size = new System.Drawing.Size(95, 20);
            this.numTitle.TabIndex = 25;
            this.numTitle.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // numCover
            // 
            this.numCover.Location = new System.Drawing.Point(141, 29);
            this.numCover.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numCover.Name = "numCover";
            this.numCover.Size = new System.Drawing.Size(95, 20);
            this.numCover.TabIndex = 24;
            this.numCover.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // ckAddCover
            // 
            this.ckAddCover.AutoSize = true;
            this.ckAddCover.Location = new System.Drawing.Point(19, 31);
            this.ckAddCover.Name = "ckAddCover";
            this.ckAddCover.Size = new System.Drawing.Size(107, 17);
            this.ckAddCover.TabIndex = 15;
            this.ckAddCover.Text = "Add Cover Sheet";
            this.ckAddCover.UseVisualStyleBackColor = true;
            // 
            // ckAddTitle
            // 
            this.ckAddTitle.AutoSize = true;
            this.ckAddTitle.Location = new System.Drawing.Point(19, 58);
            this.ckAddTitle.Name = "ckAddTitle";
            this.ckAddTitle.Size = new System.Drawing.Size(99, 17);
            this.ckAddTitle.TabIndex = 5;
            this.ckAddTitle.Text = "Add Title Sheet";
            this.ckAddTitle.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 83;
            this.label4.Text = "Drawing No Prefix";
            // 
            // txtPrefix
            // 
            this.txtPrefix.Location = new System.Drawing.Point(121, 65);
            this.txtPrefix.Name = "txtPrefix";
            this.txtPrefix.Size = new System.Drawing.Size(134, 20);
            this.txtPrefix.TabIndex = 82;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 87;
            this.label5.Text = "Discipline Name";
            // 
            // cboDiscipline
            // 
            this.cboDiscipline.FormattingEnabled = true;
            this.cboDiscipline.Location = new System.Drawing.Point(121, 26);
            this.cboDiscipline.Name = "cboDiscipline";
            this.cboDiscipline.Size = new System.Drawing.Size(134, 21);
            this.cboDiscipline.TabIndex = 88;
            this.cboDiscipline.SelectedIndexChanged += new System.EventHandler(this.cboDiscipline_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ckAll);
            this.groupBox3.Controls.Add(this.lstScopeboxes);
            this.groupBox3.Location = new System.Drawing.Point(12, 242);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(270, 258);
            this.groupBox3.TabIndex = 90;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Scope Boxes";
            // 
            // ckAll
            // 
            this.ckAll.AutoSize = true;
            this.ckAll.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckAll.Location = new System.Drawing.Point(172, 235);
            this.ckAll.Name = "ckAll";
            this.ckAll.Size = new System.Drawing.Size(74, 17);
            this.ckAll.TabIndex = 90;
            this.ckAll.Text = "Check All ";
            this.ckAll.UseVisualStyleBackColor = true;
            this.ckAll.CheckedChanged += new System.EventHandler(this.ckAll_CheckedChanged);
            // 
            // lstScopeboxes
            // 
            this.lstScopeboxes.CheckOnClick = true;
            this.lstScopeboxes.FormattingEnabled = true;
            this.lstScopeboxes.Location = new System.Drawing.Point(22, 31);
            this.lstScopeboxes.Name = "lstScopeboxes";
            this.lstScopeboxes.Size = new System.Drawing.Size(224, 199);
            this.lstScopeboxes.TabIndex = 71;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtSuffix);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.cboSheetTemplate);
            this.groupBox4.Controls.Add(this.txtPrefix);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.cboDiscipline);
            this.groupBox4.Location = new System.Drawing.Point(12, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(522, 119);
            this.groupBox4.TabIndex = 91;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Drawing Naming";
            // 
            // txtSuffix
            // 
            this.txtSuffix.Location = new System.Drawing.Point(121, 90);
            this.txtSuffix.Name = "txtSuffix";
            this.txtSuffix.Size = new System.Drawing.Size(134, 20);
            this.txtSuffix.TabIndex = 90;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(261, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 95;
            this.label9.Text = "Sheet Template";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(19, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 91;
            this.label8.Text = "Drawing No Suffix";
            // 
            // cboSheetTemplate
            // 
            this.cboSheetTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSheetTemplate.FormattingEnabled = true;
            this.cboSheetTemplate.Location = new System.Drawing.Point(349, 24);
            this.cboSheetTemplate.Name = "cboSheetTemplate";
            this.cboSheetTemplate.Size = new System.Drawing.Size(162, 21);
            this.cboSheetTemplate.TabIndex = 94;
            // 
            // btnViewDrawings
            // 
            this.btnViewDrawings.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnViewDrawings.Location = new System.Drawing.Point(8, 15);
            this.btnViewDrawings.Name = "btnViewDrawings";
            this.btnViewDrawings.Size = new System.Drawing.Size(164, 23);
            this.btnViewDrawings.TabIndex = 92;
            this.btnViewDrawings.Text = "Preview Drawing List";
            this.btnViewDrawings.UseVisualStyleBackColor = true;
            this.btnViewDrawings.Click += new System.EventHandler(this.btnViewDrawings_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReset.Location = new System.Drawing.Point(287, 15);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 93;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // numSections
            // 
            this.numSections.Location = new System.Drawing.Point(137, 53);
            this.numSections.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numSections.Name = "numSections";
            this.numSections.Size = new System.Drawing.Size(95, 20);
            this.numSections.TabIndex = 23;
            this.numSections.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // numSchedules
            // 
            this.numSchedules.Location = new System.Drawing.Point(137, 106);
            this.numSchedules.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numSchedules.Name = "numSchedules";
            this.numSchedules.Size = new System.Drawing.Size(95, 20);
            this.numSchedules.TabIndex = 22;
            this.numSchedules.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // numSchematics
            // 
            this.numSchematics.Location = new System.Drawing.Point(137, 27);
            this.numSchematics.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numSchematics.Name = "numSchematics";
            this.numSchematics.Size = new System.Drawing.Size(95, 20);
            this.numSchematics.TabIndex = 21;
            this.numSchematics.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // numDetails
            // 
            this.numDetails.Location = new System.Drawing.Point(137, 80);
            this.numDetails.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numDetails.Name = "numDetails";
            this.numDetails.Size = new System.Drawing.Size(95, 20);
            this.numDetails.TabIndex = 20;
            this.numDetails.Value = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.numElevations);
            this.groupBox5.Controls.Add(this.txtElevations);
            this.groupBox5.Controls.Add(this.txtDetails);
            this.groupBox5.Controls.Add(this.txtSchematics);
            this.groupBox5.Controls.Add(this.numSections);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.numSchedules);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.txtSchedules);
            this.groupBox5.Controls.Add(this.numSchematics);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.txtSections);
            this.groupBox5.Controls.Add(this.numDetails);
            this.groupBox5.Location = new System.Drawing.Point(290, 333);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(246, 167);
            this.groupBox5.TabIndex = 96;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Details Sections";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(66, 136);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Elevations";
            // 
            // numElevations
            // 
            this.numElevations.Location = new System.Drawing.Point(137, 132);
            this.numElevations.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numElevations.Name = "numElevations";
            this.numElevations.Size = new System.Drawing.Size(95, 20);
            this.numElevations.TabIndex = 26;
            this.numElevations.Value = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            // 
            // txtElevations
            // 
            this.txtElevations.Location = new System.Drawing.Point(20, 132);
            this.txtElevations.Name = "txtElevations";
            this.txtElevations.Size = new System.Drawing.Size(40, 20);
            this.txtElevations.TabIndex = 24;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.btnViewDrawings);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 512);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(546, 50);
            this.panel1.TabIndex = 107;
            // 
            // frmCreateDrawings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(546, 562);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmCreateDrawings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Drawings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCreateDrawings_FormClosing);
            this.Load += new System.EventHandler(this.frmNewSheets_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRoof)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRoof)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPlant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMezz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMezz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGround)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGround)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBasements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLevels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBasements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSchematics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetails)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCover)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSchedules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSchematics)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDetails)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numElevations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtElevations)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown txtLevels;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txtBasements;
        private System.Windows.Forms.NumericUpDown txtDetails;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckAddTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPrefix;
        private System.Windows.Forms.NumericUpDown txtSchedules;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown txtSchematics;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox ckAddCover;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboDiscipline;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckedListBox lstScopeboxes;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnViewDrawings;
        private System.Windows.Forms.TextBox txtSuffix;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown txtSections;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox ckAll;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSheetTemplate;
        private System.Windows.Forms.NumericUpDown numLevel;
        private System.Windows.Forms.NumericUpDown numBasements;
        private System.Windows.Forms.NumericUpDown numSections;
        private System.Windows.Forms.NumericUpDown numSchedules;
        private System.Windows.Forms.NumericUpDown numSchematics;
        private System.Windows.Forms.NumericUpDown numDetails;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown numTitle;
        private System.Windows.Forms.NumericUpDown numCover;
        private System.Windows.Forms.NumericUpDown numRoof;
        private System.Windows.Forms.NumericUpDown txtRoof;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numPlant;
        private System.Windows.Forms.NumericUpDown txtPlant;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numMezz;
        private System.Windows.Forms.NumericUpDown txtMezz;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numGround;
        private System.Windows.Forms.NumericUpDown txtGround;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numElevations;
        private System.Windows.Forms.NumericUpDown txtElevations;
        private System.Windows.Forms.Panel panel1;
    }
}