﻿namespace ViewCreator.Forms.Manage_Forms
{
    partial class frmInsertView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInsertView));
            this.tvSheets = new System.Windows.Forms.TreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.tvViews = new System.Windows.Forms.TreeView();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.optThreeD = new System.Windows.Forms.RadioButton();
            this.optPlans = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.ckDualSearch = new System.Windows.Forms.CheckBox();
            this.ckHideSheets = new System.Windows.Forms.CheckBox();
            this.ctlSearchViews = new ViewCreator.Forms.Controls.ctlSearch();
            this.ctlSearchSheets = new ViewCreator.Forms.Controls.ctlSearch();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvSheets
            // 
            this.tvSheets.FullRowSelect = true;
            this.tvSheets.HideSelection = false;
            this.tvSheets.ImageIndex = 0;
            this.tvSheets.ImageList = this.imageList;
            this.tvSheets.Location = new System.Drawing.Point(12, 101);
            this.tvSheets.Name = "tvSheets";
            this.tvSheets.SelectedImageIndex = 0;
            this.tvSheets.ShowRootLines = false;
            this.tvSheets.Size = new System.Drawing.Size(350, 427);
            this.tvSheets.TabIndex = 0;
            this.tvSheets.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvSheets_BeforeCollapse);
            this.tvSheets.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvSheets_BeforeExpand);
            this.tvSheets.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvSheets_AfterSelect);
            this.tvSheets.DoubleClick += new System.EventHandler(this.tvSheets_DoubleClick);
            this.tvSheets.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvSheets_MouseDown);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Electrical");
            this.imageList.Images.SetKeyName(1, "Fire");
            this.imageList.Images.SetKeyName(2, "Hydraulic");
            this.imageList.Images.SetKeyName(3, "Mechanical");
            this.imageList.Images.SetKeyName(4, "Structural");
            this.imageList.Images.SetKeyName(5, "Plan");
            this.imageList.Images.SetKeyName(6, "Architectural");
            this.imageList.Images.SetKeyName(7, "Coordination");
            this.imageList.Images.SetKeyName(8, "Scopebox");
            this.imageList.Images.SetKeyName(9, "Threed");
            this.imageList.Images.SetKeyName(10, "Section");
            this.imageList.Images.SetKeyName(11, "Schedule");
            this.imageList.Images.SetKeyName(12, "Sheet");
            // 
            // tvViews
            // 
            this.tvViews.FullRowSelect = true;
            this.tvViews.HideSelection = false;
            this.tvViews.ImageIndex = 0;
            this.tvViews.ImageList = this.imageList;
            this.tvViews.Location = new System.Drawing.Point(453, 101);
            this.tvViews.Name = "tvViews";
            this.tvViews.SelectedImageIndex = 0;
            this.tvViews.ShowRootLines = false;
            this.tvViews.Size = new System.Drawing.Size(350, 427);
            this.tvViews.TabIndex = 1;
            this.tvViews.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvViews_AfterSelect);
            this.tvViews.DoubleClick += new System.EventHandler(this.tvViews_DoubleClick);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(372, 101);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 2;
            this.btnAdd.Text = "<< Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.optThreeD);
            this.groupBox1.Controls.Add(this.optPlans);
            this.groupBox1.Location = new System.Drawing.Point(453, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 58);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Type";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(144, 25);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(66, 17);
            this.radioButton3.TabIndex = 8;
            this.radioButton3.Text = "Sections";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // optThreeD
            // 
            this.optThreeD.AutoSize = true;
            this.optThreeD.Location = new System.Drawing.Point(77, 25);
            this.optThreeD.Name = "optThreeD";
            this.optThreeD.Size = new System.Drawing.Size(61, 17);
            this.optThreeD.TabIndex = 7;
            this.optThreeD.Text = "ThreeD";
            this.optThreeD.UseVisualStyleBackColor = true;
            // 
            // optPlans
            // 
            this.optPlans.AutoSize = true;
            this.optPlans.Checked = true;
            this.optPlans.Location = new System.Drawing.Point(20, 25);
            this.optPlans.Name = "optPlans";
            this.optPlans.Size = new System.Drawing.Size(51, 17);
            this.optPlans.TabIndex = 6;
            this.optPlans.TabStop = true;
            this.optPlans.Text = "Plans";
            this.optPlans.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(643, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 64;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(724, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 63;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(372, 130);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 65;
            this.btnRemove.Text = "Remove >>";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // ckDualSearch
            // 
            this.ckDualSearch.AutoSize = true;
            this.ckDualSearch.Location = new System.Drawing.Point(20, 12);
            this.ckDualSearch.Name = "ckDualSearch";
            this.ckDualSearch.Size = new System.Drawing.Size(107, 17);
            this.ckDualSearch.TabIndex = 66;
            this.ckDualSearch.Text = "Use Dual Search";
            this.ckDualSearch.UseVisualStyleBackColor = true;
            this.ckDualSearch.CheckedChanged += new System.EventHandler(this.ckDualSearch_CheckedChanged);
            // 
            // ckHideSheets
            // 
            this.ckHideSheets.AutoSize = true;
            this.ckHideSheets.Checked = true;
            this.ckHideSheets.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckHideSheets.Location = new System.Drawing.Point(20, 35);
            this.ckHideSheets.Name = "ckHideSheets";
            this.ckHideSheets.Size = new System.Drawing.Size(137, 17);
            this.ckHideSheets.TabIndex = 67;
            this.ckHideSheets.Text = "Hide Sheets with Views";
            this.ckHideSheets.UseVisualStyleBackColor = true;
            this.ckHideSheets.CheckedChanged += new System.EventHandler(this.ckHideSheets_CheckedChanged);
            // 
            // ctlSearchViews
            // 
            this.ctlSearchViews.Location = new System.Drawing.Point(453, 71);
            this.ctlSearchViews.Name = "ctlSearchViews";
            this.ctlSearchViews.Search = "";
            this.ctlSearchViews.Size = new System.Drawing.Size(350, 24);
            this.ctlSearchViews.TabIndex = 4;
            this.ctlSearchViews.SearchChanged += new System.EventHandler(this.ctlSearchViews_SearchChanged);
            // 
            // ctlSearchSheets
            // 
            this.ctlSearchSheets.Location = new System.Drawing.Point(12, 71);
            this.ctlSearchSheets.Name = "ctlSearchSheets";
            this.ctlSearchSheets.Search = "";
            this.ctlSearchSheets.Size = new System.Drawing.Size(350, 24);
            this.ctlSearchSheets.TabIndex = 3;
            this.ctlSearchSheets.SearchChanged += new System.EventHandler(this.ctlSearchSheets_SearchChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 551);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(811, 50);
            this.panel1.TabIndex = 107;
            // 
            // frmInsertView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(811, 601);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ckHideSheets);
            this.Controls.Add(this.ckDualSearch);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ctlSearchViews);
            this.Controls.Add(this.ctlSearchSheets);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tvViews);
            this.Controls.Add(this.tvSheets);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmInsertView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Insert View";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmInsertView_FormClosing);
            this.Load += new System.EventHandler(this.frmInsertView_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView tvSheets;
        private System.Windows.Forms.TreeView tvViews;
        private System.Windows.Forms.Button btnAdd;
        private Controls.ctlSearch ctlSearchSheets;
        private Controls.ctlSearch ctlSearchViews;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton optThreeD;
        private System.Windows.Forms.RadioButton optPlans;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.CheckBox ckDualSearch;
        private System.Windows.Forms.CheckBox ckHideSheets;
        private System.Windows.Forms.Panel panel1;
    }
}