﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RevitManager_SRC.Classes;
using ViewCreator.Data.Forms;
using ViewCreator.Helpers;
using ViewCreator.Model.Drawings;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Sections.Drawings.Forms
{
    public partial class frmNewDrawing: Form
    {
        private NewDrawingData _data;
        private bool _loaded;
        private DrawingType _drawingType;
        private List<DrawingType> _drawingTypes;
        private List<Autodesk.Revit.DB.View> _views;
        private Autodesk.Revit.DB.View _view;
        private List<string> _disciplineNames; 

        public frmNewDrawing()
        {
            InitializeComponent();
        }

        private void frmNewDrawing_Load(object sender, EventArgs e)
        {
            _data = MainApp.AppData.FormSettings.NewDrawingData;

            _drawingTypes = App_Utils.GetDrawingTypes();
            _disciplineNames = Discipline_Utils.GetDisciplineNames();
            ckScopeboxes.Checked = _data.Scopeboxes;
            ckUseNextNo.Checked = _data.UseNextNo;
            ckAutoSelectView.Checked = _data.AutoSelectView;


            cboSheetTemplate.LoadCombo(Sheet_Utils.GetSheetTemplateNames(), _data.SheetTemplate, null);


            txtPrefix.Text = _data.Prefix;
            txtNumber.Text = _data.Number;
            txtSuffix.Text = _data.Suffix;

            cboTitle.Text = _data.Title;

            if (cboSheetTemplate.Items.Count > 0 && cboSheetTemplate.SelectedIndex == -1)
            {
                cboSheetTemplate.SelectedIndex = 0;
            }

            _loaded = true;

            cboDiscipline.LoadCombo(_disciplineNames, _data.Discipline, "");

            SetDataFromDrawingType();
            BuildViews();
        }

        private void cboDrawingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            SetDataFromDrawingType();
            BuildViews();
        }

        private void SetDataFromDrawingType()
        {
            if (cboDrawingType.SelectedIndex > -1)
            {
                _drawingType = _drawingTypes[cboDrawingType.SelectedIndex];

                if (_drawingType != null)
                {
                    txtNumber.Text = _drawingType.Number;


                    if (ckUseNextNo.Checked)
                    {
                        txtNumber.Text = Sheet_Utils.GetNextSheetNo(txtPrefix.Text , _drawingType.Number, txtSuffix.Text);
                    }

                    BuildTitles();

                    if (_drawingType.Number != "2000")
                    {
                        if (cboTitle.Items.Count > 0)
                        {
                            cboTitle.SelectedIndex = 0;
                        }
                    }
                }
            }
        }

        private void BuildTitles()
        {
            if (_drawingType != null)
            {
                cboTitle.Items.Clear();
                var titles = Drawing_Utils.GetDrawingTypeTitles(_drawingType, ckScopeboxes.Checked);
                cboTitle.Items.AddRange(titles.ToArray());

               

            }
            
        }

        private void cboDiscipline_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (cboDiscipline.Text != "")
            {
                var prefix = Discipline_Utils.GetDisciplinePrefix(cboDiscipline.Text);
                txtPrefix.Text = prefix;
                BuildDrawingTypes(prefix);
                BuildViews();
                BuildTitles();
            }
        }


        private void BuildDrawingTypes(string prefix)
        {

            cboDrawingType.LoadCombo(App_Utils.GetDrawingTypeNames(prefix), _data.DrawingType, null);
        }

        private void BuildViews()
        {
            if (!_loaded) return;

            cboView.Items.Clear();


            if (_drawingType != null)
            {
                if (_drawingType.Name == "Layouts")
                {
                    var plans = MainApp.AppData.ProjectPlans.GetPlans(Discipline_Utils.GetDiscipline(cboDiscipline.Text));
                    _views = plans.Select(x => x.GetView()).ToList();
                    
                    if (_views.Any())
                    {
                        _views = _views.Where(x => !x.View_IsOnSheet()).ToList();
                        cboView.Items.AddRange(_views.Select(x => x.ViewName).ToArray());

                        if (ckAutoSelectView.Checked)
                        {
                            if (_drawingType.Number == "2000")
                            {
                                if (cboView.Items.Count > 0)
                                {
                                    cboView.SelectedIndex = cboView.Items.Count - 1;
                                    cboView.Text = (string)cboView.SelectedItem;
                                }
                            }
                        }
                       
                        
                    }
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            var titleCheck = General_Utils.CleanInput(cboTitle.Text, Text_Utils.CharsToRemove);

            if (titleCheck != null && titleCheck != cboTitle.Text)
            {
                cboTitle.Text = titleCheck;
            }

            var numCheck = General_Utils.CleanInput(txtNumber.Text, Text_Utils.CharsToRemove);

            if (numCheck != null && numCheck != txtNumber.Text)
            {
                txtNumber.Text = numCheck;
            }



            if (txtNumber.Text == "")
            {
                MessageBox.Show("Number cannot be empty");
                return;
            }

            if (cboTitle.Text == "")
            {
                MessageBox.Show("Title cannot be empty");
                return;
            }

            if (cboSheetTemplate.Text == "")
            {
                MessageBox.Show("Sheet Template cannot be empty");
                return;
            }


            DialogResult = DialogResult.OK;
        }


        public string DrawingNo
        {
            get
            {
                var name = "";

                if (txtPrefix.Text != "") name = txtPrefix.Text;
                if (txtNumber.Text != "") name = name + txtNumber.Text;
                if (txtSuffix.Text != "") name = name + txtSuffix.Text;
                return name;
            }
        }

        public string Discipline    
        {
            get
            {
                return cboDiscipline.Text;
            }
        }

        public string DrawingType
        {
            get
            {
                return cboDrawingType.Text;
            }
        }

        public string SheetTemplate
        {
            get
            {
                return cboSheetTemplate.Text;
            }
        }

        public string Title
        {
            get
            {
                return cboTitle.Text;
            }
        }


        public Autodesk.Revit.DB.View GetView()
        {
            return _view;
        }

        private void frmNewDrawing_FormClosing(object sender, FormClosingEventArgs e)
        {
            _data.Scopeboxes = ckScopeboxes.Checked;
            _data.UseNextNo = ckUseNextNo.Checked;
            _data.AutoSelectView = ckAutoSelectView.Checked;

            _data.SheetTemplate = cboSheetTemplate.Text;
            _data.DrawingType = cboDrawingType.Text;

            _data.Discipline = cboDiscipline.Text;

           _data.Prefix = txtPrefix.Text;
            _data.Number = txtNumber.Text;
            _data.Suffix = txtSuffix.Text;

            _data.Title = cboTitle.Text;

            MainApp.AppData.UserModelData.Save();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ckScopeboxes_CheckedChanged(object sender, EventArgs e)
        {
            BuildTitles();
        }

        private void cboView_SelectedIndexChanged(object sender, EventArgs e)
        {
            _view = null;

            if (cboView.SelectedIndex > -1)
            {
                if (_views != null)
                {
                    _view = _views[cboView.SelectedIndex];

                    if (_view != null)
                    {
                        if (_view.GenLevel != null)
                        {
                            cboTitle.Text = _view.GenLevel.Name;
                        }
                        else
                        {
                            cboTitle.Text = _view.ViewName;
                        }
                    }
                    else
                    {
                        cboTitle.Text = "";
                    }
                    
                }
            }
        }

        private void cboTitle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
