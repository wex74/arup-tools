﻿namespace ViewCreator.Forms.Select_Forms
{
    partial class frmViewDrawings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.ctlViewDrawingsList = new ViewCreator.Forms.Controls.Views.ctlViewDrawingsList();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(1180, 429);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 62;
            this.btnOk.Text = "Close";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // ctlViewDrawingsList
            // 
            this.ctlViewDrawingsList.Location = new System.Drawing.Point(12, 12);
            this.ctlViewDrawingsList.Name = "ctlViewDrawingsList";
            this.ctlViewDrawingsList.Size = new System.Drawing.Size(1243, 402);
            this.ctlViewDrawingsList.TabIndex = 0;
            // 
            // frmViewDrawings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 464);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.ctlViewDrawingsList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmViewDrawings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "View Drawings";
            this.Load += new System.EventHandler(this.frmViewDrawings_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Views.ctlViewDrawingsList ctlViewDrawingsList;
        private System.Windows.Forms.Button btnOk;
    }
}