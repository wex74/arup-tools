﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Data.Forms;
using ViewCreator.Utils;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using Form = System.Windows.Forms.Form;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Manage_Forms
{
    public partial class frmInsertView : Form
    {
        private List<ViewSheet> _sheets;
        private List<View> _views;
        private ViewSheet _sheet;
        private View _view;
        private View _sheetView;
        private bool _loaded;

        private InsertViewsData _conttolsData;

        private bool blnDoubleClick;
        public Dictionary<ViewSheet, View> ViewSheets { get; set; }

        public frmInsertView()
        {
            InitializeComponent();
        }

        private void frmInsertView_Load(object sender, EventArgs e)
        {
            _conttolsData = MainApp.AppData.FormSettings.InsertViewsData;

            ViewSheets = new Dictionary<ViewSheet, View>();


            _sheets = Sheet_Utils.GetEmptySheets();

            if (MainApp.AppData.IsUserBuildings)
            {
                _views = View_Utils.GetViewsNotOnSheets(ViewType.FloorPlan);
            }

            if (MainApp.AppData.IsUserStructures)
            {
                _views = View_Utils.GetViewsNotOnSheets(ViewType.EngineeringPlan);
            }


            ckDualSearch.Checked = _conttolsData.UseDualSearch;


            ctlSearchSheets.LoadControl(_conttolsData.SheetSearch);
            ctlSearchViews.LoadControl(_conttolsData.ViewSearch);

            BuildSheets();
            BuildViews();

        }

        public bool ContainsView(ViewSheet sheet)
        {
            return ViewSheets.ContainsKey(sheet);
        }

        public void BuildSheets()
        {
            _loaded = false;

            var sheets = _sheets.Where(x => x.SheetName().Contains(ctlSearchSheets.Search, StringComparison.OrdinalIgnoreCase)).ToList();
            tvSheets.Nodes.Clear();

            foreach (var sheet in sheets)
            {
                if (ContainsView(sheet) && ckHideSheets.Checked)
                    continue;

                var sheetNode = tvSheets.Nodes.Add(sheet.SheetName());
                sheetNode.ImageKey = "Sheet";
                sheetNode.SelectedImageKey = "Sheet";
                sheetNode.Tag = sheet;

                if (_sheet != null)
                {
                    if (_sheet.Id == sheet.Id)
                    {
                        tvSheets.SelectedNode = sheetNode;
                        sheetNode.EnsureVisible();
                    }
                }
               

                if (ViewSheets.ContainsKey(sheet))
                {
                    var view = ViewSheets[sheet];

                    if (view != null)
                    {
                        var viewNode = sheetNode.Nodes.Add(view.ViewName);
                        viewNode.ImageKey = "Plan";
                        viewNode.SelectedImageKey = "Plan";
                        viewNode.Tag = view;


                        if (_sheetView != null)
                        {
                            if (_sheetView.Id == view.Id)
                            {
                                tvSheets.SelectedNode = viewNode;
                                viewNode.EnsureVisible();
                            }
                        }
                       
                    }

                }
            }


            tvSheets.SelectFirstTreeNode();
            tvSheets.ExpandAll();
            _loaded = true;

            EnableControls();


        }



        public void BuildViews()
        {
            _loaded = false;

            var views = _views.Where(x => x.ViewName.Contains(ctlSearchViews.Search, StringComparison.OrdinalIgnoreCase)).ToList();
            tvViews.Nodes.Clear();

            List<View> sheetViews = new List<View>(ViewSheets.Values);

            foreach (var view in views)
            {
                if (!sheetViews.Exists(x => x.Id == view.Id))
                {
                    var node = tvViews.Nodes.Add(view.ViewName);
                    node.ImageKey = "Plan";
                    node.SelectedImageKey = "Plan";
                    node.Tag = view;

                    if (_view != null)
                    {
                        if (_view.Id == view.Id)
                        {
                            tvViews.SelectedNode = node;
                            node.EnsureVisible();
                        }
                    }
                   

                }
                
            }


            tvViews.SelectFirstTreeNode();
            _loaded = true;

            EnableControls();


        }



        private void ctlSearchSheets_SearchChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (ckDualSearch.Checked)
            {
                _loaded = false;
                ctlSearchViews.LoadControl(ctlSearchSheets.Search);
                BuildSheetsAndViews();
                return;
            }

            BuildSheets();
        }

        private void ctlSearchViews_SearchChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (ckDualSearch.Checked)
            {
                _loaded = false;
                ctlSearchSheets.LoadControl(ctlSearchViews.Search);
                BuildSheetsAndViews();
                return;
            }

            BuildViews();
        }



        private void BuildSheetsAndViews()
        {
            BuildSheets();
            BuildViews();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (tvViews.SelectedNode != null && tvSheets.SelectedNode != null)
            {
                if (_sheet != null && _view != null)
                {
                    if (!ViewSheets.ContainsKey(_sheet))
                    {
                        ViewSheets.Add(_sheet, _view);
                        BuildSheetsAndViews();
                    }
                   
                }
               
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (tvSheets.SelectedNode != null)
            {
                var removed = false;

                if (tvSheets.SelectedNode.Level == 0)
                {
                    if (tvSheets.SelectedNode.Nodes.Count > 0)
                    {
                        if (_sheet != null)
                        {
                            ViewSheets.Remove(_sheet);
                            removed = true;
                        }
                    }
                }


                if (tvSheets.SelectedNode.Level == 1)
                {
                    if (_sheet != null)
                    {
                        ViewSheets.Remove(_sheet);
                        removed = true;
                    }

                    
                }

                if (removed)
                {
                    if (_sheetView != null)
                    {
                        _view = _sheetView;
                    }

                    BuildSheetsAndViews();
                }
            }
        }

        private void tvViews_DoubleClick(object sender, EventArgs e)
        {
            btnAdd_Click(null,null);
        }

        private void ckDualSearch_CheckedChanged(object sender, EventArgs e)
        {
            _loaded = false;

            if (ckDualSearch.Checked)
            {
                ctlSearchViews.LoadControl(ctlSearchSheets.Search);
            }
        
            BuildSheetsAndViews();
        }

        private void tvSheets_AfterSelect(object sender, TreeViewEventArgs e)
        {
            _sheet = null;
            _sheetView = null;

            if (tvSheets.SelectedNode != null)
            {
                if (tvSheets.SelectedNode.Level == 0)
                {
                    _sheet = (ViewSheet) tvSheets.SelectedNode.Tag;

                    if (tvSheets.SelectedNode.Nodes. Count > 0)
                    {
                        _sheetView = (View)tvSheets.SelectedNode.Nodes[0].Tag;
                    }
                }

                if (tvSheets.SelectedNode.Level == 1)
                {
                    _sheet = (ViewSheet)tvSheets.SelectedNode.Parent.Tag;
                    _sheetView = (View)tvSheets.SelectedNode.Tag;
                }
            }

            EnableControls();


        }

        private void tvViews_AfterSelect(object sender, TreeViewEventArgs e)
        {
            _view = null;

            if (tvViews.SelectedNode != null)
            {
                _view = (View)tvViews.SelectedNode.Tag;
            }

            EnableControls();


        }

        private void EnableControls()
        {
            if (tvSheets.Nodes.Count == 0)
            {
                btnAdd.Enabled = false;
                btnRemove.Enabled = false;

                return;
            }

            if (tvSheets.SelectedNode != null)
            {
                if (tvSheets.SelectedNode.Level == 0)
                {
                    btnRemove.Enabled = tvSheets.SelectedNode.Nodes.Count > 0;

                    if (tvSheets.SelectedNode.Nodes.Count == 0 && _view != null)
                    {
                        btnAdd.Enabled = true;
                    }
                    else
                    {
                        btnAdd.Enabled = false;
                    }   
                   
                }

                if (tvSheets.SelectedNode.Level == 1)
                {
                    btnRemove.Enabled = true;
                    btnAdd.Enabled = false;
                }

            }


           
        }

        private void tvSheets_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            if (!_loaded) return;

            if (blnDoubleClick && e.Action == TreeViewAction.Collapse)
                e.Cancel = true;
        }

        private void tvSheets_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (!_loaded) return;

            if (blnDoubleClick && e.Action == TreeViewAction.Expand)
                e.Cancel = true;
        }

        private void tvSheets_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_loaded) return;

            if (e.Clicks > 1)
                blnDoubleClick = true;
            else
                blnDoubleClick = false;
        }

        private void frmInsertView_FormClosing(object sender, FormClosingEventArgs e)
        {
            _conttolsData.UseDualSearch = ckDualSearch.Checked;
            _conttolsData.SheetSearch = ctlSearchSheets.Search;
            _conttolsData.ViewSearch = ctlSearchViews.Search;

            MainApp.AppData.UserModelData.Save();
        }

        private void tvSheets_DoubleClick(object sender, EventArgs e)
        {
            btnRemove_Click(null,null);
        }

        private void ckHideSheets_CheckedChanged(object sender, EventArgs e)
        {
            BuildSheetsAndViews();
        }
    }
}
