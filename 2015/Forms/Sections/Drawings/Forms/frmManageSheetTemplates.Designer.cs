﻿namespace ViewCreator.Forms.Manage_Forms
{
    partial class frmManageSheetTemplates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ctlSheetTemplates = new ViewCreator.Forms.Controls.Views.ctlSheetTemplates();
            this.ctlSearch = new ViewCreator.Forms.Controls.ctlSearch();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cboSheetTemplate = new System.Windows.Forms.ComboBox();
            this.btnSheetTemplate = new System.Windows.Forms.Button();
            this.cboDiscipline = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctlSheetTemplates
            // 
            this.ctlSheetTemplates.ContextMenuStrip = this.contextMenu;
            this.ctlSheetTemplates.Location = new System.Drawing.Point(15, 24);
            this.ctlSheetTemplates.Name = "ctlSheetTemplates";
            this.ctlSheetTemplates.Size = new System.Drawing.Size(560, 323);
            this.ctlSheetTemplates.TabIndex = 0;
            // 
            // ctlSearch
            // 
            this.ctlSearch.Location = new System.Drawing.Point(15, 40);
            this.ctlSearch.Name = "ctlSearch";
            this.ctlSearch.Search = "";
            this.ctlSearch.Size = new System.Drawing.Size(203, 24);
            this.ctlSearch.TabIndex = 1;
            this.ctlSearch.SearchChanged += new System.EventHandler(this.ctlSearch_SearchChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cboSheetTemplate);
            this.groupBox3.Controls.Add(this.btnSheetTemplate);
            this.groupBox3.Location = new System.Drawing.Point(12, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(594, 68);
            this.groupBox3.TabIndex = 78;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Edit Sheet Templates";
            // 
            // cboSheetTemplate
            // 
            this.cboSheetTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSheetTemplate.FormattingEnabled = true;
            this.cboSheetTemplate.Location = new System.Drawing.Point(15, 29);
            this.cboSheetTemplate.Name = "cboSheetTemplate";
            this.cboSheetTemplate.Size = new System.Drawing.Size(219, 21);
            this.cboSheetTemplate.TabIndex = 66;
            // 
            // btnSheetTemplate
            // 
            this.btnSheetTemplate.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnSheetTemplate.Location = new System.Drawing.Point(240, 29);
            this.btnSheetTemplate.Name = "btnSheetTemplate";
            this.btnSheetTemplate.Size = new System.Drawing.Size(22, 22);
            this.btnSheetTemplate.TabIndex = 47;
            this.btnSheetTemplate.Text = "ü";
            this.btnSheetTemplate.UseVisualStyleBackColor = true;
            this.btnSheetTemplate.Click += new System.EventHandler(this.btnSheetTemplate_Click);
            // 
            // cboDiscipline
            // 
            this.cboDiscipline.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDiscipline.FormattingEnabled = true;
            this.cboDiscipline.Location = new System.Drawing.Point(240, 43);
            this.cboDiscipline.Name = "cboDiscipline";
            this.cboDiscipline.Size = new System.Drawing.Size(209, 21);
            this.cboDiscipline.TabIndex = 92;
            this.cboDiscipline.SelectedIndexChanged += new System.EventHandler(this.cboDiscipline_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ctlSearch);
            this.groupBox1.Controls.Add(this.cboDiscipline);
            this.groupBox1.Location = new System.Drawing.Point(12, 82);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(594, 76);
            this.groupBox1.TabIndex = 93;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 96;
            this.label1.Text = "Search";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(240, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 95;
            this.label5.Text = "Discipline";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(531, 538);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 94;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ctlSheetTemplates);
            this.groupBox2.Location = new System.Drawing.Point(12, 164);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(594, 365);
            this.groupBox2.TabIndex = 95;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Drawings";
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSelectAll});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(123, 26);
            // 
            // mnuSelectAll
            // 
            this.mnuSelectAll.Name = "mnuSelectAll";
            this.mnuSelectAll.Size = new System.Drawing.Size(122, 22);
            this.mnuSelectAll.Text = "Select All";
            this.mnuSelectAll.Click += new System.EventHandler(this.mnuSelectAll_Click);
            // 
            // frmManageSheetTemplates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 570);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmManageSheetTemplates";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Sheet Templates";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmManageSheetTemplates_FormClosing);
            this.Load += new System.EventHandler(this.frmManageSheetTemplates_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Views.ctlSheetTemplates ctlSheetTemplates;
        private Controls.ctlSearch ctlSearch;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cboSheetTemplate;
        private System.Windows.Forms.Button btnSheetTemplate;
        private System.Windows.Forms.ComboBox cboDiscipline;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuSelectAll;
    }
}