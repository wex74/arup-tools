﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Forms.Controls.Views;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Manage_Forms
{
    public partial class frmManageSheetTemplates : Form
    {
        private List<Drawing> _drawings;
        private ManageSheetTemplatessData _formData;
        private bool _loaded;

        public frmManageSheetTemplates(List<Drawing > drawings )
        {
            
            InitializeComponent();

            _drawings = drawings;
        }


        private void frmManageSheetTemplates_Load(object sender, EventArgs e)
        {
            _formData = MainApp.AppData.FormSettings.ManageSheetTemplatessData;

            cboDiscipline.LoadCombo(Discipline_Utils.GetDisciplineNames(), _formData.Discipline, "All");
            cboSheetTemplate.LoadCombo(Sheet_Utils.GetSheetTemplateNames(), "", "");

            ctlSearch.Search = _formData.Search;

            LoadDrawings();
        }

        public List<Drawing> GetDrawings()
        {
            return _drawings;
        }



        private void btnSheetTemplate_Click(object sender, EventArgs e)
        {
            _drawings = ctlSheetTemplates.GetSelectedDrawings();
            if (!_drawings.Any()) return;

            foreach (var drawing in _drawings)
            {

                if (drawing != null)
                {
                    if (drawing.SheetTemplate != cboSheetTemplate.Text)
                    {
                        drawing.SheetTemplate = cboSheetTemplate.Text;
                    }

                }


            }


            ctlSheetTemplates.LoadControl(_drawings);
        }

        public List<Drawing> GetDrawings(string search, string discipline)
        {

            var drawings = _drawings;

            if (!string.IsNullOrEmpty(search))
            {
                drawings = drawings.Where(x => x.SearchText.Contains(search, StringComparison.OrdinalIgnoreCase)).ToList();
            }

            if (!string.IsNullOrEmpty(discipline))
            {
                if (discipline != "All")
                {
                    drawings = drawings.Where(x => x.SearchText.Contains(discipline, StringComparison.OrdinalIgnoreCase)).ToList();
                }
            }

            return drawings.OrderBy(x => x.GetFullName()).ToList();
        }


        private void LoadDrawings()
        {
            var drawings = GetDrawings(ctlSearch.Search, cboDiscipline.Text);

            _loaded = false;
            ctlSheetTemplates.LoadControl(drawings);

            _loaded = true;

        }

        private void frmManageSheetTemplates_FormClosing(object sender, FormClosingEventArgs e)
        {
            _formData.Discipline = cboDiscipline.Text;
            _formData.Search = ctlSearch.Search;

            MainApp.AppData.UserModelData.Save();
        }

        private void ctlSearch_SearchChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            LoadDrawings();
        }

        private void cboDiscipline_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            LoadDrawings();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void mnuSelectAll_Click(object sender, EventArgs e)
        {
            ctlSheetTemplates.SelectAll();
        }
    }
}
