﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Forms.Select_Forms;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using List_Utils = ViewCreator.Utils.UI.List_Utils;


namespace ViewCreator.Forms.Add_Edit_Forms
{
    public partial class frmCreateDrawings : Form
    {
        private CreateDrawingsData _formData;


        private int _numCover;

        public frmCreateDrawings()
        {
            InitializeComponent();
        }

        public List<Drawing> Drawings { get; set; }
     
 
        private bool UseScopeBoxes
        {
            get
            {
                if (lstScopeboxes.Items.Count == 0) return false;
                if (lstScopeboxes.Items.Count > 0)
                {
                    if (lstScopeboxes.CheckedItems.Count > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private void frmNewSheets_Load(object sender, EventArgs e)
        {
            _formData = MainApp.AppData.FormSettings.CreateDrawingsData;


            txtPrefix.Text = _formData.Prefix;
            txtSuffix.Text = _formData.Suffix;

            cboDiscipline.Text = _formData.DisciplineName;

            txtBasements.Value = _formData.BasementLevels;
            txtGround.Value = _formData.GroundLevels;
            txtMezz.Value = _formData.MezzanineLevels;
            txtLevels.Value = _formData.LevelLevels;
            txtPlant.Value = _formData.PlantLevels;
            txtRoof.Value = _formData.RoofLevels;

            numBasements.Value = _formData.BasementNumbering;
            numGround.Value = _formData.GroundNumbering;
            numMezz.Value = _formData.MezzanineNumbering;
            numLevel.Value = _formData.LevelsNumbering;
            numPlant.Value = _formData.PlantNumbering;
            numRoof.Value = _formData.RoofNumbering;

            txtDetails.Value = _formData.Details;
            txtSchematics.Value = _formData.Schematics;
            txtSchedules.Value = _formData.Schedules;
            txtSections.Value = _formData.Sections;
            txtElevations.Value = _formData.Elevations;

            numDetails.Value = _formData.DetailsNumbering;
            numSchematics.Value = _formData.SchematicsNumbering;
            numSchedules.Value = _formData.SchedulesNumbering;
            numSections.Value = _formData.SectionsNumbering;
            numElevations.Value = _formData.ElevationsNumbering;

            ckAddCover.Checked = _formData.AddCover;
            ckAddTitle.Checked = _formData.AddTitle;

            cboDiscipline.LoadCombo(Discipline_Utils.GetDisciplineServiceNames(), _formData.DisciplineName, "");
            lstScopeboxes.LoadAndSetList(Scopebox_Utils.GetScopeBoxNames(), _formData.Scopeboxes);
            cboSheetTemplate.LoadCombo(Sheet_Utils.GetSheetTemplateNames(), _formData.SheetTemplate, null);


            if (cboSheetTemplate.Items.Count > 0 && cboSheetTemplate.SelectedIndex == -1)
            {
                cboSheetTemplate.SelectedIndex = 0;
            }
        }

        private string GetDrawingNo(int number)
        {
            var prefix = txtPrefix.Text;
            var suffix = txtSuffix.Text;



            //if (ckNum0.Checked)
            //{
            //    if (!over99)
            //    {
            //        if (number < 10)
            //        {
            //            return prefix + "0" + number + suffix;
            //        }
            //    }
            //    else
            //    {
            //        if (number < 10)
            //        {
            //            return prefix + "00" + number + suffix;
            //        }


            //        if (number > 9 && number < 100)
            //        {
            //            return prefix + "0" + number + suffix;
            //        }
            //    }
            //}


            return prefix + number + suffix;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            GetDrawings();


            DialogResult = DialogResult.OK;
        }


        private void GetDrawings()
        {

            Drawings = new List<Drawing>();
            var drawingNo = 0;
            var drawingSectionNo = 0;

            var discipline = cboDiscipline.Text;
            var sheetTemplate = cboSheetTemplate.Text;



            drawingNo = 0;

            if (ckAddCover.Checked)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int) numCover.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);

                drawing.Title1 = discipline;
                drawing.Title2 = "Cover Sheet";
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;
            }


            if (ckAddTitle.Checked)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numTitle.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);

                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Title Sheet";
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;
            }

            drawingNo = 0;


            for (var i = 1; i < txtBasements.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numBasements.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Basement " + i;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;

                if (UseScopeBoxes)
                {
                    for (var x = 0; x < lstScopeboxes.Items.Count; x++)
                    {
                        if (lstScopeboxes.GetItemChecked(x))
                        {
                            drawing = new Drawing();

                            drawingSectionNo = (int)numBasements.Value + drawingNo;

                            var scopeBox = lstScopeboxes.GetItemText(lstScopeboxes.Items[x]);

                            drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                            drawing.Title1 = cboDiscipline.Text;
                            drawing.Title2 = "Basement " + i + " - " + scopeBox;
                            drawing.SheetTemplate = sheetTemplate;
                            drawing.Discipline = discipline;
                            drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                            Drawings.Add(drawing);

                            drawingNo++;
                        }
                    }
                }
            }


            for (var i = 1; i < txtGround.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numGround.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;

                if (i == 1)
                {
                    drawing.Title2 = "Ground Level";
                }
                else
                {
                    drawing.Title2 = "Ground Level " + i;
                }
               
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;

                if (UseScopeBoxes)
                {
                    for (var x = 0; x < lstScopeboxes.Items.Count; x++)
                    {
                        if (lstScopeboxes.GetItemChecked(x))
                        {
                            drawing = new Drawing();

                            drawingSectionNo = (int)numGround.Value + drawingNo;

                            var scopeBox = lstScopeboxes.GetItemText(lstScopeboxes.Items[x]);

                            drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                            drawing.Title1 = cboDiscipline.Text;
                            drawing.Title2 = "Ground Level" + " - " + scopeBox;
                            drawing.SheetTemplate = sheetTemplate;
                            drawing.Discipline = discipline;
                            drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                            Drawings.Add(drawing);

                            drawingNo++;
                        }
                    }
                }
            }


            for (var i = 1; i < txtLevels.Value + 1; i++)
            {
               

                if (UseScopeBoxes)
                {
                    for (var x = 0; x < lstScopeboxes.Items.Count; x++)
                    {
                        if (lstScopeboxes.GetItemChecked(x))
                        {
                            var drawing = new Drawing();

                            drawingSectionNo = (int)numLevel.Value + drawingNo;

                            var scopeBox = lstScopeboxes.GetItemText(lstScopeboxes.Items[x]);

                            drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                            drawing.Title1 = cboDiscipline.Text;
                            drawing.Title2 = "Level " + i + " - " + scopeBox;
                            drawing.SheetTemplate = sheetTemplate;
                            drawing.Discipline = discipline;
                            drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                            Drawings.Add(drawing);

                            drawingNo++;
                        }
                    }
                }
                else
                {
                    var drawing = new Drawing();

                    drawingSectionNo = (int)numLevel.Value + drawingNo;

                    drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                    drawing.Title1 = cboDiscipline.Text;
                    drawing.Title2 = "Level " + i;
                    drawing.SheetTemplate = sheetTemplate;
                    drawing.Discipline = discipline;
                    drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                    Drawings.Add(drawing);

                    drawingNo++;
                }
            }



            for (var i = 1; i < txtMezz.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numMezz.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Mezzanine Level";
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;

                if (UseScopeBoxes)
                {
                    for (var x = 0; x < lstScopeboxes.Items.Count; x++)
                    {
                        if (lstScopeboxes.GetItemChecked(x))
                        {
                            drawing = new Drawing();

                            drawingSectionNo = (int)numMezz.Value + drawingNo;

                            var scopeBox = lstScopeboxes.GetItemText(lstScopeboxes.Items[x]);

                            drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                            drawing.Title1 = cboDiscipline.Text;
                            drawing.Title2 = "Mezzanine Level" + " - " + scopeBox;
                            drawing.SheetTemplate = sheetTemplate;
                            drawing.Discipline = discipline;
                            drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                            Drawings.Add(drawing);

                            drawingNo++;
                        }
                    }
                }
            }


            for (var i = 1; i < txtPlant.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numPlant.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Plant Level " + i;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;

                if (UseScopeBoxes)
                {
                    for (var x = 0; x < lstScopeboxes.Items.Count; x++)
                    {
                        if (lstScopeboxes.GetItemChecked(x))
                        {
                            drawing = new Drawing();

                            drawingSectionNo = (int)numPlant.Value + drawingNo;

                            var scopeBox = lstScopeboxes.GetItemText(lstScopeboxes.Items[x]);

                            drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                            drawing.Title1 = cboDiscipline.Text;
                            drawing.Title2 = "Plant Level" + " - " + scopeBox;
                            drawing.SheetTemplate = sheetTemplate;
                            drawing.Discipline = discipline;
                            drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                            Drawings.Add(drawing);

                            drawingNo++;
                        }
                    }
                }
            }


            for (var i = 1; i < txtRoof.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numRoof.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Roof Level " + i;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;

                if (UseScopeBoxes)
                {
                    for (var x = 0; x < lstScopeboxes.Items.Count; x++)
                    {
                        if (lstScopeboxes.GetItemChecked(x))
                        {
                            drawing = new Drawing();

                            drawingSectionNo = (int)numRoof.Value + drawingNo;

                            var scopeBox = lstScopeboxes.GetItemText(lstScopeboxes.Items[x]);

                            drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                            drawing.Title1 = cboDiscipline.Text;
                            drawing.Title2 = "Roof Level" + " - " + scopeBox;
                            drawing.SheetTemplate = sheetTemplate;
                            drawing.Discipline = discipline;
                            drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                            Drawings.Add(drawing);

                            drawingNo++;
                        }
                    }
                }
            }

            drawingNo = 0;
            for (var i = 1; i < txtSchematics.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numSchematics.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Schematic Sheet " + i + " of " + txtSchematics.Value;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;
            }


            for (var i = 1; i < txtSections.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numSections.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Section Sheet " + i + " of " + txtSections.Value;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;
            }


            drawingNo = 0;
            for (var i = 1; i < txtDetails.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numDetails.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Detail Sheet " + i + " of " + txtDetails.Value;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;
            }


            drawingNo = 0;
            for (var i = 1; i < txtSchedules.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numSchedules.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Schedule Sheet " + i + " of " + txtSchedules.Value;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;
            }

            drawingNo = 0;
            for (var i = 1; i < txtElevations.Value + 1; i++)
            {
                var drawing = new Drawing();

                drawingSectionNo = (int)numElevations.Value + drawingNo;

                drawing.DrawingNo = GetDrawingNo(drawingSectionNo);
                drawing.Title1 = cboDiscipline.Text;
                drawing.Title2 = "Elevation Sheet " + i + " of " + txtElevations.Value;
                drawing.SheetTemplate = sheetTemplate;
                drawing.Discipline = discipline;
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
                Drawings.Add(drawing);

                drawingNo++;
            }

            //if (Drawings.Count > 99)
            //{
            //    var count = 0;
            //    foreach (var drawing in Drawings)
            //    {
            //        drawing.DrawingNo = GetDrawingNo(count, true);
            //        count++;
            //    }
            //}
        }

        private void btnViewDrawings_Click(object sender, EventArgs e)
        {
            GetDrawings();
            var frm = new frmViewDrawings(Drawings);
            frm.ShowDialog();
        }

        private void frmCreateDrawings_FormClosing(object sender, FormClosingEventArgs e)
        {
            _formData.Prefix = txtPrefix.Text;
            _formData.Suffix = txtSuffix.Text;
            _formData.DisciplineName = cboDiscipline.Text;
            _formData.SheetTemplate = cboSheetTemplate.Text;


            _formData.BasementLevels = (int) txtBasements.Value;
            _formData.GroundLevels = (int)txtGround.Value;
            _formData.MezzanineLevels = (int)txtMezz.Value;
            _formData.LevelLevels = (int) txtLevels.Value;
            _formData.PlantLevels = (int)txtPlant.Value;
            _formData.RoofLevels = (int)txtRoof.Value;


            _formData.BasementNumbering = (int)numBasements.Value;
            _formData.GroundNumbering = (int)numGround.Value;
            _formData.MezzanineNumbering = (int)numMezz.Value;
            _formData.LevelsNumbering = (int)numLevel.Value;
            _formData.PlantNumbering = (int)numPlant.Value;
            _formData.RoofNumbering = (int)numRoof.Value;


            _formData.Details = (int) txtDetails.Value;
            _formData.Schematics = (int) txtSchematics.Value;
            _formData.Schedules = (int) txtSchedules.Value;
            _formData.Sections = (int) txtSections.Value;
            _formData.Elevations = (int)txtElevations.Value;

            _formData.DetailsNumbering = (int)numDetails.Value;
            _formData.SchematicsNumbering = (int)numSchematics.Value;
            _formData.SchedulesNumbering = (int)numSchedules.Value;
            _formData.SectionsNumbering= (int)numSections.Value;
            _formData.ElevationsNumbering = (int)numElevations.Value;



            _formData.AddCover = ckAddCover.Checked;
            _formData.AddTitle = ckAddTitle.Checked;


            _formData.Scopeboxes = List_Utils.GetCheckedListboxValues(lstScopeboxes);

            MainApp.AppData.UserModelData.Save();
        }

        private void cboDiscipline_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboDiscipline.Text != "")
            {
                txtPrefix.Text = cboDiscipline.Text.Substring(0, 1) + "-";
            }
        }

      

        private void ckAll_CheckedChanged(object sender, EventArgs e)
        {
            for (var x = 0; x < lstScopeboxes.Items.Count; x++)
            {
                lstScopeboxes.SetItemChecked(x, ckAll.Checked);
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {

            numCover.Text = "1000";
            numTitle.Text = "1000";

            txtBasements.Text = Level_Utils.GetLevelCount("Basement").ToString();
            txtGround.Text = Level_Utils.GetLevelCount("Ground").ToString();
            txtLevels.Text = Level_Utils.GetLevelCount("Level").ToString();
            txtMezz.Text = Level_Utils.GetLevelCount("Mezz").ToString();
            txtPlant.Text = Level_Utils.GetLevelCount("Plant").ToString();
            txtRoof.Text = Level_Utils.GetLevelCount("Roof").ToString();

            numBasements.Text = "2000";
            numGround.Text = "2000";
            numLevel.Text = "2000";
            numMezz.Text = "2000";
            numPlant.Text = "2000";
            numRoof.Text = "2000";


            txtDetails.Text = "0";
            txtSchematics.Text = "0";
            txtSections.Text = "0";
            txtSchedules.Text = "0";
            txtElevations.Text = "0";

            numSchematics.Text = "3000";
            numSections.Text = "3000";
            numDetails.Text = "4000";
            numSchedules.Text = "5000";
            numElevations.Text = "6000";

            ckAddCover.Checked = false;
            ckAddTitle.Checked = false;


            ckAll.Checked = false;

            for (var x = 0; x < lstScopeboxes.Items.Count; x++)
            {
                lstScopeboxes.SetItemChecked(x,false);
            }
        }

       
    }
}