﻿using System;
using System.Windows.Forms;
using ViewCreator.Data.Forms;

namespace ViewCreator.Forms.Add_Edit_Forms
{
    public partial class frmEditDrawingNo : Form
    {
        private EditDrawingNoData _formData;

        public frmEditDrawingNo()
        {
            InitializeComponent();
        }

        public string Prefix { get { return txtPrefix.Text; } }
        public string Suffix { get { return txtSuffix.Text; } }

        public int Number { get { return (int) numNumber.Value; } }
        private void frmDrawingNo_Load(object sender, EventArgs e)
        {
            _formData = MainApp.AppData.FormSettings.EditDrawingNoData;

            txtPrefix.Text = _formData.Prefix;
            txtSuffix.Text = _formData.Suffix;
            numNumber.Value = _formData.Number;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void frmEditDrawingNo_FormClosing(object sender, FormClosingEventArgs e)
        {
            _formData.Prefix = txtPrefix.Text;
            _formData.Suffix = txtSuffix.Text;
            _formData.Number = (int)numNumber.Value;

            MainApp.AppData.SaveUserModelData();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
