﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Data.Repositories;
using ViewCreator.Events;
using ViewCreator.Events.Drawings;
using ViewCreator.Events.Views;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Forms.Manage_Forms;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Main_Dialogs
{
    public partial class frmManageDrawings : Form
    {
        private ManageDrawingsData _formData;
        private List<Drawing> _drawings;
        private bool _loaded;
        
        public frmManageDrawings()
        {
            InitializeComponent();
        }

        private void btnAutoBuild_Click(object sender, EventArgs e)
        {
            var frm = new frmCreateDrawings();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                MainApp.AppData.ProjectDrawings.Add(frm.Drawings);

                LoadDrawings();
            }
        }

        private void frmManageDrawings_Load(object sender, EventArgs e)
        {

        }

        public void LoadForm()
        {
            _formData = MainApp.AppData.FormSettings.ManageDrawingsData;

            _drawings = MainApp.AppData.ProjectDrawings.GetDrawings();

            cboDiscipline.LoadCombo(Discipline_Utils.GetDisciplineNames(), _formData.Discipline, "All");
      
            ctlSearch.LoadControl(_formData.Search);

            ctlDrawingEditor.LoadControl(ctlDrawings);
            ckShowSheets.Checked = _formData.ShowSheets;

            LoadDrawings();

            _loaded = true;
        }


        public List<Drawing> GetDrawings()
        {
            return _drawings;
        }


        public List<Drawing> GetNewDrawings()
        {
            return _drawings.Where(x => x.IsNew).ToList();
        }

        public bool ExecuteUpdateDrawings()
        {
            var drawings = MainApp.AppData.ProjectDrawings.GetNewDrawings();
            drawings.AddRange(MainApp.AppData.ProjectDrawings.GetDrawings().Where(x => x.UpdateRequired()));
            return drawings.Any();
        }

        public List<Drawing> GetDrawings(string search, string discipline)
        {

            var drawings = GetNewDrawings();

            if (ckShowSheets.Checked)
            {
                drawings.AddRange(MainApp.AppData.ProjectDrawings.GetDrawings());
            }

            if (!string.IsNullOrEmpty(search))
            {
                drawings = drawings.Where(x => x.SearchText.Matches(search)).ToList();
            }

            if (!string.IsNullOrEmpty(discipline))
            {
                if (discipline != "All")
                {
                    drawings = drawings.Where(x => x.SearchText.Contains(discipline, StringComparison.OrdinalIgnoreCase)).ToList();
                }
            }


            return drawings.OrderBy(x => x.GetFullName()).ToList();
        } 





        private void LoadDrawings()
        {
            var drawings = GetDrawings(ctlSearch.Search, cboDiscipline.Text);

            ctlDrawings.LoadControl(drawings);
            _loaded = true;
        }

        private void ctlDrawings_DrawingChanged(object sender, EventArgs e)
        {

        }



        private void btnDeleteDrawing_Click(object sender, EventArgs e)
        {
            var drawings = ctlDrawings.GetSelected();

            foreach (var drawing in drawings)
            {
                if (drawing.IsNew)
                {
                    MainApp.AppData.ProjectDrawings.RemoveNew(drawing.Id.ToString());
                }
                else
                {
                    MainApp.AppData.ProjectDrawings.Remove(drawing.SheetId);
                }
                
            }

            LoadDrawings();
        }

        private void btnDuplicateDrawing_Click(object sender, EventArgs e)
        {
            var drawings = ctlDrawings.GetSelected();

            foreach (var drawing in drawings)
            {
                var duplicate = drawing.GetDuplicate();
                duplicate.SheetTemplate = Sheet_Utils.GetSheetTemplateNames().FirstOrDefault();
                MainApp.AppData.ProjectDrawings.Add(duplicate);
            }


            LoadDrawings();
        }

        private void btnAddDrawing_Click(object sender, EventArgs e)
        {
            //var drawing = new Drawing();
            //App.DataAccess.ModelsData.AddDrawing(drawing);

            //LoadDrawings();
            //ctlDrawings.SelectDrawing(drawing);
        }

        private void ctlSearch_SearchChanged(object sender, EventArgs e)
        {
             if (!_loaded) return;
           
            LoadDrawings();
        }

        private void cboDiscipline_SelectedIndexChanged(object sender, EventArgs e)
        {
             if (!_loaded) return;
            
            LoadDrawings();
        }

        private void cboDiscipline_TextChanged(object sender, EventArgs e)
        {
        }

        private void btnReNumberDrawings_Click(object sender, EventArgs e)
        {
            var drawings = ctlDrawings.GetSelected();

            var frm = new frmEditDrawingNo();

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                var number = frm.Number;

                foreach (var drawing in drawings)
                {
                    drawing.DrawingNo = Drawing_Utils.GetDrawingNo(frm.Prefix, number, frm.Suffix);
                    number++;
                }
            }

           LoadDrawings();
            ctlDrawings.SelectDrawings(drawings);
        }

        private void mnuSelectAll_Click(object sender, EventArgs e)
        {
            ctlDrawings.SelectAll();
        }

        private void ctlDrawingEditor_DrawingsUpdated(object sender, EventArgs e)
        {
             if (!_loaded) return;

            LoadDrawings();
            ctlDrawings.SelectDrawings((List<Drawing>) sender);
        }

      
        private void ctlDrawingEditor_Load(object sender, EventArgs e)
        {
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (ExecuteUpdateDrawings())
            {
                BuildDrawingsEvent.Run(ctlDrawings.GetSelected());
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void frmManageDrawings_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveFormData();
        }

        public void SaveFormData()
        {
            _formData.Discipline = cboDiscipline.Text;
            _formData.Search = ctlSearch.Search;
            _formData.Drawings = _drawings.Where(x => x.IsNew).ToList();
            _formData.ShowSheets = ckShowSheets.Checked;

            MainApp.AppData.UserModelData.Save();
        }

        private void ckShowSheets_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            LoadDrawings();
        }

        private void btnSheetTemplates_Click(object sender, EventArgs e)
        {
            var drawings = _drawings.Where(x => x.IsNew).ToList();
            var frm = new frmManageSheetTemplates(drawings);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                
            }
        }

        private void btnAuotType_Click(object sender, EventArgs e)
        {
            var drawings = ctlDrawings.GetSelected();

            foreach (var drawing in drawings)
            {
                drawing.DrawingType = Drawing_Utils.GetDrawingType(drawing);
            }

            LoadDrawings();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var drawing = ctlDrawings.SelectedDrawing();
            if (drawing != null)
            {
                var sheet = drawing.GetViewSheet();
                if (sheet != null)
                {
                    NavigateEvent.Run(sheet);
                }
               
            }
          
        }
    }
}