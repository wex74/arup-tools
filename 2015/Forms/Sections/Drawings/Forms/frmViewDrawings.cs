﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Model;

namespace ViewCreator.Forms.Select_Forms
{
    public partial class frmViewDrawings : Form
    {
        private List<Drawing> _drawings;
 
        public frmViewDrawings(List<Drawing> drawings)
        {
            InitializeComponent();
            _drawings = drawings;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmViewDrawings_Load(object sender, EventArgs e)
        {
            ctlViewDrawingsList.LoadControl(_drawings);
        }
    }
}
