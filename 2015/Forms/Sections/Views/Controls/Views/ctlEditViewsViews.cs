﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevAge.Drawing.VisualElements;
using SourceGrid;
using SourceGrid.Cells;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using ComboBox = SourceGrid.Cells.Editors.ComboBox;
using GridColumn = ViewCreator.Model.GridColumn;
using TextBox = SourceGrid.Cells.Editors.TextBox;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Controls.Views
{
    public partial class ctlEditViewsViews : UserControl
    {
        private List<GridColumn> _columns;
        private bool _loaded;
        private List<string> _scaleNames;
        private List<string> _scopeboxNames;
        private List<string> _templateNames;
        private List<UpdateView> _updateViews;

        public ctlEditViewsViews()
        {
            InitializeComponent();
        }


        private void ctlViewEditViews_Load(object sender, EventArgs e)
        {
            
        }


        public UpdateView GetUpdateView()
        {
            return GetSelectedUpdateViews().FirstOrDefault();
        }


        public List<UpdateView> GetSelectedUpdateViews()
        {
            var rows = grdViews.Selection.GetSelectionRegion().GetRowsIndex();
            return rows.Select(row => (UpdateView)grdViews.Rows[row].Tag).Where(x => x != null).ToList();
        }

        public List<UpdateView> GetUpdates()
        {
            return _updateViews.Where(x => x.IsChanged).ToList();
        }

        public void SelectAll()
        {
            var range = new Range(1, 1, grdViews.Rows.Count, grdViews.Columns.Count);

            grdViews.Selection.SelectRange(range, true);
        }


        public void ReloadControl(List<UpdateView> selectUpdates = null)
        {
            UpdateViews();
            BuildViews();

            if (selectUpdates != null)
            {
                SelectUpdateViews(selectUpdates);
            }
            
        }

        public void LoadControl(List<UpdateView> views)
        {
            _updateViews = views;

            ScreensUI.EditViewsViewsControl = this;

            _columns = new List<GridColumn>();
            _columns.Add(new GridColumn("Name", 400));
            _columns.Add(new GridColumn("Template", 200));
            _columns.Add(new GridColumn("Scopebox", 150));
            _columns.Add(new GridColumn("Scale", 100));


            foreach (var column in _columns)
            {
                column.Index = _columns.IndexOf(column);
            }

            _templateNames = Template_Utils.GetViewTemplateNames();
            _templateNames.Insert(0, "None");

            _scopeboxNames = Scopebox_Utils.GetScopeBoxNames();
            _scopeboxNames.Insert(0, "");

            _scaleNames = View_Utils.GetScaleNames();

            BuildViews();

            _loaded = true;
        }


        private void UpdateViews()
        {
            _updateViews = _updateViews.Where(x => x.GetView() != null).ToList();
            foreach (var view in _updateViews)
            {
                view.Reload();
            }
        }


        private void BuildViews()
        {
            _loaded = false;

            Grid_Utils.BuildColumns(grdViews, _columns);

            foreach (var view in _updateViews)
            {
                var col = 0;


                var viewNormal = new DrawingCellBackColor(view, Color.LightGreen, Color.White);


                var row = grdViews.RowsCount;
                grdViews.Rows.Insert(row);

                grdViews.Rows[row].Tag = view;

                var txtName = new TextBox(typeof (string));
                var cellName = grdViews[row, col] = new Cell(view.ViewName);
                cellName.Value = view.ViewName;
                cellName.Editor = txtName;
                cellName.View = viewNormal;
                txtName.EditableMode = EditableMode.Default;
                txtName.Control.TextChanged += View_NameChanged;
                col++;

                var cboTemplate = new ComboBox(typeof (string), _templateNames.ToArray(), true);
                var cellTempllate = grdViews[row, col] = new Cell(view.Template);
                cellTempllate.Value = view.Template;
                cellTempllate.Editor = cboTemplate;
                cboTemplate.Control.Sorted = false;
                cellTempllate.View = viewNormal;
                cboTemplate.Control.DropDownStyle = ComboBoxStyle.DropDownList;
                cboTemplate.EditableMode = EditableMode.SingleClick;
                cboTemplate.Control.SelectedIndexChanged += View_TemplateChanged;
                col++;

                var cboScopebox = new ComboBox(typeof (string), _scopeboxNames.ToArray(), true);
                var cellScopebox = grdViews[row, col] = new Cell(view.ScopeBox);
                cellScopebox.Value = view.ScopeBox;
                cellScopebox.Editor = cboScopebox;
                cboScopebox.Control.Sorted = false;
                cellScopebox.View = viewNormal;
                cboScopebox.Control.DropDownStyle = ComboBoxStyle.DropDownList;
                cboScopebox.EditableMode = EditableMode.SingleClick;
                cboScopebox.Control.SelectedIndexChanged += View_ScopeboxChanged;
                col++;

                var cboScale = new ComboBox(typeof (string), _scaleNames.ToArray(), true);
                var cellScale = grdViews[row, col] = new Cell(View_Utils.GetScale(view.Scale));
                cellScale.Value = View_Utils.GetScale(view.Scale);
                cellScale.Editor = cboScale;
                cboScale.Control.Sorted = false;
                cellScale.View = viewNormal;
                cboScale.Control.DropDownStyle = ComboBoxStyle.DropDownList;
                cboScale.EditableMode = EditableMode.SingleClick;
                cboScale.Control.SelectedIndexChanged += View_ScaleChanged;
            }


            _loaded = true;
        }


        private void View_TemplateChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            var thisBox = (System.Windows.Forms.ComboBox) sender;
            if (thisBox.Text == "") return;


            var row = grdViews.Selection.GetSelectionRegion().GetRowsIndex().First();
            var view = (UpdateView) grdViews.Rows[row].Tag;


            if (view.Template != thisBox.Text)
            {
                view.SetTemplate(thisBox.Text);
            }


        


        }


        private void View_ScopeboxChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            var thisBox = (System.Windows.Forms.ComboBox)sender;
            if (thisBox.Text == "") return;


            var row = grdViews.Selection.GetSelectionRegion().GetRowsIndex().First();
            var view = (UpdateView)grdViews.Rows[row].Tag;

            if (view.ScopeBox != thisBox.Text)
            {
                view.SetScopeBox(thisBox.Text);
            }
       

        }


        private void View_ScaleChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            var thisBox = (System.Windows.Forms.ComboBox) sender;
            if (thisBox.Text == "") return;


            var row = grdViews.Selection.GetSelectionRegion().GetRowsIndex().First();
            var view = (UpdateView) grdViews.Rows[row].Tag;


            if (view.Scale != View_Utils.GetScale(thisBox.Text))
            {
                view.SetScale(View_Utils.GetScale(thisBox.Text));
            }


          
        }


        private void View_NameChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            var thisBox = (System.Windows.Forms.TextBox) sender;
            if (thisBox.Text == "") return;


            var row = grdViews.Selection.GetSelectionRegion().GetRowsIndex().First();
            var view = (UpdateView) grdViews.Rows[row].Tag;


            if (view.ViewName != thisBox.Text)
            {
                view.SetName(thisBox.Text);
            }

           

        }


        public class DrawingCellBackColor : SourceGrid.Cells.Views.Cell
        {
            private readonly UpdateView _updateView;


            public DrawingCellBackColor(UpdateView updateView, Color updatedColor, Color noColor)
            {
                _updateView = updateView;
                BackColorNone = new BackgroundSolid(noColor);
                BackColorUpdate = new BackgroundSolid(updatedColor);
            }

            public IVisualElement BackColorNone { get; set; }

            public IVisualElement BackColorUpdate { get; set; }

            protected override void PrepareView(CellContext context)
            {
                base.PrepareView(context);


                if (_updateView.IsChanged)
                {
                    Background = BackColorUpdate;
                }
                else
                {
                    Background = BackColorNone;
                }
               
            }
        }

        public void SelectUpdateViews(List<UpdateView> views)
        {
            foreach (var row in grdViews.Rows)
            {
                var view = (UpdateView)row.Tag;

                if (view != null)
                {
                    if (views.Exists(x => x.ViewId == view.ViewId))
                    {
                        var position = new Position(row.Index, 1);
                        grdViews.Selection.Focus(position, false);
                    }
                }
            }
        }
    }
}