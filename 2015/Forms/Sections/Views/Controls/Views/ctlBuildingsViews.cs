﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Data.App;
using ViewCreator.Data.Forms;
using ViewCreator.Events.Views;
using ViewCreator.Model;
using ViewCreator.Model.Settings.User;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Sections.Views.Controls
{
    public partial class ctlBuildingsViews : UserControl
    {

        #region event handlers

 
        public event EventHandler PlanChanged;
        public event EventHandler DependentPlanChanged;
        public event EventHandler DoubleClicked;


        #endregion

        #region properties
        private List<string> _levels;
        private bool _doubleClick;
        private BuildingsScreenData _formData;
        private bool _loaded;
        private Plan _plan;
        private DependentPlan _dependentPlan;
        private View _view;
        private TreeNode _planNode;
        private List<Plan> _plans;
        private ProjectPlans _projectPlans;
        private UserFavourites _userFavourites;
        public string Search { get; set; }
        private bool blnDoubleClick;

        #endregion

        #region loaders

        public ctlBuildingsViews()
        {
            InitializeComponent();
        }


        public void LoadControl()
        {
            Debug_Utils.WriteLine("ctlBuildingsViews", "LoadControl");

            // Set global Screen
            ScreensUI.BuildingsViews = this;

            // Set form data
            _formData = MainApp.AppData.FormSettings.BuildingsScreenData;

            // Set favourites data
            _userFavourites = MainApp.AppData.UserModelData.GetUserModelData().UserFavourites;

            // Set view data
            _projectPlans = MainApp.AppData.CurrentProject.ProjectPlans;

            // hookup plan events
            _projectPlans.PlanAdded += ProjectPlans_PlanAdded;
            _projectPlans.PlanDeleted += ProjectPlans_PlanDeleted;
            _projectPlans.PlanUpdated += ProjectPlans_PlanUpdated;


        }

       
        public void LoadViews()
        {
            Debug_Utils.WriteLine("ctlBuildingsViews", "LoadViews");

            Search = _formData.Search;
            _levels = _formData.Levels;

            treeView.Nodes.Clear();
            BuildPlans();

            _planNode.Expand();
            treeView.SelectCurrentView();
          
        }


        #endregion

        #region plan events


        protected virtual void OnDoubleClicked(object sender, EventArgs e)
        {
            var handler = DoubleClicked;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        protected virtual void OnPlanChanged(Plan plan, EventArgs e)
        {
            var handler = PlanChanged;
            if (handler != null)
            {
                handler(plan, e);
            }
        }

        protected virtual void OnDependentPlanChanged(DependentPlan dependentPlan, EventArgs e)
        {
            var handler = DependentPlanChanged;
            if (handler != null)
            {
                handler(dependentPlan, e);
            }
        }



        private void ProjectPlans_PlanUpdated(object sender, EventArgs e)
        {

            var plan = (Plan)sender;
            var node = treeView.GetNode(plan.ViewId.ToString());

            if (node != null)
            {
                var nodePLan = (Plan) node.Tag;

                if (nodePLan.Discipline != plan.Discipline)
                {
                    if (_formData.View.ShowDisciplines)
                    {
                        var disciplineNode = treeView.GetNode(plan.Discipline.ToString());

                        if (disciplineNode != null)
                        {
                            treeView.Nodes.Remove(node);
                            AddPlanNode(nodePLan, disciplineNode, true);
                        }
                    }
                }
                else
                {
                    UpdatePlanNode(plan, node);
                }

                
            }
        }


        private void ProjectPlans_PlanAdded(object sender, EventArgs e)
        {
            var plan = (Plan)sender;

            if (_formData.View.ShowLevels || _formData.View.ShowRls)
            {
                var level = Level_Utils.GetLevel(plan.Level);

                if (level != null)
                {
                    var levelNode = treeView.GetNode(level.Id.ToString());

                    if (levelNode != null)
                    {
                        AddPlanNode(plan, levelNode, true);
                    }
                }
            }

            if (_formData.View.ShowDisciplines)
            {
                var disciplineNode = treeView.GetNode(plan.Discipline.ToString());

                if (disciplineNode != null)
                {
                    AddPlanNode(plan, disciplineNode, true);
                }    
            }


            if (_formData.View.ShowFlat)
            {
                AddPlanNode(plan, _planNode, true);   
            }
        }





        private void ProjectPlans_PlanDeleted(object sender, EventArgs e)
        {
            var id = (int)sender;
            treeView.RemoveNode(id.ToString());
        }




        #endregion

        #region builders

        private void BuildPlans()
        {
            if (!_formData.View.ShowPlans) return;

            if (MainApp.AppData.IsUserBuildings)
            {
                _plans = _projectPlans.GetPlans(Search, _formData.View);
            }

            if (MainApp.AppData.IsUserStructures)
            {
                _plans = _projectPlans.GetPlans(Search);
            }
           

            if (_formData.View.ShowFavoutites)
            {
                var favIds = _userFavourites.ViewIds;
                var plans = new List<Plan>();

                foreach (var favId in favIds)
                {
                    var plan = _plans.FirstOrDefault(x => x.ViewId == favId);
                    if (plan != null)
                    {
                        plans.Add(plan);
                    }
                }

                _plans = plans;
            }


            _planNode = treeView.Nodes.Add("Plans");
            _planNode.ImageKey = "Plan";
            _planNode.SelectedImageKey = "Plan";


            if (_plans.Any())
            {
                if (_formData.View.ShowFlat)
                {
                    if (MainApp.AppData.IsUserBuildings)
                    {
                        foreach (var discipline in Discipline_Utils.GetDisciplineNames())
                        {
                            var plans = _plans.Where(x => x.Discipline.ToString() == discipline);

                            if (plans.Any())
                            {
                                foreach (var level in _levels)
                                {
                                    var levelPlans = plans.Where(x => x.Level == level).OrderBy(x => x.Name);

                                    foreach (var plan in levelPlans)
                                    {
                                        AddPlanNode(plan, _planNode);
                                    }
                                }
                            }
                        }
                    }
                    if (MainApp.AppData.IsUserStructures)
                    {
                        foreach (var level in _levels)
                        {
                            var levelPlans = _plans.Where(x => x.Level == level).OrderBy(x => x.Name);

                            foreach (var plan in levelPlans)
                            {
                                AddPlanNode(plan, _planNode);
                            }
                        }
                    }


                }


                if (_formData.View.ShowDisciplines && MainApp.AppData.IsUserBuildings)
                {
                    foreach (var discipline in Discipline_Utils.GetDisciplineNames())
                    {
                        var plans = _plans.Where(x => x.Discipline.ToString() == discipline);

                        if (plans.Any())
                        {
                            var disciplineNode = _planNode.Nodes.Add(discipline);
                            disciplineNode.ImageKey = discipline;
                            disciplineNode.SelectedImageKey = discipline;
                            disciplineNode.Name = discipline;

                            foreach (var level in _levels)
                            {
                                var levelPlans = plans.Where(x => x.Level == level).OrderBy(x => x.Name);

                                foreach (var plan in levelPlans)
                                {
                                    AddPlanNode(plan, disciplineNode);
                                }
                            }
                        }
                    }
                }


                if (_formData.View.ShowLevels || _formData.View.ShowRls)
                {
                    foreach (var level in _levels)
                    {
                        var plans = _plans.Where(x => x.Level == level).OrderBy(x => x.Name);

                        if (plans.Any())
                        {
                            var levelName = level;
                            if (_formData.View.ShowRls) levelName = Level_Utils.GetLevelAndRL(level);

                            var levelNode = _planNode.Nodes.Add(levelName);
                            levelNode.ImageKey = "Level";
                            levelNode.SelectedImageKey = "Level";
                            levelNode.Name = Level_Utils.GetLevel(level).Id.ToString();

                            foreach (var plan in plans)
                            {
                                AddPlanNode(plan, levelNode);
                            }
                        }
                    }
                }
            }


            _planNode.ExpandAll();
        }

        private void UpdatePlanNode(Plan plan, TreeNode node)
        {
            var image = plan.Discipline.ToString();

            node.Text = plan.Name;
            node.SelectedImageKey = image;
            node.ImageKey = image;

            if (!_formData.View.ShowFavoutites)
            {
                if (plan.Favourite)
                {
                    node.BackColor = Color.Yellow;
                }
            }

            if (plan.DependentPlans.Any())
            {
                node.Nodes.Clear();
                if (_formData.View.ShowDependents)
                {
                    AddPlanDependentNodes(plan, node);
                }
            }
        }

        private void AddPlanNode(Plan plan, TreeNode parentNode, bool findPos = false)
        {
            var image = plan.Discipline.ToString();

            var planNode = parentNode.Nodes.Add(plan.Name);
            planNode.SelectedImageKey = image;
            planNode.ImageKey = image;
            planNode.Tag = plan;
            planNode.Name = plan.ViewId.ToString();

            if (!_formData.View.ShowFavoutites)
            {
                if (plan.Favourite)
                {
                    planNode.BackColor = Color.Yellow;
                }
            }
           
           
            if (plan.DependentPlans.Any())
            {
                if (_formData.View.ShowDependents)
                {
                    AddPlanDependentNodes(plan, planNode);
                }
            }


            // Plan added to previously created treeview
            if (findPos)
            {
                int pos = 0;
                int loc = 0;
                treeView.Nodes.Remove(planNode);

                for (int i = parentNode.Nodes.Count-1; i >= 0; --i)
                {
                    TreeNode node = parentNode.Nodes[i];

                    var exLevel = Level_Utils.GetLevel(((Plan)node.Tag).Level);
                    var newLevel = Level_Utils.GetLevel(((Plan)planNode.Tag).Level);

                    if (exLevel.Name == newLevel.Name)
                    {
                        int sort = String.Compare(planNode.Text, node.Text, true);

                        if (sort > 0)
                        {
                            parentNode.Nodes.Insert(i+1, planNode);
                            break;
                        }

                        if (sort < 0)
                        {
                            parentNode.Nodes.Insert(i, planNode);
                            break;
                        }
                    }
                }
            }
        }




        private void AddPlanDependentNodes(Plan plan, TreeNode parent)
        {
            foreach (var dependent in plan.DependentPlans)
            {
                var dependentNode = parent.Nodes.Add(dependent.Name);
                dependentNode.SelectedImageKey = plan.Discipline.ToString();
                dependentNode.ImageKey = plan.Discipline.ToString();
                dependentNode.Tag = dependent;
                dependentNode.Name = dependent.ViewId.ToString();


            }
        }


        #endregion

        #region routines


        public void SelectCurrentView()
        {
            treeView.SelectCurrentView();
        }

        public List<View> GetViews()
        {

            var views = _plans.Select(x => x.GetView()).ToList();
            foreach (var plan in _plans)
            {
                foreach (var dependent in plan.DependentPlans)
                {
                    views.Add(dependent.GetView());
                }
            }

            return views;
        }


        public View GetView()
        {
            return _view;
        }

        public Plan GetPlan()
        {
            return _plan;
        }

        public DependentPlan GetDependentPlan()
        {
            return _dependentPlan;
        }

        private void treeView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
        }

        private static void SaveData()
        {
            MainApp.AppData.SaveUserModelData();
        }


        #endregion

        #region tree expander

        private void treeView_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            if (!_loaded) return;

            if (_doubleClick && e.Action == TreeViewAction.Collapse)
                e.Cancel = true;
        }

        private void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (!_loaded) return;

            if (_doubleClick && e.Action == TreeViewAction.Expand)
                e.Cancel = true;
        }

        private void treeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_loaded) return;

            if (e.Clicks > 1)
                blnDoubleClick = true;
            else
                blnDoubleClick = false;
        }

        #endregion

        #region commands


        private void mnuFavourite_Click(object sender, EventArgs e)
        {
            if (_plan == null) return;

            mnuFavourite.Checked = !mnuFavourite.Checked;
            if (mnuFavourite.Checked)
            {
                _userFavourites.Add(_plan.ViewId);

                if (!_formData.View.ShowFavoutites)
                {
                    treeView.SelectedNode.BackColor = Color.Yellow;
                }
            }
            else
            {
                _userFavourites.Remove(_plan.ViewId);

                if (_formData.View.ShowFavoutites)
                {
                    treeView.Nodes.Remove(treeView.SelectedNode);
                    _plans.RemoveAll(x => x.ViewId == _plan.ViewId);
                }
                else
                {
                    treeView.SelectedNode.BackColor = Color.White;
                }
            }

            SaveData();
        }


        private void contextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (_plan != null)
            {
                mnuFavourite.Checked = _userFavourites.Exists(_plan.ViewId);
            }
        }

        private void mnuEditView_Click(object sender, EventArgs e)
        {
            EditViewUI.ReLoad(true);
        }


        private void treeView_DoubleClick(object sender, EventArgs e)
        {

            View view = null;

            // Get revit view 
            if (_plan != null) view = _plan.GetView();
            if (_dependentPlan != null) view = _dependentPlan.GetView();

            // Raise event
            if (view != null)
            {
                OnDoubleClicked(sender, e);
            }

        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (_loaded) return;

            var tag = treeView.SelectedNode.Tag;

            if (tag == null) return;

            _plan = null;
            _dependentPlan = null;
            _view = null;

            if (Tree_Utils.IsPlan(tag))
            {
                _plan = (Plan)tag;
                _view = _plan.GetView();
                OnPlanChanged(_plan, e);
            }

            if (Tree_Utils.IsDependentPlan(tag))
            {
                _dependentPlan = (DependentPlan)tag;
                _view = _dependentPlan.GetView();
                OnDependentPlanChanged(_dependentPlan, e);
            }
        }



        #endregion

    
    }
}