﻿namespace ViewCreator.Forms.Sections.Views.Controls
{
    partial class ctlBuildingsViews
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlBuildingsViews));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.treeView = new System.Windows.Forms.TreeView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuEditView = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDuplicateView = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExportNavisworks = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuScreenshot = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDeletView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDependentViews = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFavourite = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Electrical");
            this.imageList.Images.SetKeyName(1, "Fire");
            this.imageList.Images.SetKeyName(2, "Hydraulic");
            this.imageList.Images.SetKeyName(3, "Mechanical");
            this.imageList.Images.SetKeyName(4, "Structural");
            this.imageList.Images.SetKeyName(5, "Architectural");
            this.imageList.Images.SetKeyName(6, "Coordination");
            this.imageList.Images.SetKeyName(7, "Scopebox");
            this.imageList.Images.SetKeyName(8, "Threed");
            this.imageList.Images.SetKeyName(9, "Dependent");
            this.imageList.Images.SetKeyName(10, "Level");
            this.imageList.Images.SetKeyName(11, "Section");
            this.imageList.Images.SetKeyName(12, "Sheet");
            this.imageList.Images.SetKeyName(13, "Schedule");
            this.imageList.Images.SetKeyName(14, "Plan");
            // 
            // treeView
            // 
            this.treeView.ContextMenuStrip = this.contextMenuStrip;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.FullRowSelect = true;
            this.treeView.HideSelection = false;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.imageList;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.Size = new System.Drawing.Size(291, 354);
            this.treeView.TabIndex = 80;
            this.treeView.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeCollapse);
            this.treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeExpand);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeView.DoubleClick += new System.EventHandler(this.treeView_DoubleClick);
            this.treeView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDoubleClick);
            this.treeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDown);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuEditView,
            this.toolStripMenuItem3,
            this.mnuDuplicateView,
            this.toolStripMenuItem1,
            this.mnuExportNavisworks,
            this.mnuScreenshot,
            this.toolStripMenuItem2,
            this.mnuDeletView,
            this.mnuDependentViews,
            this.mnuFavourite});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(171, 176);
            this.contextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_Opening);
            // 
            // mnuEditView
            // 
            this.mnuEditView.Image = global::ViewCreator.Properties.Resources.Edit;
            this.mnuEditView.Name = "mnuEditView";
            this.mnuEditView.Size = new System.Drawing.Size(170, 22);
            this.mnuEditView.Text = "Edit View";
            this.mnuEditView.Click += new System.EventHandler(this.mnuEditView_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(167, 6);
            // 
            // mnuDuplicateView
            // 
            this.mnuDuplicateView.Image = global::ViewCreator.Properties.Resources.Duplicate;
            this.mnuDuplicateView.Name = "mnuDuplicateView";
            this.mnuDuplicateView.Size = new System.Drawing.Size(170, 22);
            this.mnuDuplicateView.Text = "Duplicate View";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(167, 6);
            // 
            // mnuExportNavisworks
            // 
            this.mnuExportNavisworks.Image = global::ViewCreator.Properties.Resources.NW;
            this.mnuExportNavisworks.Name = "mnuExportNavisworks";
            this.mnuExportNavisworks.Size = new System.Drawing.Size(170, 22);
            this.mnuExportNavisworks.Text = "Export Navisworks";
            // 
            // mnuScreenshot
            // 
            this.mnuScreenshot.Image = global::ViewCreator.Properties.Resources.Camera;
            this.mnuScreenshot.Name = "mnuScreenshot";
            this.mnuScreenshot.Size = new System.Drawing.Size(170, 22);
            this.mnuScreenshot.Text = "Screenshot";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(167, 6);
            // 
            // mnuDeletView
            // 
            this.mnuDeletView.Image = global::ViewCreator.Properties.Resources.Delete;
            this.mnuDeletView.Name = "mnuDeletView";
            this.mnuDeletView.Size = new System.Drawing.Size(170, 22);
            this.mnuDeletView.Text = "Delete View";
            // 
            // mnuDependentViews
            // 
            this.mnuDependentViews.Image = global::ViewCreator.Properties.Resources.tree_view;
            this.mnuDependentViews.Name = "mnuDependentViews";
            this.mnuDependentViews.Size = new System.Drawing.Size(170, 22);
            this.mnuDependentViews.Text = "Dependent Views";
            // 
            // mnuFavourite
            // 
            this.mnuFavourite.Image = global::ViewCreator.Properties.Resources.Star;
            this.mnuFavourite.Name = "mnuFavourite";
            this.mnuFavourite.Size = new System.Drawing.Size(170, 22);
            this.mnuFavourite.Text = "Favourite";
            this.mnuFavourite.Click += new System.EventHandler(this.mnuFavourite_Click);
            // 
            // ctlBuildingsViews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeView);
            this.Name = "ctlBuildingsViews";
            this.Size = new System.Drawing.Size(291, 354);
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuEditView;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem mnuDuplicateView;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuExportNavisworks;
        private System.Windows.Forms.ToolStripMenuItem mnuScreenshot;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mnuDeletView;
        private System.Windows.Forms.ToolStripMenuItem mnuDependentViews;
        private System.Windows.Forms.ToolStripMenuItem mnuFavourite;

    }
}
