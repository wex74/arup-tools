﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using SourceGrid;

namespace ViewCreator.Forms.Controls.Views
{
    partial class ctlEditViewsViews
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grdViews = new SourceGrid.Grid();
            this.SuspendLayout();
            // 
            // grdViews
            // 
            this.grdViews.BackColor = System.Drawing.Color.White;
            this.grdViews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdViews.EnableSort = true;
            this.grdViews.ForeColor = System.Drawing.Color.Black;
            this.grdViews.Location = new System.Drawing.Point(0, 0);
            this.grdViews.Name = "grdViews";
            this.grdViews.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows;
            this.grdViews.SelectionMode = SourceGrid.GridSelectionMode.Row;
            this.grdViews.Size = new System.Drawing.Size(580, 337);
            this.grdViews.TabIndex = 64;
            this.grdViews.TabStop = true;
            this.grdViews.ToolTipText = "";
            // 
            // ctlViewEditViews
            // 
            this.Controls.Add(this.grdViews);
            this.Name = "ctlEditViewsViews";
            this.Size = new System.Drawing.Size(580, 337);
            this.Load += new System.EventHandler(this.ctlViewEditViews_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Grid grdViews;

    }
}
