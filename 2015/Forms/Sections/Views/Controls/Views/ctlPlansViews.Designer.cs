﻿namespace ViewCreator.Forms.Controls
{
    partial class ctlPlansViews
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlPlansViews));
            this.treeView = new System.Windows.Forms.TreeView();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.cmLevels = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.levelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmViewInfo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmPlan = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.planToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmDependent = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dependentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCheckAllViews = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCheckAllPans = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuUncheckAll = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCheckAllViewDependents = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCheckAllDependentPlans = new System.Windows.Forms.ToolStripMenuItem();
            this.cmLevels.SuspendLayout();
            this.cmViewInfo.SuspendLayout();
            this.cmPlan.SuspendLayout();
            this.cmDependent.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.ContextMenuStrip = this.contextMenu;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.HideSelection = false;
            this.treeView.ImageIndex = 0;
            this.treeView.ImageList = this.imageList;
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.SelectedImageIndex = 0;
            this.treeView.ShowRootLines = false;
            this.treeView.Size = new System.Drawing.Size(257, 255);
            this.treeView.TabIndex = 79;
            this.treeView.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeCollapse);
            this.treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeExpand);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            this.treeView.DoubleClick += new System.EventHandler(this.treeView_DoubleClick);
            this.treeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDown);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Electrical");
            this.imageList.Images.SetKeyName(1, "Fire");
            this.imageList.Images.SetKeyName(2, "Hydraulic");
            this.imageList.Images.SetKeyName(3, "Mechanical");
            this.imageList.Images.SetKeyName(4, "Structural");
            this.imageList.Images.SetKeyName(5, "Plan");
            this.imageList.Images.SetKeyName(6, "Architectural");
            this.imageList.Images.SetKeyName(7, "Coordination");
            this.imageList.Images.SetKeyName(8, "Scopebox");
            this.imageList.Images.SetKeyName(9, "Threed");
            this.imageList.Images.SetKeyName(10, "Dependent");
            this.imageList.Images.SetKeyName(11, "Level");
            this.imageList.Images.SetKeyName(12, "Section");
            this.imageList.Images.SetKeyName(13, "Sheet");
            this.imageList.Images.SetKeyName(14, "Schedule");
            // 
            // cmLevels
            // 
            this.cmLevels.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.levelsToolStripMenuItem});
            this.cmLevels.Name = "cmLevels";
            this.cmLevels.Size = new System.Drawing.Size(107, 26);
            // 
            // levelsToolStripMenuItem
            // 
            this.levelsToolStripMenuItem.Name = "levelsToolStripMenuItem";
            this.levelsToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.levelsToolStripMenuItem.Text = "Levels";
            // 
            // cmViewInfo
            // 
            this.cmViewInfo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.cmViewInfo.Name = "cmLevels";
            this.cmViewInfo.Size = new System.Drawing.Size(121, 26);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(120, 22);
            this.toolStripMenuItem1.Text = "ViewInfo";
            // 
            // cmPlan
            // 
            this.cmPlan.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.planToolStripMenuItem});
            this.cmPlan.Name = "cmPlan";
            this.cmPlan.Size = new System.Drawing.Size(108, 26);
            // 
            // planToolStripMenuItem
            // 
            this.planToolStripMenuItem.Name = "planToolStripMenuItem";
            this.planToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.planToolStripMenuItem.Text = "Delete";
            // 
            // cmDependent
            // 
            this.cmDependent.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dependentToolStripMenuItem});
            this.cmDependent.Name = "cmDependent";
            this.cmDependent.Size = new System.Drawing.Size(133, 26);
            // 
            // dependentToolStripMenuItem
            // 
            this.dependentToolStripMenuItem.Name = "dependentToolStripMenuItem";
            this.dependentToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.dependentToolStripMenuItem.Text = "Dependent";
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuDelete,
            this.mnuCheckAllViews,
            this.mnuCheckAllPans,
            this.mnuCheckAllViewDependents,
            this.mnuCheckAllDependentPlans,
            this.mnuUncheckAll});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(228, 158);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Image = global::ViewCreator.Properties.Resources.RemoveFile;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Size = new System.Drawing.Size(227, 22);
            this.mnuDelete.Text = "Delete";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuCheckAllViews
            // 
            this.mnuCheckAllViews.Image = global::ViewCreator.Properties.Resources.Checkall;
            this.mnuCheckAllViews.Name = "mnuCheckAllViews";
            this.mnuCheckAllViews.Size = new System.Drawing.Size(227, 22);
            this.mnuCheckAllViews.Text = "Check All Empty Plans";
            this.mnuCheckAllViews.Click += new System.EventHandler(this.mnuCheckAllViews_Click);
            // 
            // mnuCheckAllPans
            // 
            this.mnuCheckAllPans.Image = global::ViewCreator.Properties.Resources.Checkall;
            this.mnuCheckAllPans.Name = "mnuCheckAllPans";
            this.mnuCheckAllPans.Size = new System.Drawing.Size(227, 22);
            this.mnuCheckAllPans.Text = "Select All Plans";
            this.mnuCheckAllPans.Click += new System.EventHandler(this.mnuCheckAllPans_Click);
            // 
            // mnuUncheckAll
            // 
            this.mnuUncheckAll.Name = "mnuUncheckAll";
            this.mnuUncheckAll.Size = new System.Drawing.Size(227, 22);
            this.mnuUncheckAll.Text = "Uncheck All";
            this.mnuUncheckAll.Click += new System.EventHandler(this.mnuUncheckAll_Click);
            // 
            // mnuCheckAllViewDependents
            // 
            this.mnuCheckAllViewDependents.Image = global::ViewCreator.Properties.Resources.Checkall;
            this.mnuCheckAllViewDependents.Name = "mnuCheckAllViewDependents";
            this.mnuCheckAllViewDependents.Size = new System.Drawing.Size(227, 22);
            this.mnuCheckAllViewDependents.Text = "Check All Empty Dependents";
            this.mnuCheckAllViewDependents.Click += new System.EventHandler(this.mnuCheckAllViewDependents_Click);
            // 
            // mnuCheckAllDependentPlans
            // 
            this.mnuCheckAllDependentPlans.Image = global::ViewCreator.Properties.Resources.Checkall;
            this.mnuCheckAllDependentPlans.Name = "mnuCheckAllDependentPlans";
            this.mnuCheckAllDependentPlans.Size = new System.Drawing.Size(227, 22);
            this.mnuCheckAllDependentPlans.Text = "Check All Dependent Plans";
            this.mnuCheckAllDependentPlans.Click += new System.EventHandler(this.mnuCheckAllDependentPlans_Click);
            // 
            // ctlPlansViews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.treeView);
            this.Name = "ctlPlansViews";
            this.Size = new System.Drawing.Size(257, 255);
            this.cmLevels.ResumeLayout(false);
            this.cmViewInfo.ResumeLayout(false);
            this.cmPlan.ResumeLayout(false);
            this.cmDependent.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ContextMenuStrip cmLevels;
        private System.Windows.Forms.ToolStripMenuItem levelsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmViewInfo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip cmPlan;
        private System.Windows.Forms.ContextMenuStrip cmDependent;
        private System.Windows.Forms.ToolStripMenuItem planToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dependentToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuDelete;
        private System.Windows.Forms.ToolStripMenuItem mnuCheckAllViews;
        private System.Windows.Forms.ToolStripMenuItem mnuCheckAllPans;
        private System.Windows.Forms.ToolStripMenuItem mnuUncheckAll;
        private System.Windows.Forms.ToolStripMenuItem mnuCheckAllViewDependents;
        private System.Windows.Forms.ToolStripMenuItem mnuCheckAllDependentPlans;
    }
}
