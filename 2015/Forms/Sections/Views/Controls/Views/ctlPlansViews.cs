﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Data.App;
using ViewCreator.Data.App_Data;
using ViewCreator.Data.Forms;
using ViewCreator.Events.Views;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlPlansViews : UserControl
    {

        #region event handlers

        public event EventHandler ViewDoubleClicked;
        public event EventHandler ViewsLoaded;
        public event EventHandler ViewsUpdated;

        #endregion

        #region properties

        private string _search;
        private bool _showActiveLevels;

        private Discipline _discipline;
        private PlansScreenData _formData;
        private bool _loaded;

        private Level _level;
        private ViewInfo _viewInfo;
        private Plan _plan;
        private DependentPlan _dependentPlan;

        private ProjectPlans _projectPlans;
        private ProjectViewInfos _projectViewInfos;

        private List<ViewInfo> _viewInfos;
        private List<Plan> _plans;
        private List<Level> _levels;

        private List<Plan> _filteredPlans;
        private List<DependentPlan> _filteredDependentPlans;
        private List<ViewInfo> _filteredViewInfos;
        private List<ViewInfo> _filteredDependentViewInfos;

        private bool blnDoubleClick;

        #endregion

        #region loaders

        public ctlPlansViews()
        {
            InitializeComponent();
        }

        public void LoadControl()
        {
            Debug_Utils.WriteLine("ctlPlansViews", "LoadControl");

            // Set global Screen
            ScreensUI.PlansViews = this;

            // Set form data
            _formData = MainApp.AppData.FormSettings.PlansScreenData;

            // Set view data
            _projectPlans = MainApp.AppData.CurrentProject.ProjectPlans;
            _projectViewInfos = MainApp.AppData.CurrentProject.ProjectViewInfos;

            // hookup plan events
            _projectPlans.PlanAdded += ProjectPlans_PlanAdded;
            _projectPlans.PlanDeleted += ProjectPlans_PlanDeleted;
            _projectPlans.PlanUpdated += ProjectPlans_PlanUpdated;

            // hookup viewinfo events
            _projectViewInfos.ViewInfoDeleted += ProjectViewInfos_ViewInfoDeleted;

            _filteredDependentPlans = new List<DependentPlan>();
            _filteredDependentViewInfos = new List<ViewInfo>();
        }

        public void LoadViews()
        {
            Debug_Utils.WriteLine("ctlPlansViews", "LoadViews");

            // Load form data
            _levels = Level_Utils.GetLevels(_formData.Levels);
            _discipline = _formData.Discipline;
            _search = _formData.Search;
            _showActiveLevels = _formData.ShowActiveLevels;

            // Load view objects
            _viewInfos = _projectViewInfos.GetViewInfos(_discipline);
            _plans = _projectPlans.GetPlans(_discipline);

            // build treeview
            treeView.Nodes.Clear();
            BuildLevels();

            _loaded = true;

            treeView.SelectCurrentView();

            OnViewsLoaded(null, null);
        }

        #endregion

        #region plan events


        private void ProjectPlans_PlanAdded(object sender, EventArgs e)
        {
            var plan = (Plan)sender;

            if (plan.Discipline == _discipline)
            {
                var level = Level_Utils.GetLevel(plan.Level);

                if (level != null)
                {
                    var levelNode = treeView.GetNode(level.Id.ToString());

                    if (levelNode != null)
                    {
                        AddPlanNode(levelNode, plan);
                        _filteredPlans.Add(plan);
                        OnViewsUpdated(null, null);
                    }
                }
            }
        }

        private void ProjectPlans_PlanUpdated(object sender, EventArgs e)
        {
            var plan = (Plan)sender;
           
            if (PlanExists(plan))
            {
                var node = treeView.GetNode(plan.ViewId.ToString());
                if (node != null)
                {
                    UpdatePlanNode(node, plan);
                    OnViewsUpdated(null, null);
                }
            }
        }

        private void ProjectPlans_PlanDeleted(object sender, EventArgs e)
        {
            var id = (int)sender;

            treeView.RemoveNode(id.ToString(), false);
            _filteredPlans.RemoveAll(x => x.ViewId == id);
            OnViewsUpdated(null, null);
        }


        #endregion

        #region viewinfo events

        private void ProjectViewInfos_ViewInfoDeleted(object sender, EventArgs e)
        {
            var id = (string)sender;

            treeView.RemoveNode(id);
            _filteredViewInfos.RemoveAll(x => x.Id.ToString() == id);
            OnViewsUpdated(null, null);
        }


        #endregion

        #region raise view events

        protected virtual void OnViewDoubleClicked(View view, EventArgs e)
        {
            var handler = ViewDoubleClicked;
            if (handler != null)
            {
                handler(view, e);
            }
        }

        protected virtual void OnViewsLoaded(object sender, EventArgs e)
        {
            var handler = ViewsLoaded;
            if (handler != null)
            {
                handler(sender, e);
            }
        }


        protected virtual void OnViewsUpdated(object sender, EventArgs e)
        {
            var handler = ViewsUpdated;
            if (handler != null)
            {
                handler(sender, e);
            }
        }


        #endregion

        #region getters

        public List<ViewInfo> GetViewInfos()
        {
            return _filteredViewInfos;
        }

        public List<Plan> GetPlans()
        {
            return _filteredPlans;
        }


        public List<View> GetViews()
        {

            var views = _filteredPlans.Select(x => x.GetView()).ToList();
            foreach (var plan in _plans)
            {
                foreach (var dependent in plan.DependentPlans)
                {
                    views.Add(dependent.GetView());
                }
            }

            return views;
        }


        public List<Plan> GetCheckedPlans()
        {
            // Todo - find a better was to collect nodes by view type
            var plans = new List<Plan>();

            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode planNode in levelNode.Nodes)
                {
                    if (Tree_Utils.IsPlan(planNode.Tag))
                    {
                        if (planNode.Checked)
                        {
                            plans.Add((Plan)planNode.Tag);
                        }
                    }
                }
            }

            return plans;
        }


        public List<ViewInfo> GetCheckedViewInfos()
        {
            // Todo - find a better was to collect nodes by view type
            var viewInfos = new List<ViewInfo>();

            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode viewInfoNode in levelNode.Nodes)
                {
                    if (Tree_Utils.IsViewInfo(viewInfoNode.Tag))
                    {
                        if (viewInfoNode.Checked)
                        {
                            viewInfos.Add((ViewInfo)viewInfoNode.Tag);
                        }
                    }
                }
            }

            return viewInfos;
        }

        public List<ViewInfo> GetCheckedDependentViewInfos()
        {
            // Todo - find a better was to collect nodes by view type
            var viewInfos = new List<ViewInfo>();

            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode   node in levelNode.Nodes)
                {
                    foreach (TreeNode dependentNode in node.Nodes)
                    {
                        if (Tree_Utils.IsViewInfo(dependentNode.Tag))
                        {
                            if (dependentNode.Checked)
                            {
                                viewInfos.Add((ViewInfo)dependentNode.Tag);
                            }
                        }
                    }
                    
                }
            }

            return viewInfos;
        }

        public List<DependentPlan> GetCheckedDependentPlans()
        {
            // Todo - find a better was to collect nodes by view type
            var dependentPlans = new List<DependentPlan>();

            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode node in levelNode.Nodes)
                {
                    foreach (TreeNode dependentNode in node.Nodes)
                    {
                        if (Tree_Utils.IsDependentPlan(dependentNode.Tag))
                        {
                            if (dependentNode.Checked)
                            {
                                dependentPlans.Add((DependentPlan)dependentNode.Tag);
                            }
                        }
                    }

                }
            }

            return dependentPlans;
        }

        #endregion

        #region routines


        public bool PlanExists(Plan plan)
        {
            return _filteredPlans.Exists(x => x.ViewId == plan.ViewId);
        }

        private bool IsViewActive
        {
            get
            {
                return _viewInfo != null || _plan != null || _dependentPlan != null;
            }
        }

        public void ShowHideDependents()
        {
            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode viewNode in levelNode.Nodes)
                {
                    if (viewNode.Nodes.Count > 0)
                    {
                        if (_formData.ShowDependentViews) viewNode.Expand();
                        if (!_formData.ShowDependentViews) viewNode.Collapse();
                    }
                }
            }
        }


        public void RemoveNode(string id)
        {
            treeView.RemoveNode(id, false);
        }


        public void RemoveNodes(List<string> ids)
        {
            foreach (var id in ids)
            {
                treeView.RemoveNode(id);
            }
          
        }

        public void SelectCurrentView()
        {
            treeView.SelectCurrentView();
        }

        public void ShowCheckboxes(bool show)
        {
            treeView.CheckBoxes = show;
        }


        #endregion

        #region builders

        private void BuildLevels()
        {
            // Search and filter lists
            _filteredPlans = _plans.Where(x => x.TextInfo.Matches(_search)).ToList();
            _filteredViewInfos = _viewInfos.Where(x => x.TextInfo.Matches(_search)).ToList();

            // Add level nodes
            foreach (var level in _levels)
            {
                if (_showActiveLevels)
                {
                    // Show level if it has plans or viewinfos
                    var plans = _filteredPlans.Where(x => x.Level == level.Name).ToList();
                    var viewInfos = _filteredViewInfos.Where(x => x.Level.Name == level.Name).ToList();

                    if (plans.Any() || viewInfos.Any())
                    {
                        AddLevelNode(level);
                    }
                }
                else
                {
                    AddLevelNode(level);
                }
            }      
        }


      

        private TreeNode AddLevelNode(Level level)
        {
            // Add Level node
            var levelNode = treeView.Nodes.Add(level.Name);
            levelNode.SelectedImageKey = "Level";
            levelNode.ImageKey = "Level";
            levelNode.Tag = level;
            levelNode.Name = level.Id.ToString();


            // Build ViewInfos & Plans for level
            BuildViewInfoNodes(levelNode, level);
            BuildPlanNodes(levelNode, level);

            levelNode.Expand();

            return levelNode;
        }

        private void BuildViewInfoNodes(TreeNode node, Level level)
        {
            // Filter viewinfos by level 
            var viewInfos = _filteredViewInfos.Where(x => x.Level.Name == level.Name &&  x.ParentViewId == 0).ToList();

            foreach (var viewInfo in viewInfos)
            {
                AddViewInfoNode(node, viewInfo);
            }
        }

        private void BuildPlanNodes(TreeNode node, Level level)
        {
            // Filter plans by level 
            var plans = _filteredPlans.Where(x => x.Level == level.Name).ToList();

            foreach (var plan in plans)
            {
                AddPlanNode(node, plan);
            }
        }


        private void AddViewInfoNode(TreeNode parent, ViewInfo viewInfo)
        {
            // add viewinfo node
            var image = "Plan";
            var viewInfoNode = parent.Nodes.Add(viewInfo.ToString());
            viewInfoNode.SelectedImageKey = image;
            viewInfoNode.ImageKey = image;
            viewInfoNode.Tag = viewInfo;
            viewInfoNode.Name = viewInfo.Id.ToString();

            // Add Any ViewInfo Dependents
            if (viewInfo.DependentViews.Any())
            {
                AddViewInfoDependendNodes(viewInfo, viewInfoNode);

                if (_formData.ShowDependentViews) viewInfoNode.Expand();
                if (!_formData.ShowDependentViews) viewInfoNode.Collapse();
            }
;
        }

        private void AddPlanNode(TreeNode parent, Plan plan)
        {
            // Add plan node
            var image = plan.Discipline.ToString();
            var planNode = parent.Nodes.Add(plan.Name);
            planNode.SelectedImageKey = image;
            planNode.ImageKey = image;
            planNode.Tag = plan;
            planNode.Name = plan.ViewId.ToString();


            // Add any empty viewinfos associated with this plan
            var viewInfos = _projectViewInfos.GetViewInfos(plan);
            if (viewInfos.Any())
            {
                AddViewInfoPlanDependentNodes(viewInfos, planNode);
            }

            // Add any dependent nodes
            if (plan.DependentPlans.Any())
            {
                AddPlanDependentNodes(plan, planNode);
            }

            // Expand or collapse based on setting
            if (planNode.Nodes.Count > 0)
            {
                if (_formData.ShowDependentViews) planNode.Expand();
                if (!_formData.ShowDependentViews) planNode.Collapse();
            }


        }

        private void UpdatePlanNode(TreeNode planNode, Plan plan)
        {
            // Update plan node
            var image = plan.Discipline.ToString();
            planNode.Text = plan.Name;
            planNode.ImageKey = image;


            planNode.Nodes.Clear();

            // Add any empty viewinfos associated with this plan
            var viewInfos = _projectViewInfos.GetViewInfos(plan);
            if (viewInfos.Any())
            {
                AddViewInfoPlanDependentNodes(viewInfos, planNode);
            }

            // Add any dependent nodes
            if (plan.DependentPlans.Any())
            {
                AddPlanDependentNodes(plan, planNode);
            }

            // Expand or collapse based on setting
            if (planNode.Nodes.Count > 0)
            {
                if (_formData.ShowDependentViews) planNode.Expand();
                if (!_formData.ShowDependentViews) planNode.Collapse();
            }


        }
        private void AddViewInfoDependendNodes(ViewInfo viewInfo, TreeNode parent)
        {
            var image = "Plan";

            // Loop viewinfo dependents
            foreach (var dependent in viewInfo.DependentViews)
            {
                var dependentNode = parent.Nodes.Add(dependent);
                dependentNode.SelectedImageKey = image;
                dependentNode.ImageKey = image;
                dependentNode.Tag = dependent;

                _filteredDependentViewInfos.Add(viewInfo);
            }
        }


       

        private void AddViewInfoPlanDependentNodes(List<ViewInfo> viewInfos, TreeNode parent)
        {
            var image = "Plan";

            // Loop viewinfo dependents from plan
            foreach (var viewInfo in viewInfos)
            {
                var dependentNode = parent.Nodes.Add(viewInfo.ToString());
                dependentNode.SelectedImageKey = image;
                dependentNode.ImageKey = image;
                dependentNode.Tag = viewInfo;

                _filteredDependentViewInfos.Add(viewInfo);
            }
        }


        private void AddPlanDependentNodes(Plan plan, TreeNode parent)
        {
            // Loop plan dependents
            foreach (var dependent in plan.DependentPlans)
            {
                var dependentNode = parent.Nodes.Add(dependent.Name);
                dependentNode.SelectedImageKey = plan.Discipline.ToString();
                dependentNode.ImageKey = plan.Discipline.ToString();
                dependentNode.Tag = dependent;
                dependentNode.Name = dependent.ViewId.ToString();

                _filteredDependentPlans.Add(dependent);
            }
        }

        #endregion

        #region tree events

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (!_loaded) return;

            var tag = treeView.SelectedNode.Tag;

            if (tag == null) return;

            _level = null;
            _viewInfo = null;
            _plan = null;
            _dependentPlan = null;

            // Get object by level and type by node.Level
            switch (treeView.SelectedNode.Level)
            {
                // Level node
                case 0:
                    if (Tree_Utils.IsLevel(tag)) _level = (Level)tag;
                    break;

                // Viewinfo or plan node
                case 1:
                    if (Tree_Utils.IsViewInfo(tag)) _viewInfo = (ViewInfo)tag;
                    if (Tree_Utils.IsPlan(tag)) _plan = (Plan)tag;
                    break;

                // Viewinfo or dependent node
                case 2:
                    if (Tree_Utils.IsViewInfo(tag)) _viewInfo = (ViewInfo)tag;
                    if (Tree_Utils.IsDependentPlan(tag)) _dependentPlan = (DependentPlan)tag;
                    break;
            }

            // Todo - not required now
            // Set context menu
            //if (_level != null) treeView.ContextMenuStrip = cmLevels;
            //if (_viewInfo != null) treeView.ContextMenuStrip = cmViewInfo;
            //if (_plan != null) treeView.ContextMenuStrip = cmPlan;
            //if (_dependentPlan != null) treeView.ContextMenuStrip = cmDependent;
        }

        private void treeView_DoubleClick(object sender, EventArgs e)
        {
            View view = null;

            // Get revit view 
            if (_plan != null) view = _plan.GetView();
            if (_dependentPlan != null) view = _dependentPlan.GetView();

            // Raise event
            if (view != null)
            {
                if (view.Id != RVT_Utils.ActiveView.Id)
                {
                    NavigateEvent.Run(view);
                }
                else
                {
                    if (!EditViewUI.FormVisible)
                    {
                        EditViewUI.ShowForm();
                    }
                    else
                    {
                        EditViewUI.ReLoad();
                    }
                  
                }

            }


            
        }

        #endregion

        #region tree expander

        private void treeView_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            if (!_loaded) return;

            if (blnDoubleClick && e.Action == TreeViewAction.Collapse)
                e.Cancel = true;
        }

        private void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (!_loaded) return;

            if (blnDoubleClick && e.Action == TreeViewAction.Expand)
                e.Cancel = true;
        }

        private void treeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_loaded) return;

            if (e.Clicks > 1)
                blnDoubleClick = true;
            else
                blnDoubleClick = false;
        }
        #endregion

        #region menu commands

        private void mnuDelete_Click(object sender, EventArgs e)
        {
            var nodeText = treeView.DisplayText();

            // delete selected viewinfo
            if (_viewInfo != null)
            {
                if (UI_Utils.GetMessageBoxDeleteResult(nodeText, "Empty Plan") == DialogResult.Yes)
                {
                    _projectViewInfos.Remove(_viewInfo);
                }
            }

            // delete selected plan
            if (_plan != null)
            {
                if (UI_Utils.GetMessageBoxDeleteResult(nodeText, "Plan") == DialogResult.Yes)
                {
                    DeletePlanEvent.Run(_plan);
                }
          
            }

            // delete selected dependent plan
            if (_dependentPlan != null)
            {

            }
        }

        private void contextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // hide delete if no view types are selected
            mnuDelete.Visible = IsViewActive;

            // hide checkbox menus if checkboxes are off
            mnuCheckAllViews.Visible = treeView.CheckBoxes;
            mnuCheckAllPans.Visible = treeView.CheckBoxes;
            mnuCheckAllViewDependents.Visible = treeView.CheckBoxes;
            mnuCheckAllDependentPlans.Visible = treeView.CheckBoxes;
            mnuUncheckAll.Visible = treeView.CheckBoxes;

            // hide menus based on views loaded
            mnuCheckAllViews.Visible = _filteredViewInfos.Any();
            mnuCheckAllPans.Visible = _filteredPlans.Any();
            mnuCheckAllViewDependents.Visible = _filteredDependentViewInfos.Any();
            mnuCheckAllDependentPlans.Visible = _filteredDependentPlans.Any();
        }



        private void mnuCheckAllViews_Click(object sender, EventArgs e)
        {
            // Todo - find a better was to collect nodes by view type
            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode node in levelNode.Nodes)
                {
                    if (Tree_Utils.IsViewInfo(node.Tag))
                    {
                        node.Checked = true;
                    }
                }
            }
        }

        private void mnuCheckAllPans_Click(object sender, EventArgs e)
        {
            // Todo - find a better was to collect nodes by view type
            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode node in levelNode.Nodes)
                {
                    if (Tree_Utils.IsPlan(node.Tag))
                    {
                        node.Checked = true;
                    }
                }
            }
        }

        private void mnuUncheckAll_Click(object sender, EventArgs e)
        {
            // Todo - find a better was to collect nodes by view type
            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode node in levelNode.Nodes)
                {
                    node.Checked = false;
                }
            }
        }

        private void mnuCheckAllViewDependents_Click(object sender, EventArgs e)
        {
            // Todo - find a better was to collect nodes by view type
            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode node in levelNode.Nodes)
                {
                    foreach (TreeNode dependentNode in node.Nodes)
                    {
                        if (Tree_Utils.IsString(dependentNode.Tag))
                        {
                            dependentNode.Checked = true;
                        }
                    }
                }
            }
        }

        private void mnuCheckAllDependentPlans_Click(object sender, EventArgs e)
        {
            // Todo - find a better was to collect nodes by view type
            foreach (TreeNode levelNode in treeView.Nodes)
            {
                foreach (TreeNode node in levelNode.Nodes)
                {
                    foreach (TreeNode dependentNode in node.Nodes)
                    {
                        if (Tree_Utils.IsDependentPlan(dependentNode.Tag))
                        {
                            dependentNode.Checked = true;
                        }
                    }
                }
            }
        }


        #endregion



    }
}