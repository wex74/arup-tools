﻿namespace ViewCreator.Forms.Controls
{
    partial class ctlMyViews
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlMyViews));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.tbAddCurrentView = new System.Windows.Forms.ToolStripButton();
            this.tbRemoveViews = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tbManage = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuEditView = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDuplicateView = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExportNavisworks = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuScreenshot = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDeletView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDependentViews = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.ctlLevelsCoord = new ViewCreator.Forms.Sections.Views.Controls.ctlLevelsCoord();
            this.pnlTitle = new System.Windows.Forms.Panel();
            this.lblFloorSurfer = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ctlViews = new ViewCreator.Forms.Controls.ctlViewMyViews();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.ctlSearch = new ViewCreator.Forms.Controls.ctlSearch();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ctlServices1 = new ViewCreator.Forms.Sections.Views.Controls.ctlServices();
            this.toolStrip.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.pnlTitle.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Electrical");
            this.imageList.Images.SetKeyName(1, "Fire");
            this.imageList.Images.SetKeyName(2, "Hydraulic");
            this.imageList.Images.SetKeyName(3, "Mechanical");
            this.imageList.Images.SetKeyName(4, "Structural");
            this.imageList.Images.SetKeyName(5, "Plan");
            this.imageList.Images.SetKeyName(6, "Architectural");
            this.imageList.Images.SetKeyName(7, "Coordination");
            this.imageList.Images.SetKeyName(8, "Scopebox");
            this.imageList.Images.SetKeyName(9, "Threed");
            this.imageList.Images.SetKeyName(10, "Dependent");
            this.imageList.Images.SetKeyName(11, "Level");
            this.imageList.Images.SetKeyName(12, "Section");
            this.imageList.Images.SetKeyName(13, "Sheet");
            this.imageList.Images.SetKeyName(14, "Schedule");
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbAddCurrentView,
            this.tbRemoveViews,
            this.toolStripSeparator5,
            this.tbManage,
            this.toolStripSeparator1,
            this.tbRefresh,
            this.toolStripSeparator2,
            this.toolStripButton3,
            this.toolStripSeparator3,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton4,
            this.toolStripSeparator4});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(304, 25);
            this.toolStrip.TabIndex = 89;
            this.toolStrip.Text = "toolStrip1";
            // 
            // tbAddCurrentView
            // 
            this.tbAddCurrentView.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbAddCurrentView.Image = global::ViewCreator.Properties.Resources.Add1;
            this.tbAddCurrentView.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbAddCurrentView.Name = "tbAddCurrentView";
            this.tbAddCurrentView.Size = new System.Drawing.Size(23, 22);
            this.tbAddCurrentView.Text = "Add Current";
            this.tbAddCurrentView.Click += new System.EventHandler(this.tbAddCurrentView_Click);
            // 
            // tbRemoveViews
            // 
            this.tbRemoveViews.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbRemoveViews.Image = global::ViewCreator.Properties.Resources.RemoveFile;
            this.tbRemoveViews.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbRemoveViews.Name = "tbRemoveViews";
            this.tbRemoveViews.Size = new System.Drawing.Size(23, 22);
            this.tbRemoveViews.Text = "Remove All";
            this.tbRemoveViews.Click += new System.EventHandler(this.tbRemoveViews_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // tbManage
            // 
            this.tbManage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbManage.Image = global::ViewCreator.Properties.Resources.Manage;
            this.tbManage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbManage.Name = "tbManage";
            this.tbManage.Size = new System.Drawing.Size(23, 22);
            this.tbManage.Text = "toolStripButton1";
            this.tbManage.Click += new System.EventHandler(this.tbManage_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbRefresh
            // 
            this.tbRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tbRefresh.Image = global::ViewCreator.Properties.Resources.Refresh;
            this.tbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbRefresh.Name = "tbRefresh";
            this.tbRefresh.Size = new System.Drawing.Size(23, 22);
            this.tbRefresh.Text = "toolStripButton1";
            this.tbRefresh.Click += new System.EventHandler(this.tbRefresh_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::ViewCreator.Properties.Resources.Settings1;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "toolStripButton3";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::ViewCreator.Properties.Resources.NewPlan;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::ViewCreator.Properties.Resources.threeD;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::ViewCreator.Properties.Resources.Sections;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton4.Text = "toolStripButton4";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuEditView,
            this.toolStripMenuItem3,
            this.mnuDuplicateView,
            this.toolStripMenuItem1,
            this.mnuExportNavisworks,
            this.mnuScreenshot,
            this.toolStripMenuItem2,
            this.mnuDeletView,
            this.mnuDependentViews});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(171, 154);
            // 
            // mnuEditView
            // 
            this.mnuEditView.Image = global::ViewCreator.Properties.Resources.Edit;
            this.mnuEditView.Name = "mnuEditView";
            this.mnuEditView.Size = new System.Drawing.Size(170, 22);
            this.mnuEditView.Text = "Edit View";
            this.mnuEditView.Click += new System.EventHandler(this.mnuEditView_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(167, 6);
            // 
            // mnuDuplicateView
            // 
            this.mnuDuplicateView.Image = global::ViewCreator.Properties.Resources.Duplicate;
            this.mnuDuplicateView.Name = "mnuDuplicateView";
            this.mnuDuplicateView.Size = new System.Drawing.Size(170, 22);
            this.mnuDuplicateView.Text = "Duplicate View";
            this.mnuDuplicateView.Click += new System.EventHandler(this.mnuDuplicateView_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(167, 6);
            // 
            // mnuExportNavisworks
            // 
            this.mnuExportNavisworks.Image = global::ViewCreator.Properties.Resources.NW;
            this.mnuExportNavisworks.Name = "mnuExportNavisworks";
            this.mnuExportNavisworks.Size = new System.Drawing.Size(170, 22);
            this.mnuExportNavisworks.Text = "Export Navisworks";
            this.mnuExportNavisworks.Click += new System.EventHandler(this.mnuExportNavisworks_Click);
            // 
            // mnuScreenshot
            // 
            this.mnuScreenshot.Image = global::ViewCreator.Properties.Resources.Camera;
            this.mnuScreenshot.Name = "mnuScreenshot";
            this.mnuScreenshot.Size = new System.Drawing.Size(170, 22);
            this.mnuScreenshot.Text = "Screenshot";
            this.mnuScreenshot.Click += new System.EventHandler(this.mnuScreenshot_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(167, 6);
            // 
            // mnuDeletView
            // 
            this.mnuDeletView.Image = global::ViewCreator.Properties.Resources.Delete;
            this.mnuDeletView.Name = "mnuDeletView";
            this.mnuDeletView.Size = new System.Drawing.Size(170, 22);
            this.mnuDeletView.Text = "Delete View";
            this.mnuDeletView.Click += new System.EventHandler(this.mnuDeletView_Click);
            // 
            // mnuDependentViews
            // 
            this.mnuDependentViews.Image = global::ViewCreator.Properties.Resources.tree_view;
            this.mnuDependentViews.Name = "mnuDependentViews";
            this.mnuDependentViews.Size = new System.Drawing.Size(170, 22);
            this.mnuDependentViews.Text = "Dependent Views";
            this.mnuDependentViews.Click += new System.EventHandler(this.mnuDependentViews_Click);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 78);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.ctlLevelsCoord);
            this.splitContainer.Panel1.Controls.Add(this.pnlTitle);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.panel1);
            this.splitContainer.Panel2.Controls.Add(this.ctlSearch);
            this.splitContainer.Panel2.Controls.Add(this.toolStrip);
            this.splitContainer.Size = new System.Drawing.Size(304, 460);
            this.splitContainer.SplitterDistance = 230;
            this.splitContainer.TabIndex = 94;
            // 
            // ctlLevelsCoord
            // 
            this.ctlLevelsCoord.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlLevelsCoord.Location = new System.Drawing.Point(0, 25);
            this.ctlLevelsCoord.Name = "ctlLevelsCoord";
            this.ctlLevelsCoord.Size = new System.Drawing.Size(304, 205);
            this.ctlLevelsCoord.TabIndex = 93;
            this.ctlLevelsCoord.Load += new System.EventHandler(this.ctlLevelsCoord_Load);
            // 
            // pnlTitle
            // 
            this.pnlTitle.BackColor = System.Drawing.Color.SteelBlue;
            this.pnlTitle.Controls.Add(this.lblFloorSurfer);
            this.pnlTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitle.Location = new System.Drawing.Point(0, 0);
            this.pnlTitle.Name = "pnlTitle";
            this.pnlTitle.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.pnlTitle.Size = new System.Drawing.Size(304, 25);
            this.pnlTitle.TabIndex = 104;
            // 
            // lblFloorSurfer
            // 
            this.lblFloorSurfer.BackColor = System.Drawing.Color.Transparent;
            this.lblFloorSurfer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFloorSurfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFloorSurfer.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblFloorSurfer.Location = new System.Drawing.Point(5, 0);
            this.lblFloorSurfer.Name = "lblFloorSurfer";
            this.lblFloorSurfer.Size = new System.Drawing.Size(299, 25);
            this.lblFloorSurfer.TabIndex = 0;
            this.lblFloorSurfer.Text = "Coordination Levels";
            this.lblFloorSurfer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ctlViews);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(304, 170);
            this.panel1.TabIndex = 92;
            // 
            // ctlViews
            // 
            this.ctlViews.ContextMenuStrip = this.contextMenuStrip;
            this.ctlViews.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlViews.Location = new System.Drawing.Point(0, 25);
            this.ctlViews.Name = "ctlViews";
            this.ctlViews.Size = new System.Drawing.Size(304, 145);
            this.ctlViews.TabIndex = 90;
            this.ctlViews.ViewDoubleClicked += new System.EventHandler(this.ctlViews_ViewDoubleClicked);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SteelBlue;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.panel2.Size = new System.Drawing.Size(304, 25);
            this.panel2.TabIndex = 105;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "My Saved Views";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ctlSearch
            // 
            this.ctlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctlSearch.Location = new System.Drawing.Point(0, 25);
            this.ctlSearch.Name = "ctlSearch";
            this.ctlSearch.Search = null;
            this.ctlSearch.Size = new System.Drawing.Size(304, 31);
            this.ctlSearch.TabIndex = 91;
            this.ctlSearch.SearchChanged += new System.EventHandler(this.ctlSearch_SearchChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton7,
            this.toolStripSeparator6,
            this.toolStripButton5,
            this.toolStripButton6,
            this.toolStripButton8,
            this.toolStripButton9});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(304, 25);
            this.toolStrip1.TabIndex = 95;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton7.Image = global::ViewCreator.Properties.Resources.Save;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton7.Text = "toolStripButton7";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = global::ViewCreator.Properties.Resources.Level;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton5.Text = "toolStripButton5";
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Checked = true;
            this.toolStripButton6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = global::ViewCreator.Properties.Resources.LockView;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton6.Text = "toolStripButton6";
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton8.Image = global::ViewCreator.Properties.Resources.RevitLinks;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton8.Text = "toolStripButton8";
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton9.Image = global::ViewCreator.Properties.Resources.PipeLag;
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton9.Text = "toolStripButton9";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ctlServices1);
            this.panel3.Controls.Add(this.toolStrip1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(304, 78);
            this.panel3.TabIndex = 96;
            // 
            // ctlServices1
            // 
            this.ctlServices1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ctlServices1.Location = new System.Drawing.Point(0, 25);
            this.ctlServices1.Name = "ctlServices1";
            this.ctlServices1.Size = new System.Drawing.Size(304, 52);
            this.ctlServices1.TabIndex = 92;
            // 
            // ctlMyViews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panel3);
            this.Name = "ctlMyViews";
            this.Size = new System.Drawing.Size(304, 538);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.pnlTitle.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton tbAddCurrentView;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton tbRefresh;
        private ctlViewMyViews ctlViews;
        private ctlSearch ctlSearch;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuEditView;
        private System.Windows.Forms.ToolStripMenuItem mnuDuplicateView;
        private System.Windows.Forms.ToolStripMenuItem mnuDeletView;
        private System.Windows.Forms.ToolStripMenuItem mnuExportNavisworks;
        private System.Windows.Forms.ToolStripMenuItem mnuScreenshot;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mnuDependentViews;
        private System.Windows.Forms.ToolStripButton tbManage;
        private System.Windows.Forms.ToolStripButton tbRemoveViews;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private Sections.Views.Controls.ctlServices ctlServices1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private Sections.Views.Controls.ctlLevelsCoord ctlLevelsCoord;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel pnlTitle;
        private System.Windows.Forms.Label lblFloorSurfer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
    }
}
