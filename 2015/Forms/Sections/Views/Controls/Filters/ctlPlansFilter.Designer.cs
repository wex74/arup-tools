﻿namespace ViewCreator.Forms.Controls
{
    partial class ctlPlansFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckShowDependentViews = new System.Windows.Forms.CheckBox();
            this.btnLevels = new System.Windows.Forms.Button();
            this.ckShowActiveLevels = new System.Windows.Forms.CheckBox();
            this.ckAllLevels = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckAllLevels);
            this.groupBox1.Controls.Add(this.ckShowDependentViews);
            this.groupBox1.Controls.Add(this.btnLevels);
            this.groupBox1.Controls.Add(this.ckShowActiveLevels);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 75);
            this.groupBox1.TabIndex = 79;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // ckShowDependentViews
            // 
            this.ckShowDependentViews.AutoSize = true;
            this.ckShowDependentViews.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ckShowDependentViews.Location = new System.Drawing.Point(123, 46);
            this.ckShowDependentViews.Name = "ckShowDependentViews";
            this.ckShowDependentViews.Size = new System.Drawing.Size(110, 17);
            this.ckShowDependentViews.TabIndex = 66;
            this.ckShowDependentViews.Text = "Dependent Views";
            this.ckShowDependentViews.UseVisualStyleBackColor = true;
            this.ckShowDependentViews.CheckedChanged += new System.EventHandler(this.ckShowDependentViews_CheckedChanged);
            // 
            // btnLevels
            // 
            this.btnLevels.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLevels.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLevels.Location = new System.Drawing.Point(17, 21);
            this.btnLevels.Name = "btnLevels";
            this.btnLevels.Size = new System.Drawing.Size(100, 23);
            this.btnLevels.TabIndex = 68;
            this.btnLevels.Text = "Levels";
            this.btnLevels.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLevels.UseVisualStyleBackColor = true;
            this.btnLevels.Click += new System.EventHandler(this.btnLevels_Click);
            // 
            // ckShowActiveLevels
            // 
            this.ckShowActiveLevels.AutoSize = true;
            this.ckShowActiveLevels.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ckShowActiveLevels.Location = new System.Drawing.Point(123, 23);
            this.ckShowActiveLevels.Name = "ckShowActiveLevels";
            this.ckShowActiveLevels.Size = new System.Drawing.Size(90, 17);
            this.ckShowActiveLevels.TabIndex = 69;
            this.ckShowActiveLevels.Text = "Active Levels";
            this.ckShowActiveLevels.UseVisualStyleBackColor = true;
            this.ckShowActiveLevels.CheckedChanged += new System.EventHandler(this.ckShowActiveLevels_CheckedChanged);
            // 
            // ckAllLevels
            // 
            this.ckAllLevels.AutoSize = true;
            this.ckAllLevels.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ckAllLevels.Location = new System.Drawing.Point(19, 50);
            this.ckAllLevels.Name = "ckAllLevels";
            this.ckAllLevels.Size = new System.Drawing.Size(71, 17);
            this.ckAllLevels.TabIndex = 72;
            this.ckAllLevels.Text = "All Levels";
            this.ckAllLevels.UseVisualStyleBackColor = true;
            this.ckAllLevels.Click += new System.EventHandler(this.ckAllLevels_Click);
            // 
            // ctlPlansFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.Controls.Add(this.groupBox1);
            this.Name = "ctlPlansFilter";
            this.Size = new System.Drawing.Size(456, 75);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox ckShowDependentViews;
        private System.Windows.Forms.CheckBox ckShowActiveLevels;
        private System.Windows.Forms.Button btnLevels;
        private System.Windows.Forms.CheckBox ckAllLevels;
    }
}
