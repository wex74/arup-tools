﻿namespace ViewCreator.Forms.Sections.Views.Controls
{
    partial class ctlBuildingsFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckAllLevels = new System.Windows.Forms.CheckBox();
            this.ckSingleDiscipline = new System.Windows.Forms.CheckBox();
            this.btnDisciplines = new System.Windows.Forms.Button();
            this.btnLevels = new System.Windows.Forms.Button();
            this.contextMenuDisciplines = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuCoordination = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMechanical = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuElectrical = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHydraulic = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFire = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAll = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.contextMenuDisciplines.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckAllLevels);
            this.groupBox1.Controls.Add(this.ckSingleDiscipline);
            this.groupBox1.Controls.Add(this.btnDisciplines);
            this.groupBox1.Controls.Add(this.btnLevels);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(565, 75);
            this.groupBox1.TabIndex = 80;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // ckAllLevels
            // 
            this.ckAllLevels.AutoSize = true;
            this.ckAllLevels.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ckAllLevels.Location = new System.Drawing.Point(17, 48);
            this.ckAllLevels.Name = "ckAllLevels";
            this.ckAllLevels.Size = new System.Drawing.Size(71, 17);
            this.ckAllLevels.TabIndex = 71;
            this.ckAllLevels.Text = "All Levels";
            this.ckAllLevels.UseVisualStyleBackColor = true;
            this.ckAllLevels.CheckedChanged += new System.EventHandler(this.ckAllLevels_CheckedChanged);
            this.ckAllLevels.Click += new System.EventHandler(this.ckAllLevels_Click);
            // 
            // ckSingleDiscipline
            // 
            this.ckSingleDiscipline.AutoSize = true;
            this.ckSingleDiscipline.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ckSingleDiscipline.Location = new System.Drawing.Point(123, 48);
            this.ckSingleDiscipline.Name = "ckSingleDiscipline";
            this.ckSingleDiscipline.Size = new System.Drawing.Size(103, 17);
            this.ckSingleDiscipline.TabIndex = 70;
            this.ckSingleDiscipline.Text = "Single Discipline";
            this.ckSingleDiscipline.UseVisualStyleBackColor = true;
            this.ckSingleDiscipline.CheckedChanged += new System.EventHandler(this.ckSingleDiscipline_CheckedChanged);
            // 
            // btnDisciplines
            // 
            this.btnDisciplines.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnDisciplines.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDisciplines.Location = new System.Drawing.Point(122, 19);
            this.btnDisciplines.Name = "btnDisciplines";
            this.btnDisciplines.Size = new System.Drawing.Size(103, 23);
            this.btnDisciplines.TabIndex = 69;
            this.btnDisciplines.Text = "Disciplines";
            this.btnDisciplines.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDisciplines.UseVisualStyleBackColor = true;
            this.btnDisciplines.Click += new System.EventHandler(this.btnDisciplines_Click);
            // 
            // btnLevels
            // 
            this.btnLevels.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLevels.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLevels.Location = new System.Drawing.Point(16, 19);
            this.btnLevels.Name = "btnLevels";
            this.btnLevels.Size = new System.Drawing.Size(100, 23);
            this.btnLevels.TabIndex = 68;
            this.btnLevels.Text = "Levels";
            this.btnLevels.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLevels.UseVisualStyleBackColor = true;
            this.btnLevels.Click += new System.EventHandler(this.btnLevels_Click);
            // 
            // contextMenuDisciplines
            // 
            this.contextMenuDisciplines.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuCoordination,
            this.toolStripMenuItem1,
            this.mnuMechanical,
            this.mnuElectrical,
            this.mnuHydraulic,
            this.mnuFire,
            this.mnuAll});
            this.contextMenuDisciplines.Name = "contextMenuDisciplines";
            this.contextMenuDisciplines.Size = new System.Drawing.Size(148, 142);
            this.contextMenuDisciplines.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuDisciplines_Opening);
            // 
            // mnuCoordination
            // 
            this.mnuCoordination.Image = global::ViewCreator.Properties.Resources.Coordination;
            this.mnuCoordination.Name = "mnuCoordination";
            this.mnuCoordination.Size = new System.Drawing.Size(147, 22);
            this.mnuCoordination.Text = "Coordination";
            this.mnuCoordination.Click += new System.EventHandler(this.mnuCoordination_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(144, 6);
            // 
            // mnuMechanical
            // 
            this.mnuMechanical.Image = global::ViewCreator.Properties.Resources.Mechanical;
            this.mnuMechanical.Name = "mnuMechanical";
            this.mnuMechanical.Size = new System.Drawing.Size(147, 22);
            this.mnuMechanical.Text = "Mechanical";
            this.mnuMechanical.Click += new System.EventHandler(this.mnuMechanical_Click);
            // 
            // mnuElectrical
            // 
            this.mnuElectrical.Image = global::ViewCreator.Properties.Resources.Electrical;
            this.mnuElectrical.Name = "mnuElectrical";
            this.mnuElectrical.Size = new System.Drawing.Size(147, 22);
            this.mnuElectrical.Text = "Electrical";
            this.mnuElectrical.Click += new System.EventHandler(this.mnuElectrical_Click);
            // 
            // mnuHydraulic
            // 
            this.mnuHydraulic.Image = global::ViewCreator.Properties.Resources.Hydraulic;
            this.mnuHydraulic.Name = "mnuHydraulic";
            this.mnuHydraulic.Size = new System.Drawing.Size(147, 22);
            this.mnuHydraulic.Text = "Hydraulic";
            this.mnuHydraulic.Click += new System.EventHandler(this.mnuHydraulic_Click);
            // 
            // mnuFire
            // 
            this.mnuFire.Image = global::ViewCreator.Properties.Resources.Fire;
            this.mnuFire.Name = "mnuFire";
            this.mnuFire.Size = new System.Drawing.Size(147, 22);
            this.mnuFire.Text = "Fire";
            this.mnuFire.Click += new System.EventHandler(this.mnuFire_Click);
            // 
            // mnuAll
            // 
            this.mnuAll.Name = "mnuAll";
            this.mnuAll.Size = new System.Drawing.Size(147, 22);
            this.mnuAll.Text = "All Disciplines";
            this.mnuAll.Click += new System.EventHandler(this.mnuAll_Click);
            // 
            // ctlBuildingsFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.Controls.Add(this.groupBox1);
            this.Name = "ctlBuildingsFilter";
            this.Size = new System.Drawing.Size(565, 75);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.contextMenuDisciplines.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDisciplines;
        private System.Windows.Forms.Button btnLevels;
        private System.Windows.Forms.ContextMenuStrip contextMenuDisciplines;
        private System.Windows.Forms.ToolStripMenuItem mnuCoordination;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mnuMechanical;
        private System.Windows.Forms.ToolStripMenuItem mnuElectrical;
        private System.Windows.Forms.ToolStripMenuItem mnuHydraulic;
        private System.Windows.Forms.ToolStripMenuItem mnuFire;
        private System.Windows.Forms.ToolStripMenuItem mnuAll;
        private System.Windows.Forms.CheckBox ckSingleDiscipline;
        private System.Windows.Forms.CheckBox ckAllLevels;
    }
}
