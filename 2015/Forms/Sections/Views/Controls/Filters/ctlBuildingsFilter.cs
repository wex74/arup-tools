﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Forms.Sections.Views.Controls
{
    public partial class ctlBuildingsFilter : UserControl
    {
        private BuildingsScreenData _formData;

        public ctlBuildingsFilter()
        {
            InitializeComponent();
        }

        public event EventHandler DisciplinesChanged;
        public event EventHandler LevelsChanged;

        protected virtual void OnDisciplinesChanged(object sender, EventArgs e)
        {
            var handler = DisciplinesChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        protected virtual void OnLevelsChanged(object sender, EventArgs e)
        {
            var handler = LevelsChanged;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        public void LoadControl()
        {
            _formData = MainApp.AppData.FormSettings.BuildingsScreenData;


            btnDisciplines.Visible = MainApp.AppData.IsUserBuildings;
            ckSingleDiscipline.Visible = MainApp.AppData.IsUserBuildings;

            SetButtonText();
        }

        private void SetButtonText()
        {
            btnLevels.Text = "Levels " + _formData.Levels.Count + " / " + Level_Utils.GetLevels().Count;


            if (MainApp.AppData.IsUserBuildings)
            {
                btnDisciplines.Text = "Disciplines " + _formData.DisciplineCount + " / " + _formData.DisciplineTotalCount;
            }
           

            ckAllLevels.Visible = _formData.Levels.Count < Level_Utils.GetLevels().Count;

            if (_formData.Levels.Count < Level_Utils.GetLevels().Count)
            {
                ckAllLevels.Checked = false;
            }
        }

        private void btnDisciplines_Click(object sender, EventArgs e)
        {
            var btnSender = (Button) sender;
            var ptLowerLeft = new Point(0, btnSender.Height);
            ptLowerLeft = btnSender.PointToScreen(ptLowerLeft);
            contextMenuDisciplines.Show(ptLowerLeft);
        }

        private void contextMenuDisciplines_Opening(object sender, CancelEventArgs e)
        {
            mnuCoordination.Checked = _formData.View.ShowCoordination;
            mnuMechanical.Checked = _formData.View.ShowMechanical;
            mnuElectrical.Checked = _formData.View.ShowElectrical;
            mnuHydraulic.Checked = _formData.View.ShowHydraulic;
            mnuFire.Checked = _formData.View.ShowFire;


            ckSingleDiscipline.Checked = _formData.SingleDiscipline;


            if (_formData.SingleDiscipline)
            {
                mnuAll.Visible = false;
            }
            else
            {
                mnuAll.Visible = true;
                mnuAll.Checked = _formData.DisciplineCount == _formData.DisciplineTotalCount;
            }
        }

        private void ResetDisciplines()
        {
            _formData.View.ShowCoordination = false;
            _formData.View.ShowMechanical = false;
            _formData.View.ShowElectrical = false;
            _formData.View.ShowHydraulic = false;
            _formData.View.ShowFire = false;
        }

        private void mnuCoordination_Click(object sender, EventArgs e)
        {
            if (ckSingleDiscipline.Checked)
            {
                ResetDisciplines();
                _formData.View.ShowCoordination = true;
            }
            else
            {
                mnuCoordination.Checked = !mnuCoordination.Checked;
                _formData.View.ShowCoordination = mnuCoordination.Checked;
            }

            SaveData();
            OnDisciplinesChanged(Discipline.Coordination, e);
        }

        private void SaveData()
        {
            MainApp.AppData.SaveUserModelData();
            SetButtonText();
        }

        private void mnuMechanical_Click(object sender, EventArgs e)
        {
            if (ckSingleDiscipline.Checked)
            {
                ResetDisciplines();
                _formData.View.ShowMechanical = true;
            }
            else
            {
                mnuMechanical.Checked = !mnuMechanical.Checked;
                _formData.View.ShowMechanical = mnuMechanical.Checked;
            }


            SaveData();
            OnDisciplinesChanged(Discipline.Mechanical, e);
        }

        private void mnuElectrical_Click(object sender, EventArgs e)
        {
            if (ckSingleDiscipline.Checked)
            {
                ResetDisciplines();
                _formData.View.ShowElectrical = true;
            }
            else
            {
                mnuElectrical.Checked = !mnuElectrical.Checked;
                _formData.View.ShowElectrical = mnuElectrical.Checked;
            }


            SaveData();
            OnDisciplinesChanged(Discipline.Electrical, e);
        }

        private void mnuHydraulic_Click(object sender, EventArgs e)
        {
            if (ckSingleDiscipline.Checked)
            {
                ResetDisciplines();
                _formData.View.ShowHydraulic = true;
            }
            else
            {
                mnuHydraulic.Checked = !mnuHydraulic.Checked;
                _formData.View.ShowHydraulic = mnuHydraulic.Checked;
            }


            SaveData();
            OnDisciplinesChanged(Discipline.Hydraulic, e);
        }

        private void mnuFire_Click(object sender, EventArgs e)
        {
            if (ckSingleDiscipline.Checked)
            {
                ResetDisciplines();
                _formData.View.ShowFire = true;
            }
            else
            {
                mnuFire.Checked = !mnuFire.Checked;
                _formData.View.ShowFire = mnuFire.Checked;
            }


            SaveData();
            OnDisciplinesChanged(Discipline.Electrical, e);
        }

        private void btnLevels_Click(object sender, EventArgs e)
        {
            var frm = new frmSelectLevels(Level_Utils.GetLevelNames(), _formData.Levels);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _formData.Levels = frm.SelectedLevels;
                SaveData();
                OnLevelsChanged(sender, e);
            }
        }

        private void mnuAll_Click(object sender, EventArgs e)
        {
            mnuAll.Checked = !mnuAll.Checked;

            _formData.View.ShowCoordination = mnuAll.Checked;
            _formData.View.ShowMechanical = mnuAll.Checked;
            _formData.View.ShowElectrical = mnuAll.Checked;
            _formData.View.ShowHydraulic = mnuAll.Checked;
            _formData.View.ShowFire = mnuAll.Checked;

            SaveData();
            OnDisciplinesChanged(null, e);
        }

        private void ckSingleDiscipline_CheckedChanged(object sender, EventArgs e)
        {
            _formData.SingleDiscipline = ckSingleDiscipline.Checked;
            SaveData();
        }

        private void ckAllLevels_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void ckAllLevels_Click(object sender, EventArgs e)
        {
            _formData.Levels = Level_Utils.GetLevelNames();
            SaveData();
            SetButtonText();
            OnLevelsChanged(sender, e);
        }
    }
}