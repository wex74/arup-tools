﻿namespace ViewCreator.Forms.Sections.Views.Controls
{
    partial class ctlViewsFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLevels = new System.Windows.Forms.Button();
            this.chShowSheets = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox2);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.chShowSheets);
            this.groupBox1.Controls.Add(this.btnLevels);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(282, 82);
            this.groupBox1.TabIndex = 80;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // btnLevels
            // 
            this.btnLevels.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLevels.Location = new System.Drawing.Point(16, 49);
            this.btnLevels.Name = "btnLevels";
            this.btnLevels.Size = new System.Drawing.Size(100, 23);
            this.btnLevels.TabIndex = 68;
            this.btnLevels.Text = "Levels";
            this.btnLevels.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLevels.UseVisualStyleBackColor = true;
            this.btnLevels.Click += new System.EventHandler(this.btnLevels_Click);
            // 
            // chShowSheets
            // 
            this.chShowSheets.Image = global::ViewCreator.Properties.Resources.NewPlan;
            this.chShowSheets.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chShowSheets.Location = new System.Drawing.Point(16, 19);
            this.chShowSheets.Name = "chShowSheets";
            this.chShowSheets.Size = new System.Drawing.Size(79, 28);
            this.chShowSheets.TabIndex = 70;
            this.chShowSheets.Text = "Sheets";
            this.chShowSheets.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chShowSheets.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.Image = global::ViewCreator.Properties.Resources.threeD;
            this.checkBox1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox1.Location = new System.Drawing.Point(101, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(79, 28);
            this.checkBox1.TabIndex = 71;
            this.checkBox1.Text = "ThreeD";
            this.checkBox1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.Image = global::ViewCreator.Properties.Resources.Sections;
            this.checkBox2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox2.Location = new System.Drawing.Point(186, 19);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(79, 28);
            this.checkBox2.TabIndex = 72;
            this.checkBox2.Text = "Sections";
            this.checkBox2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // ctlFilterViews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "ctlViewsFilter";
            this.Size = new System.Drawing.Size(282, 82);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chShowSheets;
        private System.Windows.Forms.Button btnLevels;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}
