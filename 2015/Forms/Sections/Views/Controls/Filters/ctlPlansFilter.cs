﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlPlansFilter : UserControl
    {
        private PlansScreenData _formData;

        public event EventHandler ActiveLevelsChanged;
        public event EventHandler ShowDependentViewsChanged;
        public event EventHandler LevelsChanged;

        private bool _loaded;

        public ctlPlansFilter()
        {
            InitializeComponent();
        }

        protected virtual void OnActiveLevelsChanged(bool value, EventArgs e)
        {
            EventHandler handler = ActiveLevelsChanged;
            if (handler != null)
            {
                handler(value, e);
            }
        }

    

        protected virtual void OnShowDependentViewsChanged(bool value, EventArgs e)
        {
            EventHandler handler = ShowDependentViewsChanged;
            if (handler != null)
            {
                handler(value, e);
            }
        }

      
        protected virtual void OnLevelsChanged(object sender,EventArgs e)
        {
            EventHandler handler = LevelsChanged;
            if (handler != null)
            {
                handler(sender,e);
            }
        }


        public void LoadControl()
        {
            _formData = MainApp.AppData.FormSettings.PlansScreenData;

            ckShowActiveLevels.Checked = _formData.ShowActiveLevels;
            ckShowDependentViews.Checked = _formData.ShowDependentViews;

            SetLevelsButtonText();

            _loaded = true;
        }


        private void SetLevelsButtonText()
        {
            btnLevels.Text = "Levels " + _formData.Levels.Count + " / " + Level_Utils.LevelsCount();

            ckAllLevels.Visible = _formData.Levels.Count < Level_Utils.GetLevels().Count;

            if (_formData.Levels.Count < Level_Utils.GetLevels().Count)
            {
                ckAllLevels.Checked = false;
            }
        }

        private void btnLevels_Click(object sender, EventArgs e)
        {
            var frm = new frmSelectLevels(Level_Utils.GetLevelNames(), _formData.Levels);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                _formData.Levels = frm.SelectedLevels;
                MainApp.AppData.SaveUserModelData();

                SetLevelsButtonText();
                OnLevelsChanged(sender, e);
            }
        }

      

        private void ckShowDependentViews_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            SaveData();
            OnShowDependentViewsChanged(ckShowDependentViews.Checked, e);
        }

        private void SaveData()
        {
            _formData.ShowActiveLevels = ckShowActiveLevels.Checked;
            _formData.ShowDependentViews = ckShowDependentViews.Checked;
            MainApp.AppData.SaveUserModelData();
        }

        private void ckShowActiveLevels_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            SaveData();
            OnActiveLevelsChanged(ckShowActiveLevels.Checked, e);
        }

        private void ckAllLevels_Click(object sender, EventArgs e)
        {
            _formData.Levels = Level_Utils.GetLevelNames();
            SaveData();
            SetLevelsButtonText();
            OnLevelsChanged(sender, e);
        }
    }
}
