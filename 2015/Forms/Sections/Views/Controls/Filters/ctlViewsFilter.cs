﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Forms.Sections.Views.Controls
{
    public partial class ctlViewsFilter : UserControl
    {
        public event EventHandler LevelsChanged;

        public ctlViewsFilter()
        {
            InitializeComponent();
        }


        protected virtual void OnLevelsChanged(List<string> levels, EventArgs e)
        {
            EventHandler handler = LevelsChanged;
            if (handler != null)
            {
                handler(levels, e);
            }
        }


        private void btnLevels_Click(object sender, EventArgs e)
        {
            var frm = new frmSelectLevels();

            if (frm.ShowDialog() == DialogResult.OK)
            {
                SetLevelsButtonText();
                OnLevelsChanged(frm.SelectedLevels, e);
            }
        }


        private void SetLevelsButtonText()
        {
            btnLevels.Text = "Levels " + MainApp.DataAccess.VisibleLevels.Count + " / " + Level_Utils.GetLevels().Count;
        }
    }
}
