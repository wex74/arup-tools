﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ViewCreator.Events;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlMyViews : UserControl
    {
        private string _search;

        public ctlMyViews()
        {
            InitializeComponent();
        }

        public void LoadControl()
        {
            LoadViews();
        }

        private void LoadViews()
        {
            ctlViews.LoadControl(_search);
            ctlLevelsCoord.LoadControl();
        }

        private void ctlSearch_SearchChanged(object sender, EventArgs e)
        {
            _search = (string) sender;
            LoadViews();
        }

        private void ctlViews_ViewDoubleClicked(object sender, EventArgs e)
        {
            var view = (View) sender;
            ExternalEvents_View.Run_Navigate(view);
        }

        private void tbAddCurrentView_Click(object sender, EventArgs e)
        {
            App.DataAccess.ModelsData.AddView(RVT_Utils.ActiveView.Id.IntegerValue);
            LoadViews();
        }

        public void SelectCurrentView()
        {
            ctlViews.SelectCurrentView();
        }

        private void tbRemoveViews_Click(object sender, EventArgs e)
        {
            App.DataAccess.ModelsData.RemoveViews();
            LoadViews();
        }

        private void tbManage_Click(object sender, EventArgs e)
        {

        }

        private void mnuDependentViews_Click(object sender, EventArgs e)
        {
            UI_Utils.ShowNotImplementedMessage();
        }

        private void tbRefresh_Click(object sender, EventArgs e)
        {
            LoadViews();
        }

        private void mnuExportNavisworks_Click(object sender, EventArgs e)
        {
           UI_Utils.ShowNotImplementedMessage();
        }

     
        private void mnuDuplicateView_Click(object sender, EventArgs e)
        {
            UI_Utils.ShowNotImplementedMessage();
        }

        private void mnuEditView_Click(object sender, EventArgs e)
        {
            var view = ctlViews.GetView();

            if (view != null)
            {
                ExternalEvents_View.View = view;
                ExternalEvents_View.Run_EditView();
            }
           
        }

        private void mnuScreenshot_Click(object sender, EventArgs e)
        {
            UI_Utils.ShowNotImplementedMessage();
        }

        private void mnuDeletView_Click(object sender, EventArgs e)
        {
            UI_Utils.ShowNotImplementedMessage();
        }

        private void ctlFilterViews_LevelsChanged(object sender, EventArgs e)
        {
            List<string> levels = (List<string>) sender;
            ctlLevelsCoord.SetLevels(levels);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {

        }

        private void ctlLevelsCoord_Load(object sender, EventArgs e)
        {

        }
    }
}