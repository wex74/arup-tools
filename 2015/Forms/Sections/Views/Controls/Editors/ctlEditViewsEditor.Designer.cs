﻿namespace ViewCreator.Forms.Sections.Views.Controls
{
    partial class ctlEditViewsEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnTemplate = new System.Windows.Forms.Button();
            this.btnSelectTemplate = new System.Windows.Forms.Button();
            this.cboTemplate = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtReplace = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.cboNameTools = new System.Windows.Forms.ComboBox();
            this.btnUpdateName = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboScopebox = new System.Windows.Forms.ComboBox();
            this.btnScopebox = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboScale = new System.Windows.Forms.ComboBox();
            this.btnScale = new System.Windows.Forms.Button();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnTemplate);
            this.groupBox7.Controls.Add(this.btnSelectTemplate);
            this.groupBox7.Controls.Add(this.cboTemplate);
            this.groupBox7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox7.Location = new System.Drawing.Point(378, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(274, 68);
            this.groupBox7.TabIndex = 77;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Template";
            // 
            // btnTemplate
            // 
            this.btnTemplate.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnTemplate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTemplate.Location = new System.Drawing.Point(241, 28);
            this.btnTemplate.Name = "btnTemplate";
            this.btnTemplate.Size = new System.Drawing.Size(22, 22);
            this.btnTemplate.TabIndex = 68;
            this.btnTemplate.Text = "ü";
            this.btnTemplate.UseVisualStyleBackColor = true;
            this.btnTemplate.Click += new System.EventHandler(this.btnTemplate_Click);
            // 
            // btnSelectTemplate
            // 
            this.btnSelectTemplate.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnSelectTemplate.Location = new System.Drawing.Point(212, 27);
            this.btnSelectTemplate.Name = "btnSelectTemplate";
            this.btnSelectTemplate.Size = new System.Drawing.Size(23, 23);
            this.btnSelectTemplate.TabIndex = 67;
            this.btnSelectTemplate.Text = "...";
            this.btnSelectTemplate.UseVisualStyleBackColor = true;
            this.btnSelectTemplate.Click += new System.EventHandler(this.btnSelectTemplate_Click);
            // 
            // cboTemplate
            // 
            this.cboTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTemplate.FormattingEnabled = true;
            this.cboTemplate.Location = new System.Drawing.Point(15, 29);
            this.cboTemplate.Name = "cboTemplate";
            this.cboTemplate.Size = new System.Drawing.Size(191, 21);
            this.cboTemplate.TabIndex = 66;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtReplace);
            this.groupBox4.Controls.Add(this.txtName);
            this.groupBox4.Controls.Add(this.cboNameTools);
            this.groupBox4.Controls.Add(this.btnUpdateName);
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Location = new System.Drawing.Point(3, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(369, 68);
            this.groupBox4.TabIndex = 78;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Update Name";
            // 
            // txtReplace
            // 
            this.txtReplace.Location = new System.Drawing.Point(119, 29);
            this.txtReplace.Name = "txtReplace";
            this.txtReplace.Size = new System.Drawing.Size(98, 20);
            this.txtReplace.TabIndex = 53;
            this.txtReplace.Visible = false;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(15, 29);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(204, 20);
            this.txtName.TabIndex = 51;
            // 
            // cboNameTools
            // 
            this.cboNameTools.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboNameTools.FormattingEnabled = true;
            this.cboNameTools.Items.AddRange(new object[] {
            "Prefix",
            "Suffix",
            "Remove",
            "Find & Replace",
            "To Upper Case",
            "To Sentence Case"});
            this.cboNameTools.Location = new System.Drawing.Point(225, 29);
            this.cboNameTools.Name = "cboNameTools";
            this.cboNameTools.Size = new System.Drawing.Size(100, 21);
            this.cboNameTools.TabIndex = 52;
            this.cboNameTools.SelectedIndexChanged += new System.EventHandler(this.cboNameTools_SelectedIndexChanged);
            // 
            // btnUpdateName
            // 
            this.btnUpdateName.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnUpdateName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnUpdateName.Location = new System.Drawing.Point(331, 30);
            this.btnUpdateName.Name = "btnUpdateName";
            this.btnUpdateName.Size = new System.Drawing.Size(22, 22);
            this.btnUpdateName.TabIndex = 50;
            this.btnUpdateName.Text = "ü";
            this.btnUpdateName.UseVisualStyleBackColor = true;
            this.btnUpdateName.Click += new System.EventHandler(this.btnUpdateName_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cboScopebox);
            this.groupBox1.Controls.Add(this.btnScopebox);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(658, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 68);
            this.groupBox1.TabIndex = 79;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scopeboxes";
            // 
            // cboScopebox
            // 
            this.cboScopebox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboScopebox.FormattingEnabled = true;
            this.cboScopebox.Location = new System.Drawing.Point(15, 29);
            this.cboScopebox.Name = "cboScopebox";
            this.cboScopebox.Size = new System.Drawing.Size(149, 21);
            this.cboScopebox.TabIndex = 66;
            // 
            // btnScopebox
            // 
            this.btnScopebox.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnScopebox.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnScopebox.Location = new System.Drawing.Point(170, 29);
            this.btnScopebox.Name = "btnScopebox";
            this.btnScopebox.Size = new System.Drawing.Size(22, 22);
            this.btnScopebox.TabIndex = 47;
            this.btnScopebox.Text = "ü";
            this.btnScopebox.UseVisualStyleBackColor = true;
            this.btnScopebox.Click += new System.EventHandler(this.btnScopebox_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboScale);
            this.groupBox2.Controls.Add(this.btnScale);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(867, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(203, 68);
            this.groupBox2.TabIndex = 77;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scale";
            // 
            // cboScale
            // 
            this.cboScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboScale.FormattingEnabled = true;
            this.cboScale.Location = new System.Drawing.Point(15, 29);
            this.cboScale.Name = "cboScale";
            this.cboScale.Size = new System.Drawing.Size(149, 21);
            this.cboScale.TabIndex = 66;
            // 
            // btnScale
            // 
            this.btnScale.Font = new System.Drawing.Font("Wingdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnScale.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnScale.Location = new System.Drawing.Point(170, 29);
            this.btnScale.Name = "btnScale";
            this.btnScale.Size = new System.Drawing.Size(22, 22);
            this.btnScale.TabIndex = 47;
            this.btnScale.Text = "ü";
            this.btnScale.UseVisualStyleBackColor = true;
            this.btnScale.Click += new System.EventHandler(this.btnScale_Click);
            // 
            // ctlEditViewsEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSlateGray;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox7);
            this.Name = "ctlEditViewsEditor";
            this.Size = new System.Drawing.Size(1072, 77);
            this.groupBox7.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox cboTemplate;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.ComboBox cboNameTools;
        private System.Windows.Forms.Button btnUpdateName;
        private System.Windows.Forms.Button btnSelectTemplate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboScopebox;
        private System.Windows.Forms.Button btnScopebox;
        private System.Windows.Forms.Button btnTemplate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboScale;
        private System.Windows.Forms.Button btnScale;
        private System.Windows.Forms.TextBox txtReplace;
    }
}
