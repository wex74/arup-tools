﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Forms.Controls.Views;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Sections.Views.Controls
{
    public partial class ctlEditViewsEditor : UserControl
    {
        private ctlEditViewsViews _ctl;
        private List<UpdateView> _views;

        public event EventHandler ViewsUpdated;

        public enum NameTools
        {
            Prefix,
            Suffix,
            Remove,
            FindReplace,
            UpperCase,
            SentenceCase,
        }

        public ctlEditViewsEditor()
        {
            InitializeComponent();
        }




        public void LoadControl(ctlEditViewsViews ctl)
        {
            _ctl = ctl;

            cboTemplate.LoadCombo(Template_Utils.GetViewTemplateNames(), null, "None");
            cboScopebox.LoadCombo(Scopebox_Utils.GetScopeBoxNames(), "", "");
            cboScale.LoadCombo(View_Utils.GetScaleNames() , null, null);

        }

        protected virtual void OnViewsUpdated(List<UpdateView> views, EventArgs e)
        {
            EventHandler handler = ViewsUpdated;
            if (handler != null)
            {

                handler(views, e);
            }
        }

        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            cboTemplate.Text = UI_Utils.SelectViewTemplate(cboTemplate.Text);
        }

        private void btnTemplate_Click(object sender, EventArgs e)
        {
            if (cboTemplate.SelectedIndex < 0) return;

            _views = _ctl.GetSelectedUpdateViews();
            if (!_views.Any()) return;

            foreach (var view in _views)
            {

                if (view != null)
                {
                    if (view.Template != cboTemplate.Text)
                    {
                        view.SetTemplate(cboTemplate.Text);
                    }
                }
            }

            cboTemplate.SelectedIndex = -1;

            OnViewsUpdated(_views, e);
        }

        private void btnScopebox_Click(object sender, EventArgs e)
        {
            if (cboScopebox.SelectedIndex < 0) return;

            _views = _ctl.GetSelectedUpdateViews();
            if (!_views.Any()) return;

            foreach (var view in _views)
            {

                if (view != null)
                {
                    if (view.ScopeBox != cboScopebox.Text)
                    {
                        view.SetScopeBox(cboScopebox.Text);
                    }
                }
            }

            cboScopebox.SelectedIndex = -1;

            OnViewsUpdated(_views, e);
        }

        private void btnScale_Click(object sender, EventArgs e)
        {
            if (cboScale.SelectedIndex < 0) return;
            if (cboScale.Text == "") return;

            _views = _ctl.GetSelectedUpdateViews();
            if (!_views.Any()) return;

            foreach (var view in _views)
            {

                if (view != null)
                {
                    if (view.Scale != View_Utils.GetScale(cboScale.Text))
                    {
                        view.SetScale(View_Utils.GetScale(cboScale.Text));
                    }
                }
            }

            cboScale.SelectedIndex = -1;

            OnViewsUpdated(_views, e);
        }

        private void btnUpdateName_Click(object sender, EventArgs e)
        {
            if (cboNameTools.SelectedIndex != (int) NameTools.UpperCase &&
                cboNameTools.SelectedIndex != (int) NameTools.SentenceCase)
            {
                if (txtName.Text == "") return;
            }
           

            _views = _ctl.GetSelectedUpdateViews();
            if (!_views.Any()) return;

            foreach (var view in _views)
            {

                if (view != null)
                {

                    if (cboNameTools.SelectedIndex == -1)
                    {
                        if (view.ViewName != txtName.Text)
                        {
                            view.SetName(txtName.Text);
                        }
                    }

                    if (cboNameTools.SelectedIndex ==(int) NameTools.Prefix)
                    {
                        view.SetName(txtName.Text + view.ViewName);
                    }

                    if (cboNameTools.SelectedIndex == (int)NameTools.Suffix)
                    {
                        view.SetName(view.ViewName + txtName.Text);
                    }


                    if (cboNameTools.SelectedIndex == (int)NameTools.Remove)
                    {
                        view.SetName(view.ViewName.Replace(txtName.Text, ""));
                    }

                    if (cboNameTools.SelectedIndex == (int)NameTools.FindReplace)
                    {
  
                        if (txtName.Text != "" && txtReplace.Text != "")
                        {
                            var find = txtName.Text;
                            var replace = txtReplace.Text;


                            var newText = view.ViewName.Replace(find, replace);
                            view.SetName(newText);
                        }
                      
                    }

                    if (cboNameTools.SelectedIndex == (int)NameTools.UpperCase)
                    {
                        view.SetName(view.ViewName.ToUpper());
                    }

                    if (cboNameTools.SelectedIndex == (int)NameTools.SentenceCase)
                    {


                        string titleCase = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(view.ViewName.ToLower());

                        view.SetName(titleCase);
                    }

                }
            }

            txtName.Text = "";
            txtReplace.Text = "";
            cboNameTools.SelectedIndex = -1;


            OnViewsUpdated(_views, e);
        }

        private void cboNameTools_SelectedIndexChanged(object sender, EventArgs e)
        {
            int origWidth = 204;
            int newWidth = 98;

            if (cboNameTools.SelectedIndex == (int)NameTools.UpperCase || cboNameTools.SelectedIndex == (int)NameTools.SentenceCase)
            {
                txtName.Enabled = false;
            }


            if (cboNameTools.SelectedIndex == (int)NameTools.FindReplace)
            {
                txtName.Width = newWidth;
                txtReplace.Visible = true;
            }
            else
            {
                txtName.Width = origWidth;
                txtReplace.Visible = false;
            }

            txtName.Enabled = true;
        }
    }
}
