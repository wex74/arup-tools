﻿using System;
using System.Linq;
using Autodesk.Revit.DB;
using ViewCreator.Events.Views;
using ViewCreator.Model;
using ViewCreator.Model.Settings.User;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using Form = System.Windows.Forms.Form;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms.Add_Edit_Forms
{
    public partial class frmEditView : Form
    {
        private UserSettings _settings;
        private bool _loaded;

        public frmEditView()
        {
            InitializeComponent();
        }

        private UpdateView _updateView;
        private View _view;


        private void frmEditView_Load(object sender, EventArgs e)
        {
           
        }


        public bool ApplyOnChange
        {
            get { return ckApplyOnChange.Checked; }
        }


        public void LoadControl()
        {


            if (_view != null && _updateView != null)
            {
                if (ApplyOnChange && _updateView.IsChanged)
                {
                    ApplyChanges();
                }

            }


            _loaded = false;

            _settings = MainApp.AppData.UserSettings;

            _view = RVT_Utils.ActiveView;
            _updateView = null;

            if (_view.IsValidPlan() || _view.ViewType == ViewType.ThreeD || _view.ViewType == ViewType.Section)
            {
                btnOk.Enabled = true;
                txtName.Enabled = true;
                cboTemplate.Enabled = true;
                cboScale.Enabled = true;
                btnSelectTemplate.Enabled = true;
                ckFavourite.Enabled = true;

                _updateView = new UpdateView(_view);

                txtName.Text = _updateView.ViewName;

                var templates = _settings.GetViewTemplates();

                if (!templates.Exists(x => x == _updateView.Template) && _updateView.Template != "None")
                {
                    templates.Add(_updateView.Template);
                }

                cboScale.LoadCombo(View_Utils.GetScaleNames(), View_Utils.GetScale(_updateView.Scale),null);
                cboTemplate.LoadCombo(templates, _updateView.Template, "None");

                ckFavourite.Checked = _updateView.Favourite;

               
            }
            else
            {
                txtName.Text = "";
                cboTemplate.Items.Clear();
                cboScale.Items.Clear();


                txtName.Enabled = false;
                cboTemplate.Enabled = false;
                cboScale.Enabled = false;
                btnSelectTemplate.Enabled = false;
                btnOk.Enabled = false;
                ckFavourite.Enabled = false;
            }

          

            if (_view.IsValidPlan())
            {
                var scopeboxes = Scopebox_Utils.GetScopeBoxNames();

                if (scopeboxes.Any())
                {
                    cboScopeBox.LoadCombo(scopeboxes, _updateView.ScopeBox, "");

                }

                cboScopeBox.Enabled = scopeboxes.Any();
            }
            else
            {
                cboScopeBox.Items.Clear();
                cboScopeBox.Enabled = false;
            }



            _loaded = true;




            EnableApply();



        }



        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            var template = UI_Utils.SelectViewTemplate(cboTemplate.Text);
            if (template == null) return;


            if (!cboTemplate.Items.Contains(template))
            {
                cboTemplate.Items.Add(template);
            }

            cboTemplate.Text = template;

            if (!string.IsNullOrEmpty(template) && !MainApp.AppData.UserSettings.ViewTemplates.Exists(x => x == template))
            {
                MainApp.AppData.UserSettings.ViewTemplates.Add(template);
                MainApp.AppData.SaveUserData();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ApplyChanges();
        }

        public void ApplyChanges()
        {
            if (_updateView == null) return;
            if (_updateView.IsChanged)
            {
                UpdateViewEvent.Run(_view, _updateView);
            }
          
        }



        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

      
        private void ckFavourite_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            _updateView.SetFavourite(ckFavourite.Checked);
            EnableApply();
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
             _updateView.SetName(txtName.Text);
            EnableApply();
        }


        private void EnableApply()
        {
            if (_updateView == null) return;
            btnOk.Enabled = _updateView.IsChanged;
        }

        private void cboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            _updateView.SetTemplate(cboTemplate.Text);
            EnableApply();
        }

        private void cboScopeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            _updateView.SetScopeBox(cboScopeBox.Text);
            EnableApply();
        }

        private void cboScale_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            _updateView.SetScale(View_Utils.GetScale(cboScale.Text));
            EnableApply();
        }

        private void frmEditView_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            EditViewUI.FormVisible = false;
        }
    }
}