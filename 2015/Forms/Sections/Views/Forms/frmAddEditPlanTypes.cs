﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Model;

namespace ViewCreator.Forms
{
    public partial class frmAddEditPlanTypes : Form
    {
        public List<string> PlanTypes { get; set; }
        public List<string> UpdatedPlanTypes { get; set; }

        private bool _loaded;

        public frmAddEditPlanTypes()
        {
            InitializeComponent();
        }

        private void frmPlanTypes_Load(object sender, EventArgs e)
        {
            foreach (var plantype in PlanTypes)
            {
                lstPlanTypes.Items.Add(plantype);
            }

            if (lstPlanTypes.Items.Count > 0)
            {
                lstPlanTypes.SelectedIndex = 0;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

        }
    }
}
