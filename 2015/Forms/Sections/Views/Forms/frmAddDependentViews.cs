﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Data.Forms;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using Form = System.Windows.Forms.Form;

namespace ViewCreator.Forms.Add_Edit_Forms
{
    public partial class frmAddDependentViews : Form
    {
        private Discipline _discipline;
        private DependentViewsData _formData;
        private bool _loaded;
        private List<Plan> _plans;
        private List<Element> _scopeBoxes;
        private List<ViewInfo> _viewInfos;

        public frmAddDependentViews(Discipline discipline, List<ViewInfo> viewInfos, List<Plan> plans)
        {
            InitializeComponent();
            _discipline = discipline;
            _viewInfos = viewInfos;
            _plans = plans;
        }

        private void frmAddScopeBoxes_Load(object sender, EventArgs e)
        {
            if (_viewInfos == null || _plans == null) return;

            _formData = MainApp.AppData.FormSettings.DependentViewsData;

            _scopeBoxes = Scopebox_Utils.GetScopeBoxes();

            lstScopeboxes.Enabled = _scopeBoxes.Count > 0;

            if (_scopeBoxes.Count > 0)
            {
                lstScopeboxes.LoadList(Scopebox_Utils.GetScopeBoxNames());
                lstScopeboxes.SetCheckedItems(_formData.ScopeBoxes);

                ckAllScopeBoxes.Checked = lstScopeboxes.AllItemsChecked();
            }
            else
            {
                numBox.Value = 1;
            }

            optNewViews.Checked = _formData.NewViews;
            optModeViews.Checked = !_formData.NewViews;

            if (optNewViews.Checked && _viewInfos.Count == 0 && _plans.Count > 0)
            {
                optModeViews.Checked = true;
                optNewViews.Checked = false;
            }

            if (optModeViews.Checked && _plans.Count == 0 && _viewInfos.Count > 0)
            {
                optNewViews.Checked = true;
                optModeViews.Checked = false;
            }

            BuildPlans();

            _loaded = true;

            txtSearch.Text = _formData.Search;
        }

        private void BuildPlans()
        {
            lstViews.Items.Clear();

            var search = txtSearch.Text;


            if (optNewViews.Checked)
            {
                if (search != "")
                {
                    _viewInfos =
                        _viewInfos.Where(x => x.ToString().Matches(search))
                            .ToList();
                }

                lstViews.LoadList(_viewInfos.Select(x => x.ToString()).ToList());
            }
            else
            {
                if (search != "")
                {
                    _plans =
                        _plans.Where(x => x.ToString().Matches(search))
                            .ToList();
                }

                lstViews.LoadList(_plans.Select(x => x.ToString()).ToList());
            }

            lstViews.CheckAllItems(true);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var selectedBoxes = lstScopeboxes.GetCheckedListboxValues();

            if (selectedBoxes.Count == 0 && numBox.Value == 0)
            {
                MessageBox.Show("No scopeboxes or dependent views selected");
                return;
            }


            if (optNewViews.Checked)
            {
                for (var i = 0; i < lstViews.Items.Count; i++)
                {
                    if (lstViews.GetItemChecked(i))
                    {
                        _viewInfos[i].DependentViews.AddRange(selectedBoxes);
                    }
                }


                for (var i = 0; i < lstViews.Items.Count; i++)
                {
                    if (lstViews.GetItemChecked(i))
                    {
                        for (var d = 1; d < numBox.Value + 1; d++)
                        {
                            _viewInfos[i].DependentViews.Add(d.ToString());
                        }
                    }
                }
            }

            else
            {
                for (var i = 0; i < lstViews.Items.Count; i++)
                {
                    if (lstViews.GetItemChecked(i))
                    {
                        var plan = _plans[i];
                        var view = plan.GetView();

                        foreach (var selectedBox in selectedBoxes)
                        {
                            MainApp.AppData.ProjectViewInfos.Add(new ViewInfo
                            {
                                ParentViewId = view.IdToInt(),
                                Name = plan.Name,
                                Discipline = plan.Discipline,
                                Level  = view.GenLevel,

                                Scopebox = selectedBox
                            });
                        }
                        ;

                        for (var d = 1; d < numBox.Value + 1; d++)
                        {
                            MainApp.AppData.ProjectViewInfos.Add(new ViewInfo
                            {
                                ParentViewId = view.IdToInt(),
                                Name = plan.Name,
                                Discipline = plan.Discipline,
                                Level = view.GenLevel,

                                Scopebox = d.ToString()
                            });
                        }
                    }
                }
            }


            _formData.Dependents = (int) numBox.Value;
            _formData.Search = txtSearch.Text;
            _formData.ScopeBoxes = selectedBoxes;
            _formData.NewViews = optNewViews.Checked;
            MainApp.AppData.SaveUserModelData();

            DialogResult = DialogResult.OK;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ckAllScopeBoxes_CheckedChanged(object sender, EventArgs e)
        {
            lstScopeboxes.CheckAllItems(ckAllScopeBoxes.Checked);
        }

        private void ckCheckAllPlans_CheckedChanged(object sender, EventArgs e)
        {
            lstViews.CheckAllItems(ckCheckAllPlans.Checked);
        }

        private void txtSearchPlans_TextChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            BuildPlans();
        }

        private void frmAddDependentViews_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void lstViewInfos_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void lstViewInfos_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            ckCheckAllPlans.Checked = lstViews.AllItemsChecked();
        }

        private void optNewViews_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            BuildPlans();
        }

        private void optModeViews_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            BuildPlans();
        }
    }
}