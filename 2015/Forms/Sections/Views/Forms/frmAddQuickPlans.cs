﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Model;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Add_Edit_Forms
{
    public partial class frmAddQuickPlans : Form
    {
        private QuickPlansData _formData;
        private UserSettings _settings;
        private ModelData _modelData;

        private bool _loaded;

        public frmAddQuickPlans()
        {
            InitializeComponent();
        }

        public List<string> Levels { get; set; }
        public string Prefix { get; set; }
        public string PlanName { get; set; }
        public string Template { get; set; }
        public string ViewType { get; set; }
        public string ScopeBox { get; set; }
        public bool Favourite { get; set; }

        private void frmQuickPlan_Load(object sender, EventArgs e)
        {
            _formData = MainApp.AppData.FormSettings.QuickPlanData;
            _settings = MainApp.AppData.UserSettings;


            lstLevels.LoadAndSetList(Level_Utils.GetLevelNames(), _formData.Levels);

            cboPrefix.LoadCombo(_settings.GetPrefixes(), _formData.Prefix, "");
            cboPlanName.LoadCombo(_settings.GetPlanNames(), _formData.PlanName, "");

            cboTemplate.LoadCombo(_settings.GetViewTemplates(), _formData.Template, "None");


            cboScopeBox.LoadCombo(Scopebox_Utils.GetScopeBoxNames(), _formData.ScopeBox, "");
            cboViewType.LoadCombo(View_Utils.GetPlanViewTypes(), _formData.ViewType, null);
  

            if (cboViewType.SelectedItem == null)
            {

                if (MainApp.AppData.IsUserBuildings)
                {
                    cboViewType.Text = Autodesk.Revit.DB.ViewType.FloorPlan.ToString();
                }

                if (MainApp.AppData.IsUserStructures)
                {
                    cboViewType.Text = Autodesk.Revit.DB.ViewType.EngineeringPlan.ToString();
                }

            }


            if (Level_Utils.GetLevelNames().Count == _formData.Levels.Count)
            {
                ckAllLevels.Checked = true;
            }

            ckFavourite.Checked = _formData.Favourite;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (lstLevels.CheckedIndices.Count == 0)
            {
                MessageBox.Show("No Level Selected");
                return;
            }

            if (cboViewType.Text == "")
            {
                MessageBox.Show("No View Type Selected");
                return;
            }


            Levels = lstLevels.GetCheckedListboxValues();

            Prefix = cboPrefix.Text;
            PlanName = cboPlanName.Text;
            Template = cboTemplate.Text;
            ViewType = cboViewType.Text;
            ScopeBox = cboScopeBox.Text;
            Favourite = ckFavourite.Checked;


            _formData.Levels = Levels;

            _formData.Prefix = Prefix;
            _formData.PlanName = PlanName;
            _formData.Template = Template;
            _formData.ViewType = ViewType;
            _formData.ScopeBox = ScopeBox;
            _formData.Favourite = Favourite;

            bool updateUserdata = false;

            if (!string.IsNullOrEmpty(_formData.Prefix) && !_settings.Prefixes.Exists(x => x == _formData.Prefix))
            {
                _settings.Prefixes.Add(_formData.Prefix);
                updateUserdata = true;
            }

            if (!string.IsNullOrEmpty(_formData.PlanName) && !_settings.PlanNames.Exists(x => x == _formData.PlanName))
            {
                _settings.PlanNames.Add(_formData.PlanName);
                updateUserdata = true;
            }

            MainApp.AppData.SaveUserModelData();

            if (updateUserdata)
            {
                MainApp.AppData.SaveUserData();
            }
         

            DialogResult = DialogResult.OK;
        }

        private void frmQuickPlan_FormClosing(object sender, FormClosingEventArgs e)
        {
            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lstLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void ckAllLevels_CheckedChanged(object sender, EventArgs e)
        {
            lstLevels.CheckAllItems(ckAllLevels.Checked);
        }

        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            var template = UI_Utils.SelectViewTemplate(cboTemplate.Text);
            if (template == null) return;

            if (!cboTemplate.Items.Contains(template))
            {
                cboTemplate.Items.Add(template);
            }

            cboTemplate.Text = template;

            if (!string.IsNullOrEmpty(template) && !MainApp.AppData.UserSettings.ViewTemplates.Exists(x => x == template))
            {
                MainApp.AppData.UserSettings.ViewTemplates.Add(template);
                MainApp.AppData.SaveUserData();
            }
        }

        private void ckMyViews_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cboPrefix_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}