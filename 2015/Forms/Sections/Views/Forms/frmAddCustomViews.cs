﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Data;
using ViewCreator.Data.Forms;
using ViewCreator.Model;
using ViewCreator.Model.Base_Classes;
using Autodesk.Revit.DB;
using ViewCreator.Model.Settings;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using Form = System.Windows.Forms.Form;

namespace ViewCreator.Forms
{
    public partial class frmAddCustomViews : Form
    {
        private AddCustomViewsData _formData;

        private Discipline _discipline;
        private ModelSetting _modelSetting;
        private bool _loaded;
        private List<Element> _scopeBoxes;

        public frmAddCustomViews()
        {
            InitializeComponent();
        }

        public LevelInfo LevelInfo { get; set; }

        private void frmAddCustomViews_Load(object sender, EventArgs e)
        {
            _discipline = MainApp.AppData.FormSettings.PlansScreenData.Discipline;
            _modelSetting = MainApp.AppData.CurrentSettingsData.GetSettings(_discipline);

            _formData = MainApp.AppData.FormSettings.AddCustomViewsData;

            Text = "Add " + _discipline + " Plan Custom Views";

            _scopeBoxes = Scopebox_Utils.GetScopeBoxes();

            cboName.LoadCombo(_modelSetting.PrefixNames, _modelSetting.DefaultPrefix, null);
            lstScopeboxes.LoadAndSelect(Scopebox_Utils.GetScopeBoxNames(), _formData.Scopebox, "None");

            cboTemplate.LoadCombo(Template_Utils.GetViewTemplateNames(), _modelSetting.DefaultTemplate, "None");

            cboViewType.Items.AddRange(View_Utils.GetPlanViewTypes().ToArray());

            if (_discipline == Discipline.Structural)
            {
                cboViewType.Text = ViewType.EngineeringPlan.ToString();
            }
            else
            {
                cboViewType.Text = ViewType.FloorPlan.ToString();
            }



            BuildLevels();

            _loaded = true;

            SetCheckboxes();

        }

        private void BuildLevels()
        {

            SelectLevel();

            if (lstLevels.SelectedItem == null)
            {
                if (lstLevels.Items.Count > 0)
                {
                    lstLevels.SelectedIndex = 0;
                }
            }
        }

        private void SetCheckboxes()
        {
            ckAllLevels.Checked = _formData.CheckAllLevels;
            ckUserCopy.Checked = _formData.UserCopy;
        }

       

       

        private void SelectLevel()
        {
            for (var x = 0; x < lstLevels.Items.Count; x++)
            {
                if (lstLevels.GetItemText(lstLevels.Items[x]) == LevelInfo.ToString())
                {
                    lstLevels.SetItemChecked(x, true);
                    break;
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            if (lstLevels.CheckedIndices.Count == 0)
            {
                MessageBox.Show("No Levels Selected");
                return;
            }

           

            var levelIndexes = lstLevels.CheckedIndices.Cast<int>().ToArray();

            SetSelected(levelIndexes);

            DialogResult = DialogResult.OK;
        }

        public void SetSelected(int[] levelIndexes)
        {
            var levels = MainApp.DataAccess.ServiceLevels;

            foreach (var indexL in levelIndexes)
            {
                var levelName = lstLevels.GetItemText(lstLevels.Items[indexL]);
                var level = levels.LevelInfos.FirstOrDefault(x => x.Level.Name == levelName);

                if (level != null)
                {
                  
                    var viewInfo = new ViewInfo
                    {
                        Discipline = _discipline,
                        ViewType = View_Utils.GetViewType(cboViewType.Text),
                        Name = cboName.Text,
                        Level = level.Level,
                        Prefix = cboPrefix.Text,
                        Template = cboTemplate.Text,
                        Scopebox = lstScopeboxes.SelectedItem.ToString()
                    };

                    if (ckUserCopy.Checked) viewInfo.Name = viewInfo.Name + "_" + Environment.UserName;
                    level.ViewInfos.Add(viewInfo);
                }
            }
        }

    
    

        private void ckAllLevels_CheckedChanged(object sender, EventArgs e)
        {
            _formData.CheckAllLevels = ckAllLevels.Checked;

            for (var x = 0; x < lstLevels.Items.Count; x++)
            {
                lstLevels.SetItemChecked(x, ckAllLevels.Checked);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void ckUserCopy_CheckedChanged(object sender, EventArgs e)
        {
            _formData.UserCopy = ckUserCopy.Checked;
            MainApp.AppData.UserModelData.Save();
        }

        private void lstScopeboxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            _formData.Scopebox = lstScopeboxes.SelectedItem.ToString();
            MainApp.AppData.UserModelData.Save();
        }

        private void btnSelectTemplate_Click(object sender, EventArgs e)
        {
            var frm = new frmSelectViewTemplate();
            frm.TemplateName = cboTemplate.Text;

            frm.ShowDialog();

            if (frm.DialogResult == DialogResult.OK)
            {
                cboTemplate.Text = frm.TemplateName;
            }
        }
    }
}