﻿namespace ViewCreator.Forms.Add_Edit_Forms
{
    partial class frmAddDependentViews
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ckAllScopeBoxes = new System.Windows.Forms.CheckBox();
            this.lstScopeboxes = new System.Windows.Forms.CheckedListBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ckCheckAllPlans = new System.Windows.Forms.CheckBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lstViews = new System.Windows.Forms.CheckedListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numBox = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.optNewViews = new System.Windows.Forms.RadioButton();
            this.optModeViews = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBox)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ckAllScopeBoxes
            // 
            this.ckAllScopeBoxes.AutoSize = true;
            this.ckAllScopeBoxes.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckAllScopeBoxes.Location = new System.Drawing.Point(152, 46);
            this.ckAllScopeBoxes.Name = "ckAllScopeBoxes";
            this.ckAllScopeBoxes.Size = new System.Drawing.Size(71, 17);
            this.ckAllScopeBoxes.TabIndex = 71;
            this.ckAllScopeBoxes.Text = "Check All";
            this.ckAllScopeBoxes.UseVisualStyleBackColor = true;
            this.ckAllScopeBoxes.CheckedChanged += new System.EventHandler(this.ckAllScopeBoxes_CheckedChanged);
            // 
            // lstScopeboxes
            // 
            this.lstScopeboxes.CheckOnClick = true;
            this.lstScopeboxes.FormattingEnabled = true;
            this.lstScopeboxes.Location = new System.Drawing.Point(16, 69);
            this.lstScopeboxes.Name = "lstScopeboxes";
            this.lstScopeboxes.Size = new System.Drawing.Size(207, 199);
            this.lstScopeboxes.TabIndex = 70;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(433, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 69;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(514, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 68;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstScopeboxes);
            this.groupBox1.Controls.Add(this.ckAllScopeBoxes);
            this.groupBox1.Location = new System.Drawing.Point(347, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 287);
            this.groupBox1.TabIndex = 72;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scope Boxes";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.ckCheckAllPlans);
            this.groupBox2.Controls.Add(this.txtSearch);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lstViews);
            this.groupBox2.Location = new System.Drawing.Point(3, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(320, 419);
            this.groupBox2.TabIndex = 73;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Plans";
            // 
            // ckCheckAllPlans
            // 
            this.ckCheckAllPlans.AutoSize = true;
            this.ckCheckAllPlans.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckCheckAllPlans.Location = new System.Drawing.Point(231, 46);
            this.ckCheckAllPlans.Name = "ckCheckAllPlans";
            this.ckCheckAllPlans.Size = new System.Drawing.Size(71, 17);
            this.ckCheckAllPlans.TabIndex = 72;
            this.ckCheckAllPlans.Text = "Check All";
            this.ckCheckAllPlans.UseVisualStyleBackColor = true;
            this.ckCheckAllPlans.CheckedChanged += new System.EventHandler(this.ckCheckAllPlans_CheckedChanged);
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(13, 43);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(183, 20);
            this.txtSearch.TabIndex = 68;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearchPlans_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 69;
            this.label5.Text = "Search Plans:";
            // 
            // lstViews
            // 
            this.lstViews.CheckOnClick = true;
            this.lstViews.FormattingEnabled = true;
            this.lstViews.Location = new System.Drawing.Point(13, 69);
            this.lstViews.Name = "lstViews";
            this.lstViews.Size = new System.Drawing.Size(289, 274);
            this.lstViews.TabIndex = 57;
            this.lstViews.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstViewInfos_ItemCheck);
            this.lstViews.SelectedIndexChanged += new System.EventHandler(this.lstViewInfos_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.numBox);
            this.groupBox3.Location = new System.Drawing.Point(346, 305);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(242, 126);
            this.groupBox3.TabIndex = 74;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dependents Views";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 76;
            this.label1.Text = "Number of dependent views:";
            // 
            // numBox
            // 
            this.numBox.Location = new System.Drawing.Point(26, 56);
            this.numBox.Name = "numBox";
            this.numBox.Size = new System.Drawing.Size(120, 20);
            this.numBox.TabIndex = 75;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 443);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(597, 50);
            this.panel1.TabIndex = 111;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(336, 443);
            this.panel2.TabIndex = 112;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.optModeViews);
            this.groupBox4.Controls.Add(this.optNewViews);
            this.groupBox4.Location = new System.Drawing.Point(13, 352);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(289, 53);
            this.groupBox4.TabIndex = 73;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "New or Model Views";
            // 
            // optNewViews
            // 
            this.optNewViews.AutoSize = true;
            this.optNewViews.Location = new System.Drawing.Point(23, 22);
            this.optNewViews.Name = "optNewViews";
            this.optNewViews.Size = new System.Drawing.Size(78, 17);
            this.optNewViews.TabIndex = 70;
            this.optNewViews.TabStop = true;
            this.optNewViews.Text = "New Views";
            this.optNewViews.UseVisualStyleBackColor = true;
            this.optNewViews.CheckedChanged += new System.EventHandler(this.optNewViews_CheckedChanged);
            // 
            // optModeViews
            // 
            this.optModeViews.AutoSize = true;
            this.optModeViews.Location = new System.Drawing.Point(123, 22);
            this.optModeViews.Name = "optModeViews";
            this.optModeViews.Size = new System.Drawing.Size(85, 17);
            this.optModeViews.TabIndex = 71;
            this.optModeViews.TabStop = true;
            this.optModeViews.Text = "Model Views";
            this.optModeViews.UseVisualStyleBackColor = true;
            this.optModeViews.CheckedChanged += new System.EventHandler(this.optModeViews_CheckedChanged);
            // 
            // frmAddDependentViews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(597, 493);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAddDependentViews";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Dependent Views";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAddDependentViews_FormClosing);
            this.Load += new System.EventHandler(this.frmAddScopeBoxes_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox ckAllScopeBoxes;
        private System.Windows.Forms.CheckedListBox lstScopeboxes;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckedListBox lstViews;
        private System.Windows.Forms.CheckBox ckCheckAllPlans;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton optModeViews;
        private System.Windows.Forms.RadioButton optNewViews;
    }
}