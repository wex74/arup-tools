﻿using ViewCreator.Forms.Sections.Views.Controls;

namespace ViewCreator.Forms
{
    partial class frmEditViews
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnApply = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ckShowDependent = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ctlSearch = new ViewCreator.Forms.Controls.ctlSearch();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnOpenView = new System.Windows.Forms.Button();
            this.ctlViews = new ViewCreator.Forms.Controls.Views.ctlEditViewsViews();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.button7 = new System.Windows.Forms.Button();
            this.ctlViewEditor = new ViewCreator.Forms.Sections.Views.Controls.ctlEditViewsEditor();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cboFilterTemplates = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboFilterScopeboxes = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboFilterScale = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.SeaGreen;
            this.btnApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnApply.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnApply.Location = new System.Drawing.Point(866, 292);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(100, 23);
            this.btnApply.TabIndex = 60;
            this.btnApply.Text = "Apply Changes";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboFilterScale);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cboFilterScopeboxes);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cboFilterTemplates);
            this.groupBox1.Controls.Add(this.ckShowDependent);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.ctlSearch);
            this.groupBox1.Location = new System.Drawing.Point(9, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1070, 78);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter";
            // 
            // ckShowDependent
            // 
            this.ckShowDependent.AutoSize = true;
            this.ckShowDependent.Checked = true;
            this.ckShowDependent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckShowDependent.Location = new System.Drawing.Point(220, 46);
            this.ckShowDependent.Name = "ckShowDependent";
            this.ckShowDependent.Size = new System.Drawing.Size(140, 17);
            this.ckShowDependent.TabIndex = 111;
            this.ckShowDependent.Text = "Show Dependent Views";
            this.ckShowDependent.UseVisualStyleBackColor = true;
            this.ckShowDependent.CheckedChanged += new System.EventHandler(this.ckShowDependent_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 60;
            this.label1.Text = "Search Views";
            // 
            // ctlSearch
            // 
            this.ctlSearch.Location = new System.Drawing.Point(15, 39);
            this.ctlSearch.Name = "ctlSearch";
            this.ctlSearch.Search = "";
            this.ctlSearch.Size = new System.Drawing.Size(199, 24);
            this.ctlSearch.TabIndex = 59;
            this.ctlSearch.SearchChanged += new System.EventHandler(this.ctlSearch_SearchChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnOpenView);
            this.groupBox2.Controls.Add(this.ctlViews);
            this.groupBox2.Controls.Add(this.btnApply);
            this.groupBox2.Location = new System.Drawing.Point(12, 190);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1067, 326);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Views";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(972, 48);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 23);
            this.btnDelete.TabIndex = 61;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnOpenView
            // 
            this.btnOpenView.Location = new System.Drawing.Point(972, 19);
            this.btnOpenView.Name = "btnOpenView";
            this.btnOpenView.Size = new System.Drawing.Size(80, 23);
            this.btnOpenView.TabIndex = 60;
            this.btnOpenView.Text = "Open View";
            this.btnOpenView.UseVisualStyleBackColor = true;
            this.btnOpenView.Click += new System.EventHandler(this.btnOpenView_Click);
            // 
            // ctlViews
            // 
            this.ctlViews.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ctlViews.ContextMenuStrip = this.contextMenu;
            this.ctlViews.Location = new System.Drawing.Point(14, 19);
            this.ctlViews.Name = "ctlViews";
            this.ctlViews.Size = new System.Drawing.Size(952, 262);
            this.ctlViews.TabIndex = 58;
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSelectAll});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(123, 26);
            this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenu_Opening);
            // 
            // mnuSelectAll
            // 
            this.mnuSelectAll.Name = "mnuSelectAll";
            this.mnuSelectAll.Size = new System.Drawing.Size(122, 22);
            this.mnuSelectAll.Text = "Select All";
            this.mnuSelectAll.Click += new System.EventHandler(this.mnuSelectAll_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(1004, 15);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 57;
            this.button7.Text = "Close";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // ctlViewEditor
            // 
            this.ctlViewEditor.BackColor = System.Drawing.Color.LightSlateGray;
            this.ctlViewEditor.Location = new System.Drawing.Point(9, 6);
            this.ctlViewEditor.Name = "ctlViewEditor";
            this.ctlViewEditor.Size = new System.Drawing.Size(1097, 77);
            this.ctlViewEditor.TabIndex = 59;
            this.ctlViewEditor.ViewsUpdated += new System.EventHandler(this.ctlViewEditor_ViewsUpdated);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.button7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 525);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1090, 50);
            this.panel1.TabIndex = 108;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1090, 92);
            this.panel2.TabIndex = 109;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightSlateGray;
            this.panel3.Controls.Add(this.ctlViewEditor);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 92);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1090, 92);
            this.panel3.TabIndex = 110;
            // 
            // cboFilterTemplates
            // 
            this.cboFilterTemplates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFilterTemplates.FormattingEnabled = true;
            this.cboFilterTemplates.Location = new System.Drawing.Point(392, 42);
            this.cboFilterTemplates.Name = "cboFilterTemplates";
            this.cboFilterTemplates.Size = new System.Drawing.Size(253, 21);
            this.cboFilterTemplates.TabIndex = 60;
            this.cboFilterTemplates.SelectedIndexChanged += new System.EventHandler(this.cboFilterTemplates_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(391, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 112;
            this.label2.Text = "Filter Templates";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(672, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 114;
            this.label3.Text = "Filter Scopeboxes";
            // 
            // cboFilterScopeboxes
            // 
            this.cboFilterScopeboxes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFilterScopeboxes.FormattingEnabled = true;
            this.cboFilterScopeboxes.Location = new System.Drawing.Point(673, 42);
            this.cboFilterScopeboxes.Name = "cboFilterScopeboxes";
            this.cboFilterScopeboxes.Size = new System.Drawing.Size(181, 21);
            this.cboFilterScopeboxes.TabIndex = 113;
            this.cboFilterScopeboxes.SelectedIndexChanged += new System.EventHandler(this.cboFilterScopeboxes_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(881, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 116;
            this.label4.Text = "Filter Scale";
            // 
            // cboFilterScale
            // 
            this.cboFilterScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFilterScale.FormattingEnabled = true;
            this.cboFilterScale.Location = new System.Drawing.Point(882, 42);
            this.cboFilterScale.Name = "cboFilterScale";
            this.cboFilterScale.Size = new System.Drawing.Size(162, 21);
            this.cboFilterScale.TabIndex = 115;
            this.cboFilterScale.SelectedIndexChanged += new System.EventHandler(this.cboFilterScale_SelectedIndexChanged);
            // 
            // frmEditViews
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1090, 575);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmEditViews";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Views";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEditViews_FormClosing);
            this.Load += new System.EventHandler(this.frmRenameViews_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.Views.ctlEditViewsViews ctlViews;
        private Controls.ctlSearch ctlSearch;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private ctlEditViewsEditor ctlViewEditor;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnOpenView;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuSelectAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox ckShowDependent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboFilterScale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboFilterScopeboxes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboFilterTemplates;
    }
}