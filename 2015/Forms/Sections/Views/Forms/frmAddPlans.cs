﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using ViewCreator.Data;
using ViewCreator.Model;
using ViewCreator.Data.Forms;
using ViewCreator.Model.Settings;
using ViewCreator.Model.Base_Classes;
using Autodesk.Revit.DB;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using ViewCreator.Utils.UI;
using Form = System.Windows.Forms.Form;

namespace ViewCreator.Forms
{
    public partial class frmAddPlans : Form
    {
        private AddViewsData _formData;

        private Discipline _discipline;
        private bool _loaded;
        private List<ViewInfo> _viewInfos;
        private List<Element> _scopeBoxes;
        private ModelSetting _modelSetting;
        public List<ViewInfo> ViewInfos { get; set; }

        
        public frmAddPlans()
        {
            InitializeComponent();
        }

        public LevelInfo LevelInfo { get; set; }

        private void frmBulkAdd_Load(object sender, EventArgs e)
        {
            _discipline = MainApp.AppData.FormSettings.PlansScreenData.Discipline;
            _modelSetting = MainApp.AppData.CurrentSettingsData.GetSettings(_discipline);
            _viewInfos = _modelSetting.GetViewInfos();

            _formData = MainApp.AppData.FormSettings.AddViewsData;

            Text = "Add " + _discipline + " Plan Views";

            lstScopeboxes.LoadAndSelect(Scopebox_Utils.GetScopeBoxNames(), _formData.Scopebox, "None");
            lstLevels.LoadAndSetList(MainApp.AppData.FormSettings.PlansScreenData.Levels, _formData.Levels);


            ckUserCopy.Checked = _formData.UserCopy;

            lstViews.LoadList(_modelSetting.GetViewInfoSettingNames());
            lstViews.CheckAllItems(true);
            ckAllPlans.Checked = true;

            _loaded = true;
        }

    

     

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (lstLevels.CheckedIndices.Count == 0)
            {
                MessageBox.Show("No Levels Selected");
                return;
            }

            if (lstViews.CheckedIndices.Count == 0)
            {
                MessageBox.Show("No Views Selected");
                return;
            }

            _formData.UserCopy = ckUserCopy.Checked;
            _formData.Scopebox = lstScopeboxes.SelectedItem.ToString();
            _formData.Levels = lstLevels.GetCheckedListboxValues();


            MainApp.AppData.SaveUserModelData();

            var levelIndexes = lstLevels.CheckedIndices.Cast<int>().ToArray();
            var viewIndexes = lstViews.CheckedIndices.Cast<int>().ToArray();


            SetSelected(levelIndexes, viewIndexes);



            DialogResult = DialogResult.OK;
        }

        public void SetSelected(int[] levelIndexes, int[] viewIndexes)
        {
            ViewInfos = new List<ViewInfo>();


            foreach (var indexL in levelIndexes)
            {
                var levelName = lstLevels.GetItemText(lstLevels.Items[indexL]);

                if (levelName != null)
                {
                    foreach (var indexP in viewIndexes)
                    {
                        var foundView = _viewInfos[indexP];

                        var name = foundView.Name;
                        if (ckUserCopy.Checked) name = name + "_" + Environment.UserName;
                        var viewInfo = new ViewInfo
                        {
                            Discipline = foundView.Discipline,
                            ViewType = foundView.ViewType,
                            Name = name,
                            Level = Level_Utils.GetLevel(levelName),
                            Prefix = foundView.Prefix,
                            Template = foundView.Template,
                            Scopebox = lstScopeboxes.SelectedItem.ToString()
                        };

                        ViewInfos.Add(viewInfo);
                        Debug.WriteLine("frmAddPlans: Addding ViewInfo "  + viewInfo);

                    }
                }
            }
        }


        private void lstPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void ckAllPlans_CheckedChanged(object sender, EventArgs e)
        {
            lstViews.CheckAllItems(ckAllPlans.Checked);
        }


        private void ckAllLevels_CheckedChanged(object sender, EventArgs e)
        {
            lstLevels.CheckAllItems(ckAllLevels.Checked);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void ckUserCopy_CheckedChanged(object sender, EventArgs e)
        {
          
            MainApp.AppData.SaveUserModelData();
        }

        private void lstScopeboxes_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}