﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Events;
using ViewCreator.Events.Views;
using ViewCreator.Forms.Controls.Views;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.General;
using ViewCreator.Utils.UI;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Forms
{
    public partial class frmEditViews : Form
    {
        private bool _loaded;
        public List<UpdateView> UpdateViews { get; set; }
        private EditViewsData _formData;
        private List<UpdateView> _filteredViews;


        public frmEditViews()
        {
            InitializeComponent();
        }

        private void frmRenameViews_Load(object sender, EventArgs e)
        {

        }

        public void LoadForm(List<View> views)
        {
            _formData = MainApp.AppData.FormSettings.EditViewsData;

            UpdateViews = new List<UpdateView>();

            foreach (var view in views)
            {
                var update = new UpdateView(view);
                UpdateViews.Add(update);
            }

            _filteredViews = UpdateViews;
            ctlViewEditor.LoadControl(ctlViews);
            ctlSearch.LoadControl(_formData.Search);

            ckShowDependent.Checked = _formData.ShowDependents;


            var templates = UpdateViews.Select(x => x.Template).Distinct().ToList();
            cboFilterTemplates.LoadCombo(templates, "", "");

            var scopeboxes = UpdateViews.Select(x => x.ScopeBox).Distinct().ToList();
            cboFilterScopeboxes.LoadCombo(scopeboxes, "", "");

            var scales = UpdateViews.Select(x => View_Utils.GetScale(x.Scale)).Distinct().ToList();
            cboFilterScale.LoadCombo(scales, "", "");




            LoadViews();
            _loaded = true;


        }

        private void button7_Click(object sender, EventArgs e)
        {
           Close();
        }

        private void ctlSearch_SearchChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;
            LoadViews();
        }

        private void LoadViews()
        {
            _loaded = false;

            var views = _filteredViews;

            if (!ckShowDependent.Checked)
            {
                views = views.Where(x => !x.IsDependent).ToList();
            }

            views = views.Where(x => x.ViewName.Matches(ctlSearch.Search)).ToList();

        

            _loaded = true;
            ctlViews.LoadControl(views);
        }

     

        private void btnApply_Click(object sender, EventArgs e)
        {
            var updates = ctlViews.GetUpdates();


            if (updates.Any())
            {
                UpdateViewsEvent.Run(updates);
            }

        }

        private void ctlViewEditor_ViewsUpdated(object sender, EventArgs e)
        {
            if (!_loaded) return;

            var updates = ctlViews.GetSelectedUpdateViews();

            LoadViews();
            ctlViews.SelectUpdateViews(updates);

        }

        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {

        }

        private void mnuSelectAll_Click(object sender, EventArgs e)
        {
            ctlViews.SelectAll();
        }

        private void btnOpenView_Click(object sender, EventArgs e)
        {
            var view = ctlViews.GetUpdateView();

            if (view != null)
            {
                if (view.GetView() != null)
                {
                    NavigateEvent.Run(view.GetView());
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var views = ctlViews.GetSelectedUpdateViews();

            if (!views.Any()) return;
            var deleteViews = views.Select(x => x.GetView()).ToList();
            DeleteViewsEvent.Run(deleteViews);
        }

        private void frmEditViews_FormClosing(object sender, FormClosingEventArgs e)
        {
            _formData.Search = ctlSearch.Search;
            _formData.ShowDependents = ckShowDependent.Checked;
            MainApp.AppData.SaveUserModelData();



            EditViewsUI.FormVisible = false;
        }

        private void ckShowDependent_CheckedChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            LoadViews();
        }

        private void cboFilterTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            _filteredViews = UpdateViews.Where(x => x.Template == cboFilterTemplates.Text).ToList();

            LoadViews();
        }

        private void cboFilterScopeboxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            _filteredViews = UpdateViews.Where(x => x.ScopeBox == cboFilterScopeboxes.Text).ToList();

            LoadViews();
        }

        private void cboFilterScale_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            if (cboFilterScale.Text == "")
            {
                _filteredViews = UpdateViews;
            }
            else
            {
                _filteredViews = UpdateViews.Where(x => View_Utils.GetScale(x.Scale) == cboFilterScale.Text).ToList();
            }

            LoadViews();
        }
    }
}
