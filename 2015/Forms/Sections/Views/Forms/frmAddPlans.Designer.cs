﻿namespace ViewCreator.Forms
{
    partial class frmAddPlans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstLevels = new System.Windows.Forms.CheckedListBox();
            this.lstViews = new System.Windows.Forms.CheckedListBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ckAllLevels = new System.Windows.Forms.CheckBox();
            this.ckAllPlans = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ckUserCopy = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstScopeboxes = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstLevels
            // 
            this.lstLevels.CheckOnClick = true;
            this.lstLevels.FormattingEnabled = true;
            this.lstLevels.Location = new System.Drawing.Point(17, 42);
            this.lstLevels.Name = "lstLevels";
            this.lstLevels.Size = new System.Drawing.Size(244, 409);
            this.lstLevels.TabIndex = 55;
            // 
            // lstViews
            // 
            this.lstViews.CheckOnClick = true;
            this.lstViews.FormattingEnabled = true;
            this.lstViews.Location = new System.Drawing.Point(17, 45);
            this.lstViews.Name = "lstViews";
            this.lstViews.Size = new System.Drawing.Size(287, 229);
            this.lstViews.TabIndex = 57;
            this.lstViews.SelectedIndexChanged += new System.EventHandler(this.lstPlans_SelectedIndexChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(469, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 60;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(550, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 59;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // ckAllLevels
            // 
            this.ckAllLevels.AutoSize = true;
            this.ckAllLevels.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckAllLevels.Location = new System.Drawing.Point(190, 21);
            this.ckAllLevels.Name = "ckAllLevels";
            this.ckAllLevels.Size = new System.Drawing.Size(71, 17);
            this.ckAllLevels.TabIndex = 61;
            this.ckAllLevels.Text = "Check All";
            this.ckAllLevels.UseVisualStyleBackColor = true;
            this.ckAllLevels.CheckedChanged += new System.EventHandler(this.ckAllLevels_CheckedChanged);
            // 
            // ckAllPlans
            // 
            this.ckAllPlans.AutoSize = true;
            this.ckAllPlans.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckAllPlans.Location = new System.Drawing.Point(229, 21);
            this.ckAllPlans.Name = "ckAllPlans";
            this.ckAllPlans.Size = new System.Drawing.Size(74, 17);
            this.ckAllPlans.TabIndex = 62;
            this.ckAllPlans.Text = "Check All ";
            this.ckAllPlans.UseVisualStyleBackColor = true;
            this.ckAllPlans.CheckedChanged += new System.EventHandler(this.ckAllPlans_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstLevels);
            this.groupBox1.Controls.Add(this.ckAllLevels);
            this.groupBox1.Location = new System.Drawing.Point(9, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 468);
            this.groupBox1.TabIndex = 68;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Levels";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ckUserCopy);
            this.groupBox2.Controls.Add(this.lstViews);
            this.groupBox2.Controls.Add(this.ckAllPlans);
            this.groupBox2.Location = new System.Drawing.Point(311, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(320, 306);
            this.groupBox2.TabIndex = 69;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Views";
            // 
            // ckUserCopy
            // 
            this.ckUserCopy.AutoSize = true;
            this.ckUserCopy.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckUserCopy.Location = new System.Drawing.Point(229, 280);
            this.ckUserCopy.Name = "ckUserCopy";
            this.ckUserCopy.Size = new System.Drawing.Size(75, 17);
            this.ckUserCopy.TabIndex = 70;
            this.ckUserCopy.Text = "User Copy";
            this.ckUserCopy.UseVisualStyleBackColor = true;
            this.ckUserCopy.CheckedChanged += new System.EventHandler(this.ckUserCopy_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lstScopeboxes);
            this.groupBox3.Location = new System.Drawing.Point(311, 324);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(320, 156);
            this.groupBox3.TabIndex = 73;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Scope Boxes";
            // 
            // lstScopeboxes
            // 
            this.lstScopeboxes.FormattingEnabled = true;
            this.lstScopeboxes.Location = new System.Drawing.Point(17, 19);
            this.lstScopeboxes.Name = "lstScopeboxes";
            this.lstScopeboxes.Size = new System.Drawing.Size(287, 121);
            this.lstScopeboxes.TabIndex = 0;
            this.lstScopeboxes.SelectedIndexChanged += new System.EventHandler(this.lstScopeboxes_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 485);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(642, 50);
            this.panel1.TabIndex = 110;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(305, 485);
            this.panel2.TabIndex = 111;
            // 
            // frmAddPlans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(642, 535);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAddPlans";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add Views";
            this.Load += new System.EventHandler(this.frmBulkAdd_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.CheckedListBox lstLevels;
        private System.Windows.Forms.CheckedListBox lstViews;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox ckAllLevels;
        private System.Windows.Forms.CheckBox ckAllPlans;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox ckUserCopy;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox lstScopeboxes;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
    }
}