﻿namespace ViewCreator.Forms
{
    partial class frmAddEditPlanTypes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddSet = new System.Windows.Forms.Button();
            this.btnEditSet = new System.Windows.Forms.Button();
            this.btnRemoveSet = new System.Windows.Forms.Button();
            this.lstPlanTypes = new System.Windows.Forms.ListBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "Plan Types:";
            // 
            // btnAddSet
            // 
            this.btnAddSet.Location = new System.Drawing.Point(274, 32);
            this.btnAddSet.Name = "btnAddSet";
            this.btnAddSet.Size = new System.Drawing.Size(60, 23);
            this.btnAddSet.TabIndex = 58;
            this.btnAddSet.Text = "Add";
            this.btnAddSet.UseVisualStyleBackColor = true;
            // 
            // btnEditSet
            // 
            this.btnEditSet.Location = new System.Drawing.Point(274, 61);
            this.btnEditSet.Name = "btnEditSet";
            this.btnEditSet.Size = new System.Drawing.Size(60, 23);
            this.btnEditSet.TabIndex = 57;
            this.btnEditSet.Text = "Edit";
            this.btnEditSet.UseVisualStyleBackColor = true;
            // 
            // btnRemoveSet
            // 
            this.btnRemoveSet.Location = new System.Drawing.Point(274, 90);
            this.btnRemoveSet.Name = "btnRemoveSet";
            this.btnRemoveSet.Size = new System.Drawing.Size(60, 23);
            this.btnRemoveSet.TabIndex = 56;
            this.btnRemoveSet.Text = "Remove";
            this.btnRemoveSet.UseVisualStyleBackColor = true;
            // 
            // lstPlanTypes
            // 
            this.lstPlanTypes.FormattingEnabled = true;
            this.lstPlanTypes.Location = new System.Drawing.Point(12, 32);
            this.lstPlanTypes.Name = "lstPlanTypes";
            this.lstPlanTypes.Size = new System.Drawing.Size(256, 342);
            this.lstPlanTypes.TabIndex = 4;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(178, 389);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 61;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(259, 389);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 60;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // frmAddEditPlanTypes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 424);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnAddSet);
            this.Controls.Add(this.btnEditSet);
            this.Controls.Add(this.lstPlanTypes);
            this.Controls.Add(this.btnRemoveSet);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmAddEditPlanTypes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manage Plan Types";
            this.Load += new System.EventHandler(this.frmPlanTypes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddSet;
        private System.Windows.Forms.Button btnEditSet;
        private System.Windows.Forms.Button btnRemoveSet;
        private System.Windows.Forms.ListBox lstPlanTypes;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
    }
}