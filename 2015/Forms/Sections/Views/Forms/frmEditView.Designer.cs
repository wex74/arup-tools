﻿namespace ViewCreator.Forms.Add_Edit_Forms
{
    partial class frmEditView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboScale = new System.Windows.Forms.ComboBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.ckFavourite = new System.Windows.Forms.CheckBox();
            this.cboScopeBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectTemplate = new System.Windows.Forms.Button();
            this.cboTemplate = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ckApplyOnChange = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(256, 15);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 79;
            this.btnOk.Text = "Apply";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(337, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 78;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cboScale);
            this.groupBox2.Controls.Add(this.txtName);
            this.groupBox2.Controls.Add(this.ckFavourite);
            this.groupBox2.Controls.Add(this.cboScopeBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btnSelectTemplate);
            this.groupBox2.Controls.Add(this.cboTemplate);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(403, 214);
            this.groupBox2.TabIndex = 77;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "View Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(262, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 111;
            this.label2.Text = "Scale";
            // 
            // cboScale
            // 
            this.cboScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboScale.FormattingEnabled = true;
            this.cboScale.Items.AddRange(new object[] {
            "1:10",
            "1:20",
            "1:50",
            "1:100",
            "1:200",
            "1:500",
            "1:1000"});
            this.cboScale.Location = new System.Drawing.Point(263, 142);
            this.cboScale.Name = "cboScale";
            this.cboScale.Size = new System.Drawing.Size(123, 21);
            this.cboScale.TabIndex = 110;
            this.cboScale.SelectedIndexChanged += new System.EventHandler(this.cboScale_SelectedIndexChanged);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(24, 43);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(333, 20);
            this.txtName.TabIndex = 73;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // ckFavourite
            // 
            this.ckFavourite.AutoSize = true;
            this.ckFavourite.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckFavourite.Location = new System.Drawing.Point(316, 180);
            this.ckFavourite.Name = "ckFavourite";
            this.ckFavourite.Size = new System.Drawing.Size(70, 17);
            this.ckFavourite.TabIndex = 72;
            this.ckFavourite.Text = "Favourite";
            this.ckFavourite.UseVisualStyleBackColor = true;
            this.ckFavourite.CheckedChanged += new System.EventHandler(this.ckFavourite_CheckedChanged);
            // 
            // cboScopeBox
            // 
            this.cboScopeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboScopeBox.FormattingEnabled = true;
            this.cboScopeBox.Location = new System.Drawing.Point(22, 142);
            this.cboScopeBox.Name = "cboScopeBox";
            this.cboScopeBox.Size = new System.Drawing.Size(223, 21);
            this.cboScopeBox.TabIndex = 70;
            this.cboScopeBox.SelectedIndexChanged += new System.EventHandler(this.cboScopeBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 71;
            this.label1.Text = "Scope Box";
            // 
            // btnSelectTemplate
            // 
            this.btnSelectTemplate.Location = new System.Drawing.Point(363, 88);
            this.btnSelectTemplate.Name = "btnSelectTemplate";
            this.btnSelectTemplate.Size = new System.Drawing.Size(23, 23);
            this.btnSelectTemplate.TabIndex = 62;
            this.btnSelectTemplate.Text = "...";
            this.btnSelectTemplate.UseVisualStyleBackColor = true;
            this.btnSelectTemplate.Click += new System.EventHandler(this.btnSelectTemplate_Click);
            // 
            // cboTemplate
            // 
            this.cboTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTemplate.FormattingEnabled = true;
            this.cboTemplate.Location = new System.Drawing.Point(22, 90);
            this.cboTemplate.Name = "cboTemplate";
            this.cboTemplate.Size = new System.Drawing.Size(335, 21);
            this.cboTemplate.TabIndex = 63;
            this.cboTemplate.SelectedIndexChanged += new System.EventHandler(this.cboTemplate_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Template";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SlateGray;
            this.panel1.Controls.Add(this.ckApplyOnChange);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 238);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(424, 50);
            this.panel1.TabIndex = 109;
            // 
            // ckApplyOnChange
            // 
            this.ckApplyOnChange.AutoSize = true;
            this.ckApplyOnChange.Checked = true;
            this.ckApplyOnChange.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckApplyOnChange.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.ckApplyOnChange.Location = new System.Drawing.Point(144, 21);
            this.ckApplyOnChange.Name = "ckApplyOnChange";
            this.ckApplyOnChange.Size = new System.Drawing.Size(106, 17);
            this.ckApplyOnChange.TabIndex = 80;
            this.ckApplyOnChange.Text = "Apply on change";
            this.ckApplyOnChange.UseVisualStyleBackColor = true;
            // 
            // frmEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(424, 288);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmEditView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit View";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmEditView_FormClosing);
            this.Load += new System.EventHandler(this.frmEditView_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.CheckBox ckFavourite;
        private System.Windows.Forms.ComboBox cboScopeBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectTemplate;
        private System.Windows.Forms.ComboBox cboTemplate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboScale;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox ckApplyOnChange;
    }
}