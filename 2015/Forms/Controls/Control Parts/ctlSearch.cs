﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlSearch : UserControl
    {
        public event EventHandler SearchChanged;
        

        private string _search;
        public string Search
        {
            get
            {
                return _search;
            }

            set { _search = value; }
        }

        private bool _loaded;

        public ctlSearch()
        {
            InitializeComponent();
        }


        public void LoadControl(string search)
        {
            if (search == null) search = "";
            _search = search;
            txtSearch.Text = search;
            SetUI();
        }

     

        protected virtual void OnSearchChanged(string search, EventArgs e)
        {
            EventHandler handler = SearchChanged;
            if (handler != null)
            {
                handler(search, e);
            }
        }

       

   

        private void ctlSearch_Load(object sender, EventArgs e)
        {
        }

      
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (_search == txtSearch.Text)
            {
                txtSearch.Text = "";
            }

            _search = txtSearch.Text;
            OnSearchChanged(_search, e);
            SetUI();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

            SetUI();
        }

       
        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _search = txtSearch.Text;
                OnSearchChanged(_search, e);
                SetUI();
            }

            if (e.KeyCode == Keys.Escape)
            {
                txtSearch.Text = "";
               _search = txtSearch.Text;
                OnSearchChanged(_search, e);
                SetUI();
            }

        }


        private void SetUI()
        {
            if (_search == "" && txtSearch.Text == "")
            {
                btnSearch.Visible = false;
                txtSearch.BackColor = Color.White;
                return;
            }

            btnSearch.Visible = true;

            if (_search == txtSearch.Text)
            {
                btnSearch.Image = imageList.Images["Clear"];
                txtSearch.BackColor = Color.GreenYellow;
            }

            if (_search != txtSearch.Text)
            {
                btnSearch.Image = imageList.Images["Search"];
                txtSearch.BackColor = Color.Yellow;
            }

        }
    }
}
