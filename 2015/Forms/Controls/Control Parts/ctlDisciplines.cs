﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewCreator.Data.Forms;
using ViewCreator.Events;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.UI;

namespace ViewCreator.Forms.Controls
{
    public partial class ctlDisciplines : UserControl
    {
        public event EventHandler DisciplineChanged;

        private bool _loaded;
        private Discipline _discipline;

        public ctlDisciplines()
        {
            InitializeComponent();
        }


        public void LoadControl(Discipline discipline)
        {
            _discipline = discipline;

            // Load lists
            cboDisciplines.LoadCombo(Discipline_Utils.GetDisciplineNames(), _discipline.ToString(),null);

            _loaded = true;
        }

        public void SetDiscipline(Discipline discipline)
        {
            cboDisciplines.SelectedItem = discipline.ToString();
            OnDisciplinedChanged(discipline, new EventArgs());
        }


        protected virtual void OnDisciplinedChanged(Discipline discipline ,EventArgs e)
        {
            EventHandler handler = DisciplineChanged;
            if (handler != null)
            {
                handler(_discipline, e);
            }
        }

        private void cboDisciplines_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_loaded) return;

            _discipline = Discipline_Utils.GetDiscipline(cboDisciplines.Text);
            OnDisciplinedChanged(_discipline, e);
        }
    }
}
