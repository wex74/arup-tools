﻿namespace ViewCreator.Forms.Controls
{
    partial class ctlDisciplines
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboDisciplines = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cboDisciplines
            // 
            this.cboDisciplines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDisciplines.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDisciplines.FormattingEnabled = true;
            this.cboDisciplines.Location = new System.Drawing.Point(4, 6);
            this.cboDisciplines.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboDisciplines.Name = "cboDisciplines";
            this.cboDisciplines.Size = new System.Drawing.Size(743, 24);
            this.cboDisciplines.TabIndex = 80;
            this.cboDisciplines.SelectedIndexChanged += new System.EventHandler(this.cboDisciplines_SelectedIndexChanged);
            // 
            // ctlDisciplines
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cboDisciplines);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ctlDisciplines";
            this.Size = new System.Drawing.Size(751, 39);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cboDisciplines;
    }
}
