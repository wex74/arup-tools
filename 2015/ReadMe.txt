
========================================================================
		Revit Addin VS Project Created by RevitAddinWizardPro
		
		Updates can be checked on:
		http://spiderinnet.typepad.com
========================================================================

The project supports the following options:

+ Project properties:
	Assembly name: ViewCreator
	Revit version: 2015
	API lib path: C:\Program Files\Autodesk\Revit 2015\RevitAPI.dll
	Start Revit: True
	Vendor ID: Arup-NW
+ External application:
	Add one: True
	Class name: ExtApp
	Transaction mode: Manual
	Regeneration option: Manual
	Create a ribbon: True
		Ribbon tab name: NW
		Ribbon panel title: ViewCreator
+ External command:
	Add one: True
	Class name: ExtCmd
	Description: Generate Plans
	Transaction mode: Manual
	Regeneration option: Manual
	Add it into menu: False
