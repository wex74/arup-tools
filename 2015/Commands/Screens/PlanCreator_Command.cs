﻿#region Namespaces

using System;
using System.Windows.Forms;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.UI;
using ViewCreator.Utils.App;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class PlanCreator_Command : IExternalCommand
    {
        private static ExternalCommandData _cachedCmdData;

        public static UIApplication CachedUiApp
        {
            get { return _cachedCmdData.Application; }
        }

      

        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            if (!App_Utils.FullCheck())
            {
                return Result.Failed;
            }

            _cachedCmdData = cmdData;

            try
            {
                if (ScreensUI.PlansPane == null)
                {
                    ScreensUI.PlansPane = CachedUiApp.GetDockablePane(ScreensUI.PlansScreenId);
                }

                if (ScreensUI.PlansPane == null)
                {
                    MessageBox.Show("Pane not available");
                    return Result.Failed;
                }


                if (MainApp.AppData.CurrentProject.PlansScreenVisible)
                {
                    ScreensUI.PlansPane.Hide();
                    MainApp.AppData.CurrentProject.PlansScreenVisible = false;
                }
                else
                {
                    ScreensUI.PlansScreen = ScreensUI.PlansPage.Control;
                    // Todo - load control only if not loaded
                    ScreensUI.PlansScreen.LoadControl();

                    ScreensUI.PlansPane.Show();
                    MainApp.AppData.CurrentProject.PlansScreenVisible = true;
                }


                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }
    }
}