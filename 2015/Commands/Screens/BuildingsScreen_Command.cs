﻿#region Namespaces

using System;
using System.Windows.Forms;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.UI;
using ViewCreator.Utils.App;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class BuildingsScreen_Command : IExternalCommand
    {
        private static ExternalCommandData _cachedCmdData;

        public static UIApplication CachedUiApp
        {
            get { return _cachedCmdData.Application; }
        }

        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            if (!App_Utils.FullCheck())
            {
                return Result.Failed;
            }

            _cachedCmdData = cmdData;

            try
            {
                if (ScreensUI.BuildingsPane == null)
                {
                    ScreensUI.BuildingsPane = CachedUiApp.GetDockablePane(ScreensUI.BuildingsScreenId);
                }

                if (ScreensUI.BuildingsPane == null)
                    {
                        MessageBox.Show("Pane not available");
                        return Result.Failed;
                    }


                if (MainApp.AppData.CurrentProject.BuildingsScreenVisible)
                    {
                        ScreensUI.BuildingsPane.Hide();
                        MainApp.AppData.CurrentProject.BuildingsScreenVisible = false;
                    }
                    else
                    {
                        ScreensUI.BuildingsScreen = ScreensUI.BuildingsPage.Control;
                        // Todo - load control only if not loaded
                        ScreensUI.BuildingsScreen.LoadControl();
                        ScreensUI.BuildingsPane.Show();
                        MainApp.AppData.CurrentProject.BuildingsScreenVisible = true;
                    }
               


                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }
    }
}