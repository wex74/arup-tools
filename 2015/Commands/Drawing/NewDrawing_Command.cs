﻿#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.Forms;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Forms.Main_Dialogs;
using ViewCreator.Forms.Manage_Forms;
using ViewCreator.Forms.Sections.Drawings.Forms;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;
using View = Autodesk.Revit.DB.View;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class NewDrawing_Command : IExternalCommand
    {

        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            try
            {
                if (!App_Utils.UserModelSettingsCheck())
                {
                    return Result.Failed;
                }

                var frm = new frmNewDrawing();

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    MainApp.PauseViewUpdater = true;


                    var drawing = new Drawing(frm.DrawingNo);
                    drawing.Discipline = frm.Discipline;
                    drawing.DrawingType = frm.DrawingType;
                    drawing.SheetTemplate = frm.SheetTemplate;

                    drawing.Title1 = frm.Discipline;
                    drawing.Title2 = frm.Title;

                    var sheet = Sheet_Utils.CreateSheet(drawing);
                    var view = frm.GetView();

                    if (sheet != null)
                    {
                        MainApp.AppData.ProjectDrawings.Add(sheet);

                        if (view != null)
                        {
                            Sheet_Utils.InsertView(sheet, view);
                        }
                    }

                    MainApp.PauseViewUpdater = false;
                }

                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }
    }
}