﻿#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.Forms;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Forms.Main_Dialogs;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;
using View = Autodesk.Revit.DB.View;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class ManageDrawings_Command : IExternalCommand
    {

        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            try
            {
                if (!App_Utils.FullCheck())
                {
                    return Result.Failed;
                }

                ManageDrawingsUI.ShowForm();

                //var frm = new frmManageDrawings();

                //if (frm.ShowDialog() == DialogResult.OK)
                //{
                  // Sheet_Utils.CreateNewSheets(frm.GetDrawings());

                //    var updates = App.AppData.CurrentProject.ProjectSheets.GetDrawings().Where(x => x.UpdateRequired()).ToList();

                //    foreach (var drawing in updates)
                //    {
                //        Sheet_Utils.UpdateSheet(drawing);
                //    }
                //}

                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }
    }
}