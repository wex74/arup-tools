﻿#region Namespaces

using System;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.Forms;
using ViewCreator.Forms.Main_Dialogs;
using ViewCreator.Utils.App;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class ModelSettings_Command : IExternalCommand
    {
        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            try
            {
                if (!App_Utils.SettingsCheck())
                {
                    return Result.Failed;
                }

               
                var frm = new frmSettings();
                frm.ShowDialog();

                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }



    }
}