﻿#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.Forms;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;
using View = Autodesk.Revit.DB.View;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class DependentViews_Command : IExternalCommand
    {
        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            try
            {

                if (!App_Utils.UserModelSettingsCheck())
                {
                    return Result.Failed;
                }


                var view = RVT_Utils.ActiveView;
      
                // check if view can be duplicated
                if (!view.CanViewBeDuplicated(ViewDuplicateOption.AsDependent))
                {
                    MessageBox.Show("Unable to create dependent view from " + view.ViewName);
                    return Result.Failed;
                }

                // check if view is already dependent
                if (view.ViewSpecific)
                {
                    MessageBox.Show("View is already a dependent view");
                    return Result.Failed;
                }

                var views = new List<View>();

                // get scope and dependents
                var frm = new frmSelectScopeboxes();

                if (frm.ShowDialog() == DialogResult.OK)
                {

                    var n = frm.Count;
                    var s = "{0} of " + n + " views processed...";
                    var caption = "Creating Dependent Views";

                    using (var pf = new ProgressForm(caption, s, n))
                    {
                        MainApp.PauseViewUpdater = true;
                       

                        foreach (var scopeBox in frm.SelectedScopboxes)
                        {
                            var dependent = View_Utils.AddDependentView(view, scopeBox);
                            views.Add(dependent);
                            pf.Increment();
                        }

                        for (int i = 1; i < frm.Dependents + 1; i++)
                        {
                            var dependent = View_Utils.AddDependentView(view, i.ToString());
                            views.Add(dependent);
                            pf.Increment();
                        }

                     
                        MainApp.PauseViewUpdater = false;
                    }

                    MainApp.AppData.ProjectPlans.Add(views);
                    ScreensUI.BuildViews();

                }


                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }
    }
}