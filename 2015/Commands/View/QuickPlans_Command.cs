﻿#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;
using View = Autodesk.Revit.DB.View;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class QuickPlans_Command : IExternalCommand
    {
        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            try
            {
                if (!App_Utils.FullCheck())
                {
                    return Result.Failed;
                }


                var frm = new frmAddQuickPlans();

                if (frm.ShowDialog() == DialogResult.OK)
                {
                    View view = null;
                    var viewIds = new List<int>();


                    var n = frm.Levels.Count();
                    var s = "{0} of " + n + " plans processed...";
                    var caption = "Creating Plans";

                    using (var pf = new ProgressForm(caption, s, n))
                    {
                        MainApp.PauseViewUpdater = true;

                        foreach (var level in frm.Levels)
                        {
                            var viewInfo = new ViewInfo();
                            viewInfo.Level = Level_Utils.GetLevel(level);
                            viewInfo.Prefix = frm.Prefix;
                            viewInfo.Name = frm.PlanName;
                            viewInfo.Template = frm.Template;
                            viewInfo.ViewType = View_Utils.GetViewType(frm.ViewType);
                            viewInfo.Scopebox = frm.ScopeBox;
                            viewInfo.Favourite = frm.Favourite;

                            view = View_Utils.AddView(viewInfo);

                            if (view != null)
                            {
                                if (viewInfo.Favourite)
                                {
                                    User_Utils.AddFavourite(view.IdToInt());
                                }

                                View_Utils.CloseView(view);

                                MainApp.AppData.CurrentProject.ProjectPlans.Add(view,false);
                            }


                            pf.Increment();
                        }

                        MainApp.PauseViewUpdater = false;
                    }

                    ScreensUI.BuildViews();

                }


                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }
    }
}