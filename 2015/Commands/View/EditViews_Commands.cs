﻿#region Namespaces

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.Forms;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.RVT;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;
using View = Autodesk.Revit.DB.View;

#endregion

namespace ViewCreator.Commands
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class EditViews_Command : IExternalCommand
    {

        public Result Execute(ExternalCommandData cmdData, ref string msg, ElementSet elemSet)
        {
            try
            {
                if (!App_Utils.UserModelSettingsCheck())
                {
                    return Result.Failed;
                }

                var views = new List<View>();

                if (MainApp.AppData.IsUserBuildings)
                {
                    views = Plan_Utils.GetBuildingPlans();
                }

                if (MainApp.AppData.IsUserStructures)
                {
                    views = Plan_Utils.GetStructuralPlans();
                }
                
                EditViewsUI.ShowForm(views);

                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
                return Result.Failed;
            }
        }
    }
}