﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Model.Drawings;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Utils.App
{
    


    public static class App_Utils
    {

       

        public static string AppName
        {
            get
            {
                return "ViewCreator";
            }

        }


        public static bool CheckIsRevitUser(bool showMessage = true)
        {

            if (MainApp.AppData.IsUserRevit)
            {

                if (showMessage)
                {
                    MessageBox.Show("User settings Section Discipline (" + ViewCreator.MainApp.AppData.UserSectionDiscipline + ")"
                        + Environment.NewLine + "Not set to Structures or Buildings");

                }

                return true;
            }



            return false;
        }

        public static bool CheckCurrentProjectIsValid(bool showMessage = true)
        {

            if (MainApp.AppData.CurrentProject == null)
            {
                if (showMessage)
                {
                    MessageBox.Show("Current project not loaded");
                }

                return false;
            }

            return true;
        }


        public static bool CheckUserModelDataIsValid(bool showMessage = true)
        {

            if (MainApp.AppData.UserModelData == null)
            {
                if (showMessage)
                {
                    MessageBox.Show("UserModelData not loaded");
                }

                return false;
            }

            return true;
        }

        public static bool CheckProjectDataExists(bool showMessage = true)
        {

            if (!MainApp.AppData.CurrentProject.DataFileExists)
            {
                if (showMessage)
                {
                    MessageBox.Show("Data file does not exist." + Environment.NewLine + "Settings need to be created");
                }

                return false;
            }

            return true;
        }

        public static bool CheckUserDataExists(bool showMessage = true)
        {

            if (!File.Exists(Path_Utils.UserDataFile))
            {
                if (showMessage)
                {
                    MessageBox.Show("User Data file does not exist." + Environment.NewLine + Path_Utils.UserDataFile + " was not created");
                }

                return false;
            }

            return true;
        }

        public static bool CheckUserModelDataExists(bool showMessage = true)
        {

            if (!File.Exists(Path_Utils.UserModelDataFile))
            {
                if (showMessage)
                {
                    MessageBox.Show("User ModelData file does not exist." + Environment.NewLine + Path_Utils.UserModelDataFile + " was not created");
                }

                return false;
            }

            return true;
        }





        public static bool SettingsCheck()
        {
            // check user data has been created (UserSettingsRepository)
            // return false if not found
            if (!CheckUserDataExists())
            {
                return false;
            }

            // return false if user has not selected a role
            if (CheckIsRevitUser())
            {
                return false;
            }

            // return false if current project is null
            if (!CheckCurrentProjectIsValid())
            {
                return false;
            }

            return true;
        }


        public static bool UserModelSettingsCheck()
        {
            // check user data has been created (UserSettingsRepository)
            // return false if not found
            if (!CheckUserDataExists())
            {
                return false;
            }

            // check user model data has been created (UserModelDataRepository)
            // return false if not found
            if (!CheckUserModelDataExists())
            {
                return false;
            }


            // check user model data is loaded
            // return false if not
            if (!CheckUserModelDataIsValid())
            {
                return false;
            }

            return true;
        }

        public static bool UserSettingsCheck()
        {
            // check user data has been created (UserSettingsRepository)
            // return false if not found
            if (!CheckUserDataExists())
            {
                return false;
            }

          
            return true;
        }


        // Check all data files and settings have been made to run application
        public static bool FullCheck()
        {

            // check user data has been created (UserSettingsRepository)
            // return false if not found
            if (!CheckUserDataExists())
            {
                return false;
            }

            // check user model data has been created (UserModelDataRepository)
            // return false if not found
            if (!CheckUserModelDataExists())
            {
                return false;
            }

            // return false if user has not selected a role
            if (CheckIsRevitUser())
            {
                return false;
            }

            // return false if current project is null
            if (!CheckCurrentProjectIsValid())
            {
                return false;
            }


            // return false if project data has not been created (SettingsRepository)
            if (!CheckProjectDataExists())
            {
                return false;
            }

            return true;
        }
        public static  List<Level> GetVisibleLevels ()
        {
            var list = new List<Level>();
            var modelData = MainApp.AppData.UserModel;
            var i = 0;
            foreach (var level in Level_Utils.GetLevels())
            {
                var levelShown = modelData.VisibleLevelIds.Exists(x => x == level.Id.IntegerValue);

                if (levelShown)
                {
                    list.Add(level);
                }
            }

            return list;
        }


        public static List<string> GetVisibleLevelNames()
        {
            return GetVisibleLevels().Select(x => x.Name).ToList();
        }
    


        public static string AppPath { get; set; }


      


        public static List<string> GetUserSectionTypes()
        {
            var list = new List<string>();

            foreach (SectionDiscipline discipline in Enum.GetValues(typeof(SectionDiscipline)))
            {
                list.Add(discipline.ToString());
            }

            return list;
        }

        public static string GetDisciplineImage(string text)
        {
            foreach (Discipline discipline in Enum.GetValues(typeof(Discipline)))
            {
                if (text.Contains(discipline.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    return discipline.ToString();
                }
            }

            return "";
        } 


     

        public static List<DrawingType> GetDrawingTypes()
        {
            return new DrawingTypes().ToList();
        }

        public static List<string> GetDrawingTypeNames(string prefix)
        {
            return GetDrawingTypes().Select(x => prefix + x.ToString()).ToList();
        }
    }
}
