﻿namespace ViewCreator.Utils.App
{
    public enum Discipline
    {
        Architectural,
        Coordination,
        Structural,
        Mechanical,
        Electrical,
        Hydraulic,
        Fire
    }


    public enum DrawingsViewType
    {
        New,
        Model
    }

    public enum SectionDiscipline
    {
        Structures,
        Buildings,
        Revit
    }
}