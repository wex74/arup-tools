﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewCreator.Utils.App
{
    public static class Discipline_Utils
    {
        public static Discipline GetDiscipline(string name)
        {
            if (MainApp.AppData.IsUserStructures)
            {
                return Discipline.Structural;
            }

            if (MainApp.AppData.IsUserBuildings)
            {
                foreach (Discipline discipline in Enum.GetValues(typeof(Discipline)))
                {
                    if (discipline.ToString() == name) return discipline;
                }
            }


            return Discipline.Architectural;
        }


        public static string GetServiceTag(Discipline discipline)
        {
            var tag = discipline + " Services";
            if (discipline == Discipline.Architectural) tag = discipline.ToString();
            if (discipline == Discipline.Coordination) tag = discipline.ToString();
            if (discipline == Discipline.Structural) tag = discipline.ToString();

            return tag;
        }

        //public static string GetDiscipline(string service)
        //{
        //    var tag = discipline + " Services";
          
        //    return tag;
        //}

        // get discipline names based on user role
        public static List<string> GetDisciplineNames()
        {
            if (MainApp.AppData.IsUserStructures)
            {
                return MainApp.AppData.StructuralSettings.Select(x => x.Discipline.ToString()).ToList();
            }

            if (MainApp.AppData.IsUserBuildings)
            {
                return MainApp.AppData.BuildingSettings.Select(x => x.Discipline.ToString()).ToList();
            }

            return new List<string>();
        }
        

        public static List<string> GetDisciplineServiceNames()
        {
            var list = new List<string>();

            foreach (Discipline discipline in Enum.GetValues(typeof(Discipline)))
            {
                list.Add(GetServiceTag(discipline));
            }

            return list;
        }

        public static string GetDisciplinePrefix(Discipline discipline)
        {
            return discipline.ToString().Substring(0, 1) + "-";
        }

        public static string GetDisciplinePrefix(string discipline)
        {
            return discipline.Substring(0, 1) + "-";
        }


        public static List<string> GetAllDisciplineDrawingTypeNames()
        {
            var list = new List<string>();
            list.Add("C-0000 Cover Sheet/General");
            list.Add("C-2000 Layouts");
            list.Add("E-0000 Cover Sheet/General");
            list.Add("E-1000 Schematics");
            list.Add("E-2000 Cable Tray Layouts");
            list.Add("E-3000 Sections and Detailed Views");
            list.Add("E-4000 Standard Details");
            list.Add("E-5000 Equipment Schedules");
            list.Add("F-0000 Cover Sheet/General");
            list.Add("F-1000 Schematics");
            list.Add("F-2000 Layouts");
            list.Add("F-3000 Sections and Detailed Views");
            list.Add("F-4000 Standard Details");
            list.Add("F-5000 Equipment Schedules");
            list.Add("H-0000 Cover Sheet/General");
            list.Add("H-1000 Schematics");
            list.Add("H-2000 Layouts");
            list.Add("M-0000 Cover Sheet/General");
            list.Add("M-1000 Schematics");
            list.Add("M-2000 Layouts");
            list.Add("M-3000 Sections and Detailed Views");
            list.Add("M-4000 Standard Details");
            list.Add("M-5000 Equipment Schedules");
            list.Add("M-6000 Louvre Elevations");
            list.Add("MG-0000 Cover Sheet/General");
            list.Add("MG-1000 Schematics");
            list.Add("MG-2000 Layouts");
            list.Add("MG-3000 Sections and Detailed Views");
            list.Add("MG-4000 Standard Details");
            list.Add("MG-5000 Equipment Schedules");
            return list;
        }
    }







}
