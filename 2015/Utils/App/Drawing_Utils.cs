﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewCreator.Model;
using ViewCreator.Model.Drawings;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Utils.App
{
    public static class Drawing_Utils
    {

        public static string GetDrawingNo(string prefix, int no, string suffix = "")
        {
            var text = "";
            //if (no < 10)
            //{
            //    text = prefix + "0" + no;
            //}
            //else
            //{
            //    text = prefix + no;
            //}

            text = prefix + no;

            if (suffix != "")
            {
                text = text + suffix;
            }

            return text;
        }




        public static List<string> GetDrawingTypeTitles(DrawingType drawingType, bool scopeBoxes = false)
        {
            var list = new List<string>();

            switch (drawingType.Number)
            {
                case "0000":
                    list.Add("Cover Sheet");
                    list.Add("Title Sheet");
                    break;

                case "1000":
                    list.Add("Schematic Sheet");
                    break;

                case "2000":
                    var levelsList = Level_Utils.GetLevelNames();

                    if (scopeBoxes)
                    {
                        var scopeNames = Scopebox_Utils.GetScopeBoxNames();

                        if (scopeNames.Any())
                        {
                            levelsList = new List<string>();
                            foreach (var level in Level_Utils.GetLevelNames())
                            {
                                levelsList.Add(level);

                                foreach (var scope in scopeNames)
                                {
                                    levelsList.Add(level + " - " + scope);

                                }
                            }
                        }
                    }

                    list.AddRange(levelsList);
                    break;

                case "3000":
                    list.Add("Section Sheet");
                    list.Add("Detail Sheet");
                    break;

                case "4000":
                    list.Add("Standard Detail Sheet");
                    break;


                case "5000":
                    list.Add("Schedule Sheet");
                    break;
            }


            return list;
        }




        public static string GetDrawingType(Drawing drawing)
        {
            var text = "";
            var discipline = drawing.Discipline;
            var title = drawing.GetTitle();


            if (discipline.StartsWith("C"))
            {
                if (title.Contains("Cover", StringComparison.OrdinalIgnoreCase) || title.Contains("Title", StringComparison.OrdinalIgnoreCase))
                {
                    return "C-0000 Cover Sheet/General";
                }

                return "C-2000 Layouts";
            }

            if (discipline.StartsWith("E"))
            {
                if (title.Contains("Cover", StringComparison.OrdinalIgnoreCase) || title.Contains("Title", StringComparison.OrdinalIgnoreCase))
                {
                    return "E-0000 Cover Sheet/General";
                }

                if (title.Contains("Schematic", StringComparison.OrdinalIgnoreCase))
                {
                    return "E-2000 Schematics";
                }

                if (title.Contains("Section", StringComparison.OrdinalIgnoreCase))
                {
                    return "E-3000 Sections and Detailed Views";
                }

                if (title.Contains("Detail", StringComparison.OrdinalIgnoreCase))
                {
                    return "E-4000 Standard Details";
                }

                if (title.Contains("Schedule", StringComparison.OrdinalIgnoreCase))
                {
                    return "E-5000 Equipment Schedules";
                }

                return "E-2000 Cable Tray Layouts";
            }

            if (discipline.StartsWith("F"))
            {
                if (title.Contains("Cover", StringComparison.OrdinalIgnoreCase) || title.Contains("Title", StringComparison.OrdinalIgnoreCase))
                {
                    return "F-0000 Cover Sheet/General";
                }

                if (title.Contains("Schematic", StringComparison.OrdinalIgnoreCase))
                {
                    return "F-1000 Schematics";
                }

                if (title.Contains("Section", StringComparison.OrdinalIgnoreCase))
                {
                    return "F-3000 Sections and Detailed Views";
                }

                if (title.Contains("Detail", StringComparison.OrdinalIgnoreCase))
                {
                    return "F-4000 Standard Details";
                }

                if (title.Contains("Schedule", StringComparison.OrdinalIgnoreCase))
                {
                    return "F-5000 Equipment Schedules";
                }

                return "F-2000 Layouts";
            }


            if (discipline.StartsWith("H"))
            {
                if (title.Contains("Cover", StringComparison.OrdinalIgnoreCase) || title.Contains("Title", StringComparison.OrdinalIgnoreCase))
                {
                    return "H-0000 Cover Sheet/General";
                }

                if (title.Contains("Schematic", StringComparison.OrdinalIgnoreCase))
                {
                    return "H-1000 Schematics";
                }

                return "H-2000 Layouts";
            }

            if (discipline.StartsWith("M"))
            {
                if (title.Contains("Cover", StringComparison.OrdinalIgnoreCase) || title.Contains("Title", StringComparison.OrdinalIgnoreCase))
                {
                    return "M-0000 Cover Sheet/General";
                }

                if (title.Contains("Schematic", StringComparison.OrdinalIgnoreCase))
                {
                    return "M-1000 Schematics";
                }

                if (title.Contains("Section", StringComparison.OrdinalIgnoreCase))
                {
                    return "M-3000 Sections and Detailed Views";
                }

                if (title.Contains("Detail", StringComparison.OrdinalIgnoreCase))
                {
                    return "M-4000 Standard Details";
                }

                if (title.Contains("Schedule", StringComparison.OrdinalIgnoreCase))
                {
                    return "M-5000 Equipment Schedules";
                }

                return "M-2000 Layouts";
            }


            if (discipline.StartsWith("Med"))
            {
                if (title.Contains("Cover", StringComparison.OrdinalIgnoreCase) || title.Contains("Title", StringComparison.OrdinalIgnoreCase))
                {
                    return "MG-0000 Cover Sheet/General";
                }

                if (title.Contains("Schematic", StringComparison.OrdinalIgnoreCase))
                {
                    return "MG-1000 Schematics";
                }

                if (title.Contains("Section", StringComparison.OrdinalIgnoreCase))
                {
                    return "MG-3000 Sections and Detailed Views";
                }

                if (title.Contains("Detail", StringComparison.OrdinalIgnoreCase))
                {
                    return "MG-4000 Standard Details";
                }

                if (title.Contains("Schedule", StringComparison.OrdinalIgnoreCase))
                {
                    return "MG-5000 Equipment Schedules";
                }

                return "MG-2000 Layouts";
            }
            return text;
        }





        public static Drawing GetDuplicate(this Drawing source)
        {
            var dupDrawing = new Drawing(source.DrawingNo + " Copy");
            dupDrawing.Title1 = source.Title1;
            dupDrawing.Title2 = source.Title2;
            dupDrawing.Title3 = source.Title3;
            dupDrawing.Title4 = source.Title4;
            dupDrawing.Discipline = source.Discipline;
            dupDrawing.DrawingType = source.DrawingType;
            return dupDrawing;
        }

        public static string GetTitle(this Drawing source)
        {

            var name = "";

            if (source.Title1 != "")
            {
                name = source.Title1;
            }

            if (source.Title2 != "")
            {
                name = name + " " + source.Title2;
            }

            if (source.Title3 != "")
            {
                name = name + " " + source.Title3;
            }

            if (source.Title4 != "")
            {
                name = name + " " + source.Title4;
            }

            return name;
        }

        public static string GetFullName(this Drawing source)
        {
            return source.DrawingNo + " - " + GetTitle(source);

        }
    }
}
