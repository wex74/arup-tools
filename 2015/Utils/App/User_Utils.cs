﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewCreator.Utils.App
{
    public static class User_Utils
    {
        public static bool IsFavourite(int id)
        {
            return MainApp.AppData.UserModelData.GetUserModelData().UserFavourites.Exists(id);
        }

        public static void AddFavourite(int id)
        {
            MainApp.AppData.UserModelData.GetUserModelData().UserFavourites.Add(id);
            MainApp.AppData.SaveUserModelData();
          
        }

        public static void RemoveFavourite(int id)
        {
            MainApp.AppData.UserModelData.GetUserModelData().UserFavourites.Remove(id);
            MainApp.AppData.SaveUserModelData();
        }
    }
}
