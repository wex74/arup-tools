﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RevitManager_SRC.Classes;
using ViewCreator.Model;
using ViewCreator.Model.Base_Classes;
using View = Autodesk.Revit.DB.View;
using Autodesk.Revit.DB;

namespace ViewCreator.Utils.UI
{

    public class NodeSorter : IComparer
    {
        public int Compare(object x, object y)
        {
            TreeNode tx = (TreeNode)x;
            TreeNode ty = (TreeNode)y;


            if (tx.Text.Length < ty.Text.Length)
            {
                return -1;
            }

            if (tx.Text.Length > ty.Text.Length)
            {
                return 1;
            }

            return 0;
        }
    }


    public static class Tree_Utils
    {
        public static TreeNode GetNode(this TreeView source, string id, bool searchChildren = true)
        {
            return  source.Nodes.Find(id, searchChildren).FirstOrDefault();
            
        }


        


        public static string DisplayText(this TreeView source)
        {
            if (source.SelectedNode == null) return "";
          
            return source.SelectedNode.Text;

        }

        public static void RemoveNode(this TreeView source, string id, bool removeParent = true)
        {
            var node = source.Nodes.Find(id, true).FirstOrDefault();

            if (node != null)
            {

                TreeNode parent = null;

                if (removeParent)
                {
                    parent = node.Parent;
                }

                source.Nodes.Remove(node);;

                if (parent != null && parent.Nodes.Count == 0)
                {
                    source.Nodes.Remove(parent);
                }


            }
        }

        public static void UpdateNode(this TreeView source, Plan plan)
        {
            var node = source.Nodes.Find(plan.ViewId.ToString(), true).FirstOrDefault();

            if (node != null)
            {
                node.Text = plan.Name;
            }
        }

        public static bool IsView(object tag)
        {
            return General_Utils.IsSameOrSubclass(typeof(View), tag.GetType());
        }

        public static bool IsLevel(object tag)
        {
             return tag.GetType() == typeof(Level);
        }

        public static bool IsViewInfo(object tag)
        {
            return tag.GetType() == typeof(ViewInfo);
        }

        public static bool IsString(object tag)
        {
            return tag is string;
        }

        public static bool IsDrawing(object tag)
        {
            return tag.GetType() == typeof(Drawing);
        }


        public static bool IsPlan(object tag)
        {
            return tag.GetType() == typeof(Plan);
        }

        public static bool IsDependentPlan(object tag)
        {
            return tag.GetType() == typeof(DependentPlan);
        }

        public static void SelectFirstTreeNode(this TreeView source)
        {
            if (source.SelectedNode == null)
            {
                if (source.Nodes.Count > 0)
                {
                    source.SelectedNode = source.Nodes[0];
                    source.Nodes[0].EnsureVisible();
                }
            }

        }



        public static void SelectCurrentView(this TreeView source, bool selectIfNotFound = true)
        {
            var view = RVT_Utils.ActiveView;

            if (view != null)
            {
                var node = source.Nodes.Find(view.Id.ToString(), true).FirstOrDefault();
                if (node != null)
                {
                    source.SelectedNode = node;
                    node.EnsureVisible();
                }
            }

            if (selectIfNotFound)
            {
                if (source.SelectedNode == null)
                {
                    if (source.Nodes.Count > 0)
                    {
                        source.SelectedNode = source.Nodes[0];
                    }
                }
            }
           
        }



    }
}
