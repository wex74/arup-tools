﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Autodesk.Revit.UI;
using ViewCreator.Utils.App;

namespace ViewCreator.Utils.UI
{
   public static class Ribbon_Utils
    {
       public static RibbonButton AddPushButton(this RibbonPanel source, string command, string name, string type = "gif")
       {
           RibbonButton pushButton =
               source.AddItem(new PushButtonData(name, name, Assembly.GetExecutingAssembly().Location, "ViewCreator.Commands." + command + "_Command"))
                   as PushButton;


           string img = command + "." + type;

           pushButton.LargeImage = GetBitmapImage(img);

           return pushButton;
       }

       public static BitmapImage GetBitmapImage(string name)
       {
           var ribbonPath = Path.Combine(App_Utils.AppPath, "Images", "Ribbon");
           var path = Path.Combine(ribbonPath, name);
           BitmapImage bmp;

           if (File.Exists(path))
           {
               bmp = new BitmapImage(new Uri(path));
           }
           else
           {
               bmp = new BitmapImage();
           }

           return bmp;
       }

       public static bool IsPanelTitleUsed(string tab, string panelTitle)
       {
           List<RibbonPanel> loadedPluginPanels = RVT_Utils.UIContApp.GetRibbonPanels(tab);

           foreach (RibbonPanel p in loadedPluginPanels)
           {
               if (p.Name == panelTitle)
               {
                   return true;
               }
           }
           return false;
       }
    }
}
