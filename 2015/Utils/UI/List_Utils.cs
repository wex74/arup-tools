﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewCreator.Utils.UI
{
    public static class List_Utils
    {
        public static List<string> GetCheckedListboxValues(this CheckedListBox source)
        {
            var list = new List<string>();

            for (var i = 0; i < source.Items.Count; i++)
            {
                if (source.GetItemChecked(i))
                {
                    list.Add(source.Items[i].ToString());
                }
            }

            return list;
        }

        public static void LoadAndSetList(this CheckedListBox source, List<string> loadList, List<string> setList)
        {
            source.LoadList(loadList);
            source.SetCheckedItems(setList);
        }

        public static void LoadAndSelect(this ListBox source, List<string> list, string selectText, string insert = null)
        {
            list.RemoveAll(x => x == null);
            if (source.Items.Count > 0) source.Items.Clear();
            source.Items.AddRange(list.ToArray());

            if (insert != null)
            {
                source.Items.Insert(0, insert);
            }

            source.SelectedItem = selectText;
            if (source.SelectedItem == null)
            {
                source.SelectedIndex = 0;
            }
        }


        public static void CheckAllItems(this CheckedListBox source, bool isChecked)
        {
            for (var x = 0; x < source.Items.Count; x++)
            {
                source.SetItemChecked(x, isChecked);
            }
        }

        public static int GetCheckedCount(this CheckedListBox source)
        {
            return source.CheckedIndices.Count;
        }

        public static bool AllItemsChecked(this CheckedListBox source)
        {
            return GetCheckedCount(source) == source.Items.Count;
        }


        public static void LoadList(this CheckedListBox source, List<string> list)
        {
            list.RemoveAll(x => x == null);
            if (source.Items.Count > 0) source.Items.Clear();
            source.Items.AddRange(list.ToArray());
        }



        public static void LoadCombo(this ComboBox source, List<string> list, string selectedText, string insert)
        {
            list.RemoveAll(x => x == null);
            if (source.Items.Count > 0) source.Items.Clear();
            source.Items.AddRange(list.ToArray());


            if (insert != null)
            {
                source.Items.Insert(0, insert);
            }

            if (!String.IsNullOrEmpty(selectedText))
            {
                if (list.Exists(x => x == selectedText) || insert == selectedText)
                {
                    source.Text = selectedText;
                }
            }
        }


        public static void SetCheckedItems(this CheckedListBox source, List<string> list)
        {
            for (var i = 0; i < source.Items.Count; i++)
            {
                var name = source.GetItemText(source.Items[i]);

                var isChecked = list.Exists(x => x == name);
                source.SetItemChecked(i, isChecked);
            }
        }
    }
}
