﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SourceGrid;
using ViewCreator.Forms.Controls.Views;
using ViewCreator.Model;
using AutoSizeMode = SourceGrid.AutoSizeMode;
using ColumnHeader = SourceGrid.Cells.Views.ColumnHeader;
using GridColumn = ViewCreator.Model.GridColumn;

namespace ViewCreator.Utils.UI
{
   public static class Grid_Utils
    {


        public static void BuildColumns(Grid grid, List<GridColumn> columns)
        {
            int row;
            grid.Selection.EnableMultiSelection = true;
            grid.Rows.Clear();
            grid.ColumnsCount = columns.Count;
  
            row = grid.RowsCount;
            grid.Rows.Insert(row);

            var headerView = new ColumnHeader();


            foreach (var column in columns)
            {
                var index = column.Index;

                var header = new SourceGrid.Cells.ColumnHeader(column.Name);

                header.View = headerView;
                header.AutomaticSortEnabled = true;

                grid[row, index] = header;

                header.Column.Width = column.Width;

             
            }

  
        }
    }
}
