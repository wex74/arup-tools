﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Forms;
using ViewCreator.Forms;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Model;
using ViewCreator.Utils.RVT;
using MessageBox = System.Windows.Forms.MessageBox;
using View = Autodesk.Revit.DB.View;
using System.Windows.Forms;

namespace ViewCreator.Utils
{
    public static class UI_Utils
    {
        public static string SelectViewTemplate(string template)
        {
            string selectedName = null;
            var frm = new frmSelectViewTemplate {TemplateName = template};

            if (frm.ShowDialog() == DialogResult.OK)
            {
                selectedName = frm.TemplateName;
            }

            return selectedName;
        }



        // redundant method 
        //public static bool EditView(View view)
        //{
        //    var updated = false;
        //    var updateView = new UpdateView(view);

        //    var frm = new frmEditView {UpdateView = updateView};

        //    if (frm.ShowDialog() == DialogResult.OK)
        //    {
        //        if (updateView.UpdateName)
        //        {
        //            View_Utils.RenameView(view, updateView.ViewName);
        //            updated = true;
        //        }

        //        if (updateView.UpdateTemplate)
        //        {
        //            Template_Utils.SetViewTemplate(view, updateView.Template);
        //            updated = true;
        //        }

        //        if (updateView.UpdateScopeBox)
        //        {
        //            Scopebox_Utils.SetScopeBox(updateView.ScopeBox, updateView.GetView());
        //            updated = true;
        //        }


        //        if (updateView.UpdateScale)
        //        {
        //            View_Utils.ChangeScale(updateView.GetView(), updateView.Scale);
        //            updated = true;
        //        }

        //        if (updateView.UpdateFavourite)
        //        {
        //            if (updateView.Favourite)
        //            {
        //                MainApp.AppData.UserModelData.GetUserModelData().UserFavourites.Add(view.IdToInt());
        //                updated = true;
        //            }
        //            else
        //            {
        //                MainApp.AppData.UserModelData.GetUserModelData().UserFavourites.Remove(view.IdToInt());
        //                updated = true;
        //            }
        //        }
        //    }
        //    return updated;
        //}


        public static DialogResult GetMessageBoxDeleteResult(string text, string type)
        {
            return  MessageBox.Show("Delete " + text + " ?", "Delete " + type, MessageBoxButtons.YesNoCancel,
             MessageBoxIcon.Exclamation);
        }


        public static void ShowNotImplementedMessage()
        {
            MessageBox.Show("Not Implemented");
        }

        public class JtWindowHandle : IWin32Window
        {
            public JtWindowHandle(IntPtr h)
            {
                Debug.Assert(IntPtr.Zero != h,
                    "expected non-null window handle");

                Handle = h;
            }

            public IntPtr Handle { get; set; }
        }
    }
}