﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RevitManager_SRC.Classes;

namespace ViewCreator.Utils.UI
{
    public static class Text_Utils
    {
        public static string[] CharsToRemove { get { return new[] { "<", ">", "?", "|", "`", "*", "\"" }; } }




        private static string CheckTextBox(TextBox textBox, string textToCheck)
        {
            if (textBox.Text != textToCheck)
            {
                var textCheck = General_Utils.CleanInput(textBox.Text, CharsToRemove);

                if (textCheck != textBox.Text)
                {
                    textBox.Text = textCheck;
                }
                return textCheck;
            }

            return "";

        }

    }
}
