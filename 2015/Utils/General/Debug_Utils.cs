﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewCreator.Utils.App;

namespace ViewCreator.Utils.General
{
    public static class Debug_Utils
    {
        public static bool ShowTestOnly = false;
        public static bool HideTests = false;


        private static string GetText(string classOrMethod, string message)
        {
            var text = ">> " + App_Utils.AppName + " > " + classOrMethod + ": ";
            text += message;
            return text;
        }


        public static void Assert(bool condition, string classOrMethod, string message)
        {
            if (ShowTestOnly) return;
            Debug.Assert(condition, GetText(classOrMethod, message));
        }

        public static void WriteLineIf(bool condition,  string classOrMethod, string message)
        {
            if (ShowTestOnly) return;
            Debug.WriteLineIf(condition, GetText(classOrMethod, message));
        }

        public static void Test(string classOrMethod,string message)
        {
            if (HideTests) return;
            Debug.WriteLine(GetText("Test: " + classOrMethod, message + " >>"));
        }

        public static void WriteLine(string classOrMethod, string message)
        {
            if (ShowTestOnly) return;
            Debug.WriteLine(GetText(classOrMethod, message));
        }
    }
}
