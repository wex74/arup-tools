﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace RevitManager_RVT.Utils
{
    public static class File_Utils
    {
        private const int ERROR_SHARING_VIOLATION = 32;
        private const int ERROR_LOCK_VIOLATION = 33;

        public static void DeleteFolder(string path)
        {
            if (IsFileLocked(path)) return;

            if (Directory.Exists(path))
            {
                var dirInfo = new DirectoryInfo(path);

                foreach (FileInfo file in dirInfo.GetFiles())
                {
                    file.Delete();
                    WaitForFileDeleted(file.FullName);
                }



                foreach (DirectoryInfo dir in dirInfo.GetDirectories())
                {
                    dir.Delete(true);
                    WaitForFolderDeleted(dir.FullName);
                }


                dirInfo.Delete(true);
                WaitForFolderDeleted(dirInfo.FullName);

            }
        }


        public static bool IsRVTBackUp(string filename)
        {
            for (int i = 1; i < 20; i++)
            {
                if (filename.ToUpper().EndsWith(".000" + i + ".RVT"))
                {
                    return true;
                }
            }

            return false;
        }

        public static void DeleteFile(string fullpath)
        {
            if (!File.Exists(fullpath)) return;
            if (IsFileLocked(fullpath)) return;

           
                File.Delete(fullpath);
            WaitForFileDeleted(fullpath);


        }

        public static void DeleteFiles(string path, string type)
        {
            var files = Directory.GetFiles(path, "*." + type, SearchOption.TopDirectoryOnly);

            foreach (var file in files)
            {
                DeleteFile(file);
            }
        }

        public static void DeleteAllFiles(string path, string typeToExclude = "")
        {
            var files =
                Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly).Where(x => !x.EndsWith(typeToExclude));

            foreach (var file in files)
            {
                DeleteFile(file);
            }
        }

        public static void OpenInExplorer(string path)
        {
            if (Directory.Exists(path))
            {
                Process.Start(path);
            }
           
        }

        public static void OpenFile(string path)
        {
            if (File.Exists(path))
            {
                Process.Start(path);
            }

        }

        public static void SelectFileInExplorer(string filePath)
        {
            if (!File.Exists(filePath))
            {
                return;
            }


            string argument = @"/select, " + filePath;

            Process.Start("explorer.exe", argument);
        }




        public static void CopyFiles(List<string> files, string pathDestination)
        {
            foreach (var file in files)
            {
                var fi = new FileInfo(file);

                if (fi.Exists)
                {
                    var dest = Path.Combine(pathDestination, fi.Name);

                    if (!IsFileLocked(dest))
                    {
                        fi.CopyTo(dest, true);

                        WaitForFileCreated(dest);
                    }
                }
            }
        }

        public static void CopyFile(string filepath, string path)
        {
            var fi = new FileInfo(filepath);
            if (fi.Exists)
            {
                var dest = Path.Combine(path, fi.Name);

                if (!IsFileLocked(dest))
                {
                    fi.CopyTo(dest, true);

                    WaitForFileCreated(dest);
                }
            }
        }


        public static void MoveFile(string filepath, string path)
        {
            var fi = new FileInfo(filepath);
            if (fi.Exists)
            {
                var dest = Path.Combine(path, fi.Name);

                if (!IsFileLocked(dest) && !IsFileLocked(filepath))
                {
                    fi.MoveTo(dest);

                    WaitForFileCreated(dest);
                }
            }
        }


        public static bool IsAnyFileLockedInFolder(string path)
        {
            var files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                if (IsFileLocked(file)) return true;
            }

            return false;
        }



        public static bool IsFileLocked(string file)
        {
            //check that problem is not in destination file
            if (File.Exists(file))
            {
                FileStream stream = null;
                try
                {
                    stream = File.Open(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch (Exception ex2)
                {
                    //_log.WriteLog(ex2, "Error in checking whether file is locked " + file);
                    var errorCode = Marshal.GetHRForException(ex2) & ((1 << 16) - 1);
                    if ((ex2 is IOException) &&
                        (errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION))
                    {
                        return true;
                    }
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            return false;
        }

        public static bool WaitForFileDeleted(string path)
        {
            var count = 0;
            while (File.Exists(path))
            {
                if (count == 50)
                {
                    MessageBox.Show("Too Slow");
                    return false;
                }

                Thread.Sleep(1000);
                count++;
            }

            return true;
        }

        public static bool WaitForFolderDeleted(string path)
        {
            var count = 0;
            while (Directory.Exists(path))
            {
                if (count == 50)
                {
                    MessageBox.Show("Too Slow");
                    return false;
                }

                Thread.Sleep(1000);
                count++;

            }

            return true;
        }


        public static bool WaitForFileCreated(string path, int minutes = 1)
        {
            var count = 0;
            while (!File.Exists(path))
            {
                if (count == (60 / minutes))
                {
                    MessageBox.Show("Too Slow");
                    return false;
                }

                Thread.Sleep(1000);
                count++;
            }

            return true;
        }


        public static void CopyFilesRecursively(string sourcePath, string destinationPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourcePath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(sourcePath, destinationPath), true);
        }


        public static bool WaitForFolderCreated(string path)
        {
            var count = 0;
            while (!Directory.Exists(path))
            {
                if (count == 50)
                {
                    MessageBox.Show("Too Slow");
                    return false;
                }

                Thread.Sleep(1000);
                count++;
            }

            return true;
        }
    }
}