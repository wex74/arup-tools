﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewCreator.Model;

namespace ViewCreator.Utils.General
{
   public  static class String_Utils
    {

       public static string DumpObjects(IEnumerable<object> objects )
       {
           var val = Environment.NewLine;

           foreach (var o in objects)
           {
               val += o + Environment.NewLine;
           }
           return val;
       }
  




       public static string GetRLString(double RL)
       {
           return "RL " + String.Format("{0:#,0.000}", RL);
       }

       

       public static bool Contains(this string source, string toCheck, StringComparison comp)
       {
           if (toCheck == null) toCheck = "";
           return source.IndexOf(toCheck, comp) >= 0;
       }

       public static bool Matches(this string source, string toCheck)
       {
           if (toCheck == null) return true;

           return source.Contains(toCheck, StringComparison.OrdinalIgnoreCase);
       }


       public static string ReplaceString(this string source, string oldValue, string newValue, StringComparison comparison)
       {
           StringBuilder sb = new StringBuilder();

           int previousIndex = 0;
           int index = source.IndexOf(oldValue, comparison);
           while (index != -1)
           {
               sb.Append(source.Substring(previousIndex, index - previousIndex));
               sb.Append(newValue);
               index += oldValue.Length;

               previousIndex = index;
               index = source.IndexOf(oldValue, index, comparison);
           }
           sb.Append(source.Substring(previousIndex));

           return sb.ToString();
       }
    }
}
