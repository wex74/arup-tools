﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace RevitManager_SRC.Classes
{

   


    public static class General_Utils
    {
        

        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;



        public static bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase;
        }


        public static Point GetPoint(string point)
        {
            string[] coords = point.Split(',');
            var loc = new Point(int.Parse(coords[0].Replace("{X=", "")), int.Parse(coords[1].Replace("Y=", "").Replace("}", "")));

            return loc;
        }



        public static Size GetSize(string size)
        {
            string[] coords = size.Split(',');
            var s = new Size(int.Parse(coords[0].Replace("{Width=", "")),
                int.Parse(coords[1].Replace("Height=", "").Replace("}", "")));
            return s;
        }
        public static string GetDateStamp()
        {
            return String.Format("{0:yyyy-MM-dd}", DateTime.Today);
        }

        public static string GetDateStamp(DateTime date)
        {
            return String.Format("{0:yyyy-MM-dd}", date);
        }
        public static string GetDateStamp1()
        {
            return String.Format("{0:dd-MM-yy}", DateTime.Today);
        }

        public static string GetDateStamp1(DateTime date)
        {
            return String.Format("{0:dd-MM-yy}", date);
        }


        public static string GetDateStampTime()
        {
            return String.Format("{0:yyyy-MM-dd_hhmmss}", DateTime.Now);
        }

        public static string GetDateStampTimeOnly()
        {
            return String.Format("{0:hh.mm.ss}", DateTime.Now);
        }
        public static bool IsFileLocked(string file)
        {
            //check that problem is not in destination file
            if (File.Exists(file) == true)
            {
                FileStream stream = null;
                try
                {
                    stream = File.Open(file, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                }
                catch (Exception ex2)
                {
                    //_log.WriteLog(ex2, "Error in checking whether file is locked " + file);
                    int errorCode = Marshal.GetHRForException(ex2) & ((1 << 16) - 1);
                    if ((ex2 is IOException) && (errorCode == ERROR_SHARING_VIOLATION || errorCode == ERROR_LOCK_VIOLATION))
                    {
                        return true;
                    }
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
            return false;
        }


        public static string CleanInput(string strIn, string[] charsToRemove)
        {

            var str = strIn;
            
            foreach (var c in charsToRemove)
            {
                str = str.Replace(c, string.Empty);
            }

            return str;


            //// Replace invalid characters with empty strings. 
            //try
            //{
            //    return Regex.Replace(strIn, @"<>?|`", "",
            //                         RegexOptions.None, TimeSpan.FromSeconds(1.5));
            //}
            //// If we timeout when replacing invalid characters,  
            //// we should return Empty. 
            //catch (RegexMatchTimeoutException)
            //{
            //    return String.Empty;
            //}
        }

        public static List<String> GetRevList()
        {
            var list = new List<string>();


            for (int i = 0; i < 50; i++)
            {
                list.Add(i.ToString());
            }
            
            var alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray().ToList();

            foreach (var a in alpha)
            {
                list.Add(a.ToString());
            }


            return list;

        }

     


    }
}
