﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewCreator.Utils.App;

namespace ViewCreator.Utils.General
{
    public static class Path_Utils
    {
        public static bool ModelDataExists
        {
            get { return ModelDataFileExists(ModelDataFile); }
        }


        public static string ModelDataFile
        {
            get
            {
                return GetModelDataFile(RVT_Utils.GetCentralModelFullPath());
            }
        }



        public static string GetModelDataFile(string fullpath)
        {
            return fullpath.ReplaceString(".rvt", ".db", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool  ModelDataFileExists(string fullpath)
        {
            return File.Exists(GetModelDataFile(fullpath));
        }


        public static string UserDataPath
        {
            get
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                path = Path.Combine(path, App_Utils.AppName);

                if (!Directory.Exists(path)) Directory.CreateDirectory(path);

                return path;
            }

        }

        public static string UserModelDataFile
        {
            get
            {
                return Path.Combine(UserDataPath, "UserModelData.db"); ;
            }

        }

        public static string UserDataFile
        {
            get
            {
                return Path.Combine(UserDataPath, "UserData.db"); ;
            }

        }
    }
}
