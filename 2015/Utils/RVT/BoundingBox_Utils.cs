﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;

namespace ViewCreator.Utils
{
    public static class BoundingBox_Utils
    {
        private const double _eps = 1.0e-9;

        /// <summary>
        /// Return true if the given real number is almost zero.
        /// </summary>
        private static bool IsAlmostSero(double a)
        {
            return _eps > Math.Abs(a);
        }

        /// <summary>
        /// Return true if the given vector is almost vertical.
        /// </summary>
        private static bool IsVertical(XYZ v)
        {
            return IsAlmostSero(v.X) && IsAlmostSero(v.Y);
        }

        /// <summary>
        /// Return true if v and w are non-zero and perpendicular.
        /// </summary>
        private static bool IsPerpendicular(XYZ v, XYZ w)
        {
            double a = v.GetLength();
            double b = v.GetLength();
            double c = Math.Abs(v.DotProduct(w));
            return _eps < a
                   && _eps < b
                   && _eps > c;

            // To take the relative lengths of a and b into 
            // account, you can scale the whole test, e.g
            // c * c < _eps * a * b... can you?
        }

        /// <summary>
        /// Return the signed volume of the paralleliped 
        /// spanned by the vectors a, b and c. In German, 
        /// this is also known as Spatprodukt.
        /// </summary>
        private static double SignedParallelipedVolume(
            XYZ a,
            XYZ b,
            XYZ c)
        {
            return a.CrossProduct(b).DotProduct(c);
        }

        /// <summary>
        /// Return true if the three vectors a, b and c 
        /// form a right handed coordinate system, i.e.
        /// the signed volume of the paralleliped spanned 
        /// by them is positive.
        /// </summary>
        private static bool IsRightHanded(XYZ a, XYZ b, XYZ c)
        {
            return 0 < SignedParallelipedVolume(a, b, c);
        }

        /// <summary>
        /// Return the minimal aligned bounding box for 
        /// a Revit scope box element. The only 
        /// information we can obtain from the scope box 
        /// are its 12 boundary lines. Algorithm: Pick an 
        /// arbitrary line as the X axis and its starting 
        /// point as the origin. Find the three other 
        /// lines starting or ending at the origin, and 
        /// use them to define the Y and Z axes. If 
        /// necessary, swap Y and Z to form a
        /// right-handed coordinate system.
        /// </summary>
        public static BoundingBoxXYZ GetScopeBoxBoundingBox(
            Element scopeBox)
        {
            Document doc = scopeBox.Document;
            Application app = doc.Application;
            Options opt = app.Create.NewGeometryOptions();
            GeometryElement geo = scopeBox.get_Geometry(opt);
            int n = geo.Count<GeometryObject>();

            if (12 != n)
            {
                throw new ArgumentException("Expected exactly"
                                            + " 12 lines in scope box geometry");
            }

            XYZ origin = null;
            XYZ vx = null;
            XYZ vy = null;
            XYZ vz = null;

            // Extract the X, Y and Z axes from the lines

            foreach (GeometryObject obj in geo)
            {
                Debug.Assert(obj is Line,
                    "expected only lines in scope box geometry");

                Line line = obj as Line;

                XYZ p = line.GetEndPoint(0);
                XYZ q = line.GetEndPoint(1);
                XYZ v = q - p;

                if (null == origin)
                {
                    origin = p;
                    vx = v;
                }
                else if (p.IsAlmostEqualTo(origin)
                         || q.IsAlmostEqualTo(origin))
                {
                    if (q.IsAlmostEqualTo(origin))
                    {
                        v = v.Negate();
                    }
                    if (null == vy)
                    {
                        Debug.Assert(IsPerpendicular(vx, v),
                            "expected orthogonal lines in scope box geometry");

                        vy = v;
                    }
                    else
                    {
                        Debug.Assert(null == vz,
                            "expected exactly three orthogonal lines to originate in one point");

                        Debug.Assert(IsPerpendicular(vx, v),
                            "expected orthogonal lines in scope box geometry");

                        Debug.Assert(IsPerpendicular(vy, v),
                            "expected orthogonal lines in scope box geometry");

                        vz = v;

                        if (!IsRightHanded(vx, vy, vz))
                        {
                            XYZ tmp = vz;
                            vz = vy;
                            vy = tmp;
                        }
                        break;
                    }
                }
            }

            // Set up the transform

            Transform t = Transform.Identity;
            t.Origin = origin;
            t.BasisX = vx.Normalize();
            t.BasisY = vy.Normalize();
            t.BasisZ = vz.Normalize();

            Debug.Assert(t.IsConformal,
                "expected resulting transform to be conformal");

            // Set up the bounding box

            BoundingBoxXYZ bb = new BoundingBoxXYZ();
            bb.Transform = t;
            bb.Min = XYZ.Zero;

            //bb.Max = vx + vy + vz; // jeremy

            bb.Max = new XYZ(vx.GetLength(),
                vy.GetLength(), vz.GetLength()); // dan r

            return bb;
        }

        /// <summary>
        /// Return a suitable bounding box for a Revit 
        /// section view from the scope box position.
        /// Algorithm:
        /// 1. Assume a scope box can only be a rectangular 
        /// straight box. The user can rotate it on the 
        /// X, Y plane, but not make it slanted.
        /// 2. Retrieve a horizontal line at the bottom of 
        /// the box. Use one of its points as the origin.
        /// 3. Find the other two lines connected to the 
        /// origin.
        /// 4. Retrieve the vx, vy, vz vectors and lengths 
        /// from the three lines.
        /// 5. Create a new transform and bounding box 
        /// using this data.
        /// Revised algorithm:
        /// 1. Find vertical edge closest to viewer.
        /// 2. Use its bottom endpoint as the origin.
        /// 3. Find the other two edges emanating from the origin.
        /// 4. Use the three edges for the bounding box definition.
        /// </summary>
        public static BoundingBoxXYZ GetSectionBoundingBoxFromScopeBox(
            Element scopeBox,
            XYZ viewdirTowardViewer)
        {
            Document doc = scopeBox.Document;
            Application app = doc.Application;

            // Determine a possible view point outside the 
            // scope box extents in the direction of the 
            // viewer.

            BoundingBoxXYZ bb
                = scopeBox.get_BoundingBox(null);

            XYZ v = bb.Max - bb.Min;

            double size = v.GetLength();

            XYZ viewPoint = bb.Min
                            + 10*size*viewdirTowardViewer;

            // Retrieve scope box geometry, 
            // consisting of exactly twelve lines.

            Options opt = app.Create.NewGeometryOptions();
            GeometryElement geo = scopeBox.get_Geometry(opt);
            int n = geo.Count<GeometryObject>();

            if (12 != n)
            {
                throw new ArgumentException("Expected exactly"
                                            + " 12 lines in scope box geometry");
            }

            // Determine origin as the bottom endpoint of 
            // the edge closest to the viewer, and vz as the 
            // vertical upwards pointing vector emanating
            // from it. (Todo: if several edges are equally 
            // close, pick the leftmost one, assuming the 
            // given view direction and Z is upwards.)

            double dist = double.MaxValue;
            XYZ origin = null;
            XYZ vx = null;
            XYZ vy = null;
            XYZ vz = null;
            XYZ p, q;

            foreach (GeometryObject obj in geo)
            {
                Debug.Assert(obj is Line,
                    "expected only lines in scope box geometry");

                Line line = obj as Line;

                p = line.GetEndPoint(0);
                q = line.GetEndPoint(1);
                v = q - p;

                if (IsVertical(v))
                {
                    if (q.Z < p.Z)
                    {
                        p = q;
                        v = v.Negate();
                    }

                    if (p.DistanceTo(viewPoint) < dist)
                    {
                        origin = p;
                        dist = origin.DistanceTo(viewPoint);
                        vz = v;
                    }
                }
            }

            // Find the other two axes emanating from the 
            // origin, vx and vy, and ensure right-handedness

            foreach (GeometryObject obj in geo)
            {
                Line line = obj as Line;

                p = line.GetEndPoint(0);
                q = line.GetEndPoint(1);
                v = q - p;

                if (IsVertical(v)) // already handled this
                {
                    continue;
                }

                if (p.IsAlmostEqualTo(origin)
                    || q.IsAlmostEqualTo(origin))
                {
                    if (q.IsAlmostEqualTo(origin))
                    {
                        v = v.Negate();
                    }
                    if (null == vx)
                    {
                        Debug.Assert(IsPerpendicular(vz, v),
                            "expected orthogonal lines in scope box geometry");

                        vx = v;
                    }
                    else
                    {
                        Debug.Assert(null == vy,
                            "expected exactly three orthogonal lines to originate in one point");

                        Debug.Assert(IsPerpendicular(vz, v),
                            "expected orthogonal lines in scope box geometry");

                        Debug.Assert(IsPerpendicular(vx, v),
                            "expected orthogonal lines in scope box geometry");

                        vy = v;

                        if (!IsRightHanded(vx, vy, vz))
                        {
                            XYZ tmp = vx;
                            vx = vy;
                            vy = tmp;
                        }
                        break;
                    }
                }
            }

            // Set up the transform

            Transform t = Transform.Identity;
            t.Origin = origin;
            t.BasisX = vx.Normalize();
            t.BasisY = vy.Normalize();
            t.BasisZ = vz.Normalize();

            Debug.Assert(t.IsConformal,
                "expected resulting transform to be conformal");

            // Set up the bounding box

            bb = new BoundingBoxXYZ();
            bb.Transform = t;
            bb.Min = XYZ.Zero;

            //bb.Max = vx + vy + vz; // jeremy

            bb.Max = new XYZ(vx.GetLength(),
                vy.GetLength(), vz.GetLength()); // dan r

            return bb;
        }

    }
}
