﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;

namespace ViewCreator.Utils.RVT
{
    public static class Plan_Utils
    {

        public static List<View> GetStructuralPlans()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(ViewPlan));

            return
                collector.Cast<View>()
                  .Where(x => !x.IsTemplate && !x.View_IsDependent()
                  && (x.ViewType == ViewType.FloorPlan || x.ViewType == ViewType.EngineeringPlan)   
                  ).OrderBy(x => x.ViewName)
                  .ToList();
        }



        public static List<View> GetBuildingPlans()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(ViewPlan));

            return
              collector.Cast<View>()
                  .Where(x => !x.IsTemplate && !x.View_IsDependent()
                  && (x.ViewType == ViewType.FloorPlan || x.ViewType == ViewType.CeilingPlan)   
                  ).OrderBy(x => x.ViewName)
                  .ToList();
        }

        public static List<View> GetAllBuildingPlans()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(ViewPlan));

            return
              collector.Cast<View>()
                  .Where(x => !x.IsTemplate 
                  && (x.ViewType == ViewType.FloorPlan || x.ViewType == ViewType.CeilingPlan)
                  ).OrderBy(x => x.ViewName)
                  .ToList();
        }



        public static List<View> GetDisciplinePlans(string discipline)
        {
            var plans = GetBuildingPlans();
            plans = plans.Where(x => x.View_Discipline().ToString() == discipline).ToList();
            return plans;
           
        }

       
    }
}
