﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using ViewCreator.Utils.General;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Utils.RVT
{
    public static class Template_Utils
    {
        public static List<View> GetViewTemplates()
        {
            return (from v in new FilteredElementCollector(RVT_Utils.Document)
                .OfClass(typeof(View))
                .Cast<View>()
                    where v.IsTemplate
                    select v).OrderBy(x => x.ViewName).ToList();
        }



        public static List<View> GetViewTemplates(string search)
        {
            if (String.IsNullOrEmpty(search)) return GetViewTemplates();

            return GetViewTemplates().Where(x => x.ViewName.Contains(search, StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public static View GetViewTemplate(string name)
        {
            return GetViewTemplates().FirstOrDefault(x => x.Name == name);
        }

        public static List<string> GetViewTemplateNames()
        {
            return GetViewTemplates().Select(x => x.Name).ToList();
        }

        public static List<string> GetViewTemplateNames(string search)
        {
            return GetViewTemplates(search).Select(x => x.Name).ToList();
        }

        public static string View_TemplateName(this View source)
        {
            var name = "";

            if (source.ViewTemplateId != ElementId.InvalidElementId)
            {
                var view = View_Utils.GetView(source.ViewTemplateId);
                if (view != null)
                {
                    name = view.ViewName;
                }
            }
            else
            {
                name = "None";
            }

            return name;
        }



        public static void SetViewTemplate(View view, string templateName)
        {
            View template = null;
            if (templateName != null)
            {
                template = Template_Utils.GetViewTemplate(templateName);
            }

            if (template == null && templateName != "None")
            {
                return;
            }


            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start("SetViewTemplate");

                if (templateName == "None")
                {
                    view.ViewTemplateId = ElementId.InvalidElementId;
                }
                else
                {
                    try
                    {
                        view.ViewTemplateId = template.Id;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }

                tx.Commit();
            }
        }
    }
}
