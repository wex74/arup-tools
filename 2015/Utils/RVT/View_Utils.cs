﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using RevitManager_SRC.Classes;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.RVT;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Utils
{
    public static class View_Utils
    {

        public static void ActivateView(View view)
        {
            if (view == null) return;

            try
            {
                RVT_Utils.UIApp.ActiveUIDocument.ActiveView = view;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

     
        public static void ChangeScale(View view, int scale)
        {
            if (view.Scale == scale) return;

            using (Transaction tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start("ChangeScale");

                view.Scale = scale;

                tx.Commit();
            }
        }

     

        public static List<UIView> GetOpenViews()
        {
            return RVT_Utils.UIApp.ActiveUIDocument.GetOpenUIViews().ToList();
        }





        public static bool UpdateView(UpdateView updateView)
        {
            var updated = false;
              var view = updateView.GetView();

            if (view == null) return false;
                
                if (updateView.UpdateName)
                {
                    RenameView(view, updateView.ViewName);
                    updated = true;
                }

                if (updateView.UpdateTemplate)
                {
                    Template_Utils.SetViewTemplate(view, updateView.Template);
                    updated = true;
                }

                if (updateView.UpdateScopeBox)
                {
                    Scopebox_Utils.SetScopeBox(updateView.ScopeBox, updateView.GetView());
                    updated = true;
                }


                if (updateView.UpdateScale)
                {
                    ChangeScale(updateView.GetView(), updateView.Scale);
                    updated = true;
                }

                if (updateView.UpdateFavourite)
                {
                    if (updateView.Favourite)
                    {
                        User_Utils.AddFavourite(view.IdToInt());
                        updated = true;
                    }
                    else
                    {
                        User_Utils.RemoveFavourite(view.IdToInt());
                        updated = true;
                    }
                }
            
            return updated;
        }



        public static void UpdateViews(List<UpdateView> updateViews )
        {
            foreach (var updateView in updateViews)
            {
                UpdateView(updateView);
            }

        }


        public static List<View> GetViewsNotOnSheets(ViewType viewType)
        {

            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(View));

            return
                collector.Cast<View>()
                    .Where(x => !x.IsTemplate
                        && !x.View_IsOnSheet()
                        && (x.ViewType == viewType))
                        .OrderBy(x => x.ViewName)
                    .ToList();
        }


        public static string GetViewImage(this View source)
        {
            if (source.ViewType.ToString().Contains("Plan"))
            {
                return "Plan";
            }


            if (source.ViewType.ToString().Contains("Section"))
            {
                return "Section";
            }

            if (source.ViewType.ToString().Contains("ThreeD"))
            {
                return "ThreeD";
            }

            return "";
        }



      



        public static List<View> GetDisciplineViews(Discipline discipline)
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(View));

            var serviceTag = Discipline_Utils.GetServiceTag(discipline);

            var views = new List<View>();

            views.AddRange(
                collector.Cast<View>()
                    .Where(
                    x => !x.IsTemplate
                    && (x.ViewType == ViewType.FloorPlan)
                    && x.View_IsDiscipline(serviceTag)
                    && !x.ViewSpecific
                    ).ToList());


            views.AddRange(
               collector.Cast<View>()
                   .Where(
                   x => !x.IsTemplate
                   && (x.ViewType == ViewType.ThreeD)
                   && x.View_IsDiscipline(serviceTag)
                   ).ToList());


            views.AddRange(
              collector.Cast<View>()
                  .Where(
                  x => !x.IsTemplate
                  && (x.ViewType == ViewType.Section)
                  && x.View_IsDiscipline(serviceTag)
                  ).ToList());



            return views;

        }

        public static void CloseView(View view)
        {
            if (IsViewOpen(view))
            {
                GetUiView(view).Close();
            }
           
        }



        public static UIView GetUiView(View view)
        {
            if (RVT_Utils.UIApp != null)
            {
                if (RVT_Utils.UIApp.ActiveUIDocument != null)
                {
                    return RVT_Utils.UIApp.ActiveUIDocument.GetOpenUIViews().FirstOrDefault(x => x.ViewId == view.Id);
                }
            }

            return null;
        }

        public static bool IsViewOpen(View view)
        {
            return GetUiView(view) != null;
        }

        public static bool IsValidPlan(this View source)
        {
            if (source.ViewType == ViewType.FloorPlan
                || source.ViewType == ViewType.EngineeringPlan
                || source.ViewType == ViewType.CeilingPlan)
            {
                return true;
            }

            return false;
        }
        public static List<View> GetEngineeringFloorPlans()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(View));

            return
                collector.Cast<View>()
                    .Where(
                    x => !x.IsTemplate
                    && x.ViewType == ViewType.EngineeringPlan
                    && !x.ViewSpecific
                    ).ToList();
        }




      
        public static View GetView(int id)
        {
            var element = RVT_Utils.Document.GetElement(new ElementId(id));

            if (element != null)
            {
                if (General_Utils.IsSameOrSubclass(typeof(View), element.GetType()))
                {
                    return (View)element;
                }
            }


            return null;
        }


        public static IEnumerable<ViewFamilyType> FindViewTypes(ViewType viewType)
        {
            var ret = new FilteredElementCollector(RVT_Utils.Document)
                .WherePasses(new ElementClassFilter(typeof(ViewFamilyType), false))
                .Cast<ViewFamilyType>();

            switch (viewType)
            {

                case ViewType.AreaPlan:
                    return ret.Where(e => e.ViewFamily == ViewFamily.AreaPlan);
                case ViewType.CeilingPlan:
                    return ret.Where(e => e.ViewFamily == ViewFamily.CeilingPlan);
                case ViewType.ColumnSchedule:
                    return ret.Where(e => e.ViewFamily == ViewFamily.GraphicalColumnSchedule); //?
                case ViewType.CostReport:
                    return ret.Where(e => e.ViewFamily == ViewFamily.CostReport);
                case ViewType.Detail:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Detail);
                case ViewType.DraftingView:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Drafting);
                case ViewType.DrawingSheet:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Sheet);
                case ViewType.Elevation:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Elevation);
                case ViewType.EngineeringPlan:
                    return ret.Where(e => e.ViewFamily == ViewFamily.StructuralPlan); //?
                case ViewType.FloorPlan:
                    return ret.Where(e => e.ViewFamily == ViewFamily.FloorPlan);
                //case ViewType.Internal:
                //    return ret.Where(e => e.ViewFamily == ViewFamily.Internal); //???
                case ViewType.Legend:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Legend);
                case ViewType.LoadsReport:
                    return ret.Where(e => e.ViewFamily == ViewFamily.LoadsReport);
                case ViewType.PanelSchedule:
                    return ret.Where(e => e.ViewFamily == ViewFamily.PanelSchedule);
                case ViewType.PresureLossReport:
                    return ret.Where(e => e.ViewFamily == ViewFamily.PressureLossReport);
                case ViewType.Rendering:
                    return ret.Where(e => e.ViewFamily == ViewFamily.ImageView); //?
                //case ViewType.Report:
                //    return ret.Where(e => e.ViewFamily == ViewFamily.Report); //???
                case ViewType.Schedule:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Schedule);
                case ViewType.Section:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Section);
                case ViewType.ThreeD:
                    return ret.Where(e => e.ViewFamily == ViewFamily.ThreeDimensional);
                case ViewType.Undefined:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Invalid); //?
                case ViewType.Walkthrough:
                    return ret.Where(e => e.ViewFamily == ViewFamily.Walkthrough);
                default:
                    return ret;
            }
        }


        public static bool DeleteView(View view)
        {
            var id = view.Id;

            if (RVT_Utils.Document.ActiveView.Id == view.Id)
            {
                var draftView = GetTempDrafingView();

                if (draftView != null)
                {
                    RVT_Utils.UIApp.ActiveUIDocument.ActiveView = draftView;
                }
                else
                {
                    return false;
                }
            }

            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start("DeleteView");
                try
                {
                    RVT_Utils.Document.Delete(view.Id);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                tx.Commit();
            }
            return RVT_Utils.Document.GetElement(id) == null;

        }


        private static bool TryRenameView(View view, string name, int count)
        {
            if (count > 0)
            {
                name = name + "(" + count + ")";
            }
            var renamed = false;

            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start(name);

                try
                {
                    view.Name = name;
                    renamed = true;
                }
                catch (Exception ex)
                {
                    // ignored
                }

                tx.Commit();
            }

            return renamed;
        }

        public static View AddView(ViewInfo viewInfo)
        {
            View view = null;

            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start("AddView " + viewInfo);

                var familyType = View_Utils.FindViewTypes(viewInfo.ViewType).FirstOrDefault(x => x.Name == viewInfo.Template);
                if (familyType == null) familyType = View_Utils.FindViewTypes(viewInfo.ViewType).FirstOrDefault();

                if (familyType != null)
                {
                    try
                    {
                        view = ViewPlan.Create(RVT_Utils.Document, familyType.Id, viewInfo.Level.Id);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }


                tx.Commit();
            }

            if (view != null)
            {
                RenameView(view, viewInfo.ToString());
               Scopebox_Utils.SetScopeBox(viewInfo.Scopebox, view);
            }

            Template_Utils.SetViewTemplate(view, viewInfo.Template);


            return view;
        }


        public static View AddDependentView(View view, string name)
        {
            View dependentView = null;
            if (view == null) return null;

            var viewName = view.ViewName + " - " + name;


            if (view.CanViewBeDuplicated(ViewDuplicateOption.AsDependent))
            {
                using (var tx = new Transaction(RVT_Utils.Document))
                {
                    tx.Start("AddDependentView " + viewName);

                    try
                    {
                        var id = view.Duplicate(ViewDuplicateOption.AsDependent);
                        dependentView = (View)RVT_Utils.Document.GetElement(id);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                    tx.Commit();
                }
            }

            if (dependentView != null)
            {
                Scopebox_Utils.SetScopeBox(name, dependentView);
                RenameView(dependentView, viewName);
            }

            return dependentView;
        }

   

        public static void RenameView(View view, string name)
        {
            var count = 0;

            while (!TryRenameView(view, name, count))
            {
                TryRenameView(view, name, count++);
            }
        }


    

        public static View GetView(ElementId id)
        {
            return GetView(id.IntegerValue);

        }

      

  

        public static List<string> GetScaleNames()
        {
            var list = new List<string>();
            list.Add("1:10");
            list.Add("1:20");
            list.Add("1:50");
            list.Add("1:100");
            list.Add("1:200");
            list.Add("1:250");
            list.Add("1:500");
            list.Add("1:1000");

            return list;
        }

        public static string GetScale(int scale)
        {
            return "1:" + scale;
        }

        public static int GetScale(string scale)
        {
          return Int32.Parse(scale.Replace("1:", ""));
        }


        public static List<string> GetPlanViewTypes()
        {
            var list = new List<string>();


            if (MainApp.AppData.IsUserBuildings)
            {
                list.Add(ViewType.CeilingPlan.ToString());
                list.Add(ViewType.FloorPlan.ToString());
            }

            if (MainApp.AppData.IsUserStructures)
            {
                list.Add(ViewType.EngineeringPlan.ToString());
            }
           

            return list;
        }

        public static ViewType GetViewType(string name)
        {
            foreach (ViewType viewType in Enum.GetValues(typeof(ViewType)))
            {
                if (viewType.ToString() == name)
                {
                    return viewType;
                }
            }
            return ViewType.FloorPlan;
        }


        public static ViewDrafting GetUserDraftView()
        {
            return new FilteredElementCollector(RVT_Utils.Document).OfClass(typeof(ViewDrafting))
                .Cast<ViewDrafting>().FirstOrDefault(x => x.ViewName == "Temp");
        }


        public static ViewDrafting GetTempDrafingView()
        {
            ViewDrafting view = GetUserDraftView();

            if (view == null)
            {
                ViewFamilyType vd = new FilteredElementCollector(RVT_Utils.Document)
            .OfClass(typeof(ViewFamilyType))
                .Cast<ViewFamilyType>()
            .FirstOrDefault(q => q.ViewFamily == ViewFamily.Drafting);

                if (vd != null)
                {
                    using (var tx = new Transaction(RVT_Utils.Document))
                    {
                        tx.Start("GetTempDrafingView");
                        try
                        {
                            view = ViewDrafting.Create(RVT_Utils.Document, vd.Id);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                        view.ViewName = "Temp";
                        tx.Commit();
                    }

                }
            }


            return view;
        }


        public static int View_Discipline(this View source)
        {
            if (source.ViewType == ViewType.EngineeringPlan)
            {
                return (int)Discipline.Structural;
            }


            var viewDiscipline = source.View_DisciplineParemeter().AsString();

            if (!String.IsNullOrEmpty(viewDiscipline))
            {
                foreach (Discipline discipline in Enum.GetValues(typeof(Discipline)))
                {
                    if (Discipline_Utils.GetServiceTag(discipline) == viewDiscipline) return (int)discipline;
                }
            }


            return -1;
        }



        public static bool View_IsDependent(this View source)
        {
            return source.GetPrimaryViewId() != ElementId.InvalidElementId;
        }

        public static int IdToInt(this View source)
        {
            return source.Id.IntegerValue;
        }



        public static Parameter View_DisciplineParemeter(this View source)
        {
            var name = "Arup Discipline";
            return source.LookupParameter(name);
        }

    

      

        public static bool View_IsDiscipline(this View source, string discipline)
        {

            var par = source.View_DisciplineParemeter();

            if (par != null)
            {
                if (par.AsString() != null)
                {
                    if (par.AsString() == discipline)
                    {
                        return true;
                    }
                }
            }

            return false;
        }


        public static bool View_IsOnSheet(this View source)
        {

            var par = source.LookupParameter("Sheet Number");

            if (par != null)
            {
                if (par.AsString() != null)
                {
                    if (par.AsString() != "---")
                    {
                        return true;
                    }


                }
            }

            return false;
        }
    }
}
