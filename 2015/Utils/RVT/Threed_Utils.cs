﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;

namespace ViewCreator.Utils.RVT
{
    public static class Threed_Utils
    {

        public static List<View> GetThreedViews()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(View3D));

            return
                collector.Cast<View>()
                    .Where(x => !x.IsTemplate && x.ViewType == ViewType.ThreeD)
                    .OrderBy(x => x.ViewName)
                    .ToList();
        }




    }
}
