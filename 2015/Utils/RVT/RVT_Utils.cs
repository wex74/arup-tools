﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using RevitManager_SRC.Classes;
using ViewCreator.Model;
using View = Autodesk.Revit.DB.View;
using Level = Autodesk.Revit.DB.Level;

namespace ViewCreator.Utils
{
    public static class RVT_Utils
    {
        private const double FEET_TO_METERS = 0.3048;
        public static Document Document { get; set; }
        public static View ActiveView { get; set; }
        public static UIApplication UIApp { get; set; }
        public static UIControlledApplication UIContApp { get; set; }
        private static List<Level> _levels;

        public static ProjectInfo ProjectInformation
        {
            get { return Document.ProjectInformation; }
        }


      

        public static string GetCentralModelFullPath(Document doc = null)
        {
            if (doc == null) doc = Document;

            try
            {
                var apath = doc.GetWorksharingCentralModelPath();
                if (apath != null)
                {
                    return ModelPathUtils.ConvertModelPathToUserVisiblePath(apath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return "";
        }

       


        public static string CentralModelPath
        {
            get
            {
                try
                {
                    var apath = Document.GetWorksharingCentralModelPath();
                    if (apath != null)
                    {
                        var path = Path.GetDirectoryName(ModelPathUtils.ConvertModelPathToUserVisiblePath(apath));
                        return path;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                return "";
            }
        }



        public static string CentralModelFileName
        {
            get
            {
                try
                {
                    var apath = Document.GetWorksharingCentralModelPath();
                    if (apath != null)
                    {
                        var file = Path.GetFileNameWithoutExtension(ModelPathUtils.ConvertModelPathToUserVisiblePath(apath));
                        return file;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                return "";
            }
            
        }

        public static double FeetToMM(this double feet)
        {
            return feet*FEET_TO_METERS*1000;
        }

        public static double MMToFeet(this double mm)
        {
            return mm/ FEET_TO_METERS / 1000;
        }

     

        public static double ConvertToMeters(double measurement)
        {
            return UnitUtils.ConvertFromInternalUnits(measurement, DisplayUnitType.DUT_METERS);
        }


       

      



     

     

       



    
     

        public static List<TextNote> GetModelText()
        {
            var collector = new FilteredElementCollector(Document);
            collector.OfClass(typeof (TextNote));

            return collector.Cast<TextNote>().ToList();
        }
    }
}