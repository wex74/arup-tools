﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.Revit.DB;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Utils.RVT
{
    public static class Scopebox_Utils
    {
        public static List<Element> GetScopeBoxes()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfCategory(BuiltInCategory.OST_VolumeOfInterest);

            return collector.OrderByDescending(x => x.Name).ToList();
        }

        public static List<string> GetScopeBoxNames()
        {
            return GetScopeBoxes().Select(x => x.Name).OrderBy(x => x).ToList();
        }


        public static string View_ScopeBoxName(this View source)
        {
            var name = "";
            if (!source.IsValidPlan()) return "";
 
            try
            {
                var id = source.get_Parameter(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP).AsElementId();

                if (id != ElementId.InvalidElementId)
                {
                    var scope = Enumerable.FirstOrDefault<Element>(GetScopeBoxes(), x => x.Id == id);
                    if (scope != null)
                    {
                        name = scope.Name;
                    }
                }

            }
            catch (Exception)
            {
                //
                
            } 
           
           
            return name;
        }


        public static void SetScopeBox(string name, View view)
        {
            var scopeElement = Enumerable.FirstOrDefault<Element>(GetScopeBoxes(), x => x.Name == name);

            if (scopeElement != null)
            {
                using (var tx = new Transaction(RVT_Utils.Document))
                {
                    tx.Start("SetScopeBox " + name);

                    try
                    {
                            view.get_Parameter(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP)
                           .Set(scopeElement.Id); 
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                    tx.Commit();
                }
            }
            else
            {
                using (var tx = new Transaction(RVT_Utils.Document))
                {
                    tx.Start("SetScopeBox " + name);

                    try
                    {
                        view.get_Parameter(BuiltInParameter.VIEWER_VOLUME_OF_INTEREST_CROP)
                         .Set(new ElementId(-1));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                    tx.Commit();
                }
             
            }
         
        }
    }
}
