﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using ViewCreator.Utils.General;
using Level = Autodesk.Revit.DB.Level;


namespace ViewCreator.Utils.RVT
{
    public static class Level_Utils
    {
        private static List<Level> _levels;


        public static List<Level> GetLevels()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(Level));

            return collector.Cast<Level>().OrderByDescending(x => x.Elevation).ToList();
        }

        public static List<Level> GetLevels(List<string> levelNames )
        {
            var list = new List<Level>();
            foreach (var level in levelNames)
            {
                var rvtLevel = GetLevel(level);

                if (rvtLevel != null)
                {
                    list.Add(rvtLevel);
                }
            }

            return list;
        }


        public static int LevelsCount()
        {

            return GetLevels().Count;
        }


        public static List<string> GetLevelNames()
        {
            return GetLevels().Select(x => x.Name).ToList();
        }


        public static List<string> GetLevelAndRLNames()
        {
            return GetLevels().Select(x => GetLevelAndRL(x.Name)).ToList();
        }

        public static Level GetLevelAbove(Level level)
        {
            var levels = GetLevels();

            var cLevel = levels.FirstOrDefault(x => x.Id == level.Id);

            if (cLevel != null)
            {
                var index = levels.IndexOf(cLevel);

                if (index > 0)
                {
                    return levels[index - 1];
                }
            }

            return null;
        }

        public static int GetLevelCount(string name)
        {
            if (_levels == null)
            {
                _levels = GetLevels();
            }

            return _levels.Count(level => level.Name.StartsWith(name, StringComparison.OrdinalIgnoreCase));
        }


        public static string GetLevelAndRL(string name)
        {
            var level = GetLevels().FirstOrDefault(x => x.Name == name);

            if (level != null)
            {
                return level.Name + " - " + String_Utils.GetRLString(RVT_Utils.ConvertToMeters(level.Elevation));
            }

            return "";

        }


        public static Level GetLevel(string name)
        {
            return GetLevels().FirstOrDefault(x => x.Name == name);
        }
    }
}
