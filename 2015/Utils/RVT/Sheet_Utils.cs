﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.DB;
using RevitManager_SRC.Classes;
using ViewCreator.Helpers;
using ViewCreator.Model;
using ViewCreator.Utils.App;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Utils.RVT
{
    public static class Sheet_Utils
    {


        public static void InsertView(ViewSheet sheet, View view)
        {
            if (sheet != null && view != null)
            {

                UV location = new UV((sheet.Outline.Max.U - sheet.Outline.Min.U) / 2,
                                     (sheet.Outline.Max.V - sheet.Outline.Min.V) / 2);

                using (var tx = new Transaction(RVT_Utils.Document))
                {
                    tx.Start("InsertView");


                    Viewport.Create(RVT_Utils.Document, sheet.Id, view.Id, new XYZ(location.U, location.V, 0));
                    

                    tx.Commit();
                }

            }
        }


        public static List<ViewSheet> GetSheets()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof (ViewSheet));

            return
                collector.Cast<ViewSheet>()
                    .Where(x => !x.IsTemplate
                                && x.ViewType == ViewType.DrawingSheet
                                && !x.SheetNumber.StartsWith("0")
                    )
                    .OrderBy(x => x.SheetNumber)
                    .ToList();
        }

   




        public static string  GetNextSheetNo(string prefix, string number, string suffix)
        {
            int num;
            if (Int32.TryParse(number, out num))
            {
                var sheets = GetSheets();
                var sheetsNos = sheets.Select(x => x.SheetNumber).ToList();

                var nextNo = num;

                while (sheetsNos.Exists(x => x == GetDrgNo(prefix, suffix, nextNo)))
                {
                    nextNo++;
                }


                return nextNo.ToString();



            }

            return number;
        }

        private static string GetDrgNo(string prefix, string suffix, int nextNo)
        {
            var drwNum = nextNo.ToString();
            if (prefix != "") drwNum = prefix + drwNum;
            if (suffix != "") drwNum = drwNum + suffix;
            return drwNum;
        }


        public static List<ViewSheet> GetEmptySheets()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof(ViewSheet));

            return
                collector.Cast<ViewSheet>()
                    .Where(x => !x.IsTemplate
                                && x.ViewType == ViewType.DrawingSheet
                                && !x.SheetNumber.StartsWith("0")
                                && !x.GetAllViewports().Any()
                    )
                    .OrderBy(x => x.SheetNumber)
                    .ToList();
        }

        public static List<ViewSheet> GetSheetTemplates()
        {
            var collector = new FilteredElementCollector(RVT_Utils.Document);
            collector.OfClass(typeof (ViewSheet));

            return
                collector.Cast<ViewSheet>()
                    .Where(x => !x.IsTemplate
                                && x.ViewType == ViewType.DrawingSheet
                                && x.SheetNumber.StartsWith("0")
                    )
                    .OrderBy(x => x.SheetNumber)
                    .ToList();
        }

        public static List<string> GetSheetTemplateNames()
        {
            return GetSheetTemplates().Select(x => x.SheetName()).ToList();
        }

        public static bool SheetTemplateExists(string name)
        {
            return GetSheetTemplateNames().Exists(x => x == name);
        }



        public static string SheetName(this ViewSheet source)
        {
            return source.SheetNumber + " - " + source.Name;
        }

        public static List<View> GetSheetViews(this ViewSheet source)
        {
            var list = new List<View>();

            foreach (var vp in source.GetSheetViewports())
            {
                list.Add(View_Utils.GetView(vp.ViewId));
            }
            return list.OrderBy(x => x.ViewName).ToList();
        }

        public static List<Viewport> GetSheetViewports(this ViewSheet source)
        {
            var vps = source.GetAllViewports();
            var list = new List<Viewport>();

            if (!vps.Any()) return list;

            foreach (var vp in vps)
            {
                var viewport = (Viewport) RVT_Utils.Document.GetElement(vp);
                if (viewport != null)
                {
                    list.Add(viewport);
                }
            }
            return list.OrderBy(x => x.Name).ToList();
        }

      

        public static void CreateNewSheets(List<Drawing> drawings)
        {
            if (!drawings.Any()) return;

            var deletedDrawings = new List<Drawing>();

            drawings = drawings.Where(x => x.IsNew && x.SheetTemplateExists).ToList();


            var n = drawings.Count;
            var s = "{0} of " + n + " sheets processed...";
            var caption = "Creating Sheets";

            using (var pf = new ProgressForm(caption, s, n))
            {
                foreach (var drawing in drawings)
                {
                    ViewSheet newSheet = CreateSheet(drawing);

                    if (newSheet != null)
                    {
                        deletedDrawings.Add(drawing);
                        MainApp.AppData.CurrentProject.ProjectDrawings.Add(newSheet);
                        View_Utils.CloseView(drawing.GetViewSheet());
                    }
                    pf.Increment();
                  
                }
            }

            foreach (var drawing in deletedDrawings)
            {
                MainApp.AppData.FormSettings.ManageDrawingsData.Drawings.RemoveAll(x => x.Id == drawing.Id);
            }

            MainApp.AppData.UserModelData.Save();

        }

        public static ViewSheet GetViewSheet(int id)
        {
            var element = RVT_Utils.Document.GetElement(new ElementId(id));

            if (element != null)
            {
                if (General_Utils.IsSameOrSubclass(typeof(ViewSheet), element.GetType()))
                {
                    return (ViewSheet)element;
                }
            }


            return null;
        }

        public static ViewSheet  CreateSheet(Drawing drawing)
        {
            ViewSheet newSheet = null;

            var template = GetSheetTemplates().FirstOrDefault(x => x.SheetName() == drawing.SheetTemplate);

            if (template == null)
            {
                return null;
            }


            ICollection<ElementId> copyIds = new Collection<ElementId>();
            copyIds.Add(template.Id);

            ICollection<ElementId> copyIdsViewSpecific = new Collection<ElementId>();
            foreach (var e in new FilteredElementCollector(RVT_Utils.Document).OwnedByView(template.Id))
            {
                if (!(e is Viewport))
                    copyIdsViewSpecific.Add(e.Id);
            }


            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start("CreateNewSheets");

                var id =
                    RVT_Utils.Document.GetElement(
                        ElementTransformUtils.CopyElements(RVT_Utils.Document, copyIds, RVT_Utils.Document,
                            Transform.Identity,
                            new CopyPasteOptions()).First()).Id;

                newSheet = RVT_Utils.Document.GetElement(id) as ViewSheet;


                if (newSheet != null)
                {
                    drawing.SheetId = newSheet.Id.IntegerValue;
                    ElementTransformUtils.CopyElements(template, copyIdsViewSpecific, newSheet,
                        Transform.Identity, new CopyPasteOptions());

                  
                }

                tx.Commit();
            }

            if (newSheet != null)
            {
                UpdateSheet(drawing);
            }

            return newSheet;
        }

        private static bool TryRenameView(ViewSheet sheet, string name, int count)
        {
            if (count > 0)
            {
                name = name + "(" + count + ")";
            }
            var renamed = false;

            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start(name);

                try
                {
                    sheet.Name = name;
                    renamed = true;
                }
                catch (Exception ex)
                {
                    // ignored
                }

                tx.Commit();
            }

            return renamed;
        }

        private static bool TryRenameSheetNo(ViewSheet sheet, string name, int count)
        {
            if (count > 0)
            {
                name = name + "(" + count + ")";
            }
            var renamed = false;

            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start(name);

                try
                {
                    sheet.SheetNumber = name;
                    renamed = true;
                }
                catch (Exception ex)
                {
                    // ignored
                }

                tx.Commit();
            }

            return renamed;
        }

        public static void RenameSheet(ViewSheet sheet, string name)
        {
            var count = 0;

            while (!TryRenameView(sheet, name, count))
            {
                TryRenameView(sheet, name, count++);
            }
        }

        public static void RenameSheetNo(ViewSheet sheet, string name)
        {
            var count = 0;

            while (!TryRenameSheetNo(sheet, name, count))
            {
                TryRenameSheetNo(sheet, name, count++);
            }
        }

        public static void UpdateSheetName(ViewSheet sheet, Drawing drawing)
        {
            if (sheet.SheetNumber != drawing.DrawingNo)
            {
                RenameSheetNo(sheet, drawing.DrawingNo);
                RenameSheet(sheet, "A");
                RenameSheet(sheet, drawing.GetTitle());
            }


            if (sheet.Name != drawing.GetTitle())
            {
                RenameSheet(sheet, drawing.GetTitle());
            }
          
        }

        public static void UpdateTitles(ViewSheet sheet, Drawing drawing)
        {
            using (var tx = new Transaction(RVT_Utils.Document))
            {
                tx.Start("UpdateTitles");

                if (sheet.GetTitle(1) != drawing.Title1)
                {
                    UpdateTitle(sheet, 1, drawing.Title1);
                }

                if (sheet.GetTitle(2) != drawing.Title2)
                {
                    UpdateTitle(sheet,2, drawing.Title2);
                }

                if (sheet.GetTitle(3) != drawing.Title3)
                {
                    UpdateTitle(sheet, 3, drawing.Title3);
                }


                if (sheet.GetTitle(4) != drawing.Title4)
                {
                    UpdateTitle(sheet,4, drawing.Title4);
                }

              

                tx.Commit();
            }
        }

        public static string GetTitle(this ViewSheet sheet, int number)
        {
            var para = sheet.Sheet_TitleParemeter(number);

            try
            {
                return para.AsString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string GetViewTemplate(this ViewSheet sheet)
        {
            try
            {
                var name = sheet.View_TemplateName();
                if (name == "None") return "";
                return name;
            }
            catch (Exception)
            {
                return "";
            }
        }


        public static string Sheet_DrawingType(this View source)
        {
            var value = source.Sheet_DrawingTypeParemeter().AsString();

            if (String.IsNullOrEmpty(value))
            {
                return "";
            }


            return value;
        }


        public static string Sheet_DisciplineName(this View source)
        {
            var value = source.Sheet_DisciplineParemeter().AsString();

            if (String.IsNullOrEmpty(value))
            {
                return "";
            }


            return value;
        }


        public static Parameter Sheet_DisciplineParemeter(this View source)
        {
            var name = "ARUP_BDR_DISCIPLINE";
            return source.LookupParameter(name);
        }

        public static Parameter Sheet_DrawingTypeParemeter(this View source)
        {
            var name = "ARUP_BDR_DRAWING TYPE";
            return source.LookupParameter(name);
        }


        public static Parameter Sheet_TitleParemeter(this View source, int number)
        {
            var name = "ARUP_BDR_TITLE" + number;
            return source.LookupParameter(name);
        }

        public static void UpdateDiscipline(ViewSheet sheet, string value)
        {
            if (sheet.Sheet_DisciplineName() == value) return;

            var para = sheet.Sheet_DisciplineParemeter();

            if (para != null)
            {
                using (var tx = new Transaction(RVT_Utils.Document))
                {
                    tx.Start("UpdateDiscipline");

                    if (para.StorageType == StorageType.String)
                    {
                        para.Set(value);
                    }

                    tx.Commit();
                }
            }
        }


        public static void UpdateDrawingType(ViewSheet sheet, string value)
        {
            if (sheet.Sheet_DrawingType() == value) return;

            var para = sheet.Sheet_DrawingTypeParemeter();

            if (para != null)
            {
                using (var tx = new Transaction(RVT_Utils.Document))
                {
                    tx.Start("UpdateDrawingType");

                    if (para.StorageType == StorageType.String)
                    {
                        para.Set(value);
                    }

                    tx.Commit();
                }
            }
        }

        public static void UpdateTitle(ViewSheet sheet, int number, string value)
        {
            var para = sheet.Sheet_TitleParemeter(number);

            if (para != null)
            {
                if (para.StorageType == StorageType.String)
                {
                    para.Set(value);
                }
            }
        }


        public static void UpdateSheet(Drawing drawing)
        {
            var sheet = drawing.GetViewSheet();
            if (sheet == null) return;

            UpdateTitles(sheet, drawing);
            UpdateDiscipline(sheet, drawing.Discipline);
            UpdateDrawingType(sheet, drawing.DrawingType);
            UpdateSheetName(sheet, drawing);
        }
    }
}