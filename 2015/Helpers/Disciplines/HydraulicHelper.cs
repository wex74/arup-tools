﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Services
{
    public static class HydraulicHelper
    {
        public static Discipline Discipline
        {
            get { return Discipline.Hydraulic; }
        }

        public static string DefaultTemplate
        {
            get { return "AUS-H-Hydraulics"; }
        }

        public static string DefaultPrefix
        {
            get { return PrefixNames[0]; }
        }

        public static List<string> PrefixNames
        {
            get
            {
                return new List<string>
                {
                    "H",
                    "Sh",
                    "Wo"
                };
            }
        }

        public static List<ViewInfo> GetViewInfos()
        {
            var list = new List<ViewInfo>();

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = Discipline.ToString(),
                    Prefix = "H",
                    Template = "AUS-H-Hydraulics"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Drainage",
                    Prefix = "H",
                    Template = "AUS-H-Drainage"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Water & Gas",
                    Prefix = "H",
                    Template = "AUS-H-Water & Gas"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Drainage Out of Strata",
                    Prefix = "H",
                    Template = "AUS-H-Drainage"
                });


            return list;
        }
    }
}