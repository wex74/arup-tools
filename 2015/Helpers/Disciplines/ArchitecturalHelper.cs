﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Services
{
    public static class ArchitecturalHelper
    {
        public static Discipline Discipline
        {
            get { return Discipline.Architectural; }
        }

        public static string DefaultTemplate
        {
            get { return "AUS-A-Architectural Reference"; }
        }

        public static string DefaultPrefix
        {
            get { return PrefixNames[0]; }
        }

        public static List<string> PrefixNames
        {
            get
            {
                return new List<string>
                {
                    "A"
                };
            }
        }

        public static List<ViewInfo> GetViewInfos()
        {
            var list = new List<ViewInfo>();

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = Discipline.ToString(),
                    Prefix = "A",
                    Template = DefaultTemplate
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.CeilingPlan,
                    Name = Discipline + " RCP",
                    Prefix = "A",
                    Template = DefaultTemplate
                });


            return list;
        }
    }
}