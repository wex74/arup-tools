﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Services
{
    public static class MechanicalHelper
    {
        public static Discipline Discipline
        {
            get { return Discipline.Mechanical; }
        }

        public static string DefaultTemplate
        {
            get { return "AUS-M-HVAC Zoning"; }
        }

        public static string DefaultPrefix
        {
            get { return PrefixNames[0]; }
        }

        public static List<string> PrefixNames
        {
            get
            {
                return new List<string>
                {
                    "M",
                    "MG",
                    "Sh",
                    "Wo"
                };
            }
        }

        public static List<ViewInfo> GetViewInfos()
        {
            var list = new List<ViewInfo>();


            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = Discipline.ToString(),
                    Prefix = "M",
                    Template = "AUS-M-HVAC Zoning"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "HVAC",
                    Prefix = "M",
                    Template = "AUS-M-HVAC Zoning"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Medical Gases",
                    Prefix = "MG",
                    Template = "AUS-MG-Medical/Specialist Gases"
                });

      
            return list;
        }
    }
}