﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Services
{
    public static class StructuralHelper
    {
        public static Discipline Discipline
        {
            get { return Discipline.Structural; }
        }

        public static string DefaultTemplate
        {
            get { return "10 - Wo - G.A. Plan"; }
        }

        public static string DefaultPrefix
        {
            get { return PrefixNames[0]; }
        }

        public static List<string> PrefixNames
        {
            get
            {
                return new List<string>
                {
                    "Re",
                    "Pt",
                    "Sh",
                    "Wo"
                };
            }
        }

        public static List<ViewInfo> GetViewInfos()
        {
            var list = new List<ViewInfo>();

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.EngineeringPlan,
                    Name = "GA Plan",
                    Prefix = "Wo",
                    Template = "10 - Wo - G.A. Plan"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.EngineeringPlan,
                    Name = "GA Plan",
                    Prefix = "Sh",
                    Template = "70 - Sh - G.A. Plan"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.EngineeringPlan,
                    Name = "Combined Reo Plan",
                    Prefix = "Sh",
                    Template = "70 - Sh - G.A. Framing Plan"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.EngineeringPlan,
                    Name = "Top Reo Plan",
                    Prefix = "Sh",
                    Template = "70 - Sh - G.A. Framing Plan"
                });


            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.EngineeringPlan,
                    Name = "Bottom Reo Plan",
                    Prefix = "Sh",
                    Template = "70 - Sh - G.A. Framing Plan"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.EngineeringPlan,
                    Name = "Post Tensioning Plan",
                    Prefix = "Sh",
                    Template = "70 - Sh - G.A. Framing Plan"
                });


            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.EngineeringPlan,
                    Name = "Steel Framing Plan",
                    Prefix = "Sh",
                    Template = "70 - Sh - G.A. Framing Plan"
                });
            return list;
        }
    }
}