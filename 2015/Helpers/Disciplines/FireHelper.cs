﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Services
{
    public static class FireHelper
    {
        public static Discipline Discipline
        {
            get { return Discipline.Fire; }
        }

        public static string DefaultTemplate
        {
            get { return "AUS-F-Fire Protection"; }
        }

        public static string DefaultPrefix
        {
            get { return PrefixNames[0]; }
        }

        public static List<string> PrefixNames
        {
            get
            {
                return new List<string>
                {
                    "F",
                    "E",
                    "G",
                    "H",
                    "Sh",
                    "Wo"
                };
            }
        }

        public static List<ViewInfo> GetViewInfos()
        {
            var list = new List<ViewInfo>();


            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = Discipline.ToString(),
                    Prefix = "F",
                    Template = "AUS-F-Fire Protection"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Dry Fire",
                    Prefix = "E",
                    Template = "AUS-F-Fire Detection"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Fire Hydrant & Hose Reel",
                    Prefix = "H",
                    Template = "AUS-F-Fire Protection"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Fire Sprinkler",
                    Prefix = "F",
                    Template = "AUS-F-Fire Protection"
                });


           

            return list;
        }
    }
}