﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Services
{
    public static class ElectricalHelper
    {
        public static Discipline Discipline
        {
            get { return Discipline.Electrical; }
        }

        public static string DefaultTemplate
        {
            get { return "AUS-E-Lighting"; }
        }

        public static string DefaultPrefix
        {
            get { return PrefixNames[0]; }
        }

        public static List<string> PrefixNames
        {
            get
            {
                return new List<string>
                {
                    "E",
                    "Sh",
                    "Wo"
                };
            }
        }

        public static List<ViewInfo> GetViewInfos()
        {
            var list = new List<ViewInfo>();

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = Discipline.ToString(),
                    Prefix = "E",
                    Template = "AUS-E-Power/Comms/Security"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Power/Comms/Security",
                    Prefix = "E",
                    Template = "AUS-E-Lighting"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Lighting",
                    Prefix = "E",
                    Template = "AUS-E-Lighting"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Security",
                    Prefix = "E",
                    Template = "AUS-E-Security"
                });


          
            return list;
        }
    }
}