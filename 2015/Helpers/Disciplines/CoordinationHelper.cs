﻿using System.Collections.Generic;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.Utils;
using ViewCreator.Utils.App;

namespace ViewCreator.Services
{
    public static class CoordinationHelper
    {
        public static Discipline Discipline
        {
            get { return Discipline.Coordination; }
        }

        public static string DefaultTemplate
        {
            get { return "AUS-C-Coordination"; }
        }

        public static string DefaultPrefix
        {
            get { return PrefixNames[0]; }
        }

        public static List<string> PrefixNames
        {
            get
            {
                return new List<string>
                {
                    "C",
                    "Sh"
                };
            }
        }

        public static List<ViewInfo> GetViewInfos()
        {
            var list = new List<ViewInfo>();

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = Discipline.ToString(),
                    Prefix = "C",
                    Template = "AUS-C-Coordination"
                });

            list.Add(
                new ViewInfo
                {
                    Discipline = Discipline,
                    ViewType = ViewType.FloorPlan,
                    Name = "Builderswork",
                    Prefix = "C",
                    Template = "AUS-C-Builderswork"
                });

           
            return list;
        }
    }
}