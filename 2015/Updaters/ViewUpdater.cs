﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator
{
    public class ViewUpdater : IUpdater
    {
        private static AddInId m_appId;
        private static UpdaterId m_updaterId;

        public ViewUpdater(AddInId id)
        {
            m_appId = id;
            m_updaterId = new UpdaterId(m_appId, new Guid("61BC6AA9-0309-469E-8FC0-D18306B3E7F5"));
        }



        public bool IsValidView(View view)
        {
            // check condition to return false
            if (view == null) return false;
            if (view.IsTemplate) return false;

            // If no data exists return false
            if (!App_Utils.CheckUserDataExists(false)) return false;
            if (!App_Utils.CheckUserModelDataExists(false)) return false;
            if (!App_Utils.CheckProjectDataExists(false)) return false;
            if (MainApp.AppData.UserSectionDiscipline == SectionDiscipline.Revit) return false;

            // allow sections and threed
            if (view.ViewType == ViewType.Section || view.ViewType == ViewType.ThreeD)
            {
                Debug_Utils.WriteLine("IsValidView", view.ViewType.ToString());
                // Todo - change to true when adding sections and threed views
                return false;
            }

            // check if structure
            if (MainApp.AppData.UserSectionDiscipline == SectionDiscipline.Structures)
            {
                if (view.ViewType == ViewType.EngineeringPlan)
                {
                    Debug_Utils.WriteLine("IsValidView", "UserSectionDiscipline: Structures");
                    Debug_Utils.WriteLine("IsValidView" , view.ViewType.ToString());
                    return true;
                }
            }

            // check if buildings
            if (MainApp.AppData.UserSectionDiscipline == SectionDiscipline.Buildings)
            {
                if (view.ViewType == ViewType.FloorPlan
                || view.ViewType == ViewType.CeilingPlan
                || view.ViewType == ViewType.DrawingSheet)
                {
                    Debug_Utils.WriteLine("IsValidView", "UserSectionDiscipline: Buildings");
                    Debug_Utils.WriteLine("IsValidView" , view.ViewType.ToString());
                    return true;
                }
            }

            return false;
        }

        public void Execute(UpdaterData data)
        {

            if (MainApp.PauseViewUpdater) return;

            var doc = data.GetDocument();

            MainApp.ViewUpdaterLoading = true;

            var plans = MainApp.AppData.ProjectPlans;
            var drawings = MainApp.AppData.ProjectDrawings;
            var sections = MainApp.AppData.ProjectSections;
            var threeds = MainApp.AppData.ProjectThreeds;

            // loop views added
            foreach (var id in data.GetAddedElementIds())
            {
                Debug_Utils.WriteLine("ViewUpdater", "Added: " + id);

                var view = (View)doc.GetElement(id);

                // check view is valid
                if (IsValidView(view))
                {
                    switch (view.ViewType)
                    {
                        case ViewType.DrawingSheet:
                            drawings.Add((ViewSheet)view);
                            break;

                        case ViewType.FloorPlan:
                        case ViewType.CeilingPlan:
                        case ViewType.EngineeringPlan:
                            plans.Add(view);
                            break;

                        case ViewType.Section:
                            // Todo - future requirement
                            //threeds.Add(id.IntegerValue);
                            break;

                        case ViewType.ThreeD:
                            // Todo - future requirement
                            //threeds.Add(id.IntegerValue);
                            break;

                        default:
                            return;
                    }

                }
            }

            // loop views updated
            foreach (var id in data.GetModifiedElementIds())
            {
                Debug_Utils.WriteLine("ViewUpdater", "Modified: " + id);

                var view = (View)doc.GetElement(id);

                // check view is valid
                if (IsValidView(view))
                {
                    switch (view.ViewType)
                    {
                        case ViewType.DrawingSheet:
                            drawings.Update(id.IntegerValue);
                            break;

                        case ViewType.FloorPlan:
                        case ViewType.CeilingPlan:
                        case ViewType.EngineeringPlan:
                            plans.Update(id.IntegerValue);
                            break;

                        case ViewType.Section:
                            // Todo - future requirement
                            //threeds.Update(id.IntegerValue);
                            break;

                        case ViewType.ThreeD:
                            // Todo - future requirement
                            //threeds.Update(id.IntegerValue);
                            break;

                        default:
                            return;
                    }

                }
            }


            // loop views deleted
            foreach (var id in data.GetDeletedElementIds())
            {
                Debug_Utils.WriteLine("ViewUpdater", "Deleted: " + id);

                if (drawings.Exists(id.IntegerValue))
                {
                    drawings.Remove(id.IntegerValue);
                }

                if (plans.Exists(id.IntegerValue))
                {
                    MainApp.AppData.ProjectPlans.Remove(id.IntegerValue);
                }
            }


            MainApp.ViewUpdaterLoading = false;

        }

        public string GetAdditionalInformation()
        {
            return "ViewUpdater";
        }

        public ChangePriority GetChangePriority()
        {
            return ChangePriority.Views;
        }

        public UpdaterId GetUpdaterId()
        {
            return m_updaterId;
        }

        public string GetUpdaterName()
        {
            return "ViewUpdater";
        }
    }
}
