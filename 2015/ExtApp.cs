﻿#region Namespaces

//using Autodesk.Revit.Collections;
using System;
using System.IO;
using System.Windows.Forms;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI;
using ViewCreator.Data.App;
using ViewCreator.Data.App_Data.App;
using ViewCreator.Data.Repositories;
using ViewCreator.Events;
using ViewCreator.Events.Drawings;
using ViewCreator.Events.Revit_Doc_Events;
using ViewCreator.Events.Views;
using ViewCreator.Forms.Controls;
using ViewCreator.Model.Settings;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;
using ViewCreator.Utils.UI;
using ViewCreatorWPF;
using ViewCreatorWPF.Pages;
using RvtApplication = Autodesk.Revit.ApplicationServices.Application;
using RvtDocument = Autodesk.Revit.DB.Document;
using View = Autodesk.Revit.DB.View;

#endregion

namespace ViewCreator
{
    [Transaction(TransactionMode.Manual)]
    [Regeneration(RegenerationOption.Manual)]
    public class ExtApp : IExternalApplication
    {
        internal static ExtApp _extApp;
        private ControlledApplication _app;
        private AppDocEvents _appDocEvents;
       
        private UIControlledApplication _uiApp;

        public static ExtApp Instance
        {
            get { return _extApp; }
        }

        public Result OnStartup(UIControlledApplication application)
        {
            MainApp.AppData = new AppData();
            _extApp = this;


            application.ControlledApplication.ApplicationInitialized += OnApplicationInitialized;
            App_Utils.AppPath = Path.GetDirectoryName(GetType().Assembly.Location);

            _app = application.ControlledApplication;
            _uiApp = application;
            RVT_Utils.UIContApp = _uiApp;

            EventLoader.Load();
           
            SetViewUpdater(application);

            LoadPallete();

            try
            {
                CreateRibbonPanel();

                AddAppDocEvents();
                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return Result.Failed;
            }
        }

        public Result OnShutdown(UIControlledApplication uiApp)
        {
            try
            {
                EventLoader.Dispose();
              

                var viewUpdater = new ViewUpdater(uiApp.ActiveAddInId);
                UpdaterRegistry.UnregisterUpdater(viewUpdater.GetUpdaterId());

                return Result.Succeeded;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return Result.Failed;
            }
        }

        public void LoadPallete()
        {
            Debug_Utils.WriteLine("LoadPallete", "Loading Palletes");

            ScreensUI.PlansPage = new PlansPage();
            ScreensUI.PlansPage.LoadControl();
            ScreensUI.PlansScreenId = new DockablePaneId(Guid.NewGuid());
            _uiApp.RegisterDockablePane(ScreensUI.PlansScreenId, "Plan Manager", ScreensUI.PlansPage);

            ScreensUI.BuildingsPage = new BuildingsPage();
            ScreensUI.BuildingsPage.LoadControl();
            ScreensUI.BuildingsScreenId = new DockablePaneId(Guid.NewGuid());
            _uiApp.RegisterDockablePane(ScreensUI.BuildingsScreenId, "Buildings", ScreensUI.BuildingsPage);



        }

        public void SetViewUpdater(UIControlledApplication application)
        {
            var updater = new ViewUpdater(application.ActiveAddInId);
            UpdaterRegistry.RegisterUpdater(updater);

            var filter = new ElementClassFilter(typeof (View));

            UpdaterRegistry.AddTrigger(updater.GetUpdaterId(), filter, Element.GetChangeTypeElementAddition());
            UpdaterRegistry.AddTrigger(updater.GetUpdaterId(), filter, Element.GetChangeTypeElementDeletion());
            UpdaterRegistry.AddTrigger(updater.GetUpdaterId(), filter, Element.GetChangeTypeAny());
        }

        private void OnApplicationInitialized(object sender, ApplicationInitializedEventArgs e)
        {
            var app = sender as RvtApplication;

            var uiApp = new UIApplication(app);
            RVT_Utils.UIApp = uiApp;


            ScreensUI.BuildingsPane = RVT_Utils.UIApp.GetDockablePane(ScreensUI.BuildingsScreenId);
            if (ScreensUI.BuildingsPane != null)
            {
                ScreensUI.BuildingsPane.Hide();
            }

            ScreensUI.PlansPane = RVT_Utils.UIApp.GetDockablePane(ScreensUI.PlansScreenId);
            if (ScreensUI.PlansPane != null)
            {
                ScreensUI.PlansPane.Hide();
            }

        }

        private void AddAppDocEvents()
        {
            _appDocEvents = new AppDocEvents(_app, _uiApp);
            _appDocEvents.EnableEvents();
        }

        private void CreateRibbonPanel()
        {
            var tabName = "Arup-Tools";

            _uiApp.CreateRibbonTab(tabName);

            RibbonButton button;
            string cmdName;

            if (!Ribbon_Utils.IsPanelTitleUsed(tabName, "Views"))
            {
                var viewsPanel = _uiApp.CreateRibbonPanel(tabName, "Views");


                cmdName = "QuickPlans";
                viewsPanel.AddPushButton(cmdName, "New Plans", "jpg");

                cmdName = "DependentViews";
                viewsPanel.AddPushButton(cmdName, "Dependents", "png");

                viewsPanel.AddSeparator();

                cmdName = "EditView";
                viewsPanel.AddPushButton(cmdName, "Edit View", "png");

                cmdName = "EditViews";
                viewsPanel.AddPushButton(cmdName, "Batch Edit Views", "png");

                viewsPanel.AddSeparator();

                cmdName = "PlanCreator";
                viewsPanel.AddPushButton(cmdName, "Plan Creator", "jpg");



                cmdName = "BuildingsScreen";
                viewsPanel.AddPushButton(cmdName, "Buildings", "png");

     
                
            }





            if (MainApp.AppData.IsUserBuildings)
            {
                if (!Ribbon_Utils.IsPanelTitleUsed(tabName, "Drawings"))
                {
                    var drawingsPanel = _uiApp.CreateRibbonPanel(tabName, "Drawings");

                    cmdName = "NewDrawing";
                    drawingsPanel.AddPushButton(cmdName, "New Drawing", "png");

                    cmdName = "ManageDrawings";
                    drawingsPanel.AddPushButton(cmdName, "Manage", "png");

                    cmdName = "InsertViews";
                    drawingsPanel.AddPushButton(cmdName, "Insert Views", "png");

                }
  
            }
     


            if (!Ribbon_Utils.IsPanelTitleUsed(tabName, "Export"))
            {
                var exportPanel = _uiApp.CreateRibbonPanel(tabName, "Export");


                cmdName = "ExportRVT";
                exportPanel.AddPushButton(cmdName, "RVT", "png");

                cmdName = "ExportNW";
                exportPanel.AddPushButton(cmdName, "NWC", "png");


                cmdName = "ExportDWG";
                exportPanel.AddPushButton(cmdName, "DWG", "png");

                exportPanel.AddSeparator();

                cmdName = "RVTRevs";
                exportPanel.AddPushButton(cmdName, "Revs", "png");
            }


            if (!Ribbon_Utils.IsPanelTitleUsed(tabName, "Settings"))
            {
                var settingsPanel = _uiApp.CreateRibbonPanel(tabName, "Settings");


                cmdName = "ModelSettings";
                settingsPanel.AddPushButton(cmdName, "Model", "png");

                cmdName = "UserSettings";
                settingsPanel.AddPushButton(cmdName, "User", "png");
            }
        }

    }
}