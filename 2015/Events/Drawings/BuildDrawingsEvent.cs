﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;

namespace ViewCreator.Events.Drawings
{
    public static class BuildDrawingsEvent
    {
        public static ExternalEvent BuildDrawings;
        public static List<Drawing> SelecteDrawings;

        public static void Run(List<Drawing> selecteDrawings )
        {
            SelecteDrawings = selecteDrawings;
            BuildDrawings.Raise();
        }

        public static void Dispose()
        {
            BuildDrawings.Dispose();
        }
        public class EventBuildDrawings : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {
                MainApp.PauseViewUpdater = true;

                var newDrawings = MainApp.AppData.ProjectDrawings.GetNewDrawings();

                var count = newDrawings.Count;

                if (newDrawings.Any())
                {

                    var n = newDrawings.Count;
                    var s = "{0} of " + n + " drawings processed...";
                    var caption = "Creating Drawings";

                    using (var pf = new ProgressForm(caption, s, n))
                    {
                        foreach (var drawing in newDrawings)
                        {
                            ViewSheet newSheet = Sheet_Utils.CreateSheet(drawing);

                            if (newSheet != null)
                            {
                                MainApp.AppData.ProjectDrawings.RemoveNew(drawing.Id.ToString());
                                MainApp.AppData.ProjectDrawings.Add(newSheet);
                                View_Utils.CloseView(drawing.GetViewSheet());
                            }
                            pf.Increment();

                        }
                    }
                }

                var updateDrawings = MainApp.AppData.ProjectDrawings.GetDrawings().Where(x => x.UpdateRequired()).ToList();

                if (updateDrawings.Any())
                {
                    var n = updateDrawings.Count;
                    var s = "{0} of " + n + " drawings processed...";
                    var caption = "Update Drawings";

                    using (var pf = new ProgressForm(caption, s, n))
                    {
                        foreach (var drawing in updateDrawings)
                        {
                            Sheet_Utils.UpdateSheet(drawing);
                            pf.Increment();

                        }
                    }
                }
                

                ScreensUI.DrawingsControl.BuildDrawings();

                if (SelecteDrawings != null)
                {
                    ScreensUI.DrawingsControl.SelectDrawings(SelecteDrawings);
                }
              

                MainApp.PauseViewUpdater = false;
            }

            public string GetName()
            {
                return "EventBuildDrawings";
            }
        }
    }
}
