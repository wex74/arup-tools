﻿using Autodesk.Revit.UI;
using ViewCreator.Events.Views;

namespace ViewCreator.Events.Drawings
{
    public static class EventLoader_Drawings
    {
        public static void Load()
        {
            var buildDrawingsEvent = new BuildDrawingsEvent.EventBuildDrawings();
            BuildDrawingsEvent.BuildDrawings = ExternalEvent.Create(buildDrawingsEvent);

        
        }


        public static void Dispose()
        {
            BuildDrawingsEvent.Dispose();
        }
    }
}
