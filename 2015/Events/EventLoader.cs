﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewCreator.Events.Drawings;
using ViewCreator.Events.Views;

namespace ViewCreator.Events
{
    public static class EventLoader
    {

        public static void Load()
        {
            EventLoader_Views.Load();
            EventLoader_Drawings.Load();

        }

        public static void Dispose()
        {
            EventLoader_Views.Dispose();
            EventLoader_Drawings.Dispose();
        }

    }
}
