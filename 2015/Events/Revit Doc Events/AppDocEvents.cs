using System.Diagnostics;
using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Events;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.App;
using ViewCreator.Utils.General;

namespace ViewCreator.Events.Revit_Doc_Events
{
    public class AppDocEvents
    {
        private readonly ControlledApplication _app;
        private readonly UIControlledApplication _uiApp;

        public AppDocEvents(ControlledApplication app, UIControlledApplication uiApp)
        {
            _app = app;
            _uiApp = uiApp;
        }

        public void EnableEvents()
        {
            _app.FileImporting += (FileImporting);
            _app.DocumentClosed += (DocumentClosed);
            _app.DocumentOpened += (DocumentOpened);
            _app.DocumentSaved += (DocumentSaved);
            _app.DocumentSavedAs += (DocumentSavedAs);
            _app.DocumentChanged += (DocumentChanged);
            _uiApp.ViewActivated += (ViewActivated);
        }

        public void DisableEvents()
        {
            _app.FileImporting -= (FileImporting);
            _app.DocumentClosed -= (DocumentClosed);
            _app.DocumentOpened -= (DocumentOpened);
            _app.DocumentSaved -= (DocumentSaved);
            _app.DocumentSavedAs -= (DocumentSavedAs);
            _app.DocumentChanged -= (DocumentChanged);
            _uiApp.ViewActivated -= (ViewActivated);
        }

        private void SetDataObjects()
        {
        }

        private void DocumentOpened(object sender, DocumentOpenedEventArgs e)
        {
            Document doc = e.Document;

            Debug_Utils.WriteLine("DocumentOpened" , doc.PathName);

            MainApp.AppData.LoadProject();


        }

        private void ViewActivated(object sender, ViewActivatedEventArgs e)
        {
            var view = e.CurrentActiveView;

            var doc = view.Document;
            RVT_Utils.Document = doc;
            RVT_Utils.ActiveView = view;

            Debug_Utils.WriteLine("ViewActivated", view.ViewType + "." +   view.ViewName + " in " + RVT_Utils.GetCentralModelFullPath());

            MainApp.AppData.LoadProject();


            ScreensUI.SelectCurrentViews();
            EditViewUI.ReLoad();

        }

        private void DocumentChanged(object sender, DocumentChangedEventArgs e)
        {
            var doc = e.GetDocument();
            var view = doc.ActiveView;


        }

        private void DocumentSavedAs(object sender, DocumentSavedAsEventArgs e)
        {
        }

        private void DocumentSaved(object sender, DocumentSavedEventArgs e)
        {
        }

        private void FileImporting(object sender, FileImportingEventArgs e)
        {
        }

        private void DocumentClosed(object sender, DocumentClosedEventArgs e)
        {
        }
    }
}