﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Events.Views
{
    public static class DeletePlansEvent
    {
        public static ExternalEvent DeletePlans;
        public static List<Plan> Plans;

        public static void Run(List<Plan> plans)
        {
            Plans = plans;
            DeletePlans.Raise();
        }

        public static void Dispose()
        {
            DeletePlans.Dispose();
        }

        public  class EventDeletePlans : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {
                if (Plans == null) return;

                MainApp.PauseViewUpdater = true;

                var n = Plans.Count;
                var s = "{0} of " + n + " Plans deleted...";
                var caption = "Deleting Plans";


                using (var pf = new ProgressForm(caption, s, n))
                {
                    foreach (var plan in Plans)
                    {
                        var id = plan.ViewId;
                        var view = plan.GetView();

                        if (view != null)
                        {
                            var deleted = View_Utils.DeleteView(view);
                            if (deleted)
                            {
                                MainApp.AppData.ProjectPlans.Remove(id);
                            }
                        }
                        

                        pf.Increment();
                    }
                }

                MainApp.PauseViewUpdater = false;
            }

            public string GetName()
            {
                return "EventDeletePlans";
            }
        }
    }
}
