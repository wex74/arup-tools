﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils.RVT;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Events.Views
{
    public static class DeleteViewsEvent
    {
        public static ExternalEvent DeleteViews;
        public static List<View> Views;

        public static void Run(List<View> views)
        {
            Views = views;
            DeleteViews.Raise();
        }

        public static void Dispose()
        {
            DeleteViews.Dispose();
        }

        public  class EventDeleteViews : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {
                if (Views == null) return;

                MainApp.PauseViewUpdater = true;

                var n = Views.Count;
                var s = "{0} of " + n + " Plans deleted...";
                var caption = "Deleting Views";


                using (var pf = new ProgressForm(caption, s, n))
                {
                    foreach (var view in Views)
                    {
                        if (view != null)
                        {
                            var id = view.IdToInt();
                            var deleted = View_Utils.DeleteView(view);
                            if (deleted)
                            {
                                MainApp.AppData.ProjectPlans.Remove(id);
                            }
                        }
                        

                        pf.Increment();
                    }
                }

                var views = Plan_Utils.GetBuildingPlans();
                EditViewsUI.Form.LoadForm(views);
                ScreensUI.BuildViews();

                MainApp.PauseViewUpdater = false;
            }

            public string GetName()
            {
                return "EventDeleteViews";
            }
        }
    }
}
