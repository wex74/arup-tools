﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.UI;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Events.Views
{
    public static class NavigateEvent
    {
        public static ExternalEvent Navigate;
        public static View View;

        public static void Run(View view)
        {
            View = view;
            Navigate.Raise();
        }

        public static void Dispose()
        {
            Navigate.Dispose();
        }


        public class EventNavigate : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {
                View_Utils.ActivateView(View);
            }

            public string GetName()
            {
                return "EventNavigate";
            }
        }
    }
}
