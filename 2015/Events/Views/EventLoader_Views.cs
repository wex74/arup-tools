﻿using Autodesk.Revit.UI;

namespace ViewCreator.Events.Views
{
    public static class EventLoader_Views
    {
        public static void Load()
        {
            var addPlansEvent = new AddPlansEvent.EventAddPlans();
            AddPlansEvent.AddPlans = ExternalEvent.Create(addPlansEvent);

            var navigateEvent = new NavigateEvent.EventNavigate();
            NavigateEvent.Navigate = ExternalEvent.Create(navigateEvent);

            var deletePlanEvent = new DeletePlanEvent.EventDeletePlan();
            DeletePlanEvent.DeletePlan = ExternalEvent.Create(deletePlanEvent);

            var deletePlansEvent = new DeletePlansEvent.EventDeletePlans();
            DeletePlansEvent.DeletePlans = ExternalEvent.Create(deletePlansEvent);

            var deleteViewsEvent = new DeleteViewsEvent.EventDeleteViews();
            DeleteViewsEvent.DeleteViews = ExternalEvent.Create(deleteViewsEvent);


            var deleteDependentPlansEvent = new DeleteDependentPlansEvent.EventDeleteDependentPlans();
            DeleteDependentPlansEvent.DeleteDependentPlans = ExternalEvent.Create(deleteDependentPlansEvent);

            var updateViews = new UpdateViewsEvent.EventUpdateViews();
            UpdateViewsEvent.UpdateViews = ExternalEvent.Create(updateViews);

            var updateViewEvent = new UpdateViewEvent.EventUpdateView();
            UpdateViewEvent.UpdateView = ExternalEvent.Create(updateViewEvent);
        }


        public static void Dispose()
        {
            AddPlansEvent.Dispose();
            NavigateEvent.Dispose();
            DeletePlanEvent.Dispose();
            DeletePlansEvent.Dispose();
            DeleteViewsEvent.Dispose();
            DeleteDependentPlansEvent.Dispose();
            UpdateViewsEvent.Dispose();
            UpdateViewEvent.Dispose();
        }
    }
}
