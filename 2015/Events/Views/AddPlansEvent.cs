﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using ViewCreator.Utils;
using ViewCreator.Utils.General;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Events.Views
{
    /// <summary>
    /// Add Plans via ViewInfos collected in PlansScreen
    /// Revit Views and dependents are created from ViewInfos collection 
    /// Update ProjectPlans and ProjectViewInfos
    /// Reload Views Screens
    /// </summary>
   
    public static class AddPlansEvent
    {
        public static ExternalEvent AddPlans;

        public static void Run()
        {
            AddPlans.Raise();
        }
        public static void Dispose()
        {
            AddPlans.Dispose();
        }
        public class EventAddPlans : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {
                Debug.WriteLine("EventAddPlans:");

                // Get all ViewInfos in current project model
                var viewInfos = MainApp.AppData.ProjectViewInfos.GetViewInfos().OrderBy(x => x.Discipline).ToList();
                Debug.WriteLine("ViewInfos: " + String_Utils.DumpObjects(viewInfos));

                var addedViews = new List<View>();
                var deletedViewInfos = new List<ViewInfo>();
                var updateViews = new List<View>();

                var count = MainApp.AppData.ProjectViewInfos.TotalCount;
                Debug.WriteLine("ViewInfos Total Count: " + count);
 

                if (count == 0) return;

                MainApp.PauseViewUpdater = true;

                var addedCount = 0;

                var n = count;
                var s = "{0} of " + n + " views processed...";
                var caption = "Creating Views";

                using (var pf = new ProgressForm(caption, s, n))
                {
                    // Loop all ViewInfos that don't have a ParentView (new views)
                    foreach (var viewInfo in viewInfos.Where(x => x.ParentViewId == 0))
                    {
                        // Create View from ViewInfo
                        var view = View_Utils.AddView(viewInfo);
                       
                        pf.Increment();

                        if (view != null)
                        {
                            addedCount++;
                            Debug.WriteLine(view.ViewName);


                            // Loop ViewInfo Dependents
                            foreach (var dependent in viewInfo.DependentViews)
                            {
                                var dependentView = View_Utils.AddDependentView(view, dependent);
                                pf.Increment();

                                if (dependentView != null)
                                {
                                    addedCount++;
                                    Debug.WriteLine(dependentView.ViewName + " (Dependent)");
                                }
                            }

                            // Store created view
                            addedViews.Add(view);
                            // Sore ViewInfo to be removed
                            deletedViewInfos.Add(viewInfo);

                        }
                    }

                    // Loop all ViewInfos that have a ParentView (model views to create dependents)
                    foreach (var viewInfo in MainApp.AppData.ProjectViewInfos.GetPlanViewInfos())
                    {
                        var dependentView = View_Utils.AddDependentView(viewInfo.GetParentView(), viewInfo.Scopebox);
                        pf.Increment();

                        if (dependentView != null)
                        {
                            addedCount++;
                            Debug.WriteLine(dependentView.ViewName + " (Dependent of " +  viewInfo.GetParentView().ViewName + ")");

                            // Store update view
                            if (!updateViews.Exists(x => x.Id.IntegerValue == viewInfo.ParentViewId))
                            {
                                updateViews.Add(viewInfo.GetParentView());
                            }
                            // Sore ViewInfo to be removed
                            deletedViewInfos.Add(viewInfo);
                        }
                    }
                }



                // Add newly created View
                MainApp.AppData.ProjectPlans.Add(addedViews);
                // Update dependents from Views
                MainApp.AppData.ProjectPlans.UpdateDependents(updateViews);
                // Remove ViewInfos
                MainApp.AppData.ProjectViewInfos.Remove(deletedViewInfos);


                Debug.WriteLine("Views Added: " + String_Utils.DumpObjects(addedViews.Select(x => x.ViewName)));
                Debug.WriteLine("Views Updated: " + String_Utils.DumpObjects(updateViews.Select(x => x.ViewName)));
                Debug.WriteLine("ViewIndos Deleted: " + String_Utils.DumpObjects(deletedViewInfos));
                Debug.Assert(MainApp.AppData.ProjectViewInfos.Count == 0, "*** Not all ViewInfos were deleted ***");

                ScreensUI.BuildViews();

                MainApp.PauseViewUpdater = false;

                if (count != addedCount)
                {
                    MessageBox.Show(addedCount + " / " + count + " added");
                    Debug.Assert(addedCount == count, "*** Total count does not match added count ***");
                }

            }

            public string GetName()
            {
                return "EventAddPlan";
            }
        }
    }
}
