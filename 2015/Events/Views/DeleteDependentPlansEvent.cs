﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Events.Views
{
    public static class DeleteDependentPlansEvent
    {
        public static ExternalEvent DeleteDependentPlans;
        public static List<Plan> Plans;
        public static List<DependentPlan> DependentPlans;

        public static void Run(List<Plan> plans, List<DependentPlan> dependentPlans)
        {
            Plans = plans;
            DependentPlans = dependentPlans;
            DeleteDependentPlans.Raise();
        }

        public static void Dispose()
        {
            DeleteDependentPlans.Dispose();
        }

        public  class EventDeleteDependentPlans : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {
                if (Plans == null || DependentPlans == null) return;

                MainApp.PauseViewUpdater = true;

                var n = DependentPlans.Count;
                var s = "{0} of " + n + " Plans deleted...";
                var caption = "Deleting Plans";


                using (var pf = new ProgressForm(caption, s, n))
                {
                    foreach (var dependent in DependentPlans)
                    {
                        var id = dependent.ViewId.ToString();
                        var view = dependent.GetView();

                        if (view != null)
                        {
                            var deleted = View_Utils.DeleteView(view);
                            if (deleted)
                            {
                                ScreensUI.PlansViews.RemoveNode(id);
                            }
                        }
                        

                        pf.Increment();
                    }
                }

                var views = Plans.Select(x => x.GetView()).ToList();
                MainApp.AppData.ProjectPlans.UpdateDependents(views);
        

                MainApp.PauseViewUpdater = false;
            }

            public string GetName()
            {
                return "EventDeleteDependentPlans";
            }
        }
    }
}
