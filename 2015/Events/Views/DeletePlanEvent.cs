﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Events.Views
{
    public static class DeletePlanEvent
    {
        public static ExternalEvent DeletePlan;
        public static Plan Plan;

        public static void Run(Plan plan)
        {
            Plan = plan;
            DeletePlan.Raise();
        }

        public static void Dispose()
        {
            DeletePlan.Dispose();
        }

        public  class EventDeletePlan : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {

                if (Plan == null) return;

                MainApp.PauseViewUpdater = true;

                var id = Plan.ViewId;
                var view = Plan.GetView();

                if (view != null)
                {
                    var deleted = View_Utils.DeleteView(view);
                    if (deleted)
                    {
                        MainApp.AppData.ProjectPlans.Remove(id);
                    }
                }

                MainApp.PauseViewUpdater = false;
            }

            public string GetName()
            {
                return "EventDeletePlans";
            }
        }
    }
}
