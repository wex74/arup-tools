﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.Events.Views
{
    public static class UpdateViewEvent
    {
        public static ExternalEvent UpdateView;
        public static UpdateView UpdateView1;

        public static void Run(View view,UpdateView updateView )
        {
            UpdateView1 = updateView;
            UpdateView.Raise();
        }

        public static void Dispose()
        {
            UpdateView.Dispose();
        }


        public class EventUpdateView : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {
                MainApp.PauseViewUpdater = true;

                var updated = View_Utils.UpdateView(UpdateView1);
                if (updated)
                {
                    MainApp.AppData.ProjectPlans.Update(UpdateView1.ViewId);
                    EditViewUI.ReLoad();
                }
               

                MainApp.PauseViewUpdater = true;

            }

            public string GetName()
            {
                return "EventUpdateView";
            }
        }
    }
}
