﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AdnRme;
using Autodesk.Revit.UI;
using ViewCreator.Model;
using ViewCreator.UI;
using ViewCreator.Utils;

namespace ViewCreator.Events.Views
{
    public static class UpdateViewsEvent
    {
        public static ExternalEvent UpdateViews;
        public static List<UpdateView> UpdateViewsViews;

        public static void Run(List<UpdateView> updateViews)
        {
            UpdateViewsViews = updateViews;
            UpdateViews.Raise();
        }

        public static void Dispose()
        {
            UpdateViews.Dispose();
        }

        public  class EventUpdateViews : IExternalEventHandler
        {
            public void Execute(UIApplication app)
            {

                MainApp.PauseViewUpdater = true;

                View_Utils.UpdateViews(UpdateViewsViews);

                foreach (var updateViewsView in UpdateViewsViews)
                {
                    if (updateViewsView.IsDependent)
                    {
                        MainApp.AppData.ProjectPlans.UpdateDependent(updateViewsView.ViewId);
                    }
                    else
                    {
                        MainApp.AppData.ProjectPlans.Update(updateViewsView.ViewId);
                    }
                    
                }

                EditViewUI.ReLoad();

                ScreensUI.EditViewsViewsControl.ReloadControl(UpdateViewsViews);
                MainApp.PauseViewUpdater = true;
            }

            public string GetName()
            {
                return "EventUpdateViews";
            }
        }
    }
}
