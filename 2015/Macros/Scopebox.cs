﻿/*
 * Created by SharpDevelop.
 * User: NW
 * Date: 22/03/2016
 * Time: 8:49 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Collections.Generic;
using System.Linq;

namespace ScopeTest
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    [Autodesk.Revit.DB.Macros.AddInId("C2E073CB-8A2A-4E7D-B470-FAAED22B2027")]
	public partial class ThisDocument
	{
		
		public void Scopebox(){
			Document doc = this.ActiveUIDocument.Document;
			
		}
		private void Module_Startup(object sender, EventArgs e)
		{
			
		}

		private void Module_Shutdown(object sender, EventArgs e)
		{

		}

		#region Revit Macros generated code
		private void InternalStartup()
		{
			this.Startup += new System.EventHandler(Module_Startup);
			this.Shutdown += new System.EventHandler(Module_Shutdown);
		}
		#endregion
	}
}