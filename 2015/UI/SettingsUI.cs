﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Autodesk.Windows;
using ViewCreator.Forms;
using ViewCreator.Utils;


namespace ViewCreator.UI
{
    public static class SettingsUI
    {

        public static frmSettings Form = new frmSettings();
        public static bool FormVisible { get; set; }

        public static void ShowForm()
        {
            if (Form == null)
            {
                Form = new frmSettings();
                FormVisible = false;
            }

            if (Form.IsDisposed)
            {
                Form = new frmSettings();
                FormVisible = false;
            }



            if (!FormVisible)
            {
                try
                {
                    Form.Show(new UI_Utils.JtWindowHandle(ComponentManager.ApplicationWindow));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    throw;
                }
            }
            else
            {
                Form.Hide();
            }

            FormVisible = !FormVisible;

        }

       

     
    }
}