﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.UI;
using ViewCreator.Forms.Controls;
using ViewCreator.Forms.Controls.Views;
using ViewCreator.Forms.Sections.Views.Controls;
using ViewCreator.Utils.App;
using ViewCreatorWPF;
using ViewCreatorWPF.Pages;


namespace ViewCreator.UI
{
    public static class ScreensUI
    {
        public static DockablePaneId PlansScreenId;
        public static PlansPage PlansPage;
        public static DockablePane PlansPane;


        public static DockablePaneId BuildingsScreenId;
        public static BuildingsPage BuildingsPage;
        public static DockablePane BuildingsPane;


        public static ctlPlansViews PlansViews { get; set; }
        public static ctlBuildingsViews BuildingsViews { get; set; }


        public static ctlPlansScreen PlansScreen { get; set; }
        public static ctlBuildingsScreen BuildingsScreen { get; set; }

        public static ctlDrawings DrawingsControl { get; set; }
        public static ctlEditViewsViews EditViewsViewsControl { get; set; }

        public static void LoadControls()
        {
            if (!App_Utils.CheckCurrentProjectIsValid(false)) return;


            if (!App_Utils.CheckProjectDataExists(false))
            {
                if (PlansPane != null)
                {
                    PlansPane.Hide();
                    MainApp.AppData.CurrentProject.PlansScreenVisible = false;
                }

                if (BuildingsPane != null)
                {
                    BuildingsPane.Hide();
                    MainApp.AppData.CurrentProject.BuildingsScreenVisible = false;
                }



                return;
            }


            //if (PlansPane != null)
            //{
            //    if (MainApp.AppData.CurrentProject.PlansScreenVisible && !PlansPane.IsShown())
            //    {
            //        PlansPane.Show();
            //    }

            //    if (!MainApp.AppData.CurrentProject.PlansScreenVisible && PlansPane.IsShown())
            //    {
            //        PlansPane.Hide();
            //    }
            //}

            //if (BuildingsPane != null)
            //{
            //    if (MainApp.AppData.CurrentProject.BuildingsScreenVisible && !BuildingsPane.IsShown())
            //    {
            //        BuildingsPane.Show();
            //    }

            //    if (!MainApp.AppData.CurrentProject.BuildingsScreenVisible && BuildingsPane.IsShown())
            //    {
            //        BuildingsPane.Hide();
            //    }

            //}


            PlansScreen_LoadControl();
            BuildingsScreen_LoadControl();
        }

        public static void BuildViews()
        {
            ViewPlans_BuildViews();
            BuildingsViews_BuildViews();
        }



        public static void PlansScreen_LoadControl()
        {
            if (PlansScreen != null)
            {
                PlansScreen.LoadControl();
            }
        }

        public static void BuildingsScreen_LoadControl()
        {
            if (BuildingsScreen != null)
            {
                BuildingsScreen.LoadControl();
            }
        }

      

        public static void SelectCurrentViews()
        {
            ViewPlans_SelectCurrentView();
            BuildingsViews_SelectCurrentView();
        }

     


        public static void ViewPlans_SelectCurrentView()
        {
            if (PlansViews != null)
            {
                PlansViews.SelectCurrentView();
            }
        }

        public static void BuildingsViews_SelectCurrentView()
        {
            if (BuildingsViews != null)
            {
                BuildingsViews.SelectCurrentView();
            }
        }


        public static void ViewPlans_BuildViews()
        {
            if (PlansViews != null)
            {
                PlansViews.LoadViews();
            }
        }

        public static void BuildingsViews_BuildViews()
        {
            if (BuildingsViews != null)
            {
                BuildingsViews.LoadViews();
            }
        }

    }
}
