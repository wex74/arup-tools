﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Autodesk.Windows;
using ViewCreator.Forms.Add_Edit_Forms;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.UI
{
    public static class EditViewUI
    {
        public static frmEditView Form = new frmEditView();
        public static bool FormVisible { get; set; }


        public static void ReLoad(bool showIfUnloaded = false)
        {
            if (FormVisible)
            {
                Form.LoadControl();
            }

            if (showIfUnloaded)
            {
                ShowForm();
            }

        }


        public static void ShowForm()
        {
            if (Form == null)
            {
                Form = new frmEditView();
                FormVisible = false;
            }

            if (Form.IsDisposed)
            {
                Form = new frmEditView();
                FormVisible = false;
            }


            if (!FormVisible)
            {
                try
                {
                    Form.LoadControl();
                    Form.Show(new UI_Utils.JtWindowHandle(ComponentManager.ApplicationWindow));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    throw;
                }
            }
            else
            {
                Form.Hide();
            }

            FormVisible = !FormVisible;
        }
    }
}