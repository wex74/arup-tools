﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using Autodesk.Windows;
using ViewCreator.Forms;
using ViewCreator.Utils;
using View = Autodesk.Revit.DB.View;

namespace ViewCreator.UI
{
    public static class EditViewsUI
    {

        public static frmEditViews Form = new frmEditViews();
        public static bool FormVisible { get; set; }

      
       
        public static void ShowForm(List<View> views)
        {
            if (Form == null)
            {
                Form = new frmEditViews();
                FormVisible = false;
            }

            if (Form.IsDisposed)
            {
                Form = new frmEditViews();
                FormVisible = false;
            }



            if (!FormVisible)
            {
                try
                {
                    Form.LoadForm(views);
                    Form.Show(new UI_Utils.JtWindowHandle(ComponentManager.ApplicationWindow));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    throw;
                }
            }
            else
            {
                Form.Hide();
            }

            FormVisible = !FormVisible;

        }

       

     
    }
}