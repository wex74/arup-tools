﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Autodesk.Windows;
using ViewCreator.Forms;
using ViewCreator.Forms.Main_Dialogs;
using ViewCreator.Utils;


namespace ViewCreator.UI
{
    public static class ManageDrawingsUI
    {

        public static frmManageDrawings Form = new frmManageDrawings();
        public static bool FormVisible { get; set; }

        public static void ShowForm()
        {
            if (Form == null)
            {
                Form = new frmManageDrawings();
                FormVisible = false;
            }

            if (Form.IsDisposed)
            {
                Form = new frmManageDrawings();
                FormVisible = false;
            }



            if (!FormVisible)
            {
                try
                {
                    Form.LoadForm();
                    Form.Show(new UI_Utils.JtWindowHandle(ComponentManager.ApplicationWindow));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    throw;
                }
            }
            else
            {
                Form.SaveFormData();
                Form.Hide();

            }

            FormVisible = !FormVisible;

        }

       

     
    }
}