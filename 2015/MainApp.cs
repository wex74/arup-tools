﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using ViewCreator.Data;
using ViewCreator.Data.App_Data.App;
using ViewCreator.Data.Repositories;
using ViewCreator.Model;
using ViewCreator.Utils.General;

namespace ViewCreator
{
    public static class MainApp
    {
        public static DataAccess DataAccess { get; set; }
        public static AppData AppData { get; set; }
        public static List<Drawing> Sheets { get; set; }
        public static string DocumentFullPath { get; set; }
        public static bool IsLoaded { get; set; }
        public static bool ViewUpdaterLoading { get; set; }
        public static bool PauseViewUpdater { get; set; }

    
      
    }
}